import 'package:flutter/material.dart';
import 'package:fluro_fork/fluro_fork.dart';
import 'package:weash_cars/models/vehicle_type.dart';
import 'package:weash_cars/screens/add_new_employee_screen/add_new_employee_screen.dart';
import 'package:weash_cars/screens/assign_employee_screen/assign_employee_screen.dart';
import 'package:weash_cars/screens/company_settings_screen/company_settings_screen.dart';
import 'package:weash_cars/screens/confirm_order_screen/confirm_order_screen.dart';
import 'package:weash_cars/screens/customer_dashboard_screen/customer_dashboard_screen.dart';
import 'package:weash_cars/screens/customer_order_track_screen/customer_order_track_screen.dart';
import 'package:weash_cars/screens/customer_profile_edit_screen/customer_profile_edit_screen.dart';
import 'package:weash_cars/screens/done_order_details_screen/done_order_details_screen.dart';
import 'package:weash_cars/screens/edit_employee_screen/edit_employee_screen.dart';
import 'package:weash_cars/screens/edit_package_screen/edit_package_screen.dart';
import 'package:weash_cars/screens/employee_dashboard_screen/employee_dashboard_screen.dart';
import 'package:weash_cars/screens/employee_order_management_screen/employee_order_management_screen.dart';
import 'package:weash_cars/screens/employee_profile_edit_screen/employee_profile_edit_screen.dart';
import 'package:weash_cars/screens/employees_management_screen/employees_management_screen.dart';
import 'package:weash_cars/screens/finish_account_screen/finish_account_screen.dart';
import 'package:weash_cars/screens/indiv_order_details_screen/indiv_order_details_screen.dart';

import 'package:weash_cars/screens/intro_screen/intro_screen.dart';
import 'package:weash_cars/screens/home_screen/home_screen.dart';
import 'package:weash_cars/screens/join_us_screen/join_us_screen.dart';
import 'package:weash_cars/screens/language_screen/language_screen.dart';
import 'package:weash_cars/screens/login_screen/login_screen.dart';
import 'package:weash_cars/screens/company_dashboard_screen/company_dashboard_screen.dart';
import 'package:weash_cars/screens/add_package_screen/add_package_screen.dart';
import 'package:weash_cars/screens/login_screen/login_with_password_screen.dart';
import 'package:weash_cars/screens/make_order_screen/make_order_screen.dart';
import 'package:weash_cars/screens/manage_order_screen/manage_order_screen.dart';
import 'package:weash_cars/screens/order_details_screen/order_details_screen.dart';
import 'package:weash_cars/screens/packages_management_screen/packages_management_screen.dart';
import 'package:weash_cars/screens/take_order_screen/take_order_screen.dart';
import 'package:weash_cars/screens/vehicle_pictures_capture_screen/vehicle_pictures_capture_screen.dart';

class Routes {
  static Router _router;
  static bool _routesDefined = false;

  static Handler _introScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return IntroScreen();
  });

  static Handler _languageScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return LanguageScreen();
  });

  static Handler _homeScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return HomeScreen();
  });

  static Handler _loginScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return LoginWithPasswordScreen();
  });

  static Handler _companyDashboardScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return CompanyDashboardScreen();
  });

  static Handler _companySettingsScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return CompanySettingsScreen();
  });

  static Handler _employeeDashboardScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return EmployeeDashboardScreen();
  });

  static Handler _customerDashboardScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return CustomerDashboardScreen();
  });

  static Handler _addPackageScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return AddPackageScreen();
  });

  static Handler _customerProfileEditScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return CustomerProfileEditScreen();
  });

  static Handler _employeeProfileEditScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return EmployeeProfileEditScreen();
  });

  static Handler _makeOrderScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return MakeOrderScreen();
  });

  static Handler _customerSignupScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return FinishAccountScreen();
  });

  static Handler _orderDetailsScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    final orderId = params['orderId'][0];
    return OrderDetailsScreen(orderId: orderId);
  });

  static Handler _customerOrderTrackScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    final orderId = params['orderId'][0];
    return CustomerOrderTrackScreen(orderId);
  });

  static Handler _employeeOrderManagementScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    final orderId = params['orderId'][0];
    return EmployeeOrderManagementScreen(orderId: orderId);
  });

  static Handler _employeesManagementScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return EmployeesManagementScreen();
  });

  static Handler _packagesManagementScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return PackagesManagementScreen();
  });

  static Handler _editPackageScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    final packageId = params['packageId'][0];
    final vehicleTypeStr = params['vehicleType'][0];
    final vehicleTypes = {
      VehicleType.sedan.toString(): VehicleType.sedan,
      VehicleType.motorcycle.toString(): VehicleType.motorcycle,
      VehicleType.suv.toString(): VehicleType.suv,
      VehicleType.boat.toString(): VehicleType.boat,
    };
    return EditPackageScreen(
        packageId: packageId, vehicleType: vehicleTypes[vehicleTypeStr]);
  });

  static Handler _addNewEmployeeScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return AddNewEmployeeScreen();
  });

  static Handler _editEmployeeScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    final employeeId = params['employeeId'][0];
    return EditEmployeeScreen(employeeId: employeeId);
  });

  static Handler _joinUsScreenHandler = new Handler(
      handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return JoinUsScreen();
  });

  static Router getRouter() {
    if (_routesDefined) {
      return _router;
    }

    _router = new Router();
    _router.define('/',
        handler: _languageScreenHandler, transitionType: TransitionType.native);
    _router.define('/intro',
        handler: _introScreenHandler, transitionType: TransitionType.native);
    _router.define('/home',
        handler: _homeScreenHandler, transitionType: TransitionType.native);
    _router.define('/login',
        handler: _loginScreenHandler, transitionType: TransitionType.native);
    _router.define('/signup',
        handler: _customerSignupScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/joinus',
        handler: _joinUsScreenHandler, transitionType: TransitionType.native);
    _router.define('/dashboard/company',
        handler: _companyDashboardScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/dashboard/employee',
        handler: _employeeDashboardScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/dashboard/customer',
        handler: _customerDashboardScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/settings/company',
        handler: _companySettingsScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/packages/add',
        handler: _addPackageScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/profile/customer/edit',
        handler: _customerProfileEditScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/profile/employee/edit',
        handler: _employeeProfileEditScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/orders/new',
        handler: _makeOrderScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/orders/customer/track/:orderId',
        handler: _customerOrderTrackScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/orders/details/:orderId',
        handler: _orderDetailsScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/orders/employee/manage/:orderId',
        handler: _employeeOrderManagementScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/employees/manage',
        handler: _employeesManagementScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/employees/add',
        handler: _addNewEmployeeScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/employees/edit/:employeeId',
        handler: _editEmployeeScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/packages/manage',
        handler: _packagesManagementScreenHandler,
        transitionType: TransitionType.native);
    _router.define('/packages/edit/:packageId/:vehicleType',
        handler: _editPackageScreenHandler,
        transitionType: TransitionType.native);

    return _router;
  }
}
