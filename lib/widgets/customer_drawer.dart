import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/screens/customer_webview_screen/customer_webview_screen.dart';
import 'package:weash_cars/stores/application_store.dart';

class CustomerDrawer extends StatelessWidget {
  final String avatarUrl;
  final Function onCloseDrawer;

  CustomerDrawer({this.avatarUrl, this.onCloseDrawer});

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return WillPopScope(
      onWillPop: () {
        onCloseDrawer();
        return Future.value(true);
      },
      child: Row(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(267),
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/drawer-menu-background.png'),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                // SizedBox(height: ScreenUtil().setHeight(121)),
                Container(
                  width: ScreenUtil().setWidth(118),
                  height: ScreenUtil().setWidth(118),
                  padding: EdgeInsets.all(ScreenUtil().setWidth(5)),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Color(0x70ffffff),
                      width: 1,
                    ),
                  ),
                  child: Container(
                    width: ScreenUtil().setWidth(107),
                    height: ScreenUtil().setWidth(107),
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(5),
                      vertical: ScreenUtil().setHeight(5),
                    ),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    child: ClipPath(
                      clipper: ShapeBorderClipper(
                        shape: CircleBorder(),
                      ),
                      child: Image(
                        image: avatarUrl == null
                            ? AssetImage('assets/images/avatar-user.png')
                            : NetworkImage(avatarUrl),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(64)),
                Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () =>
                              Navigator.pushNamed(context, '/orders/new'),
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Icon(
                                Icons.home,
                                color: Colors.white,
                              ),
                              // Image(
                              //   image: AssetImage(
                              //       'assets/images/profile-icon.png'),
                              //   width: ScreenUtil().setWidth(23),
                              //   height: ScreenUtil().setHeight(17),
                              //   color: Colors.white,
                              // ),
                              SizedBox(width: ScreenUtil().setWidth(28)),
                              SizedBox(
                                height: ScreenUtil().setHeight(24),
                                child: FittedBox(
                                  fit: BoxFit.fitHeight,
                                  child: Text(
                                    S.of(context).homePage,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'Lato',
                                      fontSize: ScreenUtil().setSp(20),
                                      fontWeight: FontWeight.w900,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(25)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => Navigator.pushNamed(
                              context, '/profile/customer/edit'),
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Image(
                                image: AssetImage(
                                    'assets/images/profile-icon.png'),
                                width: ScreenUtil().setWidth(23),
                                height: ScreenUtil().setHeight(17),
                                color: Colors.white,
                              ),
                              SizedBox(width: ScreenUtil().setWidth(28)),
                              SizedBox(
                                height: ScreenUtil().setHeight(24),
                                child: FittedBox(
                                  fit: BoxFit.fitHeight,
                                  child: Text(
                                    S.of(context).profile,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'Lato',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: ScreenUtil().setSp(20),
                                      fontWeight: FontWeight.w900,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(25)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          onTap: () => Navigator.pushNamed(
                              context, '/dashboard/customer'),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Image(
                                image: AssetImage(
                                    'assets/images/list-orders-icon.png'),
                                width: ScreenUtil().setWidth(17),
                                height: ScreenUtil().setHeight(21),
                                color: Colors.white,
                              ),
                              SizedBox(width: ScreenUtil().setWidth(34)),
                              SizedBox(
                                height: ScreenUtil().setHeight(24),
                                child: FittedBox(
                                  fit: BoxFit.fitHeight,
                                  child: Text(
                                    S.of(context).Orders,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'Lato',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: ScreenUtil().setSp(20),
                                      fontWeight: FontWeight.w900,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(144)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () async {
                            final prefs = await SharedPreferences.getInstance();
                            prefs.remove('user_id');
                            Provider.of<ApplicationStore>(context)
                                .setIsSignedIn(false);
                            Navigator.pushNamed(context, '/home');
                          },
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Image(
                                image: AssetImage(
                                    'assets/images/signout-icon.png'),
                                width: ScreenUtil().setWidth(24),
                                height: ScreenUtil().setHeight(22),
                                color: Colors.white,
                              ),
                              SizedBox(width: ScreenUtil().setWidth(27)),
                              SizedBox(
                                height: ScreenUtil().setHeight(24),
                                child: FittedBox(
                                  fit: BoxFit.fitHeight,
                                  child: Text(
                                    S.of(context).signOut,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'Lato',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: ScreenUtil().setSp(20),
                                      fontWeight: FontWeight.w900,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(102)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      CustomerWebviewScreen(
                                        url:
                                            'https://quirky-varahamihira-b947be.netlify.com/terms_and_conditions.html',
                                      ))),
                          child: SizedBox(
                            height: ScreenUtil().setHeight(20),
                            child: FittedBox(
                              fit: BoxFit.fitHeight,
                              child: Text(
                                S.of(context).termsAndConditions,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'Lato',
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff91a6d9),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(3)),
                      SizedBox(
                        height: ScreenUtil().setHeight(20),
                        child: FittedBox(
                          fit: BoxFit.fitHeight,
                          child: Text(
                            S.of(context).softwareLicense,
                            style: TextStyle(
                              fontFamily:
                                  application.locale == 'ar' ? 'Cairo' : 'Lato',
                              fontWeight: FontWeight.w500,
                              color: Color(0xff91a6d9),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(2)),
                      SizedBox(
                        height: ScreenUtil().setHeight(20),
                        child: FittedBox(
                          fit: BoxFit.fitHeight,
                          child: Text(
                            S.of(context).copyright,
                            style: TextStyle(
                              fontFamily:
                                  application.locale == 'ar' ? 'Cairo' : 'Lato',
                              fontWeight: FontWeight.w500,
                              color: Color(0xff91a6d9),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(3)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      CustomerWebviewScreen(
                                        url:
                                            'https://quirky-varahamihira-b947be.netlify.com/privacy_policy.html',
                                      ))),
                          child: SizedBox(
                            height: ScreenUtil().setHeight(20),
                            child: FittedBox(
                              fit: BoxFit.fitHeight,
                              child: Text(
                                S.of(context).privacyPolicy,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'Lato',
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff91a6d9),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(40)),
              ],
            ),
          ),
          Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
                onCloseDrawer();
              },
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: SizedBox(
                width: ScreenUtil().setWidth(375.0 - 267.0),
                height: MediaQuery.of(context).size.height,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
