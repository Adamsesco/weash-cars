import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/stores/application_store.dart';

class CustomAppBar extends AppBar {
  CustomAppBar(BuildContext context,
      {Key key,
      List<PopupMenuEntry<String>> popupMenuItems = const [],
      int nbNotifications,
      String avatarUrl})
      : super(
          key: key,
          backgroundColor: Colors.lightBlue[900],
          elevation: 0,
          leading: PopupMenuButton<String>(
            onSelected: (String route) => Navigator.pushNamed(context, route),
            itemBuilder: (_) => popupMenuItems,
          ),
          title: Text(
            'DASHBOARD',
            style: TextStyle(
              fontFamilyFallback: <String>['SF Pro Text'],
              fontSize: 10,
              fontWeight: FontWeight.bold,
            ),
          ),
          centerTitle: true,
          actions: <Widget>[
            nbNotifications == null
                ? Container()
                : IconButton(
                    icon: CircleAvatar(
                      backgroundColor:
                          nbNotifications == 0 ? Colors.grey : Colors.red,
                      radius: 16,
                      child: Text(nbNotifications.toString()),
                    ),
                    onPressed: () {},
                  ),
            IconButton(
              icon: CircleAvatar(
                backgroundImage: avatarUrl == null
                    ? AssetImage('assets/images/avatar-user.png')
                    : NetworkImage(avatarUrl),
                radius: 16,
              ),
              onPressed: () {},
            ),
          ],
        );
}

class CustomAppBar2 extends StatelessWidget {
  final List<PopupMenuEntry<String>> popupMenuItems;
  final String title;
  final int nbNotifications;
  final ImageProvider avatar;
  final bool transparentBackground;
  final Function onOpenDrawer;
  final Function onAvatarTap;
  final Color titleColor;
  final Color buttonColor;
  final bool isCompany;
  final bool isAvailable;

  CustomAppBar2(
      {this.popupMenuItems,
      this.title = 'DASHBOARD',
      this.nbNotifications,
      this.avatar,
      this.transparentBackground = false,
      this.onOpenDrawer,
      this.onAvatarTap,
      this.titleColor = const Color(0xFF91A6D9),
      this.buttonColor = Colors.white,
      this.isCompany = false,
      this.isAvailable = false});

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        transparentBackground
            ? Container(
                width: MediaQuery.of(context).size.width,
                height: ScreenUtil().setHeight(131),
                color: Colors.transparent,
              )
            : Container(
                width: MediaQuery.of(context).size.width,
                height: ScreenUtil().setHeight(131),
                child: Image(
                  image: AssetImage(
                      'assets/images/customer-app-bar-background.png'),
                  fit: BoxFit.fill,
                ),
              ),
        Positioned(
          top: ScreenUtil().setHeight(61.1),
          // left: isCompany ? ScreenUtil().setWidth(16) : 0,
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding:
                EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(33)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Transform.translate(
                  offset: Offset(
                      ScreenUtil()
                          .setWidth(application.locale == 'ar' ? 10 : -10),
                      ScreenUtil().setHeight(73 - 61.1 - 10)),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: onOpenDrawer,
                      enableFeedback: false,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(10),
                          vertical: ScreenUtil().setHeight(10),
                        ),
                        child: Image(
                          image: AssetImage('assets/images/menu-icon.png'),
                          width: ScreenUtil().setWidth(34),
                          height: ScreenUtil().setHeight(20),
                          color: buttonColor,
                          matchTextDirection: true,
                        ),
                      ),
                    ),
                  ),
                ),
                Transform.translate(
                  offset: Offset(0, ScreenUtil().setHeight(73 - 61.1)),
                  child: Text(
                    title,
                    style: TextStyle(
                      fontFamily: application.locale == 'ar'
                          ? 'Cairo'
                          : 'SF Pro Display',
                      fontSize: ScreenUtil().setSp(15),
                      fontWeight: FontWeight.w900,
                      color: titleColor,
                    ),
                  ),
                ),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    // onTap: onAvatarTap,
                    highlightColor: Colors.transparent,
                    splashColor: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          width: ScreenUtil().setWidth(66),
                          height: ScreenUtil().setHeight(66),
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(3),
                            vertical: ScreenUtil().setHeight(3),
                          ),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                color: Colors.black.withOpacity(0.16),
                                offset: Offset(0, 3),
                                blurRadius: 6,
                              ),
                            ],
                          ),
                          child: ClipPath(
                            clipper: ShapeBorderClipper(
                              shape: CircleBorder(),
                            ),
                            child: Image(
                              image: avatar == null
                                  ? AssetImage('assets/images/avatar-user.png')
                                  : avatar,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        // if (isCompany)
                        //   Transform.translate(
                        //     offset: Offset(-ScreenUtil().setWidth(20), 0),
                        //     child: CircleAvatar(
                        //       radius: ScreenUtil().setWidth(8),
                        //       backgroundColor:
                        //           isAvailable ? Color(0xFF07D80F) : Colors.grey,
                        //     ),
                        //   ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
