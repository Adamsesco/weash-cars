import 'dart:async';
import 'dart:math';
import 'dart:ui' as ui;

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FullScreenImageView extends StatelessWidget {
  final ImageProvider imageProvider;
  final String imageUrl;
  final double animationValue;
  final Completer<ui.Image> _completer;

  FullScreenImageView({this.imageProvider, this.imageUrl, this.animationValue})
      : _completer = new Completer() {
    imageProvider.resolve(ImageConfiguration()).addListener(ImageStreamListener(
          (ImageInfo info, bool _) => _completer.complete(info.image),
          onError: (error, StackTrace stackTrace) =>
              _completer.completeError(error, stackTrace),
        ));
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          Center(
            child: FutureBuilder<ui.Image>(
              future: _completer.future,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final imageInfo = snapshot.data;
                  final imageRatio = imageInfo.width / imageInfo.height;
                  final showInLandscape = imageRatio > 1;

                  return Transform.scale(
                    scale: animationValue,
                    child: Transform.rotate(
                      angle: showInLandscape
                          ? (animationValue * (pi / 2)) - pi / 2
                          : 0,
                      child: RotatedBox(
                        quarterTurns: showInLandscape ? 1 : 0,
                        child: Image(
                          image: imageProvider,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  );
                } else {
                  return CupertinoActivityIndicator();
                }
              },
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).viewPadding.top,
            right: ScreenUtil().setWidth(10),
            left: ScreenUtil().setWidth(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: _shareImage,
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(12),
                      vertical: ScreenUtil().setHeight(8),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    child: Text(
                      'Share',
                      style: TextStyle(
                        fontFamily: 'SF UI Display',
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () => Navigator.of(context).pop(),
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  child: Container(
                    width: ScreenUtil().setWidth(40),
                    height: ScreenUtil().setWidth(40),
                    padding: EdgeInsets.all(ScreenUtil().setWidth(12)),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    child: Image(
                      image: AssetImage('assets/images/close-icon.png'),
                      color: Colors.white,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _shareImage() async {
    final image = await _completer.future;
    final byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    final bytes = byteData.buffer.asUint8List();
    await Share.file('Washy', 'picture.png', bytes, 'image/png');
  }
}
