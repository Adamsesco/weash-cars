import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/screens/availability_screen/availability_screen.dart';
import 'package:weash_cars/screens/company_webview_screen/company_webview_screen.dart';
import 'package:weash_cars/stores/application_store.dart';

class CompanyDrawer extends StatelessWidget {
  final String avatarUrl;
  final Function onCloseDrawer;

  CompanyDrawer({this.avatarUrl, this.onCloseDrawer});

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return WillPopScope(
      onWillPop: () {
        onCloseDrawer();
        return Future.value(true);
      },
      child: Row(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(267),
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/drawer-menu-background.png'),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: ScreenUtil().setWidth(118),
                  height: ScreenUtil().setWidth(118),
                  padding: EdgeInsets.all(ScreenUtil().setWidth(5)),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Color(0x70ffffff),
                      width: 1,
                    ),
                  ),
                  child: Container(
                    width: ScreenUtil().setWidth(107),
                    height: ScreenUtil().setWidth(107),
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(5),
                      vertical: ScreenUtil().setHeight(5),
                    ),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    child: ClipPath(
                      clipper: ShapeBorderClipper(
                        shape: CircleBorder(),
                      ),
                      child: Image(
                        image: avatarUrl == null
                            ? AssetImage('assets/images/avatar-user.png')
                            : NetworkImage(avatarUrl),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(44)),
                Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => Navigator.pushNamed(
                              context, '/dashboard/company'),
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(10),
                              vertical: ScreenUtil().setHeight(10),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Image(
                                  image: AssetImage(
                                      'assets/images/dashboard-icon.png'),
                                  width: ScreenUtil().setWidth(23),
                                  height: ScreenUtil().setHeight(17),
                                  color: Colors.white,
                                ),
                                SizedBox(width: ScreenUtil().setWidth(28)),
                                SizedBox(
                                  height: ScreenUtil().setHeight(24),
                                  child: FittedBox(
                                    fit: BoxFit.fitHeight,
                                    child: Text(
                                      S
                                          .of(context)
                                          .dashboard
                                          .toLowerCase()
                                          .replaceFirst('d', 'D'),
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'Lato',
                                        fontWeight: FontWeight.w900,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(5)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () =>
                              Navigator.pushNamed(context, '/packages/manage'),
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(10),
                              vertical: ScreenUtil().setHeight(10),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Image(
                                  image: AssetImage(
                                      'assets/images/packages-icon.png'),
                                  width: ScreenUtil().setWidth(23),
                                  height: ScreenUtil().setHeight(17),
                                  color: Colors.white,
                                ),
                                SizedBox(width: ScreenUtil().setWidth(28)),
                                SizedBox(
                                  height: ScreenUtil().setHeight(24),
                                  child: FittedBox(
                                    fit: BoxFit.fitHeight,
                                    child: Text(
                                      S.of(context).packages,
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'Lato',
                                        fontWeight: FontWeight.w900,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(5)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          onTap: () =>
                              Navigator.pushNamed(context, '/employees/manage'),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(10),
                              vertical: ScreenUtil().setHeight(10),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Image(
                                  image: AssetImage(
                                      'assets/images/employees-icon.png'),
                                  width: ScreenUtil().setWidth(17),
                                  height: ScreenUtil().setHeight(21),
                                  color: Colors.white,
                                ),
                                SizedBox(width: ScreenUtil().setWidth(34)),
                                SizedBox(
                                  height: ScreenUtil().setHeight(24),
                                  child: FittedBox(
                                    fit: BoxFit.fitHeight,
                                    child: Text(
                                      S.of(context).employees,
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'Lato',
                                        fontFamilyFallback: <String>[
                                          'SF Pro Text'
                                        ],
                                        fontSize: ScreenUtil().setSp(20),
                                        fontWeight: FontWeight.w900,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(5)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          onTap: () => Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => AvailabilityScreen(),
                            ),
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(10),
                              vertical: ScreenUtil().setHeight(10),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Image(
                                  image: AssetImage(
                                      'assets/images/availability-icon.png'),
                                  width: ScreenUtil().setWidth(19.74),
                                  height: ScreenUtil().setHeight(19.34),
                                  color: Colors.white,
                                ),
                                SizedBox(width: ScreenUtil().setWidth(31)),
                                SizedBox(
                                  height: ScreenUtil().setHeight(24),
                                  child: FittedBox(
                                    fit: BoxFit.fitHeight,
                                    child: Text(
                                      S
                                          .of(context)
                                          .availability
                                          .toLowerCase()
                                          .replaceFirst('a', 'A'),
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'Lato',
                                        fontSize: ScreenUtil().setSp(20),
                                        fontWeight: FontWeight.w900,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(5)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () =>
                              Navigator.pushNamed(context, '/settings/company'),
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(10),
                              vertical: ScreenUtil().setHeight(10),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Image(
                                  image: AssetImage(
                                      'assets/images/settings-icon.png'),
                                  width: ScreenUtil().setWidth(19),
                                  height: ScreenUtil().setHeight(19),
                                  color: Colors.white,
                                ),
                                SizedBox(width: ScreenUtil().setWidth(32)),
                                SizedBox(
                                  height: ScreenUtil().setHeight(24),
                                  child: FittedBox(
                                    fit: BoxFit.fitHeight,
                                    child: Text(
                                      S.of(context).settings,
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'Lato',
                                        fontFamilyFallback: <String>[
                                          'SF Pro Text'
                                        ],
                                        fontSize: ScreenUtil().setSp(20),
                                        fontWeight: FontWeight.w900,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(114)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () async {
                            final prefs = await SharedPreferences.getInstance();
                            prefs.remove('user_id');
                            Provider.of<ApplicationStore>(context)
                                .setIsSignedIn(false);
                            Navigator.pushNamed(context, '/home');
                          },
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(10),
                              vertical: ScreenUtil().setHeight(10),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Image(
                                  image: AssetImage(
                                      'assets/images/signout-icon.png'),
                                  width: ScreenUtil().setWidth(24),
                                  height: ScreenUtil().setHeight(22),
                                  color: Colors.white,
                                ),
                                SizedBox(width: ScreenUtil().setWidth(27)),
                                SizedBox(
                                  height: ScreenUtil().setHeight(24),
                                  child: FittedBox(
                                    fit: BoxFit.fitHeight,
                                    child: Text(
                                      S.of(context).signOut,
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'Lato',
                                        fontFamilyFallback: <String>[
                                          'SF Pro Text'
                                        ],
                                        fontSize: ScreenUtil().setSp(20),
                                        fontWeight: FontWeight.w900,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(72)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      CompanyWebviewScreen(
                                        url:
                                            'https://quirky-varahamihira-b947be.netlify.com/terms_and_conditions.html',
                                      ))),
                          enableFeedback: false,
                          child: SizedBox(
                            height: ScreenUtil().setHeight(20),
                            child: FittedBox(
                              fit: BoxFit.fitHeight,
                              child: Text(
                                S.of(context).termsAndConditions,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'Lato',
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff91a6d9),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(3)),
                      SizedBox(
                        height: ScreenUtil().setHeight(20),
                        child: FittedBox(
                          fit: BoxFit.fitHeight,
                          child: Text(
                            S.of(context).softwareLicense,
                            style: TextStyle(
                              fontFamily:
                                  application.locale == 'ar' ? 'Cairo' : 'Lato',
                              fontWeight: FontWeight.w500,
                              color: Color(0xff91a6d9),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(2)),
                      SizedBox(
                        height: ScreenUtil().setHeight(20),
                        child: FittedBox(
                          fit: BoxFit.fitHeight,
                          child: Text(
                            S.of(context).copyright,
                            style: TextStyle(
                              fontFamily:
                                  application.locale == 'ar' ? 'Cairo' : 'Lato',
                              fontWeight: FontWeight.w500,
                              color: Color(0xff91a6d9),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(3)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      CompanyWebviewScreen(
                                        url:
                                            'https://quirky-varahamihira-b947be.netlify.com/privacy_policy.html',
                                      ))),
                          enableFeedback: false,
                          child: SizedBox(
                            height: ScreenUtil().setHeight(20),
                            child: FittedBox(
                              fit: BoxFit.fitHeight,
                              child: Text(
                                S.of(context).privacyPolicy,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'Lato',
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff91a6d9),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(40)),
              ],
            ),
          ),
          Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
                onCloseDrawer();
              },
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: SizedBox(
                width: ScreenUtil().setWidth(375.0 - 267.0),
                height: MediaQuery.of(context).size.height,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
