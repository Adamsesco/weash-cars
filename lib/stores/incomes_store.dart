import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';

part 'incomes_store.g.dart';

class IncomesStore = _IncomesStore with _$IncomesStore;

abstract class _IncomesStore with Store {
  StreamSubscription<QuerySnapshot> _subscription;

  @observable
  Map<String, dynamic> incomes;

  @observable
  bool isListening = false;

  void subscribe(String userId) {
    var currentYear = DateTime.now().year;

    _subscription = Firestore.instance
        .collection('incomes')
        .where('company_id', isEqualTo: userId)
        .where('year', isEqualTo: currentYear)
        .snapshots()
        .listen(_setIncomes);

    isListening = true;
  }

  @action
  void setIncomes(Map<String, dynamic> incomes) {
    this.incomes = incomes;
  }

  void _setIncomes(QuerySnapshot data) {
    var incomesDocument = data.documents[0];

    var incomes = {
      'company_id': incomesDocument.data['company_id'],
      'year': incomesDocument.data['year'],
      'january': incomesDocument.data['january'] ?? 0.0,
      'february': incomesDocument.data['february'] ?? 0.0,
      'march': incomesDocument.data['march'] ?? 0.0,
      'april': incomesDocument.data['april'] ?? 0.0,
      'mai': incomesDocument.data['mai'] ?? 0.0,
      'june': incomesDocument.data['june'] ?? 0.0,
      'july': incomesDocument.data['july'] ?? 0.0,
      'august': incomesDocument.data['august'] ?? 0.0,
      'september': incomesDocument.data['september'] ?? 0.0,
      'october': incomesDocument.data['october'] ?? 0.0,
      'november': incomesDocument.data['november'] ?? 0.0,
      'december': incomesDocument.data['december'] ?? 0.0,
    };

    setIncomes(incomes);
  }

  void unsubscribe() {
    _subscription.cancel();
  }
}
