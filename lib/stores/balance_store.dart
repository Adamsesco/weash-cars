import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';

part 'balance_store.g.dart';

class BalanceStore = _BalanceStore with _$BalanceStore;

abstract class _BalanceStore with Store {
  StreamSubscription<QuerySnapshot> _subscription;

  @observable
  double balance = 0.0;

  @observable
  bool isListening = false;

  void subscribe(String userId) {
    _subscription = Firestore.instance
        .collection('balances')
        .where('company_id', isEqualTo: userId)
        .snapshots()
        .listen(_setBalance);

    isListening = true;
  }

  @action
  void setBalance(double balance) {
    this.balance = balance;
  }

  void _setBalance(QuerySnapshot data) {
    if (data.documents == null || data.documents.length == 0) {
      setBalance(0.0);
    } else {
      var balanceDocument = data.documents[0];
      var balance = balanceDocument.data['balance'].toDouble() as double;
      setBalance(balance);
    }
  }

  void unsubscribe() {
    _subscription.cancel();
  }
}
