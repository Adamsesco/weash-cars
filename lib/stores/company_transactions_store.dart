import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';

part 'company_transactions_store.g.dart';

class CompanyTransactionsStore = _CompanyTransactionsStore
    with _$CompanyTransactionsStore;

abstract class _CompanyTransactionsStore with Store {
  StreamSubscription<QuerySnapshot> _subscription;

  @observable
  List<Map<String, dynamic>> transactions = [];

  @observable
  bool isListening = false;

  @computed
  double get balance {
    if (!isListening) {
      return 0.0;
    }

    final b = transactions.fold(
        0.0, (acc, transaction) => acc + transaction['amount'] as double);
    return b;
  }

  @action
  void subscribe(String userId, DateTime startDate, DateTime endDate) {
    var startDateStr = startDate.toString();
    print(startDateStr);
    _subscription = Firestore.instance
        .collection('transactions')
        .where('company_id', isEqualTo: userId)
        .snapshots()
        .listen(_setTransactions);
  }

  @action
  void _setTransactions(QuerySnapshot data) {
    List<Map<String, dynamic>> transactionsTmp = [];
    isListening = true;

    data.documents.forEach((document) {
      var transaction = {
        'amount': document['amount'],
        'currency': document['currency'],
      };
      transactionsTmp.add(transaction);
    });

    transactions = transactionsTmp;
  }

  void unsubscribe() {
    _subscription.cancel();
  }
}
