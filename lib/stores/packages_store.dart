import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';

part 'packages_store.g.dart';

class PackagesStore = _PackagesStore with _$PackagesStore;

abstract class _PackagesStore with Store {
  StreamSubscription<QuerySnapshot> _subscription;

  @observable
  List<Map<String, dynamic>> packages;

  @observable
  bool isListening = false;

  void subscribe(String userId) {
    _subscription = Firestore.instance
        .collection('packages')
        .where('company_id', isEqualTo: userId)
        .snapshots()
        .listen(_setPackages);

    isListening = true;
  }

  @action
  void setPackages(List<Map<String, dynamic>> packages) {
    this.packages = packages;
  }

  void _setPackages(QuerySnapshot data) async {
    List<Map<String, dynamic>> packagesTmp = [];

    await Future.forEach(data.documents,
        (DocumentSnapshot packageDocument) async {
      var package = {
        'package_id': packageDocument.documentID,
        'company_id': packageDocument.data['company_id'],
        'title': packageDocument.data['title'],
        'description': packageDocument.data['description'],
        'price': packageDocument.data['price'],
        'currency': packageDocument.data['currency'],
      };
      packagesTmp.add(package);
    });

    setPackages(packagesTmp);
  }

  void unsubscribe() {
    _subscription.cancel();
  }
}
