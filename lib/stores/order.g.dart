// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$Order on _Order, Store {
  final _$idAtom = Atom(name: '_Order.id');

  @override
  String get id {
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.checkIfStateModificationsAreAllowed(_$idAtom);
    super.id = value;
    _$idAtom.reportChanged();
  }

  final _$companyAtom = Atom(name: '_Order.company');

  @override
  Company get company {
    _$companyAtom.reportObserved();
    return super.company;
  }

  @override
  set company(Company value) {
    _$companyAtom.context.checkIfStateModificationsAreAllowed(_$companyAtom);
    super.company = value;
    _$companyAtom.reportChanged();
  }

  final _$packageAtom = Atom(name: '_Order.package');

  @override
  Package get package {
    _$packageAtom.reportObserved();
    return super.package;
  }

  @override
  set package(Package value) {
    _$packageAtom.context.checkIfStateModificationsAreAllowed(_$packageAtom);
    super.package = value;
    _$packageAtom.reportChanged();
  }

  final _$addressAtom = Atom(name: '_Order.address');

  @override
  String get address {
    _$addressAtom.reportObserved();
    return super.address;
  }

  @override
  set address(String value) {
    _$addressAtom.context.checkIfStateModificationsAreAllowed(_$addressAtom);
    super.address = value;
    _$addressAtom.reportChanged();
  }

  final _$locationAtom = Atom(name: '_Order.location');

  @override
  GeoPoint get location {
    _$locationAtom.reportObserved();
    return super.location;
  }

  @override
  set location(GeoPoint value) {
    _$locationAtom.context.checkIfStateModificationsAreAllowed(_$locationAtom);
    super.location = value;
    _$locationAtom.reportChanged();
  }

  final _$localityAtom = Atom(name: '_Order.locality');

  @override
  String get locality {
    _$localityAtom.reportObserved();
    return super.locality;
  }

  @override
  set locality(String value) {
    _$localityAtom.context.checkIfStateModificationsAreAllowed(_$localityAtom);
    super.locality = value;
    _$localityAtom.reportChanged();
  }

  final _$dateAtom = Atom(name: '_Order.date');

  @override
  DateTime get date {
    _$dateAtom.reportObserved();
    return super.date;
  }

  @override
  set date(DateTime value) {
    _$dateAtom.context.checkIfStateModificationsAreAllowed(_$dateAtom);
    super.date = value;
    _$dateAtom.reportChanged();
  }

  final _$paymentMethodAtom = Atom(name: '_Order.paymentMethod');

  @override
  PaymentMethod get paymentMethod {
    _$paymentMethodAtom.reportObserved();
    return super.paymentMethod;
  }

  @override
  set paymentMethod(PaymentMethod value) {
    _$paymentMethodAtom.context
        .checkIfStateModificationsAreAllowed(_$paymentMethodAtom);
    super.paymentMethod = value;
    _$paymentMethodAtom.reportChanged();
  }
}
