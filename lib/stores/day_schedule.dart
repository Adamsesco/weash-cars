import 'package:mobx/mobx.dart';

part 'day_schedule.g.dart';

class DaySchedule = _DaySchedule with _$DaySchedule;

abstract class _DaySchedule with Store {
  @observable
  int weekDay;

  @observable
  int from;

  @observable
  int to;

  @observable
  bool isDayOff;
}