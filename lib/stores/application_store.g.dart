// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'application_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$ApplicationStore on _ApplicationStore, Store {
  final _$isFirstRunAtom = Atom(name: '_ApplicationStore.isFirstRun');

  @override
  bool get isFirstRun {
    _$isFirstRunAtom.reportObserved();
    return super.isFirstRun;
  }

  @override
  set isFirstRun(bool value) {
    _$isFirstRunAtom.context
        .checkIfStateModificationsAreAllowed(_$isFirstRunAtom);
    super.isFirstRun = value;
    _$isFirstRunAtom.reportChanged();
  }

  final _$isSignedInAtom = Atom(name: '_ApplicationStore.isSignedIn');

  @override
  bool get isSignedIn {
    _$isSignedInAtom.reportObserved();
    return super.isSignedIn;
  }

  @override
  set isSignedIn(bool value) {
    _$isSignedInAtom.context
        .checkIfStateModificationsAreAllowed(_$isSignedInAtom);
    super.isSignedIn = value;
    _$isSignedInAtom.reportChanged();
  }

  final _$userRoleAtom = Atom(name: '_ApplicationStore.userRole');

  @override
  UserRole get userRole {
    _$userRoleAtom.reportObserved();
    return super.userRole;
  }

  @override
  set userRole(UserRole value) {
    _$userRoleAtom.context.checkIfStateModificationsAreAllowed(_$userRoleAtom);
    super.userRole = value;
    _$userRoleAtom.reportChanged();
  }

  final _$localeAtom = Atom(name: '_ApplicationStore.locale');

  @override
  String get locale {
    _$localeAtom.reportObserved();
    return super.locale;
  }

  @override
  set locale(String value) {
    _$localeAtom.context.checkIfStateModificationsAreAllowed(_$localeAtom);
    super.locale = value;
    _$localeAtom.reportChanged();
  }

  final _$notificationsCountAtom =
      Atom(name: '_ApplicationStore.notificationsCount');

  @override
  int get notificationsCount {
    _$notificationsCountAtom.reportObserved();
    return super.notificationsCount;
  }

  @override
  set notificationsCount(int value) {
    _$notificationsCountAtom.context
        .checkIfStateModificationsAreAllowed(_$notificationsCountAtom);
    super.notificationsCount = value;
    _$notificationsCountAtom.reportChanged();
  }

  final _$isPendingNotificationAtom =
      Atom(name: '_ApplicationStore.isPendingNotification');

  @override
  bool get isPendingNotification {
    _$isPendingNotificationAtom.reportObserved();
    return super.isPendingNotification;
  }

  @override
  set isPendingNotification(bool value) {
    _$isPendingNotificationAtom.context
        .checkIfStateModificationsAreAllowed(_$isPendingNotificationAtom);
    super.isPendingNotification = value;
    _$isPendingNotificationAtom.reportChanged();
  }

  final _$_ApplicationStoreActionController =
      ActionController(name: '_ApplicationStore');

  @override
  void setFirstRun(bool value) {
    final _$actionInfo = _$_ApplicationStoreActionController.startAction();
    try {
      return super.setFirstRun(value);
    } finally {
      _$_ApplicationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setIsSignedIn(bool value) {
    final _$actionInfo = _$_ApplicationStoreActionController.startAction();
    try {
      return super.setIsSignedIn(value);
    } finally {
      _$_ApplicationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setUserRole(UserRole role) {
    final _$actionInfo = _$_ApplicationStoreActionController.startAction();
    try {
      return super.setUserRole(role);
    } finally {
      _$_ApplicationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setLocale(String locale) {
    final _$actionInfo = _$_ApplicationStoreActionController.startAction();
    try {
      return super.setLocale(locale);
    } finally {
      _$_ApplicationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addNotification() {
    final _$actionInfo = _$_ApplicationStoreActionController.startAction();
    try {
      return super.addNotification();
    } finally {
      _$_ApplicationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeNotification() {
    final _$actionInfo = _$_ApplicationStoreActionController.startAction();
    try {
      return super.removeNotification();
    } finally {
      _$_ApplicationStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setIsPendingNotification(bool value) {
    final _$actionInfo = _$_ApplicationStoreActionController.startAction();
    try {
      return super.setIsPendingNotification(value);
    } finally {
      _$_ApplicationStoreActionController.endAction(_$actionInfo);
    }
  }
}
