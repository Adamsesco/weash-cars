import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';
import 'package:weash_cars/models/user_role.dart';
import 'package:weash_cars/models/vehicle_type.dart';

import 'package:weash_cars/stores/company_transactions_store.dart';
import 'package:weash_cars/stores/order.dart';
import 'package:weash_cars/stores/package_store.dart';

import 'employee.dart';
import 'schedule.dart';

part 'company.g.dart';

class Company = _Company with _$Company;

abstract class _Company with Store {
  // final transactions = new CompanyTransactionsStore();

  @observable
  String id;

  @observable
  String name;

  @observable
  String address;

  @observable
  String avatarUrl;

  @observable
  String currency;

  @observable
  DateTime dateActivated;

  @observable
  DateTime dateCreated;

  @observable
  String email;

  @observable
  String fcmToken;

  @observable
  bool isActivated;

  @observable
  bool isAvailable;

  @observable
  int jobsDoneCount;

  @observable
  String locale;

  @observable
  String locality;

  @observable
  GeoPoint location;

  @observable
  String phoneNumber;

  @observable
  double rating;

  @observable
  int ordersTotal;

  @observable
  int reviewsTotal;

  @observable
  List<VehicleType> vehicleTypes;

  @observable
  List<Package> packages;

  @observable
  List<Employee> employees;

  @observable
  List<Order> orders;

  @observable
  Schedule schedule;

  @observable
  bool isLoggedIn;

  UserRole get role => employees == null || employees.isEmpty
      ? UserRole.selfEmployed
      : UserRole.company;
}
