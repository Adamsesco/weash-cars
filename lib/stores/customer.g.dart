// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$Customer on _Customer, Store {
  final _$idAtom = Atom(name: '_Customer.id');

  @override
  String get id {
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.checkIfStateModificationsAreAllowed(_$idAtom);
    super.id = value;
    _$idAtom.reportChanged();
  }

  final _$emailAtom = Atom(name: '_Customer.email');

  @override
  String get email {
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.checkIfStateModificationsAreAllowed(_$emailAtom);
    super.email = value;
    _$emailAtom.reportChanged();
  }

  final _$phoneNumberAtom = Atom(name: '_Customer.phoneNumber');

  @override
  String get phoneNumber {
    _$phoneNumberAtom.reportObserved();
    return super.phoneNumber;
  }

  @override
  set phoneNumber(String value) {
    _$phoneNumberAtom.context
        .checkIfStateModificationsAreAllowed(_$phoneNumberAtom);
    super.phoneNumber = value;
    _$phoneNumberAtom.reportChanged();
  }

  final _$avtarUrlAtom = Atom(name: '_Customer.avtarUrl');

  @override
  String get avtarUrl {
    _$avtarUrlAtom.reportObserved();
    return super.avtarUrl;
  }

  @override
  set avtarUrl(String value) {
    _$avtarUrlAtom.context.checkIfStateModificationsAreAllowed(_$avtarUrlAtom);
    super.avtarUrl = value;
    _$avtarUrlAtom.reportChanged();
  }

  final _$fcmTokenAtom = Atom(name: '_Customer.fcmToken');

  @override
  String get fcmToken {
    _$fcmTokenAtom.reportObserved();
    return super.fcmToken;
  }

  @override
  set fcmToken(String value) {
    _$fcmTokenAtom.context.checkIfStateModificationsAreAllowed(_$fcmTokenAtom);
    super.fcmToken = value;
    _$fcmTokenAtom.reportChanged();
  }

  final _$firstNameAtom = Atom(name: '_Customer.firstName');

  @override
  String get firstName {
    _$firstNameAtom.reportObserved();
    return super.firstName;
  }

  @override
  set firstName(String value) {
    _$firstNameAtom.context
        .checkIfStateModificationsAreAllowed(_$firstNameAtom);
    super.firstName = value;
    _$firstNameAtom.reportChanged();
  }

  final _$lastNameAtom = Atom(name: '_Customer.lastName');

  @override
  String get lastName {
    _$lastNameAtom.reportObserved();
    return super.lastName;
  }

  @override
  set lastName(String value) {
    _$lastNameAtom.context.checkIfStateModificationsAreAllowed(_$lastNameAtom);
    super.lastName = value;
    _$lastNameAtom.reportChanged();
  }

  final _$localeAtom = Atom(name: '_Customer.locale');

  @override
  String get locale {
    _$localeAtom.reportObserved();
    return super.locale;
  }

  @override
  set locale(String value) {
    _$localeAtom.context.checkIfStateModificationsAreAllowed(_$localeAtom);
    super.locale = value;
    _$localeAtom.reportChanged();
  }

  final _$ordersAtom = Atom(name: '_Customer.orders');

  @override
  List<Order> get orders {
    _$ordersAtom.reportObserved();
    return super.orders;
  }

  @override
  set orders(List<Order> value) {
    _$ordersAtom.context.checkIfStateModificationsAreAllowed(_$ordersAtom);
    super.orders = value;
    _$ordersAtom.reportChanged();
  }
}
