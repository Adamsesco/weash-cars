// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employee_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$EmployeeStore on _EmployeeStore, Store {
  final _$employeeIdAtom = Atom(name: '_EmployeeStore.employeeId');

  @override
  String get employeeId {
    _$employeeIdAtom.reportObserved();
    return super.employeeId;
  }

  @override
  set employeeId(String value) {
    _$employeeIdAtom.context
        .checkIfStateModificationsAreAllowed(_$employeeIdAtom);
    super.employeeId = value;
    _$employeeIdAtom.reportChanged();
  }

  final _$companyIdAtom = Atom(name: '_EmployeeStore.companyId');

  @override
  String get companyId {
    _$companyIdAtom.reportObserved();
    return super.companyId;
  }

  @override
  set companyId(String value) {
    _$companyIdAtom.context
        .checkIfStateModificationsAreAllowed(_$companyIdAtom);
    super.companyId = value;
    _$companyIdAtom.reportChanged();
  }

  final _$emailAtom = Atom(name: '_EmployeeStore.email');

  @override
  String get email {
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.checkIfStateModificationsAreAllowed(_$emailAtom);
    super.email = value;
    _$emailAtom.reportChanged();
  }

  final _$phoneNumberAtom = Atom(name: '_EmployeeStore.phoneNumber');

  @override
  String get phoneNumber {
    _$phoneNumberAtom.reportObserved();
    return super.phoneNumber;
  }

  @override
  set phoneNumber(String value) {
    _$phoneNumberAtom.context
        .checkIfStateModificationsAreAllowed(_$phoneNumberAtom);
    super.phoneNumber = value;
    _$phoneNumberAtom.reportChanged();
  }

  final _$firstNameAtom = Atom(name: '_EmployeeStore.firstName');

  @override
  String get firstName {
    _$firstNameAtom.reportObserved();
    return super.firstName;
  }

  @override
  set firstName(String value) {
    _$firstNameAtom.context
        .checkIfStateModificationsAreAllowed(_$firstNameAtom);
    super.firstName = value;
    _$firstNameAtom.reportChanged();
  }

  final _$lastNameAtom = Atom(name: '_EmployeeStore.lastName');

  @override
  String get lastName {
    _$lastNameAtom.reportObserved();
    return super.lastName;
  }

  @override
  set lastName(String value) {
    _$lastNameAtom.context.checkIfStateModificationsAreAllowed(_$lastNameAtom);
    super.lastName = value;
    _$lastNameAtom.reportChanged();
  }

  final _$avatarUrlAtom = Atom(name: '_EmployeeStore.avatarUrl');

  @override
  String get avatarUrl {
    _$avatarUrlAtom.reportObserved();
    return super.avatarUrl;
  }

  @override
  set avatarUrl(String value) {
    _$avatarUrlAtom.context
        .checkIfStateModificationsAreAllowed(_$avatarUrlAtom);
    super.avatarUrl = value;
    _$avatarUrlAtom.reportChanged();
  }

  final _$jobsDoneCountAtom = Atom(name: '_EmployeeStore.jobsDoneCount');

  @override
  int get jobsDoneCount {
    _$jobsDoneCountAtom.reportObserved();
    return super.jobsDoneCount;
  }

  @override
  set jobsDoneCount(int value) {
    _$jobsDoneCountAtom.context
        .checkIfStateModificationsAreAllowed(_$jobsDoneCountAtom);
    super.jobsDoneCount = value;
    _$jobsDoneCountAtom.reportChanged();
  }

  final _$localeAtom = Atom(name: '_EmployeeStore.locale');

  @override
  String get locale {
    _$localeAtom.reportObserved();
    return super.locale;
  }

  @override
  set locale(String value) {
    _$localeAtom.context.checkIfStateModificationsAreAllowed(_$localeAtom);
    super.locale = value;
    _$localeAtom.reportChanged();
  }

  final _$fcmTokenAtom = Atom(name: '_EmployeeStore.fcmToken');

  @override
  String get fcmToken {
    _$fcmTokenAtom.reportObserved();
    return super.fcmToken;
  }

  @override
  set fcmToken(String value) {
    _$fcmTokenAtom.context.checkIfStateModificationsAreAllowed(_$fcmTokenAtom);
    super.fcmToken = value;
    _$fcmTokenAtom.reportChanged();
  }

  final _$_EmployeeStoreActionController =
      ActionController(name: '_EmployeeStore');

  @override
  void setData(
      {String employeeId,
      String companyId,
      String email,
      String phoneNumber,
      String firstName,
      String lastName,
      String avatarUrl,
      int jobsDoneCount,
      String locale,
      String fcmToken}) {
    final _$actionInfo = _$_EmployeeStoreActionController.startAction();
    try {
      return super.setData(
          employeeId: employeeId,
          companyId: companyId,
          email: email,
          phoneNumber: phoneNumber,
          firstName: firstName,
          lastName: lastName,
          avatarUrl: avatarUrl,
          jobsDoneCount: jobsDoneCount,
          locale: locale,
          fcmToken: fcmToken);
    } finally {
      _$_EmployeeStoreActionController.endAction(_$actionInfo);
    }
  }
}
