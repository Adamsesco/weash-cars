import 'dart:async';

import 'package:mobx/mobx.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:weash_cars/models/user_role.dart';

import 'company_store.dart';
import 'customer_store.dart';
import 'employee_store.dart';

part 'application_store.g.dart';

class ApplicationStore = _ApplicationStore with _$ApplicationStore;

abstract class _ApplicationStore with Store {
  CompanyStore companyStore = new CompanyStore();
  CustomerStore customerStore = new CustomerStore();
  EmployeeStore employeeStore = new EmployeeStore();

  Database database;

  @observable
  bool isFirstRun = true;

  @observable
  bool isSignedIn = false;

  @observable
  UserRole userRole;

  @observable
  String locale;

  @observable
  int notificationsCount;

  @observable
  bool isPendingNotification = false;

  StreamController<String> localeStream = new StreamController();

  BehaviorSubject<String> notificationsStream = new BehaviorSubject();

  @action
  void setFirstRun(bool value) {
    isFirstRun = value;
  }

  @action
  void setIsSignedIn(bool value) {
    isSignedIn = value;
  }

  @action
  void setUserRole(UserRole role) {
    userRole = role;
  }

  @action
  void setLocale(String locale) {
    this.locale = locale;
  }

  @action
  void addNotification() {
    notificationsCount += 1;
    SharedPreferences.getInstance().then((SharedPreferences prefs) {
      prefs.setInt('notificationsCount', notificationsCount);
    });
  }

  @action
  void removeNotification() {
    if (notificationsCount > 0) {
      notificationsCount -= 1;
      SharedPreferences.getInstance().then((SharedPreferences prefs) {
        prefs.setInt('notificationsCount', notificationsCount);
      });
    }
  }

  @action
  void setIsPendingNotification(bool value) {
    isPendingNotification = value;
  }
}
