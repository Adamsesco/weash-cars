import 'package:mobx/mobx.dart';
import 'package:weash_cars/stores/day_schedule.dart';

part 'schedule.g.dart';

class Schedule = _Schedule with _$Schedule;

abstract class _Schedule with Store {
  @observable
  String id;

  @observable
  List<DaySchedule> daySchedules;
}
