import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';

import 'package:weash_cars/models/payment_method.dart';
import 'package:weash_cars/models/vehicle_type.dart';

part 'order_store.g.dart';

class OrderStore = _OrderStore with _$OrderStore;

abstract class _OrderStore with Store {
  @observable
  Map<String, dynamic> company;

  @observable
  Map<String, dynamic> package;

  @observable
  VehicleType vehicleType;

  @observable
  String address;

  @observable
  GeoPoint location;

  @observable
  String country;

  @observable
  String locality;

  @observable
  DateTime date;

  @observable
  PaymentMethod paymentMethod;

  @action
  void setOrder(
      {Map<String, dynamic> company,
      Map<String, dynamic> package,
      VehicleType vehicleType,
      String address,
      GeoPoint location,
      String country,
      String locality,
      DateTime date,
      PaymentMethod method}) {
    this.company = company;
    this.package = package;
    this.vehicleType = vehicleType;
    this.address = address;
    this.location = location;
    this.country = country;
    this.locality = locality;
    this.date = date;
    this.paymentMethod = paymentMethod;
  }
}
