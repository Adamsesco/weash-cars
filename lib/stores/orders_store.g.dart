// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'orders_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$OrdersStore on _OrdersStore, Store {
  final _$ordersAtom = Atom(name: '_OrdersStore.orders');

  @override
  List<Map<String, dynamic>> get orders {
    _$ordersAtom.reportObserved();
    return super.orders;
  }

  @override
  set orders(List<Map<String, dynamic>> value) {
    _$ordersAtom.context.checkIfStateModificationsAreAllowed(_$ordersAtom);
    super.orders = value;
    _$ordersAtom.reportChanged();
  }

  final _$ordersFutureAtom = Atom(name: '_OrdersStore.ordersFuture');

  @override
  Future<List<Map<String, dynamic>>> get ordersFuture {
    _$ordersFutureAtom.reportObserved();
    return super.ordersFuture;
  }

  @override
  set ordersFuture(Future<List<Map<String, dynamic>>> value) {
    _$ordersFutureAtom.context
        .checkIfStateModificationsAreAllowed(_$ordersFutureAtom);
    super.ordersFuture = value;
    _$ordersFutureAtom.reportChanged();
  }

  final _$isListeningAtom = Atom(name: '_OrdersStore.isListening');

  @override
  bool get isListening {
    _$isListeningAtom.reportObserved();
    return super.isListening;
  }

  @override
  set isListening(bool value) {
    _$isListeningAtom.context
        .checkIfStateModificationsAreAllowed(_$isListeningAtom);
    super.isListening = value;
    _$isListeningAtom.reportChanged();
  }

  final _$_OrdersStoreActionController = ActionController(name: '_OrdersStore');

  @override
  void setOrdersFuture(Future<List<Map<String, dynamic>>> future) {
    final _$actionInfo = _$_OrdersStoreActionController.startAction();
    try {
      return super.setOrdersFuture(future);
    } finally {
      _$_OrdersStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setOrders(List<Map<String, dynamic>> orders) {
    final _$actionInfo = _$_OrdersStoreActionController.startAction();
    try {
      return super.setOrders(orders);
    } finally {
      _$_OrdersStoreActionController.endAction(_$actionInfo);
    }
  }
}
