import 'package:mobx/mobx.dart';

import 'orders_store.dart';

part 'employee_store.g.dart';

class EmployeeStore = _EmployeeStore with _$EmployeeStore;

abstract class _EmployeeStore with Store {
  final orders = new OrdersStore();

  @observable
  String employeeId;

  @observable
  String companyId;

  @observable
  String email;

  @observable
  String phoneNumber;

  @observable
  String firstName;

  @observable
  String lastName;

  @observable
  String avatarUrl;

  @observable
  int jobsDoneCount;

  @observable
  String locale;

  @observable
  String fcmToken;

  @action
  void setData({
    String employeeId,
    String companyId,
    String email,
    String phoneNumber,
    String firstName,
    String lastName,
    String avatarUrl,
    int jobsDoneCount,
    String locale,
    String fcmToken,
  }) {
    if (employeeId != null) this.employeeId = employeeId;
    if (companyId != null) this.companyId = companyId;
    if (email != null) this.email = email;
    if (phoneNumber != null) this.phoneNumber = phoneNumber;
    if (firstName != null) this.firstName = firstName;
    if (lastName != null) this.lastName = lastName;
    if (avatarUrl != null) this.avatarUrl = avatarUrl;
    if (jobsDoneCount != null) this.jobsDoneCount = jobsDoneCount;
    if (locale != null) this.locale = locale;
    if (fcmToken != null) this.fcmToken = fcmToken;
  }
}
