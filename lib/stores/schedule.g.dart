// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'schedule.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$Schedule on _Schedule, Store {
  final _$idAtom = Atom(name: '_Schedule.id');

  @override
  String get id {
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.checkIfStateModificationsAreAllowed(_$idAtom);
    super.id = value;
    _$idAtom.reportChanged();
  }

  final _$daySchedulesAtom = Atom(name: '_Schedule.daySchedules');

  @override
  List<DaySchedule> get daySchedules {
    _$daySchedulesAtom.reportObserved();
    return super.daySchedules;
  }

  @override
  set daySchedules(List<DaySchedule> value) {
    _$daySchedulesAtom.context
        .checkIfStateModificationsAreAllowed(_$daySchedulesAtom);
    super.daySchedules = value;
    _$daySchedulesAtom.reportChanged();
  }
}
