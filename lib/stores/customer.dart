import 'package:mobx/mobx.dart';
import 'package:weash_cars/models/user_role.dart';
import 'package:weash_cars/stores/order.dart';

part 'customer.g.dart';

class Customer = _Customer with _$Customer;

abstract class _Customer with Store {
  @observable
  String id;

  @observable
  String email;

  @observable
  String phoneNumber;

  @observable
  String avtarUrl;

  @observable
  String fcmToken;

  @observable
  String firstName;

  @observable
  String lastName;

  @observable
  String locale;

  @observable
  List<Order> orders;

  UserRole get role => UserRole.customer;
}