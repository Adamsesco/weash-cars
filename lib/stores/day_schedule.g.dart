// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'day_schedule.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$DaySchedule on _DaySchedule, Store {
  final _$weekDayAtom = Atom(name: '_DaySchedule.weekDay');

  @override
  int get weekDay {
    _$weekDayAtom.reportObserved();
    return super.weekDay;
  }

  @override
  set weekDay(int value) {
    _$weekDayAtom.context.checkIfStateModificationsAreAllowed(_$weekDayAtom);
    super.weekDay = value;
    _$weekDayAtom.reportChanged();
  }

  final _$fromAtom = Atom(name: '_DaySchedule.from');

  @override
  int get from {
    _$fromAtom.reportObserved();
    return super.from;
  }

  @override
  set from(int value) {
    _$fromAtom.context.checkIfStateModificationsAreAllowed(_$fromAtom);
    super.from = value;
    _$fromAtom.reportChanged();
  }

  final _$toAtom = Atom(name: '_DaySchedule.to');

  @override
  int get to {
    _$toAtom.reportObserved();
    return super.to;
  }

  @override
  set to(int value) {
    _$toAtom.context.checkIfStateModificationsAreAllowed(_$toAtom);
    super.to = value;
    _$toAtom.reportChanged();
  }

  final _$isDayOffAtom = Atom(name: '_DaySchedule.isDayOff');

  @override
  bool get isDayOff {
    _$isDayOffAtom.reportObserved();
    return super.isDayOff;
  }

  @override
  set isDayOff(bool value) {
    _$isDayOffAtom.context.checkIfStateModificationsAreAllowed(_$isDayOffAtom);
    super.isDayOff = value;
    _$isDayOffAtom.reportChanged();
  }
}
