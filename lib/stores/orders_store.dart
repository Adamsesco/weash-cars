import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';
import 'package:weash_cars/models/payment_method.dart';

part 'orders_store.g.dart';

class OrdersStore = _OrdersStore with _$OrdersStore;

abstract class _OrdersStore with Store {
  StreamSubscription<QuerySnapshot> _subscription;

  @observable
  List<Map<String, dynamic>> orders = [];

  @observable
  Future<List<Map<String, dynamic>>> ordersFuture;

  @observable
  bool isListening = false;

  @action
  void setOrdersFuture(Future<List<Map<String, dynamic>>> future) {
    ordersFuture = future;
  }

  void subscribe({String companyId, String employeeId, String customerId}) {
    assert(companyId != null || employeeId != null || customerId != null);

    String idField;
    String id;
    if (companyId != null) {
      idField = 'company_id';
      id = companyId;
    } else if (employeeId != null) {
      idField = 'assigned_employee_id';
      id = employeeId;
    } else if (customerId != null) {
      idField = 'customer_id';
      id = customerId;
    }

    _subscription = Firestore.instance
        .collection('orders')
        .where(idField, isEqualTo: id)
        .snapshots()
        .listen(_setOrders);

    isListening = true;
  }

  @action
  void setOrders(List<Map<String, dynamic>> orders) {
    this.orders = orders;
  }

  void _setOrders(QuerySnapshot data) async {
    List<Map<String, dynamic>> ordersTmp = [];

    await Future.forEach(data.documents,
        (DocumentSnapshot orderDocument) async {
      final customerDocument = await Firestore.instance
          .collection('users')
          .document(orderDocument.data['customer_id'])
          .get();
      if (customerDocument == null || customerDocument.data == null) return;
      final customer = {
        'customer_id': orderDocument.data['customer_id'] as String,
        'first_name': customerDocument.data['first_name'] as String,
        'last_name': customerDocument.data['last_name'] as String,
        'phone_number': customerDocument.data['phone_number'] as String,
        'avatar_url': customerDocument.data['avatar_url'] as String,
      };

      final companyDocument = await Firestore.instance
          .collection('users')
          .document(orderDocument.data['company_id'])
          .get();
      if (companyDocument == null || companyDocument.data == null) return;
      final company = {
        'company_id': companyDocument.data['company_id'],
        'company_name': companyDocument.data['company_name'],
        'avatar_url': companyDocument.data['avatar_url'],
      };

      final packageDocument = await Firestore.instance
          .collection('packages')
          .document(orderDocument.data['package_id'])
          .get();
      if (packageDocument == null || packageDocument.data == null) return;
      final package = {
        'package_id': orderDocument.data['package_id'] as String,
        'company_id': packageDocument.data['company_id'] as String,
        'title': packageDocument.data['title'] as String,
        'description': packageDocument.data['description'] as String,
        'price': (packageDocument.data['price'] as num).toDouble(),
        'currency': packageDocument.data['currency'] as String,
        'vehicle_type': packageDocument.data['vehicle_type'],
      };

      final paymentMethod = orderDocument.data['payment_method'];
      final paymentMethods = {
        'visa': PaymentMethod.visa,
        'cash': PaymentMethod.cash,
      };

      final order = {
        'order_id': orderDocument.documentID,
        'order_number': orderDocument.data['order_number'] as int,
        'customer': customer,
        'company': company,
        'package': package,
        'date': (orderDocument.data['date'] as Timestamp).toDate(),
        'address': orderDocument.data['address'] == null
            ? null
            : orderDocument.data['address'].toString(),
        'location': orderDocument.data['location'] as GeoPoint,
        'locality': orderDocument.data['locality'] == null
            ? null
            : orderDocument.data['locality'].toString(),
        'status': orderDocument.data['status'] as int,
        'assigned_employee_id':
            orderDocument.data['assigned_employee_id'].toString(),
        'payment_method': paymentMethods[paymentMethod],
        'pictures_urls': orderDocument.data['pictures_urls'],
        'rating': orderDocument.data['rating'],
        'review': orderDocument.data['review'],
      };
      ordersTmp.add(order);
    });

    setOrders(ordersTmp);
  }

  void unsubscribe() {
    _subscription.cancel();
  }
}
