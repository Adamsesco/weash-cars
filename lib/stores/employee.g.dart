// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employee.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$Employee on _Employee, Store {
  final _$idAtom = Atom(name: '_Employee.id');

  @override
  String get id {
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.checkIfStateModificationsAreAllowed(_$idAtom);
    super.id = value;
    _$idAtom.reportChanged();
  }

  final _$avatarUrlAtom = Atom(name: '_Employee.avatarUrl');

  @override
  String get avatarUrl {
    _$avatarUrlAtom.reportObserved();
    return super.avatarUrl;
  }

  @override
  set avatarUrl(String value) {
    _$avatarUrlAtom.context
        .checkIfStateModificationsAreAllowed(_$avatarUrlAtom);
    super.avatarUrl = value;
    _$avatarUrlAtom.reportChanged();
  }

  final _$companyAtom = Atom(name: '_Employee.company');

  @override
  Company get company {
    _$companyAtom.reportObserved();
    return super.company;
  }

  @override
  set company(Company value) {
    _$companyAtom.context.checkIfStateModificationsAreAllowed(_$companyAtom);
    super.company = value;
    _$companyAtom.reportChanged();
  }

  final _$dateCreatedAtom = Atom(name: '_Employee.dateCreated');

  @override
  DateTime get dateCreated {
    _$dateCreatedAtom.reportObserved();
    return super.dateCreated;
  }

  @override
  set dateCreated(DateTime value) {
    _$dateCreatedAtom.context
        .checkIfStateModificationsAreAllowed(_$dateCreatedAtom);
    super.dateCreated = value;
    _$dateCreatedAtom.reportChanged();
  }

  final _$fcmTokenAtom = Atom(name: '_Employee.fcmToken');

  @override
  String get fcmToken {
    _$fcmTokenAtom.reportObserved();
    return super.fcmToken;
  }

  @override
  set fcmToken(String value) {
    _$fcmTokenAtom.context.checkIfStateModificationsAreAllowed(_$fcmTokenAtom);
    super.fcmToken = value;
    _$fcmTokenAtom.reportChanged();
  }

  final _$firstNameAtom = Atom(name: '_Employee.firstName');

  @override
  String get firstName {
    _$firstNameAtom.reportObserved();
    return super.firstName;
  }

  @override
  set firstName(String value) {
    _$firstNameAtom.context
        .checkIfStateModificationsAreAllowed(_$firstNameAtom);
    super.firstName = value;
    _$firstNameAtom.reportChanged();
  }

  final _$lastNameAtom = Atom(name: '_Employee.lastName');

  @override
  String get lastName {
    _$lastNameAtom.reportObserved();
    return super.lastName;
  }

  @override
  set lastName(String value) {
    _$lastNameAtom.context.checkIfStateModificationsAreAllowed(_$lastNameAtom);
    super.lastName = value;
    _$lastNameAtom.reportChanged();
  }

  final _$isAvailableAtom = Atom(name: '_Employee.isAvailable');

  @override
  bool get isAvailable {
    _$isAvailableAtom.reportObserved();
    return super.isAvailable;
  }

  @override
  set isAvailable(bool value) {
    _$isAvailableAtom.context
        .checkIfStateModificationsAreAllowed(_$isAvailableAtom);
    super.isAvailable = value;
    _$isAvailableAtom.reportChanged();
  }

  final _$jobsDoneCountAtom = Atom(name: '_Employee.jobsDoneCount');

  @override
  int get jobsDoneCount {
    _$jobsDoneCountAtom.reportObserved();
    return super.jobsDoneCount;
  }

  @override
  set jobsDoneCount(int value) {
    _$jobsDoneCountAtom.context
        .checkIfStateModificationsAreAllowed(_$jobsDoneCountAtom);
    super.jobsDoneCount = value;
    _$jobsDoneCountAtom.reportChanged();
  }

  final _$localeAtom = Atom(name: '_Employee.locale');

  @override
  String get locale {
    _$localeAtom.reportObserved();
    return super.locale;
  }

  @override
  set locale(String value) {
    _$localeAtom.context.checkIfStateModificationsAreAllowed(_$localeAtom);
    super.locale = value;
    _$localeAtom.reportChanged();
  }

  final _$phoneNumberAtom = Atom(name: '_Employee.phoneNumber');

  @override
  String get phoneNumber {
    _$phoneNumberAtom.reportObserved();
    return super.phoneNumber;
  }

  @override
  set phoneNumber(String value) {
    _$phoneNumberAtom.context
        .checkIfStateModificationsAreAllowed(_$phoneNumberAtom);
    super.phoneNumber = value;
    _$phoneNumberAtom.reportChanged();
  }

  final _$typeAtom = Atom(name: '_Employee.type');

  @override
  EmployeeType get type {
    _$typeAtom.reportObserved();
    return super.type;
  }

  @override
  set type(EmployeeType value) {
    _$typeAtom.context.checkIfStateModificationsAreAllowed(_$typeAtom);
    super.type = value;
    _$typeAtom.reportChanged();
  }
}
