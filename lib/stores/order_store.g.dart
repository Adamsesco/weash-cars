// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$OrderStore on _OrderStore, Store {
  final _$companyAtom = Atom(name: '_OrderStore.company');

  @override
  Map<String, dynamic> get company {
    _$companyAtom.reportObserved();
    return super.company;
  }

  @override
  set company(Map<String, dynamic> value) {
    _$companyAtom.context.checkIfStateModificationsAreAllowed(_$companyAtom);
    super.company = value;
    _$companyAtom.reportChanged();
  }

  final _$packageAtom = Atom(name: '_OrderStore.package');

  @override
  Map<String, dynamic> get package {
    _$packageAtom.reportObserved();
    return super.package;
  }

  @override
  set package(Map<String, dynamic> value) {
    _$packageAtom.context.checkIfStateModificationsAreAllowed(_$packageAtom);
    super.package = value;
    _$packageAtom.reportChanged();
  }

  final _$vehicleTypeAtom = Atom(name: '_OrderStore.vehicleType');

  @override
  VehicleType get vehicleType {
    _$vehicleTypeAtom.reportObserved();
    return super.vehicleType;
  }

  @override
  set vehicleType(VehicleType value) {
    _$vehicleTypeAtom.context
        .checkIfStateModificationsAreAllowed(_$vehicleTypeAtom);
    super.vehicleType = value;
    _$vehicleTypeAtom.reportChanged();
  }

  final _$addressAtom = Atom(name: '_OrderStore.address');

  @override
  String get address {
    _$addressAtom.reportObserved();
    return super.address;
  }

  @override
  set address(String value) {
    _$addressAtom.context.checkIfStateModificationsAreAllowed(_$addressAtom);
    super.address = value;
    _$addressAtom.reportChanged();
  }

  final _$locationAtom = Atom(name: '_OrderStore.location');

  @override
  GeoPoint get location {
    _$locationAtom.reportObserved();
    return super.location;
  }

  @override
  set location(GeoPoint value) {
    _$locationAtom.context.checkIfStateModificationsAreAllowed(_$locationAtom);
    super.location = value;
    _$locationAtom.reportChanged();
  }

  final _$countryAtom = Atom(name: '_OrderStore.country');

  @override
  String get country {
    _$countryAtom.reportObserved();
    return super.country;
  }

  @override
  set country(String value) {
    _$countryAtom.context.checkIfStateModificationsAreAllowed(_$countryAtom);
    super.country = value;
    _$countryAtom.reportChanged();
  }

  final _$localityAtom = Atom(name: '_OrderStore.locality');

  @override
  String get locality {
    _$localityAtom.reportObserved();
    return super.locality;
  }

  @override
  set locality(String value) {
    _$localityAtom.context.checkIfStateModificationsAreAllowed(_$localityAtom);
    super.locality = value;
    _$localityAtom.reportChanged();
  }

  final _$dateAtom = Atom(name: '_OrderStore.date');

  @override
  DateTime get date {
    _$dateAtom.reportObserved();
    return super.date;
  }

  @override
  set date(DateTime value) {
    _$dateAtom.context.checkIfStateModificationsAreAllowed(_$dateAtom);
    super.date = value;
    _$dateAtom.reportChanged();
  }

  final _$paymentMethodAtom = Atom(name: '_OrderStore.paymentMethod');

  @override
  PaymentMethod get paymentMethod {
    _$paymentMethodAtom.reportObserved();
    return super.paymentMethod;
  }

  @override
  set paymentMethod(PaymentMethod value) {
    _$paymentMethodAtom.context
        .checkIfStateModificationsAreAllowed(_$paymentMethodAtom);
    super.paymentMethod = value;
    _$paymentMethodAtom.reportChanged();
  }

  final _$_OrderStoreActionController = ActionController(name: '_OrderStore');

  @override
  void setOrder(
      {Map<String, dynamic> company,
      Map<String, dynamic> package,
      VehicleType vehicleType,
      String address,
      GeoPoint location,
      String country,
      String locality,
      DateTime date,
      PaymentMethod method}) {
    final _$actionInfo = _$_OrderStoreActionController.startAction();
    try {
      return super.setOrder(
          company: company,
          package: package,
          vehicleType: vehicleType,
          address: address,
          location: location,
          country: country,
          locality: locality,
          date: date,
          method: method);
    } finally {
      _$_OrderStoreActionController.endAction(_$actionInfo);
    }
  }
}
