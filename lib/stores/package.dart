import 'package:mobx/mobx.dart';
import 'package:weash_cars/models/vehicle_type.dart';

import 'company.dart';

part 'package.g.dart';

class Package = _Package with _$Package;

abstract class _Package with Store {
  @observable
  String id;

  @observable
  Company company;

  @observable
  String title;

  @observable
  double price;

  @observable
  Duration duration;

  @observable
  VehicleType vehicleType;

  @observable
  List<String> services;
}