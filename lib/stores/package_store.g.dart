// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'package_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$Package on _Package, Store {
  final _$idAtom = Atom(name: '_Package.id');

  @override
  String get id {
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.checkIfStateModificationsAreAllowed(_$idAtom);
    super.id = value;
    _$idAtom.reportChanged();
  }

  final _$companyAtom = Atom(name: '_Package.company');

  @override
  CompanyStore get company {
    _$companyAtom.reportObserved();
    return super.company;
  }

  @override
  set company(CompanyStore value) {
    _$companyAtom.context.checkIfStateModificationsAreAllowed(_$companyAtom);
    super.company = value;
    _$companyAtom.reportChanged();
  }

  final _$titleAtom = Atom(name: '_Package.title');

  @override
  String get title {
    _$titleAtom.reportObserved();
    return super.title;
  }

  @override
  set title(String value) {
    _$titleAtom.context.checkIfStateModificationsAreAllowed(_$titleAtom);
    super.title = value;
    _$titleAtom.reportChanged();
  }

  final _$priceAtom = Atom(name: '_Package.price');

  @override
  double get price {
    _$priceAtom.reportObserved();
    return super.price;
  }

  @override
  set price(double value) {
    _$priceAtom.context.checkIfStateModificationsAreAllowed(_$priceAtom);
    super.price = value;
    _$priceAtom.reportChanged();
  }

  final _$durationAtom = Atom(name: '_Package.duration');

  @override
  int get duration {
    _$durationAtom.reportObserved();
    return super.duration;
  }

  @override
  set duration(int value) {
    _$durationAtom.context.checkIfStateModificationsAreAllowed(_$durationAtom);
    super.duration = value;
    _$durationAtom.reportChanged();
  }

  final _$vehicleTypeAtom = Atom(name: '_Package.vehicleType');

  @override
  VehicleType get vehicleType {
    _$vehicleTypeAtom.reportObserved();
    return super.vehicleType;
  }

  @override
  set vehicleType(VehicleType value) {
    _$vehicleTypeAtom.context
        .checkIfStateModificationsAreAllowed(_$vehicleTypeAtom);
    super.vehicleType = value;
    _$vehicleTypeAtom.reportChanged();
  }

  final _$servicesAtom = Atom(name: '_Package.services');

  @override
  List<String> get services {
    _$servicesAtom.reportObserved();
    return super.services;
  }

  @override
  set services(List<String> value) {
    _$servicesAtom.context.checkIfStateModificationsAreAllowed(_$servicesAtom);
    super.services = value;
    _$servicesAtom.reportChanged();
  }

  final _$_PackageActionController = ActionController(name: '_Package');

  @override
  void setData(
      {String id,
      CompanyStore company,
      String title,
      double price,
      int duration,
      VehicleType vehicleType,
      List<String> services}) {
    final _$actionInfo = _$_PackageActionController.startAction();
    try {
      return super.setData(
          id: id,
          company: company,
          title: title,
          price: price,
          duration: duration,
          vehicleType: vehicleType,
          services: services);
    } finally {
      _$_PackageActionController.endAction(_$actionInfo);
    }
  }
}
