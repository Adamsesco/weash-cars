import 'package:mobx/mobx.dart';
import 'package:weash_cars/models/vehicle_type.dart';

import 'company_store.dart';

part 'package_store.g.dart';

class Package = _Package with _$Package;

abstract class _Package with Store {
  @observable
  String id;

  @observable
  CompanyStore company;

  @observable
  String title;

  @observable
  double price;

  @observable
  int duration;

  @observable
  VehicleType vehicleType;

  @observable
  List<String> services;

  @action
  void setData({
    String id,
    CompanyStore company,
    String title,
    double price,
    int duration,
    VehicleType vehicleType,
    List<String> services,
  }) {
    if (id != null) this.id = id;
    if (company != null) this.company = company;
    if (title != null) this.title = title;
    if (price != null) this.price = price;
    if (duration != null) this.duration = duration;
    if (vehicleType != null) this.vehicleType = vehicleType;
    if (services != null) this.services = services;
  }
}
