// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'customer_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$CustomerStore on _CustomerStore, Store {
  final _$customerIdAtom = Atom(name: '_CustomerStore.customerId');

  @override
  String get customerId {
    _$customerIdAtom.reportObserved();
    return super.customerId;
  }

  @override
  set customerId(String value) {
    _$customerIdAtom.context
        .checkIfStateModificationsAreAllowed(_$customerIdAtom);
    super.customerId = value;
    _$customerIdAtom.reportChanged();
  }

  final _$usernameAtom = Atom(name: '_CustomerStore.username');

  @override
  String get username {
    _$usernameAtom.reportObserved();
    return super.username;
  }

  @override
  set username(String value) {
    _$usernameAtom.context.checkIfStateModificationsAreAllowed(_$usernameAtom);
    super.username = value;
    _$usernameAtom.reportChanged();
  }

  final _$emailAtom = Atom(name: '_CustomerStore.email');

  @override
  String get email {
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.checkIfStateModificationsAreAllowed(_$emailAtom);
    super.email = value;
    _$emailAtom.reportChanged();
  }

  final _$phoneNumberAtom = Atom(name: '_CustomerStore.phoneNumber');

  @override
  String get phoneNumber {
    _$phoneNumberAtom.reportObserved();
    return super.phoneNumber;
  }

  @override
  set phoneNumber(String value) {
    _$phoneNumberAtom.context
        .checkIfStateModificationsAreAllowed(_$phoneNumberAtom);
    super.phoneNumber = value;
    _$phoneNumberAtom.reportChanged();
  }

  final _$fullNameAtom = Atom(name: '_CustomerStore.fullName');

  @override
  String get fullName {
    _$fullNameAtom.reportObserved();
    return super.fullName;
  }

  @override
  set fullName(String value) {
    _$fullNameAtom.context.checkIfStateModificationsAreAllowed(_$fullNameAtom);
    super.fullName = value;
    _$fullNameAtom.reportChanged();
  }

  final _$firstNameAtom = Atom(name: '_CustomerStore.firstName');

  @override
  String get firstName {
    _$firstNameAtom.reportObserved();
    return super.firstName;
  }

  @override
  set firstName(String value) {
    _$firstNameAtom.context
        .checkIfStateModificationsAreAllowed(_$firstNameAtom);
    super.firstName = value;
    _$firstNameAtom.reportChanged();
  }

  final _$lastNameAtom = Atom(name: '_CustomerStore.lastName');

  @override
  String get lastName {
    _$lastNameAtom.reportObserved();
    return super.lastName;
  }

  @override
  set lastName(String value) {
    _$lastNameAtom.context.checkIfStateModificationsAreAllowed(_$lastNameAtom);
    super.lastName = value;
    _$lastNameAtom.reportChanged();
  }

  final _$avatarUrlAtom = Atom(name: '_CustomerStore.avatarUrl');

  @override
  String get avatarUrl {
    _$avatarUrlAtom.reportObserved();
    return super.avatarUrl;
  }

  @override
  set avatarUrl(String value) {
    _$avatarUrlAtom.context
        .checkIfStateModificationsAreAllowed(_$avatarUrlAtom);
    super.avatarUrl = value;
    _$avatarUrlAtom.reportChanged();
  }

  final _$localeAtom = Atom(name: '_CustomerStore.locale');

  @override
  String get locale {
    _$localeAtom.reportObserved();
    return super.locale;
  }

  @override
  set locale(String value) {
    _$localeAtom.context.checkIfStateModificationsAreAllowed(_$localeAtom);
    super.locale = value;
    _$localeAtom.reportChanged();
  }

  final _$addressAtom = Atom(name: '_CustomerStore.address');

  @override
  String get address {
    _$addressAtom.reportObserved();
    return super.address;
  }

  @override
  set address(String value) {
    _$addressAtom.context.checkIfStateModificationsAreAllowed(_$addressAtom);
    super.address = value;
    _$addressAtom.reportChanged();
  }

  final _$locationAtom = Atom(name: '_CustomerStore.location');

  @override
  GeoPoint get location {
    _$locationAtom.reportObserved();
    return super.location;
  }

  @override
  set location(GeoPoint value) {
    _$locationAtom.context.checkIfStateModificationsAreAllowed(_$locationAtom);
    super.location = value;
    _$locationAtom.reportChanged();
  }

  final _$fcmTokenAtom = Atom(name: '_CustomerStore.fcmToken');

  @override
  String get fcmToken {
    _$fcmTokenAtom.reportObserved();
    return super.fcmToken;
  }

  @override
  set fcmToken(String value) {
    _$fcmTokenAtom.context.checkIfStateModificationsAreAllowed(_$fcmTokenAtom);
    super.fcmToken = value;
    _$fcmTokenAtom.reportChanged();
  }

  final _$_CustomerStoreActionController =
      ActionController(name: '_CustomerStore');

  @override
  void setData(
      {String customerId,
      String username,
      String email,
      String phoneNumber,
      String fullName,
      String firstName,
      String lastName,
      String avatarUrl,
      String locale,
      String address,
      GeoPoint location,
      String fcmToken}) {
    final _$actionInfo = _$_CustomerStoreActionController.startAction();
    try {
      return super.setData(
          customerId: customerId,
          username: username,
          email: email,
          phoneNumber: phoneNumber,
          fullName: fullName,
          firstName: firstName,
          lastName: lastName,
          avatarUrl: avatarUrl,
          locale: locale,
          address: address,
          location: location,
          fcmToken: fcmToken);
    } finally {
      _$_CustomerStoreActionController.endAction(_$actionInfo);
    }
  }
}
