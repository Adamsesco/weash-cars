import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:mobx/mobx.dart';

import 'order_store.dart';
import 'orders_store.dart';

part 'customer_store.g.dart';

class CustomerStore = _CustomerStore with _$CustomerStore;

abstract class _CustomerStore with Store {
  OrderStore newOrder = new OrderStore();
  OrdersStore orders = new OrdersStore();

  @observable
  String customerId;

  @observable
  String username;

  @observable
  String email;

  @observable
  String phoneNumber;

  @observable
  String fullName;

  @observable
  String firstName;

  @observable
  String lastName;

  @observable
  String avatarUrl;

  @observable
  String locale;

  @observable
  String address;

  @observable
  GeoPoint location;

  @observable
  String fcmToken;

  @action
  void setData(
      {String customerId,
      String username,
      String email,
      String phoneNumber,
      String fullName,
      String firstName,
      String lastName,
      String avatarUrl,
      String locale,
      String address,
      GeoPoint location,
      String fcmToken}) {
    if (customerId != null) this.customerId = customerId;
    if (username != null) this.username = username;
    if (email != null) this.email = email;
    if (phoneNumber != null) this.phoneNumber = phoneNumber;
    if (fullName != null) this.fullName = fullName;
    if (firstName != null) this.firstName = firstName;
    if (lastName != null) this.lastName = lastName;
    if (avatarUrl != null) this.avatarUrl = avatarUrl;
    if (locale != null) this.locale = locale;
    if (address != null) this.address = address;
    if (location != null) this.location = location;
    if (fcmToken != null) this.fcmToken = fcmToken;
  }

  Future<void> saveData() {
    final _geo = Geoflutterfire();
    GeoFirePoint myLocation;
    if (location != null) {
      myLocation = _geo.point(
          latitude: location?.latitude, longitude: location?.longitude);
    }

    return Firestore.instance
        .collection('users')
        .document(customerId)
        .updateData({
      'first_name': firstName,
      'last_name': lastName,
      'email': email,
      'phone_number': phoneNumber,
      'avatar_url': avatarUrl,
      'locale': locale,
      'address': address,
      'location': myLocation?.data,
      'fcm_token': fcmToken,
    });
  }
}
