// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'company_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$CompanyStore on _CompanyStore, Store {
  final _$companyIdAtom = Atom(name: '_CompanyStore.companyId');

  @override
  String get companyId {
    _$companyIdAtom.reportObserved();
    return super.companyId;
  }

  @override
  set companyId(String value) {
    _$companyIdAtom.context
        .checkIfStateModificationsAreAllowed(_$companyIdAtom);
    super.companyId = value;
    _$companyIdAtom.reportChanged();
  }

  final _$roleAtom = Atom(name: '_CompanyStore.role');

  @override
  UserRole get role {
    _$roleAtom.reportObserved();
    return super.role;
  }

  @override
  set role(UserRole value) {
    _$roleAtom.context.checkIfStateModificationsAreAllowed(_$roleAtom);
    super.role = value;
    _$roleAtom.reportChanged();
  }

  final _$emailAtom = Atom(name: '_CompanyStore.email');

  @override
  String get email {
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.checkIfStateModificationsAreAllowed(_$emailAtom);
    super.email = value;
    _$emailAtom.reportChanged();
  }

  final _$phoneNumberAtom = Atom(name: '_CompanyStore.phoneNumber');

  @override
  String get phoneNumber {
    _$phoneNumberAtom.reportObserved();
    return super.phoneNumber;
  }

  @override
  set phoneNumber(String value) {
    _$phoneNumberAtom.context
        .checkIfStateModificationsAreAllowed(_$phoneNumberAtom);
    super.phoneNumber = value;
    _$phoneNumberAtom.reportChanged();
  }

  final _$fcmTokenAtom = Atom(name: '_CompanyStore.fcmToken');

  @override
  String get fcmToken {
    _$fcmTokenAtom.reportObserved();
    return super.fcmToken;
  }

  @override
  set fcmToken(String value) {
    _$fcmTokenAtom.context.checkIfStateModificationsAreAllowed(_$fcmTokenAtom);
    super.fcmToken = value;
    _$fcmTokenAtom.reportChanged();
  }

  final _$companyNameAtom = Atom(name: '_CompanyStore.companyName');

  @override
  String get companyName {
    _$companyNameAtom.reportObserved();
    return super.companyName;
  }

  @override
  set companyName(String value) {
    _$companyNameAtom.context
        .checkIfStateModificationsAreAllowed(_$companyNameAtom);
    super.companyName = value;
    _$companyNameAtom.reportChanged();
  }

  final _$descriptionAtom = Atom(name: '_CompanyStore.description');

  @override
  String get description {
    _$descriptionAtom.reportObserved();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.context
        .checkIfStateModificationsAreAllowed(_$descriptionAtom);
    super.description = value;
    _$descriptionAtom.reportChanged();
  }

  final _$locationAtom = Atom(name: '_CompanyStore.location');

  @override
  GeoPoint get location {
    _$locationAtom.reportObserved();
    return super.location;
  }

  @override
  set location(GeoPoint value) {
    _$locationAtom.context.checkIfStateModificationsAreAllowed(_$locationAtom);
    super.location = value;
    _$locationAtom.reportChanged();
  }

  final _$addressAtom = Atom(name: '_CompanyStore.address');

  @override
  String get address {
    _$addressAtom.reportObserved();
    return super.address;
  }

  @override
  set address(String value) {
    _$addressAtom.context.checkIfStateModificationsAreAllowed(_$addressAtom);
    super.address = value;
    _$addressAtom.reportChanged();
  }

  final _$localityAtom = Atom(name: '_CompanyStore.locality');

  @override
  String get locality {
    _$localityAtom.reportObserved();
    return super.locality;
  }

  @override
  set locality(String value) {
    _$localityAtom.context.checkIfStateModificationsAreAllowed(_$localityAtom);
    super.locality = value;
    _$localityAtom.reportChanged();
  }

  final _$countryAtom = Atom(name: '_CompanyStore.country');

  @override
  String get country {
    _$countryAtom.reportObserved();
    return super.country;
  }

  @override
  set country(String value) {
    _$countryAtom.context.checkIfStateModificationsAreAllowed(_$countryAtom);
    super.country = value;
    _$countryAtom.reportChanged();
  }

  final _$avatarUrlAtom = Atom(name: '_CompanyStore.avatarUrl');

  @override
  String get avatarUrl {
    _$avatarUrlAtom.reportObserved();
    return super.avatarUrl;
  }

  @override
  set avatarUrl(String value) {
    _$avatarUrlAtom.context
        .checkIfStateModificationsAreAllowed(_$avatarUrlAtom);
    super.avatarUrl = value;
    _$avatarUrlAtom.reportChanged();
  }

  final _$coverPhotoUrlAtom = Atom(name: '_CompanyStore.coverPhotoUrl');

  @override
  String get coverPhotoUrl {
    _$coverPhotoUrlAtom.reportObserved();
    return super.coverPhotoUrl;
  }

  @override
  set coverPhotoUrl(String value) {
    _$coverPhotoUrlAtom.context
        .checkIfStateModificationsAreAllowed(_$coverPhotoUrlAtom);
    super.coverPhotoUrl = value;
    _$coverPhotoUrlAtom.reportChanged();
  }

  final _$currencyAtom = Atom(name: '_CompanyStore.currency');

  @override
  String get currency {
    _$currencyAtom.reportObserved();
    return super.currency;
  }

  @override
  set currency(String value) {
    _$currencyAtom.context.checkIfStateModificationsAreAllowed(_$currencyAtom);
    super.currency = value;
    _$currencyAtom.reportChanged();
  }

  final _$ratingAtom = Atom(name: '_CompanyStore.rating');

  @override
  double get rating {
    _$ratingAtom.reportObserved();
    return super.rating;
  }

  @override
  set rating(double value) {
    _$ratingAtom.context.checkIfStateModificationsAreAllowed(_$ratingAtom);
    super.rating = value;
    _$ratingAtom.reportChanged();
  }

  final _$ordersCountAtom = Atom(name: '_CompanyStore.ordersCount');

  @override
  int get ordersCount {
    _$ordersCountAtom.reportObserved();
    return super.ordersCount;
  }

  @override
  set ordersCount(int value) {
    _$ordersCountAtom.context
        .checkIfStateModificationsAreAllowed(_$ordersCountAtom);
    super.ordersCount = value;
    _$ordersCountAtom.reportChanged();
  }

  final _$packagesCountAtom = Atom(name: '_CompanyStore.packagesCount');

  @override
  int get packagesCount {
    _$packagesCountAtom.reportObserved();
    return super.packagesCount;
  }

  @override
  set packagesCount(int value) {
    _$packagesCountAtom.context
        .checkIfStateModificationsAreAllowed(_$packagesCountAtom);
    super.packagesCount = value;
    _$packagesCountAtom.reportChanged();
  }

  final _$vehicleTypesAtom = Atom(name: '_CompanyStore.vehicleTypes');

  @override
  List<VehicleType> get vehicleTypes {
    _$vehicleTypesAtom.reportObserved();
    return super.vehicleTypes;
  }

  @override
  set vehicleTypes(List<VehicleType> value) {
    _$vehicleTypesAtom.context
        .checkIfStateModificationsAreAllowed(_$vehicleTypesAtom);
    super.vehicleTypes = value;
    _$vehicleTypesAtom.reportChanged();
  }

  final _$galleryImagesUrlsAtom = Atom(name: '_CompanyStore.galleryImagesUrls');

  @override
  List<String> get galleryImagesUrls {
    _$galleryImagesUrlsAtom.reportObserved();
    return super.galleryImagesUrls;
  }

  @override
  set galleryImagesUrls(List<String> value) {
    _$galleryImagesUrlsAtom.context
        .checkIfStateModificationsAreAllowed(_$galleryImagesUrlsAtom);
    super.galleryImagesUrls = value;
    _$galleryImagesUrlsAtom.reportChanged();
  }

  final _$isAvailableAtom = Atom(name: '_CompanyStore.isAvailable');

  @override
  bool get isAvailable {
    _$isAvailableAtom.reportObserved();
    return super.isAvailable;
  }

  @override
  set isAvailable(bool value) {
    _$isAvailableAtom.context
        .checkIfStateModificationsAreAllowed(_$isAvailableAtom);
    super.isAvailable = value;
    _$isAvailableAtom.reportChanged();
  }

  final _$scheduleAtom = Atom(name: '_CompanyStore.schedule');

  @override
  Map<int, Map<String, dynamic>> get schedule {
    _$scheduleAtom.reportObserved();
    return super.schedule;
  }

  @override
  set schedule(Map<int, Map<String, dynamic>> value) {
    _$scheduleAtom.context.checkIfStateModificationsAreAllowed(_$scheduleAtom);
    super.schedule = value;
    _$scheduleAtom.reportChanged();
  }

  final _$daysUnavailableAtom = Atom(name: '_CompanyStore.daysUnavailable');

  @override
  List<DateTime> get daysUnavailable {
    _$daysUnavailableAtom.reportObserved();
    return super.daysUnavailable;
  }

  @override
  set daysUnavailable(List<DateTime> value) {
    _$daysUnavailableAtom.context
        .checkIfStateModificationsAreAllowed(_$daysUnavailableAtom);
    super.daysUnavailable = value;
    _$daysUnavailableAtom.reportChanged();
  }

  final _$dateCreatedAtom = Atom(name: '_CompanyStore.dateCreated');

  @override
  DateTime get dateCreated {
    _$dateCreatedAtom.reportObserved();
    return super.dateCreated;
  }

  @override
  set dateCreated(DateTime value) {
    _$dateCreatedAtom.context
        .checkIfStateModificationsAreAllowed(_$dateCreatedAtom);
    super.dateCreated = value;
    _$dateCreatedAtom.reportChanged();
  }

  final _$dateActivatedAtom = Atom(name: '_CompanyStore.dateActivated');

  @override
  DateTime get dateActivated {
    _$dateActivatedAtom.reportObserved();
    return super.dateActivated;
  }

  @override
  set dateActivated(DateTime value) {
    _$dateActivatedAtom.context
        .checkIfStateModificationsAreAllowed(_$dateActivatedAtom);
    super.dateActivated = value;
    _$dateActivatedAtom.reportChanged();
  }

  final _$employeesAtom = Atom(name: '_CompanyStore.employees');

  @override
  List<Map<String, dynamic>> get employees {
    _$employeesAtom.reportObserved();
    return super.employees;
  }

  @override
  set employees(List<Map<String, dynamic>> value) {
    _$employeesAtom.context
        .checkIfStateModificationsAreAllowed(_$employeesAtom);
    super.employees = value;
    _$employeesAtom.reportChanged();
  }

  final _$packagesAtom = Atom(name: '_CompanyStore.packages');

  @override
  List<Map<String, dynamic>> get packages {
    _$packagesAtom.reportObserved();
    return super.packages;
  }

  @override
  set packages(List<Map<String, dynamic>> value) {
    _$packagesAtom.context.checkIfStateModificationsAreAllowed(_$packagesAtom);
    super.packages = value;
    _$packagesAtom.reportChanged();
  }

  final _$jobsDoneCountAtom = Atom(name: '_CompanyStore.jobsDoneCount');

  @override
  int get jobsDoneCount {
    _$jobsDoneCountAtom.reportObserved();
    return super.jobsDoneCount;
  }

  @override
  set jobsDoneCount(int value) {
    _$jobsDoneCountAtom.context
        .checkIfStateModificationsAreAllowed(_$jobsDoneCountAtom);
    super.jobsDoneCount = value;
    _$jobsDoneCountAtom.reportChanged();
  }

  final _$isLoggedInAtom = Atom(name: '_CompanyStore.isLoggedIn');

  @override
  bool get isLoggedIn {
    _$isLoggedInAtom.reportObserved();
    return super.isLoggedIn;
  }

  @override
  set isLoggedIn(bool value) {
    _$isLoggedInAtom.context
        .checkIfStateModificationsAreAllowed(_$isLoggedInAtom);
    super.isLoggedIn = value;
    _$isLoggedInAtom.reportChanged();
  }

  final _$localeAtom = Atom(name: '_CompanyStore.locale');

  @override
  String get locale {
    _$localeAtom.reportObserved();
    return super.locale;
  }

  @override
  set locale(String value) {
    _$localeAtom.context.checkIfStateModificationsAreAllowed(_$localeAtom);
    super.locale = value;
    _$localeAtom.reportChanged();
  }

  final _$setDataAsyncAction = AsyncAction('setData');

  @override
  Future<dynamic> setData(
      {String companyId,
      UserRole role,
      String email,
      String phoneNumber,
      String fcmToken,
      String companyName,
      GeoPoint location,
      String descripton,
      String locality,
      String country,
      String address,
      String avatarUrl,
      String coverPhotoUrl,
      String currency,
      double rating,
      int ordersCount,
      int packagesCount,
      List<VehicleType> vehicleTypes,
      List<String> galleryImagesUrls,
      bool isAvailable,
      Map<int, Map<String, dynamic>> schedule,
      List<DateTime> daysUnavailable,
      DateTime dateCreated,
      DateTime dateActivated,
      List<Map<String, dynamic>> employees,
      List<Map<String, dynamic>> packages,
      int jobsDoneCount,
      String locale}) {
    return _$setDataAsyncAction.run(() => super.setData(
        companyId: companyId,
        role: role,
        email: email,
        phoneNumber: phoneNumber,
        fcmToken: fcmToken,
        companyName: companyName,
        location: location,
        descripton: descripton,
        locality: locality,
        country: country,
        address: address,
        avatarUrl: avatarUrl,
        coverPhotoUrl: coverPhotoUrl,
        currency: currency,
        rating: rating,
        ordersCount: ordersCount,
        packagesCount: packagesCount,
        vehicleTypes: vehicleTypes,
        galleryImagesUrls: galleryImagesUrls,
        isAvailable: isAvailable,
        schedule: schedule,
        daysUnavailable: daysUnavailable,
        dateCreated: dateCreated,
        dateActivated: dateActivated,
        employees: employees,
        packages: packages,
        jobsDoneCount: jobsDoneCount,
        locale: locale));
  }
}
