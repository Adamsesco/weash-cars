import 'package:mobx/mobx.dart';
import 'package:weash_cars/models/employee_type.dart';
import 'package:weash_cars/models/user_role.dart';

import 'company.dart';

part 'employee.g.dart';

class Employee = _Employee with _$Employee;

abstract class _Employee with Store {
  @observable
  String id;

  @observable
  String avatarUrl;

  @observable
  Company company;

  @observable
  DateTime dateCreated;

  @observable
  String fcmToken;

  @observable
  String firstName;

  @observable
  String lastName;

  @observable
  bool isAvailable;

  @observable
  int jobsDoneCount;

  @observable
  String locale;

  @observable
  String phoneNumber;

  @observable
  EmployeeType type;

  UserRole get role => UserRole.employee;
}
