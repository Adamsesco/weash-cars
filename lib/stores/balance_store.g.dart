// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'balance_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$BalanceStore on _BalanceStore, Store {
  final _$balanceAtom = Atom(name: '_BalanceStore.balance');

  @override
  double get balance {
    _$balanceAtom.reportObserved();
    return super.balance;
  }

  @override
  set balance(double value) {
    _$balanceAtom.context.checkIfStateModificationsAreAllowed(_$balanceAtom);
    super.balance = value;
    _$balanceAtom.reportChanged();
  }

  final _$isListeningAtom = Atom(name: '_BalanceStore.isListening');

  @override
  bool get isListening {
    _$isListeningAtom.reportObserved();
    return super.isListening;
  }

  @override
  set isListening(bool value) {
    _$isListeningAtom.context
        .checkIfStateModificationsAreAllowed(_$isListeningAtom);
    super.isListening = value;
    _$isListeningAtom.reportChanged();
  }

  final _$_BalanceStoreActionController =
      ActionController(name: '_BalanceStore');

  @override
  void setBalance(double balance) {
    final _$actionInfo = _$_BalanceStoreActionController.startAction();
    try {
      return super.setBalance(balance);
    } finally {
      _$_BalanceStoreActionController.endAction(_$actionInfo);
    }
  }
}
