// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'incomes_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$IncomesStore on _IncomesStore, Store {
  final _$incomesAtom = Atom(name: '_IncomesStore.incomes');

  @override
  Map<String, dynamic> get incomes {
    _$incomesAtom.reportObserved();
    return super.incomes;
  }

  @override
  set incomes(Map<String, dynamic> value) {
    _$incomesAtom.context.checkIfStateModificationsAreAllowed(_$incomesAtom);
    super.incomes = value;
    _$incomesAtom.reportChanged();
  }

  final _$isListeningAtom = Atom(name: '_IncomesStore.isListening');

  @override
  bool get isListening {
    _$isListeningAtom.reportObserved();
    return super.isListening;
  }

  @override
  set isListening(bool value) {
    _$isListeningAtom.context
        .checkIfStateModificationsAreAllowed(_$isListeningAtom);
    super.isListening = value;
    _$isListeningAtom.reportChanged();
  }

  final _$_IncomesStoreActionController =
      ActionController(name: '_IncomesStore');

  @override
  void setIncomes(Map<String, dynamic> incomes) {
    final _$actionInfo = _$_IncomesStoreActionController.startAction();
    try {
      return super.setIncomes(incomes);
    } finally {
      _$_IncomesStoreActionController.endAction(_$actionInfo);
    }
  }
}
