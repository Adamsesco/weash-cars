// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'company_transactions_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$CompanyTransactionsStore on _CompanyTransactionsStore, Store {
  Computed<double> _$balanceComputed;

  @override
  double get balance =>
      (_$balanceComputed ??= Computed<double>(() => super.balance)).value;

  final _$transactionsAtom =
      Atom(name: '_CompanyTransactionsStore.transactions');

  @override
  List<Map<String, dynamic>> get transactions {
    _$transactionsAtom.reportObserved();
    return super.transactions;
  }

  @override
  set transactions(List<Map<String, dynamic>> value) {
    _$transactionsAtom.context
        .checkIfStateModificationsAreAllowed(_$transactionsAtom);
    super.transactions = value;
    _$transactionsAtom.reportChanged();
  }

  final _$isListeningAtom = Atom(name: '_CompanyTransactionsStore.isListening');

  @override
  bool get isListening {
    _$isListeningAtom.reportObserved();
    return super.isListening;
  }

  @override
  set isListening(bool value) {
    _$isListeningAtom.context
        .checkIfStateModificationsAreAllowed(_$isListeningAtom);
    super.isListening = value;
    _$isListeningAtom.reportChanged();
  }

  final _$_CompanyTransactionsStoreActionController =
      ActionController(name: '_CompanyTransactionsStore');

  @override
  void subscribe(String userId, DateTime startDate, DateTime endDate) {
    final _$actionInfo =
        _$_CompanyTransactionsStoreActionController.startAction();
    try {
      return super.subscribe(userId, startDate, endDate);
    } finally {
      _$_CompanyTransactionsStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void _setTransactions(QuerySnapshot data) {
    final _$actionInfo =
        _$_CompanyTransactionsStoreActionController.startAction();
    try {
      return super._setTransactions(data);
    } finally {
      _$_CompanyTransactionsStoreActionController.endAction(_$actionInfo);
    }
  }
}
