import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';
import 'package:weash_cars/models/payment_method.dart';

import 'company.dart';
import 'package.dart';

part 'order.g.dart';

class Order = _Order with _$Order;

abstract class _Order with Store {
  @observable
  String id;

  // @observable
  // Client client;

  @observable
  Company company;

  @observable
  Package package;

  @observable
  String address;

  @observable
  GeoPoint location;

  @observable
  String locality;

  @observable
  DateTime date;

  @observable
  PaymentMethod paymentMethod;
}
