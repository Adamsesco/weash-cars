import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';
import 'package:weash_cars/models/user_role.dart';
import 'package:weash_cars/models/vehicle_type.dart';

import 'package:weash_cars/stores/company_transactions_store.dart';
import 'package:weash_cars/stores/orders_store.dart';
import 'package:weash_cars/stores/incomes_store.dart';
import 'package:weash_cars/stores/balance_store.dart';

part 'company_store.g.dart';

class CompanyStore = _CompanyStore with _$CompanyStore;

abstract class _CompanyStore with Store {
  final transactions = new CompanyTransactionsStore();
  final orders = new OrdersStore();
  final incomes = new IncomesStore();
  final balance = new BalanceStore();

  @observable
  String companyId;

  @observable
  UserRole role;

  @observable
  String email;

  @observable
  String phoneNumber;

  @observable
  String fcmToken;

  @observable
  String companyName;

  @observable
  String description;

  @observable
  GeoPoint location;

  @observable
  String address;

  @observable
  String locality;

  @observable
  String country;

  @observable
  String avatarUrl;

  @observable
  String coverPhotoUrl;

  @observable
  String currency;

  @observable
  double rating;

  @observable
  int ordersCount;

  @observable
  int packagesCount;

  @observable
  List<VehicleType> vehicleTypes;

  @observable
  List<String> galleryImagesUrls;

  @observable
  bool isAvailable;

  @observable
  Map<int, Map<String, dynamic>> schedule;

  @observable
  List<DateTime> daysUnavailable;

  @observable
  DateTime dateCreated;

  @observable
  DateTime dateActivated;

  @observable
  List<Map<String, dynamic>> employees;

  @observable
  List<Map<String, dynamic>> packages;

  @observable
  int jobsDoneCount;

  @observable
  bool isLoggedIn;

  @observable
  String locale;

  @action
  Future setData({
    String companyId,
    UserRole role,
    String email,
    String phoneNumber,
    String fcmToken,
    String companyName,
    GeoPoint location,
    String descripton,
    String locality,
    String country,
    String address,
    String avatarUrl,
    String coverPhotoUrl,
    String currency,
    double rating,
    int ordersCount,
    int packagesCount,
    List<VehicleType> vehicleTypes,
    List<String> galleryImagesUrls,
    bool isAvailable,
    Map<int, Map<String, dynamic>> schedule,
    List<DateTime> daysUnavailable,
    DateTime dateCreated,
    DateTime dateActivated,
    List<Map<String, dynamic>> employees,
    List<Map<String, dynamic>> packages,
    int jobsDoneCount,
    String locale,
  }) async {
    if (companyId != null) this.companyId = companyId;
    if (role != null) this.role = role;
    if (email != null) this.email = email;
    if (phoneNumber != null) this.phoneNumber = phoneNumber;
    if (fcmToken != null) this.fcmToken = fcmToken;
    if (companyName != null) this.companyName = companyName;
    if (descripton != null) this.description = descripton;
    if (location != null) this.location = location;
    if (address != null) this.address = address;
    if (locality != null) this.locality = locality;
    if (country != null) this.country = country;
    if (avatarUrl != null) this.avatarUrl = avatarUrl;
    if (coverPhotoUrl != null) this.coverPhotoUrl = coverPhotoUrl;
    if (currency != null) this.currency = currency;
    if (rating != null) this.rating = rating;
    if (ordersCount != null) this.ordersCount = ordersCount;
    if (vehicleTypes != null) this.vehicleTypes = vehicleTypes;
    if (galleryImagesUrls != null) this.galleryImagesUrls = galleryImagesUrls;
    if (packagesCount != null) this.packagesCount = packagesCount;
    if (isAvailable != null) this.isAvailable = isAvailable;
    if (schedule != null) this.schedule = schedule;
    if (daysUnavailable != null) this.daysUnavailable = daysUnavailable;
    if (dateCreated != null) this.dateCreated = dateCreated;
    if (dateActivated != null) this.dateActivated = dateActivated;
    if (employees != null) this.employees = employees;
    if (packages != null) this.packages = packages;
    if (jobsDoneCount != null) this.jobsDoneCount = jobsDoneCount;
    if (locale != null) this.locale = locale;
  }

  Future<void> saveData() async {
    await Firestore.instance
        .collection('users')
        .document(companyId)
        .updateData({
      'email': email,
      'fcm_token': fcmToken,
      'company_name': companyName,
      'description': description,
      'address': address,
      'avatar_url': avatarUrl,
      'gallery_images_urls': galleryImagesUrls,
      'cover_photo_url': coverPhotoUrl,
      'is_available': isAvailable,
      'days_unavailable': daysUnavailable,
    });
  }
}
