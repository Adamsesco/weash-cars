// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'packages_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$PackagesStore on _PackagesStore, Store {
  final _$packagesAtom = Atom(name: '_PackagesStore.packages');

  @override
  List<Map<String, dynamic>> get packages {
    _$packagesAtom.reportObserved();
    return super.packages;
  }

  @override
  set packages(List<Map<String, dynamic>> value) {
    _$packagesAtom.context.checkIfStateModificationsAreAllowed(_$packagesAtom);
    super.packages = value;
    _$packagesAtom.reportChanged();
  }

  final _$isListeningAtom = Atom(name: '_PackagesStore.isListening');

  @override
  bool get isListening {
    _$isListeningAtom.reportObserved();
    return super.isListening;
  }

  @override
  set isListening(bool value) {
    _$isListeningAtom.context
        .checkIfStateModificationsAreAllowed(_$isListeningAtom);
    super.isListening = value;
    _$isListeningAtom.reportChanged();
  }

  final _$_PackagesStoreActionController =
      ActionController(name: '_PackagesStore');

  @override
  void setPackages(List<Map<String, dynamic>> packages) {
    final _$actionInfo = _$_PackagesStoreActionController.startAction();
    try {
      return super.setPackages(packages);
    } finally {
      _$_PackagesStoreActionController.endAction(_$actionInfo);
    }
  }
}
