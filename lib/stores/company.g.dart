// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'company.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$Company on _Company, Store {
  final _$idAtom = Atom(name: '_Company.id');

  @override
  String get id {
    _$idAtom.reportObserved();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.context.checkIfStateModificationsAreAllowed(_$idAtom);
    super.id = value;
    _$idAtom.reportChanged();
  }

  final _$nameAtom = Atom(name: '_Company.name');

  @override
  String get name {
    _$nameAtom.reportObserved();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.context.checkIfStateModificationsAreAllowed(_$nameAtom);
    super.name = value;
    _$nameAtom.reportChanged();
  }

  final _$addressAtom = Atom(name: '_Company.address');

  @override
  String get address {
    _$addressAtom.reportObserved();
    return super.address;
  }

  @override
  set address(String value) {
    _$addressAtom.context.checkIfStateModificationsAreAllowed(_$addressAtom);
    super.address = value;
    _$addressAtom.reportChanged();
  }

  final _$avatarUrlAtom = Atom(name: '_Company.avatarUrl');

  @override
  String get avatarUrl {
    _$avatarUrlAtom.reportObserved();
    return super.avatarUrl;
  }

  @override
  set avatarUrl(String value) {
    _$avatarUrlAtom.context
        .checkIfStateModificationsAreAllowed(_$avatarUrlAtom);
    super.avatarUrl = value;
    _$avatarUrlAtom.reportChanged();
  }

  final _$currencyAtom = Atom(name: '_Company.currency');

  @override
  String get currency {
    _$currencyAtom.reportObserved();
    return super.currency;
  }

  @override
  set currency(String value) {
    _$currencyAtom.context.checkIfStateModificationsAreAllowed(_$currencyAtom);
    super.currency = value;
    _$currencyAtom.reportChanged();
  }

  final _$dateActivatedAtom = Atom(name: '_Company.dateActivated');

  @override
  DateTime get dateActivated {
    _$dateActivatedAtom.reportObserved();
    return super.dateActivated;
  }

  @override
  set dateActivated(DateTime value) {
    _$dateActivatedAtom.context
        .checkIfStateModificationsAreAllowed(_$dateActivatedAtom);
    super.dateActivated = value;
    _$dateActivatedAtom.reportChanged();
  }

  final _$dateCreatedAtom = Atom(name: '_Company.dateCreated');

  @override
  DateTime get dateCreated {
    _$dateCreatedAtom.reportObserved();
    return super.dateCreated;
  }

  @override
  set dateCreated(DateTime value) {
    _$dateCreatedAtom.context
        .checkIfStateModificationsAreAllowed(_$dateCreatedAtom);
    super.dateCreated = value;
    _$dateCreatedAtom.reportChanged();
  }

  final _$emailAtom = Atom(name: '_Company.email');

  @override
  String get email {
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.checkIfStateModificationsAreAllowed(_$emailAtom);
    super.email = value;
    _$emailAtom.reportChanged();
  }

  final _$fcmTokenAtom = Atom(name: '_Company.fcmToken');

  @override
  String get fcmToken {
    _$fcmTokenAtom.reportObserved();
    return super.fcmToken;
  }

  @override
  set fcmToken(String value) {
    _$fcmTokenAtom.context.checkIfStateModificationsAreAllowed(_$fcmTokenAtom);
    super.fcmToken = value;
    _$fcmTokenAtom.reportChanged();
  }

  final _$isActivatedAtom = Atom(name: '_Company.isActivated');

  @override
  bool get isActivated {
    _$isActivatedAtom.reportObserved();
    return super.isActivated;
  }

  @override
  set isActivated(bool value) {
    _$isActivatedAtom.context
        .checkIfStateModificationsAreAllowed(_$isActivatedAtom);
    super.isActivated = value;
    _$isActivatedAtom.reportChanged();
  }

  final _$isAvailableAtom = Atom(name: '_Company.isAvailable');

  @override
  bool get isAvailable {
    _$isAvailableAtom.reportObserved();
    return super.isAvailable;
  }

  @override
  set isAvailable(bool value) {
    _$isAvailableAtom.context
        .checkIfStateModificationsAreAllowed(_$isAvailableAtom);
    super.isAvailable = value;
    _$isAvailableAtom.reportChanged();
  }

  final _$jobsDoneCountAtom = Atom(name: '_Company.jobsDoneCount');

  @override
  int get jobsDoneCount {
    _$jobsDoneCountAtom.reportObserved();
    return super.jobsDoneCount;
  }

  @override
  set jobsDoneCount(int value) {
    _$jobsDoneCountAtom.context
        .checkIfStateModificationsAreAllowed(_$jobsDoneCountAtom);
    super.jobsDoneCount = value;
    _$jobsDoneCountAtom.reportChanged();
  }

  final _$localeAtom = Atom(name: '_Company.locale');

  @override
  String get locale {
    _$localeAtom.reportObserved();
    return super.locale;
  }

  @override
  set locale(String value) {
    _$localeAtom.context.checkIfStateModificationsAreAllowed(_$localeAtom);
    super.locale = value;
    _$localeAtom.reportChanged();
  }

  final _$localityAtom = Atom(name: '_Company.locality');

  @override
  String get locality {
    _$localityAtom.reportObserved();
    return super.locality;
  }

  @override
  set locality(String value) {
    _$localityAtom.context.checkIfStateModificationsAreAllowed(_$localityAtom);
    super.locality = value;
    _$localityAtom.reportChanged();
  }

  final _$locationAtom = Atom(name: '_Company.location');

  @override
  GeoPoint get location {
    _$locationAtom.reportObserved();
    return super.location;
  }

  @override
  set location(GeoPoint value) {
    _$locationAtom.context.checkIfStateModificationsAreAllowed(_$locationAtom);
    super.location = value;
    _$locationAtom.reportChanged();
  }

  final _$phoneNumberAtom = Atom(name: '_Company.phoneNumber');

  @override
  String get phoneNumber {
    _$phoneNumberAtom.reportObserved();
    return super.phoneNumber;
  }

  @override
  set phoneNumber(String value) {
    _$phoneNumberAtom.context
        .checkIfStateModificationsAreAllowed(_$phoneNumberAtom);
    super.phoneNumber = value;
    _$phoneNumberAtom.reportChanged();
  }

  final _$ratingAtom = Atom(name: '_Company.rating');

  @override
  double get rating {
    _$ratingAtom.reportObserved();
    return super.rating;
  }

  @override
  set rating(double value) {
    _$ratingAtom.context.checkIfStateModificationsAreAllowed(_$ratingAtom);
    super.rating = value;
    _$ratingAtom.reportChanged();
  }

  final _$ordersTotalAtom = Atom(name: '_Company.ordersTotal');

  @override
  int get ordersTotal {
    _$ordersTotalAtom.reportObserved();
    return super.ordersTotal;
  }

  @override
  set ordersTotal(int value) {
    _$ordersTotalAtom.context
        .checkIfStateModificationsAreAllowed(_$ordersTotalAtom);
    super.ordersTotal = value;
    _$ordersTotalAtom.reportChanged();
  }

  final _$reviewsTotalAtom = Atom(name: '_Company.reviewsTotal');

  @override
  int get reviewsTotal {
    _$reviewsTotalAtom.reportObserved();
    return super.reviewsTotal;
  }

  @override
  set reviewsTotal(int value) {
    _$reviewsTotalAtom.context
        .checkIfStateModificationsAreAllowed(_$reviewsTotalAtom);
    super.reviewsTotal = value;
    _$reviewsTotalAtom.reportChanged();
  }

  final _$vehicleTypesAtom = Atom(name: '_Company.vehicleTypes');

  @override
  List<VehicleType> get vehicleTypes {
    _$vehicleTypesAtom.reportObserved();
    return super.vehicleTypes;
  }

  @override
  set vehicleTypes(List<VehicleType> value) {
    _$vehicleTypesAtom.context
        .checkIfStateModificationsAreAllowed(_$vehicleTypesAtom);
    super.vehicleTypes = value;
    _$vehicleTypesAtom.reportChanged();
  }

  final _$packagesAtom = Atom(name: '_Company.packages');

  @override
  List<Package> get packages {
    _$packagesAtom.reportObserved();
    return super.packages;
  }

  @override
  set packages(List<Package> value) {
    _$packagesAtom.context.checkIfStateModificationsAreAllowed(_$packagesAtom);
    super.packages = value;
    _$packagesAtom.reportChanged();
  }

  final _$employeesAtom = Atom(name: '_Company.employees');

  @override
  List<Employee> get employees {
    _$employeesAtom.reportObserved();
    return super.employees;
  }

  @override
  set employees(List<Employee> value) {
    _$employeesAtom.context
        .checkIfStateModificationsAreAllowed(_$employeesAtom);
    super.employees = value;
    _$employeesAtom.reportChanged();
  }

  final _$ordersAtom = Atom(name: '_Company.orders');

  @override
  List<Order> get orders {
    _$ordersAtom.reportObserved();
    return super.orders;
  }

  @override
  set orders(List<Order> value) {
    _$ordersAtom.context.checkIfStateModificationsAreAllowed(_$ordersAtom);
    super.orders = value;
    _$ordersAtom.reportChanged();
  }

  final _$scheduleAtom = Atom(name: '_Company.schedule');

  @override
  Schedule get schedule {
    _$scheduleAtom.reportObserved();
    return super.schedule;
  }

  @override
  set schedule(Schedule value) {
    _$scheduleAtom.context.checkIfStateModificationsAreAllowed(_$scheduleAtom);
    super.schedule = value;
    _$scheduleAtom.reportChanged();
  }

  final _$isLoggedInAtom = Atom(name: '_Company.isLoggedIn');

  @override
  bool get isLoggedIn {
    _$isLoggedInAtom.reportObserved();
    return super.isLoggedIn;
  }

  @override
  set isLoggedIn(bool value) {
    _$isLoggedInAtom.context
        .checkIfStateModificationsAreAllowed(_$isLoggedInAtom);
    super.isLoggedIn = value;
    _$isLoggedInAtom.reportChanged();
  }
}
