import 'package:flutter/rendering.dart';

class DistDuration {
  final double distance;
  final int duration;
  final TextDirection textDirection;

  DistDuration(this.distance, this.duration, this.textDirection);

  @override
  String toString() {
    String distanceStr;
    String durationStr;

    final kilometers = distance ~/ 1000;
    if (kilometers == 0) {
      distanceStr =
          textDirection == TextDirection.ltr ? "$distance m" : "$distance م";
    } else {
      distanceStr = textDirection == TextDirection.ltr
          ? "$kilometers Km"
          : "$kilometers كم";
    }

    final hours = duration ~/ 3600;
    final minutes = (duration % 3600) ~/ 60;
    if (hours > 0) {
      durationStr =
          textDirection == TextDirection.ltr ? "$hours h" : "$hours س";
    } else {
      durationStr = textDirection == TextDirection.ltr
          ? "$hours h $minutes min"
          : "$hours س $minutes د";
    }

    return "$durationStr - $distanceStr";
  }
}
