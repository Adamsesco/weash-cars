class NotificationMessage {
  final String id;
  final String type;
  final String targetId;

  NotificationMessage({this.id, this.type, this.targetId});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'type': type,
      'targetId': targetId,
    };
  }
}
