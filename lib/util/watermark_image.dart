import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class WatermarkImage {
  final ui.Image image;
  final Widget waterMark;
  final Offset waterMarkOffset;

  WatermarkImage({this.image, this.waterMark, this.waterMarkOffset});

  Future<ui.Image> get rendered async {
    final logoImageData = await rootBundle.load('assets/images/logo.png');
    final completer = new Completer<ui.Image>();
    ui.decodeImageFromList(logoImageData.buffer.asUint8List(),
        (ui.Image image) {
      completer.complete(image);
    });
    final logoImage = await completer.future;

    final span = new TextSpan(
      text: 'Washy',
      style: TextStyle(
        fontFamily: 'SF UI Display',
        fontSize: 28,
        fontWeight: FontWeight.w900,
        color: Color.fromARGB(120, 255, 255, 255),
      ),
    );
    final textPainter = new TextPainter(
      text: span,
      textAlign: TextAlign.left,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout();

    final recorder = ui.PictureRecorder();
    final canvas = new Canvas(recorder);
    final imagePaint = new Paint();
    final waterMarkPaint = new Paint()
      ..color = Color.fromARGB(120, 255, 255, 255);

    canvas.drawImage(image, Offset(0, 0), imagePaint);
    textPainter.paint(
      canvas,
      Offset(
          (image.width - logoImage.width - 20).toDouble() + (logoImage.width - textPainter.width) / 2,
          (image.height - logoImage.height - 20 - textPainter.height - 8)
              .toDouble()),
    );
    canvas.drawImage(
        logoImage,
        Offset((image.width - logoImage.width - 20).toDouble(), (image.height - logoImage.height - 20).toDouble()),
        waterMarkPaint);

    final result = recorder.endRecording().toImage(image.width, image.height);

    return result;
  }

  Future<Uint8List> get byteData async {
    final image = await rendered;
    final bytes = await image.toByteData(format: ui.ImageByteFormat.png);
    return bytes.buffer.asUint8List();
  }
}
