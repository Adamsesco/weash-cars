import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:fluro_fork/fluro_fork.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import 'package:uni_links/uni_links.dart';
import 'package:weash_cars/models/notification_message.dart';
import 'package:weash_cars/screens/company_dashboard_screen/company_dashboard_screen.dart';
import 'package:weash_cars/screens/customer_dashboard_screen/customer_dashboard_screen.dart';
import 'package:weash_cars/screens/employee_dashboard_screen/employee_dashboard_screen.dart';
import 'package:weash_cars/screens/home_screen/home_screen.dart';
import 'package:weash_cars/screens/language_screen/language_screen.dart';

import 'generated/i18n.dart';
import 'models/user_role.dart';
import 'routes.dart';
import 'stores/application_store.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(WeashCarsApp());
}

class WeashCarsApp extends StatefulWidget {
  @override
  _WeashCarsAppState createState() => _WeashCarsAppState();
}

class _WeashCarsAppState extends State<WeashCarsApp> {
  final Router _router = Routes.getRouter();
  Future<ApplicationStore> _applicationStore;
  Map<String, Locale> _locales = {
    'ar': Locale('ar'),
    'en': Locale('en'),
  };
  StreamSubscription _linksSubscription;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  EventChannel notificationsChannel;

  @override
  void initState() {
    super.initState();
    _initUniLinks();
    _applicationStore = _initApp();
  }

  @override
  void dispose() {
    if (_linksSubscription != null) {
      _linksSubscription.cancel();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _applicationStore,
      builder:
          (BuildContext context, AsyncSnapshot<ApplicationStore> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
          case ConnectionState.active:
          case ConnectionState.none:
            return MaterialApp(
              title: 'Washy',
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                primarySwatch: Colors.blue,
              ),
              home: Builder(
                builder: (BuildContext context) => Scaffold(
                  body: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                            'assets/images/splash_sceen_background.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                    child: Image(
                      image: AssetImage('assets/images/logo.png'),
                    ),
                  ),
                ),
              ),
            );
          case ConnectionState.done:
            final application = snapshot.data;

            if (Platform.isIOS && notificationsChannel == null) {
              notificationsChannel =
                  EventChannel("washy.weashcars.com/notifications");
              notificationsChannel
                  .receiveBroadcastStream()
                  .listen((notification) {
                print(notification);
                final notif = <String, dynamic>{};
                notif['data'] = <String, String>{
                  'type': notification['type'] as String,
                  'targetId': notification['targetId'] as String,
                };
                _onLaunch(notif, application);
              });
            }

            if (application.locale == null) {
              application.setLocale('en');
            }

            // application.setLocale('ar');
            Widget firstScreen;
            if (application.isSignedIn) {
              switch (application.userRole) {
                case UserRole.company:
                case UserRole.selfEmployed:
                  firstScreen = CompanyDashboardScreen();
                  break;
                case UserRole.employee:
                  firstScreen = EmployeeDashboardScreen();
                  break;
                case UserRole.customer:
                  firstScreen = CustomerDashboardScreen();
                  break;
              }
            } else if (!application.isFirstRun) {
              firstScreen = HomeScreen();
            } else {
              firstScreen = LanguageScreen();
            }

            return Provider<ApplicationStore>(
              value: application,
              child: StreamBuilder<String>(
                stream: application.localeStream.stream,
                initialData: application.locale,
                builder: (BuildContext context, AsyncSnapshot<String> locale) {
                  _storeLocale(locale.data);
                  application.setLocale(locale.data);

                  return MaterialApp(
                    title: 'Washy',
                    debugShowCheckedModeBanner: false,
                    theme: ThemeData(
                      primarySwatch: Colors.blue,
                      fontFamily: 'SF Pro Display',
                    ),
                    onGenerateRoute: _router.generator,
                    localizationsDelegates: [
                      S.delegate,
                      GlobalMaterialLocalizations.delegate,
                      GlobalCupertinoLocalizations.delegate,
                      GlobalWidgetsLocalizations.delegate,
                    ],
                    supportedLocales: S.delegate.supportedLocales,
                    locale: _locales[locale.data],
                    home: firstScreen,
                  );
                },
              ),
            );
        }

        return Container();
      },
    );
  }

  Future<ApplicationStore> _initApp() async {
    final application = new ApplicationStore();
    application.setFirstRun(await _isFirstRun());

    print('Init app from flutter');

    final prefs = await SharedPreferences.getInstance();

    final locale = prefs.getString('locale');
    application.locale = locale ?? 'en';

    var notificationsCount = prefs.getInt('notificationsCount');
    if (notificationsCount == null) {
      notificationsCount = 0;
      prefs.setInt('notificationsCount', 0);
    }
    application.notificationsCount = notificationsCount;

    application.database = await openDatabase(
      join(await getDatabasesPath(), 'database.db'),
      onCreate: (db, version) {
        // Run the CREATE TABLE statement on the database.
        return db.execute(
          "CREATE TABLE notifications(id TEXT PRIMARY KEY, type TEXT, targetId TEXT)",
        );
      },
      // Set the version. This executes the onCreate function and provides a
      // path to perform database upgrades and downgrades.
      version: 1,
    );

    if (Platform.isIOS) _iOSPermission();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) =>
          _onMessage(message, application),
      onResume: (Map<String, dynamic> message) =>
          _onResume(message, application),
      onLaunch: (Map<String, dynamic> message) =>
          _onLaunch(message, application),
    );

    final userId = prefs.getString('user_id');
    if (userId == null) {
      return application;
    }

    final ds =
        await Firestore.instance.collection('users').document(userId).get();
    if (ds == null || !ds.exists) {
      return application;
    }

    final userData = ds.data;
    final userRole = userData['role'];
    UserRole role;

    switch (userRole) {
      case 'customer':
        role = UserRole.customer;
        break;

      case 'employee':
        role = UserRole.employee;
        break;

      case 'company':
        role = UserRole.company;
        break;

      case 'self_employed':
        role = UserRole.selfEmployed;
        break;
    }

    if (userData['locale'] != null &&
        ['ar', 'en'].contains(userData['locale'])) {
      application.localeStream.add(userData['locale']);
    }

    if (role == UserRole.company || role == UserRole.selfEmployed) {
      if (userData['is_activated'] == false) {
        return application;
      } else {
        final employeesDocuments = await Firestore.instance
            .collection('users')
            .where('role', isEqualTo: 'employee')
            .where('company_id', isEqualTo: ds.documentID)
            .getDocuments();
        final employees = new List<Map<String, dynamic>>();

        if (role == UserRole.company) {
          await Future.forEach(employeesDocuments.documents,
              (DocumentSnapshot employeeDocument) {
            final employee = <String, dynamic>{
              'employee_id': employeeDocument.documentID,
              'company_id': employeeDocument.data['company_id'],
              'first_name': employeeDocument.data['first_name'],
              'last_name': employeeDocument.data['last_name'],
              'phone_number': employeeDocument.data['phone_number'],
              'avatar_url': employeeDocument.data['avatar_url'],
              'jobs_done': employeeDocument.data['jobs_done'] as int,
              'type': employeeDocument.data['type'] ?? 'smart',
              'created_at':
                  (employeeDocument.data['created_at'] as Timestamp).toDate(),
              'is_available': employeeDocument.data['is_available'] ?? true,
            };

            employees.add(employee);
          });
        }

        final schedule = (userData['schedule'] as Map<dynamic, dynamic>)
            .map((day, daySchedule) {
          final dayNumber = int.tryParse(day as String);
          final timeFrom = daySchedule['from'] as int;
          final timeTo = daySchedule['to'] as int;
          final isDayOff = daySchedule['is_day_off'] as bool;

          return MapEntry(dayNumber, {
            'from': timeFrom,
            'to': timeTo,
            'is_day_off': isDayOff,
          });
        });

        application.setUserRole(role);
        application.companyStore.setData(
          companyId: ds.documentID,
          role: role,
          email: userData['email'],
          phoneNumber: userData['phone_number'],
          fcmToken: userData['fcm_token'],
          companyName: userData['company_name'],
          descripton: userData['description'],
          location: userData['location'] == null
              ? null
              : userData['location']['geopoint'] as GeoPoint,
          address: userData['address'],
          locality: userData['locality'],
          country: userData['country'],
          avatarUrl: userData['avatar_url'],
          rating: userData['rating'].toDouble() as double,
          ordersCount: userData['total_orders'] as int,
          packagesCount: userData['total_packages'] as int,
          isAvailable: userData['is_available'] as bool,
          schedule: schedule,
          daysUnavailable: userData['days_unavailable'] == null
              ? new List<DateTime>()
              : new List<DateTime>.from(userData['days_unavailable']
                  .map((date) => (date as Timestamp).toDate())),
          dateCreated: (userData['date_created'] as Timestamp).toDate(),
          dateActivated: userData['date_activated'] == null
              ? null
              : (userData['date_activated'] as Timestamp).toDate(),
          employees: employees,
          jobsDoneCount: userData['jobs_done'] as int,
          locale: userData['locale'],
          currency: userData['currency'],
        );
        application.setIsSignedIn(true);
      }
    } else if (role == UserRole.customer) {
      application.setUserRole(role);
      application.customerStore.setData(
        customerId: ds.documentID,
        phoneNumber: userData['phone_number'],
        fullName: userData['full_name'],
        firstName: userData['first_name'],
        lastName: userData['last_name'],
        email: userData['email'],
        avatarUrl: userData['avatar_url'],
        locale: userData['locale'],
        location: userData['location'] == null
            ? null
            : userData['location']['geopoint'] as GeoPoint,
        address: userData['address'],
        fcmToken: userData['fcm_token'],
      );
      application.setIsSignedIn(true);
    } else if (role == UserRole.employee) {
      application.setUserRole(role);
      application.employeeStore.setData(
        employeeId: ds.documentID,
        companyId: userData['company_id'],
        phoneNumber: userData['phone_number'],
        firstName: userData['first_name'],
        lastName: userData['last_name'],
        avatarUrl: userData['avatar_url'],
        jobsDoneCount: userData['jobs_done'] as int,
        locale: userData['locale'],
        fcmToken: userData['fcm_token'],
      );
      application.setIsSignedIn(true);
    }

    return application;
  }

  Future<void> _onMessage(
      Map<String, dynamic> message, ApplicationStore application) async {
    print('on message $message');
    if (message != null && message['data'] != null) {
      await _storeMessage(application, message);
    }
  }

  Future<void> _onResume(
      Map<String, dynamic> message, ApplicationStore application) async {
    print('on resume $message');
    if (message != null && message['data'] != null) {
      application.isPendingNotification = true;
      await _storeMessage(application, message);
    }
  }

  Future<void> _onLaunch(
      Map<String, dynamic> message, ApplicationStore application) async {
    print('on launch $message');
    if (message != null && message['data'] != null) {
      application.isPendingNotification = true;
      await _storeMessage(application, message);
    }
  }

  void _iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future<void> _storeMessage(
      ApplicationStore application, Map<String, dynamic> message) async {
    final database = application.database;
    final data = message['data'];
    final id = data['type'] + data['targetId'];

    final notificationMessage = new NotificationMessage(
      id: id,
      type: data['type'],
      targetId: data['targetId'],
    );

    await database.insert('notifications', notificationMessage.toMap());
    application.addNotification();

    print("Received notification: ${data['targetId']}");
    application.notificationsStream.add(id);
  }

  Future<void> _initUniLinks() async {
    try {
      final initialUri = await getInitialUri();
      if (initialUri != null) {
        print(initialUri);
      }
    } on FormatException {
      print('Incorrect URI');
    }

    _linksSubscription = getUriLinksStream().listen((Uri uri) {
      print(uri);
    }, onError: (err) {
      print("Incorrect URI: $err");
    });
  }

  Future<bool> _isFirstRun() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int runsCount = prefs.getInt('runsCount') ?? 0;
    prefs.setInt('runsCount', runsCount + 1);
    return runsCount == 0;
  }

  Future<void> _storeLocale(String locale) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('locale', locale);
  }
}
