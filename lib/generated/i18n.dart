// DO NOT EDIT. This is code generated via package:gen_lang/generate.dart

import 'dart:async';

import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

import 'messages_all.dart';

class S {
 
  static const GeneratedLocalizationsDelegate delegate = GeneratedLocalizationsDelegate();

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }
  
  static Future<S> load(Locale locale) {
    final String name = locale.countryCode == null ? locale.languageCode : locale.toString();

    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return new S();
    });
  }
  
  String get locale {
    return Intl.message("English", name: 'locale');
  }

  String get quickOrder {
    return Intl.message("QUICK ORDER", name: 'quickOrder');
  }

  String get alreadyHaveAccount {
    return Intl.message("Already have an account ?", name: 'alreadyHaveAccount');
  }

  String get login {
    return Intl.message("Login", name: 'login');
  }

  String get selectRegion {
    return Intl.message("Select your region...", name: 'selectRegion');
  }

  String get confirmLocation {
    return Intl.message("CONFIRM LOCATION", name: 'confirmLocation');
  }

  String get chooseVehicleType {
    return Intl.message("CHOOSE YOUR VEHICLE", name: 'chooseVehicleType');
  }

  String get pickDateAndTime {
    return Intl.message("PICK A DATE & TIME", name: 'pickDateAndTime');
  }

  String get flexibleTiming {
    return Intl.message("FLEXIBLE TIMING", name: 'flexibleTiming');
  }

  String get questionMark {
    return Intl.message("?", name: 'questionMark');
  }

  String get next {
    return Intl.message("NEXT", name: 'next');
  }

  String get selectPackage {
    return Intl.message("SELECT A PACKAGE", name: 'selectPackage');
  }

  String get selectCompany {
    return Intl.message("SELECT A COMPANY", name: 'selectCompany');
  }

  String get filters {
    return Intl.message("FILTERS", name: 'filters');
  }

  String get sortBy {
    return Intl.message("Sort by", name: 'sortBy');
  }

  String get distance {
    return Intl.message("DISTANCE:", name: 'distance');
  }

  String get rating {
    return Intl.message("RATING:", name: 'rating');
  }

  String get price {
    return Intl.message("PRICE:", name: 'price');
  }

  String get nearestFirst {
    return Intl.message("NEAREST FIRST", name: 'nearestFirst');
  }

  String get highestFirst {
    return Intl.message("HIGHEST FIRST", name: 'highestFirst');
  }

  String get lowestFirst {
    return Intl.message("LOWEST FIRST", name: 'lowestFirst');
  }

  String get reviews {
    return Intl.message("Reviews", name: 'reviews');
  }

  String get startsFrom {
    return Intl.message("Starts from", name: 'startsFrom');
  }

  String get bookThis {
    return Intl.message("BOOK THIS", name: 'bookThis');
  }

  String get enterMobileNumber {
    return Intl.message("Enter your mobile number", name: 'enterMobileNumber');
  }

  String get send {
    return Intl.message("SEND", name: 'send');
  }

  String get sendSixDigits {
    return Intl.message("A 6 digit one-time password will be sent via SMS\nto verify your mobile number!", name: 'sendSixDigits');
  }

  String get otpVerification {
    return Intl.message("OTP Verification", name: 'otpVerification');
  }

  String get enterSixDigits {
    return Intl.message("Enter the 6 digit one-time password received\nvia SMS to your mobile !", name: 'enterSixDigits');
  }

  String get resendOtp {
    return Intl.message("RESEND OTP", name: 'resendOtp');
  }

  String get dashboard {
    return Intl.message("DASHBOARD", name: 'dashboard');
  }

  String get orderSummary {
    return Intl.message("ORDER DETAILS", name: 'orderSummary');
  }

  String get vehicleLocated {
    return Intl.message("Vehicle is located at :", name: 'vehicleLocated');
  }

  String get vehicleType {
    return Intl.message("Vehicle type : ", name: 'vehicleType');
  }

  String get dateAndTime {
    return Intl.message("Date & time : ", name: 'dateAndTime');
  }

  String get washCompany {
    return Intl.message("Wash Company : ", name: 'washCompany');
  }

  String get package {
    return Intl.message("Package : ", name: 'package');
  }

  String get cancel {
    return Intl.message("CANCEL", name: 'cancel');
  }

  String get edit {
    return Intl.message("EDIT", name: 'edit');
  }

  String get sedan {
    return Intl.message("sedan", name: 'sedan');
  }

  String get suv {
    return Intl.message("suv", name: 'suv');
  }

  String get motorcycle {
    return Intl.message("motorcycle", name: 'motorcycle');
  }

  String get boat {
    return Intl.message("boat", name: 'boat');
  }

  String get paymentMethods {
    return Intl.message("PAYMENT METHODS", name: 'paymentMethods');
  }

  String get cashOn {
    return Intl.message("CASH ON", name: 'cashOn');
  }

  String get delivery {
    return Intl.message("DELIVERY", name: 'delivery');
  }

  String get orderNow {
    return Intl.message("ORDER NOW", name: 'orderNow');
  }

  String get at {
    return Intl.message("at", name: 'at');
  }

  String get orders {
    return Intl.message("ORDERS", name: 'orders');
  }

  String get dateOfOrder {
    return Intl.message("Date of the order", name: 'dateOfOrder');
  }

  String get packageOrdered {
    return Intl.message("Package ordered", name: 'packageOrdered');
  }

  String get packagePrice {
    return Intl.message("Package price", name: 'packagePrice');
  }

  String get orderNumber {
    return Intl.message("Order number : ", name: 'orderNumber');
  }

  String get pending {
    return Intl.message("PENDING", name: 'pending');
  }

  String get orderDate {
    return Intl.message("ORDER DATE:", name: 'orderDate');
  }

  String get latestFirst {
    return Intl.message("LATEST FIRST", name: 'latestFirst');
  }

  String get CANCELLED {
    return Intl.message("CANCELLED", name: 'CANCELLED');
  }

  String get giveFeedback {
    return Intl.message("Give a feedback", name: 'giveFeedback');
  }

  String get report {
    return Intl.message("Report", name: 'report');
  }

  String get trackOrder {
    return Intl.message("TRACK THIS ORDER", name: 'trackOrder');
  }

  String get company {
    return Intl.message("COMPANY:", name: 'company');
  }

  String get highestAz {
    return Intl.message("HIGHEST A-Z", name: 'highestAz');
  }

  String get howSatisfied {
    return Intl.message("HOW SATISFIED\nARE YOU ?", name: 'howSatisfied');
  }

  String get writeReview {
    return Intl.message("Write your review here", name: 'writeReview');
  }

  String get trackingOrder {
    return Intl.message("TRACKING ORDER", name: 'trackingOrder');
  }

  String get onTheWay {
    return Intl.message("On the way !", name: 'onTheWay');
  }

  String get employeeOnHisWay {
    return Intl.message("Our employee is on\nhis way to your location.", name: 'employeeOnHisWay');
  }

  String get workingOn {
    return Intl.message("Working on !", name: 'workingOn');
  }

  String get vehicleBeingCleaned {
    return Intl.message("Vehicle is being cleaned\nright now", name: 'vehicleBeingCleaned');
  }

  String get thankForPatience {
    return Intl.message("Thank you for your patience", name: 'thankForPatience');
  }

  String get done {
    return Intl.message("Done !", name: 'done');
  }

  String get vehicleIsClean {
    return Intl.message("Your vehicle is clean now\nand ready to be used.", name: 'vehicleIsClean');
  }

  String get confirm {
    return Intl.message("CONFIRM", name: 'confirm');
  }

  String get confirmed {
    return Intl.message("Confirmed", name: 'confirmed');
  }

  String get orderWasConfirmed {
    return Intl.message("Your order was confirmed. Thank you.", name: 'orderWasConfirmed');
  }

  String get ok {
    return Intl.message("Ok", name: 'ok');
  }

  String get editYourProfile {
    return Intl.message("EDIT\nYOUR PROFILE", name: 'editYourProfile');
  }

  String get firstName {
    return Intl.message("First Name", name: 'firstName');
  }

  String get firstNameError {
    return Intl.message("The first name must contain at least 2 characters.", name: 'firstNameError');
  }

  String get lastName {
    return Intl.message("Last Name", name: 'lastName');
  }

  String get lastNameError {
    return Intl.message("The last name must contain at least 2 characters.", name: 'lastNameError');
  }

  String get email {
    return Intl.message("Email", name: 'email');
  }

  String get emailError {
    return Intl.message("Invalid e-mail address.", name: 'emailError');
  }

  String get phoneNumber {
    return Intl.message("Phone Number", name: 'phoneNumber');
  }

  String get phoneNumberError {
    return Intl.message("Invalid phone number.", name: 'phoneNumberError');
  }

  String get update {
    return Intl.message("UPDATE", name: 'update');
  }

  String get changeProfilePicture {
    return Intl.message("Change Profile Picture", name: 'changeProfilePicture');
  }

  String get takeNewPhoto {
    return Intl.message("Take new photo", name: 'takeNewPhoto');
  }

  String get chooseExistingPhoto {
    return Intl.message("Choose existing photo", name: 'chooseExistingPhoto');
  }

  String get Cancel {
    return Intl.message("Cancel", name: 'Cancel');
  }

  String get success {
    return Intl.message("Success", name: 'success');
  }

  String get profileWasUpdated {
    return Intl.message("Your profile was updated.", name: 'profileWasUpdated');
  }

  String get profile {
    return Intl.message("Profile", name: 'profile');
  }

  String get Orders {
    return Intl.message("Orders", name: 'Orders');
  }

  String get Messages {
    return Intl.message("Messages", name: 'Messages');
  }

  String get signOut {
    return Intl.message("Sign out", name: 'signOut');
  }

  String get termsAndConditions {
    return Intl.message("Terms & conditions", name: 'termsAndConditions');
  }

  String get softwareLicense {
    return Intl.message("Software license", name: 'softwareLicense');
  }

  String get copyright {
    return Intl.message("Copyright", name: 'copyright');
  }

  String get privacyPolicy {
    return Intl.message("Privacy Policy", name: 'privacyPolicy');
  }

  String get packages {
    return Intl.message("Packages", name: 'packages');
  }

  String get employees {
    return Intl.message("Employees", name: 'employees');
  }

  String get settings {
    return Intl.message("Settings", name: 'settings');
  }

  String get year {
    return Intl.message("YEAR", name: 'year');
  }

  String get month {
    return Intl.message("MONTH", name: 'month');
  }

  String get day {
    return Intl.message("DAY", name: 'day');
  }

  String get currentBalance {
    return Intl.message("CURRENT BALANCE", name: 'currentBalance');
  }

  String get monday {
    return Intl.message("MONDAY", name: 'monday');
  }

  String get tuesday {
    return Intl.message("TUESDAY", name: 'tuesday');
  }

  String get wednesday {
    return Intl.message("WEDNESDAY", name: 'wednesday');
  }

  String get thursday {
    return Intl.message("THURSDAY", name: 'thursday');
  }

  String get friday {
    return Intl.message("FRIDAY", name: 'friday');
  }

  String get saturday {
    return Intl.message("SATURDAY", name: 'saturday');
  }

  String get sunday {
    return Intl.message("SUNDAY", name: 'sunday');
  }

  String get mondayShort {
    return Intl.message("MON", name: 'mondayShort');
  }

  String get tuesdayShort {
    return Intl.message("TUE", name: 'tuesdayShort');
  }

  String get wednesdayShort {
    return Intl.message("WED", name: 'wednesdayShort');
  }

  String get thursdayShort {
    return Intl.message("THU", name: 'thursdayShort');
  }

  String get fridayShort {
    return Intl.message("FRI", name: 'fridayShort');
  }

  String get saturdayShort {
    return Intl.message("SAT", name: 'saturdayShort');
  }

  String get sundayShort {
    return Intl.message("SUN", name: 'sundayShort');
  }

  String get january {
    return Intl.message("January", name: 'january');
  }

  String get february {
    return Intl.message("February", name: 'february');
  }

  String get march {
    return Intl.message("March", name: 'march');
  }

  String get april {
    return Intl.message("April", name: 'april');
  }

  String get may {
    return Intl.message("May", name: 'may');
  }

  String get june {
    return Intl.message("June", name: 'june');
  }

  String get july {
    return Intl.message("July", name: 'july');
  }

  String get august {
    return Intl.message("August", name: 'august');
  }

  String get september {
    return Intl.message("September", name: 'september');
  }

  String get october {
    return Intl.message("October", name: 'october');
  }

  String get november {
    return Intl.message("November", name: 'november');
  }

  String get december {
    return Intl.message("December", name: 'december');
  }

  String get januaryShort {
    return Intl.message("JAN", name: 'januaryShort');
  }

  String get februaryShort {
    return Intl.message("FEB", name: 'februaryShort');
  }

  String get marchShort {
    return Intl.message("MAR", name: 'marchShort');
  }

  String get aprilShort {
    return Intl.message("APR", name: 'aprilShort');
  }

  String get mayShort {
    return Intl.message("MAY", name: 'mayShort');
  }

  String get juneShort {
    return Intl.message("JUN", name: 'juneShort');
  }

  String get julyShort {
    return Intl.message("JUL", name: 'julyShort');
  }

  String get augustShort {
    return Intl.message("AUG", name: 'augustShort');
  }

  String get septemberShort {
    return Intl.message("SEP", name: 'septemberShort');
  }

  String get octoberShort {
    return Intl.message("OCT", name: 'octoberShort');
  }

  String get novemberShort {
    return Intl.message("NOV", name: 'novemberShort');
  }

  String get decemberShort {
    return Intl.message("DEC", name: 'decemberShort');
  }

  String get newOrder {
    return Intl.message("NEW\nORDER", name: 'newOrder');
  }

  String get carsPackages {
    return Intl.message("CARS\nPACKAGES", name: 'carsPackages');
  }

  String get motorcyclesPackages {
    return Intl.message("MOTORCYCLES\nPACKAGES", name: 'motorcyclesPackages');
  }

  String get suvsPackages {
    return Intl.message("SUVs\nPACKAGES", name: 'suvsPackages');
  }

  String get boatsPackages {
    return Intl.message("BOATS\nPACKAGES", name: 'boatsPackages');
  }

  String get delete {
    return Intl.message("DELETE", name: 'delete');
  }

  String get confirmAction {
    return Intl.message("Confirm action", name: 'confirmAction');
  }

  String get deletePackageConfirmation {
    return Intl.message("Are you sure you want to delete this package ?", name: 'deletePackageConfirmation');
  }

  String get Delete {
    return Intl.message("Delete", name: 'Delete');
  }

  String get editPackages {
    return Intl.message("EDIT\nPACKAGES", name: 'editPackages');
  }

  String get Price {
    return Intl.message("Price", name: 'Price');
  }

  String get saveChanges {
    return Intl.message("SAVE CHANGES", name: 'saveChanges');
  }

  String get changesSaved {
    return Intl.message("Changes saved successfully.", name: 'changesSaved');
  }

  String get jobsDone {
    return Intl.message("Jobs done", name: 'jobsDone');
  }

  String get employeeSince {
    return Intl.message("Employee since", name: 'employeeSince');
  }

  String get editEmployee {
    return Intl.message("EDIT\nEMPLOYEE", name: 'editEmployee');
  }

  String get removeEmployee {
    return Intl.message("Remove Employee", name: 'removeEmployee');
  }

  String get employeeUpdated {
    return Intl.message("Employee updated successfully.", name: 'employeeUpdated');
  }

  String get removeEmployeeConfirmation {
    return Intl.message("Are you sure you want to remove this employee ?", name: 'removeEmployeeConfirmation');
  }

  String get remove {
    return Intl.message("Remove", name: 'remove');
  }

  String get addEmployee {
    return Intl.message("ADD\nEMPLOYEE", name: 'addEmployee');
  }

  String get employeeAdded {
    return Intl.message("Employee addedd successfully.", name: 'employeeAdded');
  }

  String get editSettings {
    return Intl.message("EDIT YOUR SETTINGS", name: 'editSettings');
  }

  String get companyName {
    return Intl.message("Company Name", name: 'companyName');
  }

  String get companyNameError {
    return Intl.message("The name must contain at least 2 characters.", name: 'companyNameError');
  }

  String get yourLocation {
    return Intl.message("YOUR LOCATION", name: 'yourLocation');
  }

  String get workingHours {
    return Intl.message("WORKING HOURS", name: 'workingHours');
  }

  String get evereyDay {
    return Intl.message("Everyday", name: 'evereyDay');
  }

  String get dayOff {
    return Intl.message("DAY OFF", name: 'dayOff');
  }

  String get DONE {
    return Intl.message("DONE", name: 'DONE');
  }

  String get newOrders {
    return Intl.message("NEW ORDERS", name: 'newOrders');
  }

  String get orderN {
    return Intl.message("ORDER Nº", name: 'orderN');
  }

  String get location {
    return Intl.message("Location", name: 'location');
  }

  String get payment {
    return Intl.message("Payment : ", name: 'payment');
  }

  String get onDelivery {
    return Intl.message("On Delivery", name: 'onDelivery');
  }

  String get online {
    return Intl.message("Online", name: 'online');
  }

  String get assignEmployee {
    return Intl.message("ASSIGN EMPLOYEE", name: 'assignEmployee');
  }

  String get proceed {
    return Intl.message("PROCEED", name: 'proceed');
  }

  String get theEmployee {
    return Intl.message("The employee ", name: 'theEmployee');
  }

  String get isNowAssigned {
    return Intl.message(" is now assigned to this order.", name: 'isNowAssigned');
  }

  String get client {
    return Intl.message("CLIENT", name: 'client');
  }

  String get summary {
    return Intl.message("SUMMARY", name: 'summary');
  }

  String get takeThisOrder {
    return Intl.message("TAKE THIS ORDER", name: 'takeThisOrder');
  }

  String get time {
    return Intl.message("Time : ", name: 'time');
  }

  String get navigate {
    return Intl.message("NAVIGATE", name: 'navigate');
  }

  String get start {
    return Intl.message("START", name: 'start');
  }

  String get hourShort {
    return Intl.message("h", name: 'hourShort');
  }

  String get minuteShort {
    return Intl.message("min", name: 'minuteShort');
  }

  String get kilometerShort {
    return Intl.message("km", name: 'kilometerShort');
  }

  String get meterShort {
    return Intl.message("m", name: 'meterShort');
  }

  String get uploadVehiclePictures {
    return Intl.message("UPLOAD\nVEHICLE PICTURES", name: 'uploadVehiclePictures');
  }

  String get takeAtLeast {
    return Intl.message("Take at least two photos from different angles !", name: 'takeAtLeast');
  }

  String get weComeToYou {
    return Intl.message("We come to you\nanywhere, anytime !", name: 'weComeToYou');
  }

  String get atYourHome {
    return Intl.message("At your home, your office or wherever !", name: 'atYourHome');
  }

  String get lowestPrice {
    return Intl.message("Lowest Prices with\nGuaranteed Results", name: 'lowestPrice');
  }

  String get cheaperPrice {
    return Intl.message("Cheaper price, on the market !", name: 'cheaperPrice');
  }

  String get liveChat {
    return Intl.message("Live chat customer\nservice", name: 'liveChat');
  }

  String get askBefore {
    return Intl.message("Ask, before making any purchase !", name: 'askBefore');
  }

  String get skip {
    return Intl.message("SKIP", name: 'skip');
  }

  String get language {
    return Intl.message("Language", name: 'language');
  }

  String get sponsored {
    return Intl.message("Sponsored", name: 'sponsored');
  }

  String get addPackage {
    return Intl.message("ADD\nPACKAGE", name: 'addPackage');
  }

  String get addNewPackage {
    return Intl.message("Add new package...", name: 'addNewPackage');
  }

  String get addService {
    return Intl.message("Add a service", name: 'addService');
  }

  String get packageAdded {
    return Intl.message("Package added successfully.", name: 'packageAdded');
  }

  String get submit {
    return Intl.message("SUBMIT", name: 'submit');
  }

  String get byContinuing {
    return Intl.message("By continuing you agree to our terms & conditions\nand Privacy Policy", name: 'byContinuing');
  }

  String get finishAccount {
    return Intl.message("Finish your account !", name: 'finishAccount');
  }

  String get notAssigned {
    return Intl.message("Not Assigned", name: 'notAssigned');
  }

  String get noFeedBack {
    return Intl.message("No feedback", name: 'noFeedBack');
  }

  String get Feedback {
    return Intl.message("Feedback", name: 'Feedback');
  }

  String get Paymentmethod {
    return Intl.message("Payment method", name: 'Paymentmethod');
  }

  String get Cashondelivery {
    return Intl.message("Cash on delivery", name: 'Cashondelivery');
  }

  String get AssignedEmployee {
    return Intl.message("Assigned Employee", name: 'AssignedEmployee');
  }

  String get chooseAction {
    return Intl.message("Choose an action", name: 'chooseAction');
  }

  String get manageOrder {
    return Intl.message("Manage order", name: 'manageOrder');
  }

  String get assignAnEmployee {
    return Intl.message("Assign an employee", name: 'assignAnEmployee');
  }

  String get assignMyself {
    return Intl.message("Assign to me", name: 'assignMyself');
  }

  String get changeStatus {
    return Intl.message("Change status", name: 'changeStatus');
  }

  String get available {
    return Intl.message("Available", name: 'available');
  }

  String get unavailable {
    return Intl.message("Unavailable", name: 'unavailable');
  }

  String get Employeeassigned {
    return Intl.message("Employee assigned", name: 'Employeeassigned');
  }

  String get endOrder {
    return Intl.message("END ORDER", name: 'endOrder');
  }

  String get oldPrice {
    return Intl.message("Old Price", name: 'oldPrice');
  }

  String get homePage {
    return Intl.message("Home", name: 'homePage');
  }

  String get Duration {
    return Intl.message("Duration", name: 'Duration');
  }

  String get areYouCompany {
    return Intl.message("Are you a company ?", name: 'areYouCompany');
  }

  String get joinUs {
    return Intl.message("Join us !", name: 'joinUs');
  }

  String get fillTheForm {
    return Intl.message("Fill in the form below", name: 'fillTheForm');
  }

  String get Country {
    return Intl.message("Country", name: 'Country');
  }

  String get chooseYourCountry {
    return Intl.message("Please choose your country", name: 'chooseYourCountry');
  }

  String get chooseYourDialCode {
    return Intl.message("Please choose your dialing code", name: 'chooseYourDialCode');
  }

  String get chooseYourStatus {
    return Intl.message("Please choose your status", name: 'chooseYourStatus');
  }

  String get Status {
    return Intl.message("Status", name: 'Status');
  }

  String get Company {
    return Intl.message("Company", name: 'Company');
  }

  String get Individual {
    return Intl.message("Individual", name: 'Individual');
  }

  String get Send {
    return Intl.message("Send", name: 'Send');
  }

  String get Egypt {
    return Intl.message("Egypt", name: 'Egypt');
  }

  String get Jordan {
    return Intl.message("Jordan", name: 'Jordan');
  }

  String get Kuwait {
    return Intl.message("Kuwait", name: 'Kuwait');
  }

  String get Morocco {
    return Intl.message("Morocco", name: 'Morocco');
  }

  String get UAE {
    return Intl.message("UAE", name: 'UAE');
  }

  String get UnitedKingdom {
    return Intl.message("United Kingdom", name: 'UnitedKingdom');
  }

  String get KSA {
    return Intl.message("KSA", name: 'KSA');
  }

  String get Bahrain {
    return Intl.message("Bahrain", name: 'Bahrain');
  }

  String get Oman {
    return Intl.message("Oman", name: 'Oman');
  }

  String get Qatar {
    return Intl.message("Qatar", name: 'Qatar');
  }

  String get TAKEORDER {
    return Intl.message("TAKE ORDER", name: 'TAKEORDER');
  }

  String get noResult {
    return Intl.message("Sorry, no company is available at the selected date and time.\n\nPlease select another date and try again.", name: 'noResult');
  }

  String get alreadySentRequest {
    return Intl.message("Your request to join us is under review. We will send you an email once your request is reviewed.\n\nThank you for your patience.", name: 'alreadySentRequest');
  }

  String get requestApproved {
    return Intl.message("Request approved", name: 'requestApproved');
  }

  String get requestApprovedMessage {
    return Intl.message("Your request to join us was approved !\nYou can now sign in to access your dashboard.", name: 'requestApprovedMessage');
  }

  String get signInNow {
    return Intl.message("Sign in now", name: 'signInNow');
  }

  String get requestDeclined {
    return Intl.message("Request declined", name: 'requestDeclined');
  }

  String get requestDeclinedMessage {
    return Intl.message("Your request to join us was unfortunately declined.", name: 'requestDeclinedMessage');
  }

  String get requestUnderReview {
    return Intl.message("Request under review", name: 'requestUnderReview');
  }

  String get sendNewRequest {
    return Intl.message("Send a new request", name: 'sendNewRequest');
  }

  String get requestSubmitted {
    return Intl.message("Your request was submitted successfully. We will contact you soon.\nThank you !", name: 'requestSubmitted');
  }

  String get enableLocationService {
    return Intl.message("Please enable the location service. This application depends on it.", name: 'enableLocationService');
  }

  String get allowLocation {
    return Intl.message("This application needs access to your location to function properly.\nPlease grant the permission to access your location from settings.", name: 'allowLocation');
  }

  String get connectToInternet {
    return Intl.message("This application needs access to the internet to function properly.\nPlease connect to the internet and restart the application.", name: 'connectToInternet');
  }

  String get addYourAddress {
    return Intl.message("ADD YOUR ADDRESS", name: 'addYourAddress');
  }

  String get theSelectedLocationIs {
    return Intl.message("The selected location is :", name: 'theSelectedLocationIs');
  }

  String get pleaseAddYourAddess {
    return Intl.message("Please confirm your address :", name: 'pleaseAddYourAddess');
  }

  String get add {
    return Intl.message("CONFIRM", name: 'add');
  }

  String get address {
    return Intl.message("Address", name: 'address');
  }

  String get toChangeTheAddress {
    return Intl.message("TO CHANGE THE ADDRESS PLEASE OPEN THE MAP", name: 'toChangeTheAddress');
  }

  String get availability {
    return Intl.message("AVAILABILITY", name: 'availability');
  }

  String get writeAddress {
    return Intl.message("Please write your address.", name: 'writeAddress');
  }

  String get makeWholeDayOff {
    return Intl.message("MAKE THE WHOLE DAY OFF", name: 'makeWholeDayOff');
  }

  String get remaining {
    return Intl.message("Remaining", name: 'remaining');
  }

  String get number {
    return Intl.message("number", name: 'number');
  }

  String get willBeCanceled {
    return Intl.message("THIS WILL BE AUTOMATICALLY CANCELED, PLEASE TAKE THIS ORDER OR DECLINE IT", name: 'willBeCanceled');
  }

  String get decline {
    return Intl.message("DECLINE", name: 'decline');
  }

  String get orderDeclinedAutomatically {
    return Intl.message("This order is now declined after the delay has elapsed.", name: 'orderDeclinedAutomatically');
  }

  String get dayoff {
    return Intl.message("DAY OFF", name: 'dayoff');
  }

  String get youCanChangeHours {
    return Intl.message("You can change the working hours from your settings.", name: 'youCanChangeHours');
  }

  String get phoneNumberExists {
    return Intl.message("An account with this phone number already exists.", name: 'phoneNumberExists');
  }

  String get busy {
    return Intl.message("Busy", name: 'busy');
  }

  String get enterTitle {
    return Intl.message("Please enter a title for the package", name: 'enterTitle');
  }

  String get enterServices {
    return Intl.message("Please enter a list of service for this package", name: 'enterServices');
  }

  String get selectVehicleType {
    return Intl.message("Please select the vehicle type for this package", name: 'selectVehicleType');
  }

  String get enterPrice {
    return Intl.message("Please enter a price for this package", name: 'enterPrice');
  }

  String get enterDuration {
    return Intl.message("Please enter a duration for this package", name: 'enterDuration');
  }

  String get rateThisApp {
    return Intl.message("Rate this app", name: 'rateThisApp');
  }

  String get ifYouLikeThisApp {
    return Intl.message("If you like this app, please take a little bit of your time to review it !\nIt really helps us and it shouldn't take you more than one minute.", name: 'ifYouLikeThisApp');
  }

  String get rate {
    return Intl.message("RATE", name: 'rate');
  }

  String get notNow {
    return Intl.message("Not now", name: 'notNow');
  }

  String get An_error_occurred {
    return Intl.message("An error occurred", name: 'An_error_occurred');
  }

  String get An_unexpected_error_happened_Please_try_again {
    return Intl.message("An unexpected error happened. Please try again.", name: 'An_unexpected_error_happened_Please_try_again');
  }


}

class GeneratedLocalizationsDelegate extends LocalizationsDelegate<S> {
  const GeneratedLocalizationsDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
			Locale("en", ""),
			Locale("ar", ""),

    ];
  }

  LocaleListResolutionCallback listResolution({Locale fallback}) {
    return (List<Locale> locales, Iterable<Locale> supported) {
      if (locales == null || locales.isEmpty) {
        return fallback ?? supported.first;
      } else {
        return _resolve(locales.first, fallback, supported);
      }
    };
  }

  LocaleResolutionCallback resolution({Locale fallback}) {
    return (Locale locale, Iterable<Locale> supported) {
      return _resolve(locale, fallback, supported);
    };
  }

  Locale _resolve(Locale locale, Locale fallback, Iterable<Locale> supported) {
    if (locale == null || !isSupported(locale)) {
      return fallback ?? supported.first;
    }

    final Locale languageLocale = Locale(locale.languageCode, "");
    if (supported.contains(locale)) {
      return locale;
    } else if (supported.contains(languageLocale)) {
      return languageLocale;
    } else {
      final Locale fallbackLocale = fallback ?? supported.first;
      return fallbackLocale;
    }
  }

  @override
  Future<S> load(Locale locale) {
    return S.load(locale);
  }

  @override
  bool isSupported(Locale locale) =>
    locale != null && supportedLocales.contains(locale);

  @override
  bool shouldReload(GeneratedLocalizationsDelegate old) => false;
}
