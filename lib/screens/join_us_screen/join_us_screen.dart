import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weash_cars/consts/country_codes.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:flutter/services.dart';
import "package:google_maps_webservice/geocoding.dart" as geocoding;

class JoinUsScreen extends StatefulWidget {
  @override
  _JoinUsScreenState createState() => _JoinUsScreenState();
}

class _JoinUsScreenState extends State<JoinUsScreen> {
  List<_Country> _countries;
  String _companyName;
  Future<String> _dialCodeFuture;
  String _dialCode;
  String _phoneNumber;
  String _email;
  _Country _country;
  _Tuple _clientsCountEstimate;
  _Status _status;
  Completer<_JoinRequestStatus> _joinRequestStatus = new Completer();

  Location _locationService;
  geocoding.GoogleMapsGeocoding _geocoder;

  GlobalKey<FormState> _formKey = new GlobalKey();

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    _geocoder = new geocoding.GoogleMapsGeocoding(
        apiKey: 'AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I');
    _locationService = new Location();
    _dialCodeFuture = _getCountryDialCode();

    _checkRequestStatus();
  }

//  @override
//  void didChangeDependencies() {
//    if (_countries == null) {
//      final countries = [
//        _Country(
//          name: 'Bahrain',
//          text: S.of(context).Bahrain,
//          currency: 'BHD',
//          dialCode: '+973',
//        ),
//        _Country(
//          name: 'Egypt',
//          text: S.of(context).Egypt,
//          currency: 'EGP',
//          dialCode: '+20',
//        ),
//        _Country(
//          name: 'Jordan',
//          text: S.of(context).Jordan,
//          currency: 'JOD',
//          dialCode: '+962',
//        ),
//        _Country(
//          name: 'KSA',
//          text: S.of(context).KSA,
//          currency: 'SAR',
//          dialCode: '+966',
//        ),
//        _Country(
//          name: 'Kuwait',
//          text: S.of(context).Kuwait,
//          currency: 'KWD',
//          dialCode: '+965',
//        ),
//        _Country(
//          name: 'Morocco',
//          text: S.of(context).Morocco,
//          currency: 'MAD',
//          dialCode: '+212',
//        ),
//        _Country(
//          name: 'Oman',
//          text: S.of(context).Oman,
//          currency: 'OMR',
//          dialCode: '+968',
//        ),
//        _Country(
//          name: 'Qatar',
//          text: S.of(context).Qatar,
//          currency: 'QAR',
//          dialCode: '+974',
//        ),
//        _Country(
//          name: 'UAE',
//          text: S.of(context).UAE,
//          currency: 'AED',
//          dialCode: '+971',
//        ),
//        _Country(
//          name: 'UK',
//          text: S.of(context).UnitedKingdom,
//          currency: 'GBP',
//          dialCode: '+44',
//        ),
//      ];
//      countries.forEach((country) {
//        Firestore.instance.collection('countries').add({
//          'name_en': country.name,
//          'name_ar': country.text,
//          'currency': country.currency,
//          'dial_code': country.dialCode,
//        });
//      });
//    }
//
//    super.didChangeDependencies();
//  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(40),
          // vertical: ScreenUtil().setHeight(60),
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/splash_sceen_background.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: FutureBuilder<_JoinRequestStatus>(
          future: _joinRequestStatus.future,
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
              case ConnectionState.active:
                return Container();
              case ConnectionState.done:
                final requestStatus = snapshot.data;

                switch (requestStatus) {
                  case _JoinRequestStatus.pending:
                    return Column(
                      children: <Widget>[
                        SizedBox(
                            height: MediaQuery.of(context).padding.top +
                                ScreenUtil().setHeight(20)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            InkWell(
                              onTap: () => Navigator.of(context).pop(),
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Container(
                                width: ScreenUtil().setWidth(35),
                                height: ScreenUtil().setWidth(35),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white,
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.16),
                                      blurRadius: 11,
                                    ),
                                  ],
                                ),
                                child: Center(
                                  child: Image(
                                    image: AssetImage(
                                        'assets/images/back-button-icon.png'),
                                    width: ScreenUtil().setWidth(17.39),
                                    height: ScreenUtil().setHeight(12.15),
                                    matchTextDirection: true,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                S.of(context).requestUnderReview,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Display',
                                  fontSize: 24,
                                  fontWeight: FontWeight.w900,
                                  color: Color(0xFFA0AEE6),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(20)),
                              Text(
                                S.of(context).alreadySentRequest,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontSize: 15,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    );

                  case _JoinRequestStatus.approved:
                    return Column(
                      children: <Widget>[
                        SizedBox(
                            height: MediaQuery.of(context).padding.top +
                                ScreenUtil().setHeight(20)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            InkWell(
                              onTap: () => Navigator.of(context).pop(),
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Container(
                                width: ScreenUtil().setWidth(35),
                                height: ScreenUtil().setWidth(35),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white,
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.16),
                                      blurRadius: 11,
                                    ),
                                  ],
                                ),
                                child: Center(
                                  child: Image(
                                    image: AssetImage(
                                        'assets/images/back-button-icon.png'),
                                    width: ScreenUtil().setWidth(17.39),
                                    height: ScreenUtil().setHeight(12.15),
                                    matchTextDirection: true,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                S.of(context).requestApproved,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Display',
                                  fontSize: 24,
                                  fontWeight: FontWeight.w900,
                                  color: Color(0xFFA0AEE6),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(20)),
                              Text(
                                S.of(context).requestApprovedMessage,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontSize: 15,
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(80)),
                              CupertinoButton.filled(
                                onPressed: () => Navigator.of(context)
                                    .pushReplacementNamed('/login'),
                                child: Text(S.of(context).signInNow),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );

                  case _JoinRequestStatus.declined:
                    return Column(
                      children: <Widget>[
                        SizedBox(
                            height: MediaQuery.of(context).padding.top +
                                ScreenUtil().setHeight(20)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            InkWell(
                              onTap: () => Navigator.of(context).pop(),
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Container(
                                width: ScreenUtil().setWidth(35),
                                height: ScreenUtil().setWidth(35),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white,
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.16),
                                      blurRadius: 11,
                                    ),
                                  ],
                                ),
                                child: Center(
                                  child: Image(
                                    image: AssetImage(
                                        'assets/images/back-button-icon.png'),
                                    width: ScreenUtil().setWidth(17.39),
                                    height: ScreenUtil().setHeight(12.15),
                                    matchTextDirection: true,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                S.of(context).requestDeclined,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Display',
                                  fontSize: 24,
                                  fontWeight: FontWeight.w900,
                                  color: Color(0xFFA0AEE6),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(20)),
                              Text(
                                S.of(context).requestDeclinedMessage,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontSize: 15,
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(80)),
                              CupertinoButton.filled(
                                onPressed: () => Navigator.of(context)
                                    .pushReplacementNamed('/joinus'),
                                child: Text(S.of(context).sendNewRequest),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );

                  case _JoinRequestStatus.none:
                    return Column(
                      children: <Widget>[
                        SizedBox(
                            height: MediaQuery.of(context).padding.top +
                                ScreenUtil().setHeight(20)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            InkWell(
                              onTap: () => Navigator.of(context).pop(),
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Container(
                                width: ScreenUtil().setWidth(35),
                                height: ScreenUtil().setWidth(35),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white,
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.16),
                                      blurRadius: 11,
                                    ),
                                  ],
                                ),
                                child: Center(
                                  child: Image(
                                    image: AssetImage(
                                        'assets/images/back-button-icon.png'),
                                    width: ScreenUtil().setWidth(17.39),
                                    height: ScreenUtil().setHeight(12.15),
                                    matchTextDirection: true,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Expanded(
                          child: Form(
                            key: _formKey,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Align(
                                  alignment: application.locale == 'ar'
                                      ? Alignment.topRight
                                      : Alignment.topLeft,
                                  child: Text(
                                    S.of(context).fillTheForm,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Display',
                                      fontSize: 24,
                                      fontWeight: FontWeight.w900,
                                      color: Color(0xFFA0AEE6),
                                    ),
                                  ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(30)),
                                FormField<String>(
                                  validator: (String value) {
                                    if (value == null || value.length < 2) {
                                      return S.of(context).companyNameError;
                                    }
                                    return null;
                                  },
                                  onSaved: (String value) {
                                    setState(() {
                                      _companyName = value;
                                    });
                                  },
                                  builder: (FormFieldState<String> builder) =>
                                      Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      CupertinoTextField(
                                        onChanged: (String value) {
                                          builder.didChange(value);
                                        },
                                        placeholder: S.of(context).companyName,
                                      ),
                                      if (builder.hasError) ...[
                                        SizedBox(
                                            height: ScreenUtil().setHeight(6)),
                                        Text(
                                          builder.errorText,
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.red,
                                          ),
                                        ),
                                      ]
                                    ],
                                  ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(30)),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: FutureBuilder<String>(
                                        future: _dialCodeFuture,
                                        builder: (context, snapshot) {
                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                            case ConnectionState.active:
                                              return Center(
                                                child:
                                                    CupertinoActivityIndicator(),
                                              );
                                            case ConnectionState.none:
                                            case ConnectionState.done:
                                              return DropdownButtonFormField<
                                                  String>(
                                                value: _dialCode,
                                                isDense: true,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                ),
                                                onChanged: (String dialCode) {
                                                  print('Dial code: $dialCode');
                                                  setState(() {
                                                    _dialCode = dialCode;
                                                  });
                                                },
                                                validator: (String dialCode) {
                                                  if (dialCode == null ||
                                                      dialCode.isEmpty) {
                                                    return S
                                                        .of(context)
                                                        .chooseYourDialCode;
                                                  }
                                                  return null;
                                                },
                                                onSaved: (String dialCode) {
                                                  setState(() {
                                                    _dialCode = dialCode;
                                                  });
                                                },
                                                items: [
                                                  if (!_countries.any(
                                                      (country) =>
                                                          country.dialCode ==
                                                          _dialCode))
                                                    DropdownMenuItem<String>(
                                                      value: _dialCode,
                                                      child: Text(_dialCode),
                                                    ),
                                                  for (var country
                                                      in _countries)
                                                    DropdownMenuItem<String>(
                                                      value: country.dialCode,
                                                      child: Text(
                                                          country.dialCode),
                                                    ),
                                                ],
                                              );
                                          }

                                          return Container();
                                        },
                                      ),
                                    ),
                                    SizedBox(width: ScreenUtil().setWidth(10)),
                                    Expanded(
                                      flex: 3,
                                      child: FormField<String>(
                                        validator: (String value) {
                                          final regexp =
                                              new RegExp(r'^[0-9]+$');
                                          final v = value == null
                                              ? null
                                              : value.replaceAll(
                                                  RegExp(r'[ \-()]*'), '');
                                          if (value == null ||
                                              !regexp.hasMatch(v)) {
                                            return S
                                                .of(context)
                                                .phoneNumberError;
                                          }
                                          return null;
                                        },
                                        onSaved: (String value) {
                                          setState(() {
                                            _phoneNumber = value;
                                          });
                                        },
                                        builder:
                                            (FormFieldState<String> builder) =>
                                                Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Directionality(
                                              textDirection: TextDirection.ltr,
                                              child: CupertinoTextField(
                                                onChanged: (String value) {
                                                  builder.didChange(value);
                                                },
                                                keyboardType:
                                                    TextInputType.phone,
                                                placeholder:
                                                    S.of(context).phoneNumber,
                                                textAlign:
                                                    application.locale == 'ar'
                                                        ? TextAlign.right
                                                        : TextAlign.left,
                                              ),
                                            ),
                                            if (builder.hasError) ...[
                                              SizedBox(
                                                  height: ScreenUtil()
                                                      .setHeight(6)),
                                              Text(
                                                builder.errorText,
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.red,
                                                ),
                                              ),
                                            ]
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: ScreenUtil().setHeight(30)),
                                FormField<String>(
                                  validator: (String value) {
                                    final regexp = new RegExp(
                                        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
                                    if (value == null ||
                                        !regexp.hasMatch(value)) {
                                      return S.of(context).emailError;
                                    }
                                    return null;
                                  },
                                  onSaved: (String value) {
                                    setState(() {
                                      _email = value;
                                    });
                                  },
                                  builder: (FormFieldState<String> builder) =>
                                      Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      CupertinoTextField(
                                        onChanged: (String value) {
                                          builder.didChange(value);
                                        },
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        placeholder: S.of(context).email,
                                      ),
                                      if (builder.hasError) ...[
                                        SizedBox(
                                            height: ScreenUtil().setHeight(6)),
                                        Text(
                                          builder.errorText,
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.red,
                                          ),
                                        ),
                                      ]
                                    ],
                                  ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(30)),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Text("${S.of(context).Country} :"),
                                    ),
                                    SizedBox(width: ScreenUtil().setWidth(30)),
                                    Expanded(
                                      flex: 2,
                                      child: DropdownButtonFormField<_Country>(
                                        value: _country,
                                        isDense: true,
                                        onChanged: (_Country country) {
                                          print(
                                              "Country: $country. Currency: ${country.currency}");
                                          setState(() {
                                            _country = country;
                                          });
                                        },
                                        validator: (_Country country) {
                                          if (country == null) {
                                            return S
                                                .of(context)
                                                .chooseYourCountry;
                                          }
                                          return null;
                                        },
                                        onSaved: (_Country country) {
                                          setState(() {
                                            _country = country;
                                          });
                                        },
                                        items: [
                                          for (var country in _countries)
                                            DropdownMenuItem<_Country>(
                                              value: country,
                                              child: Text(country.text),
                                            ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                FormField(
                                  validator: (_Status value) {
                                    if (value == null) {
                                      return S.of(context).chooseYourStatus;
                                    }
                                    return null;
                                  },
                                  builder: (FormFieldState<_Status> builder) =>
                                      Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: Text(
                                                "${S.of(context).Status} :"),
                                          ),
                                          Expanded(
                                            flex: 3,
                                            child: Row(
                                              children: <Widget>[
                                                Text(S.of(context).Company),
                                                Radio<_Status>(
                                                  value: _Status.company,
                                                  groupValue: _status,
                                                  onChanged: (_Status status) {
                                                    builder.didChange(status);
                                                    setState(() {
                                                      _status = status;
                                                    });
                                                  },
                                                ),
                                                Text(S.of(context).Individual),
                                                Radio<_Status>(
                                                  value: _Status.individual,
                                                  groupValue: _status,
                                                  onChanged: (_Status status) {
                                                    builder.didChange(status);
                                                    setState(() {
                                                      _status = status;
                                                    });
                                                  },
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      if (builder.hasError) ...[
                                        Text(
                                          builder.errorText,
                                          style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.red,
                                          ),
                                        ),
                                      ]
                                    ],
                                  ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(80)),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    InkWell(
                                      enableFeedback: false,
                                      onTap: _send,
                                      borderRadius: BorderRadius.circular(20),
                                      child: Container(
                                        width: ScreenUtil().setWidth(120),
                                        height: ScreenUtil().setHeight(40),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          gradient: LinearGradient(
                                            colors: [
                                              Color(0xFF0026A1),
                                              Color(0xFF000F66)
                                            ],
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomRight,
                                          ),
                                          boxShadow: <BoxShadow>[
                                            BoxShadow(
                                              color: Color(0x66000000),
                                              offset: Offset(0, 3),
                                              blurRadius: 4,
                                            ),
                                          ],
                                        ),
                                        child: _isLoading
                                            ? CupertinoActivityIndicator(
                                                animating: true,
                                              )
                                            : Text(
                                                S.of(context).Send,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'SF UI Display',
                                                  fontSize:
                                                      ScreenUtil().setSp(11),
                                                  fontWeight: FontWeight.w700,
                                                  color: Color(0xfffcfcfc),
                                                ),
                                              ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    );
                }
            }

            return Container();
          },
        ),
      ),
    );
  }

  Future<String> _getCountryCode() async {
    await _locationService.changeSettings(accuracy: LocationAccuracy.HIGH);

    LocationData location;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      bool serviceStatus = await _locationService.serviceEnabled();
      if (serviceStatus) {
        final permission = await _locationService.requestPermission();
        if (permission) {
          location = await _locationService.getLocation();

          final addressResult = await _geocoder.searchByLocation(
              new geocoding.Location(location.latitude, location.longitude));
          if (addressResult.isOkay && addressResult.results.length > 0) {
            final countryCode = addressResult.results[0].addressComponents
                .firstWhere(
                    (addressComponent) =>
                        addressComponent.types.contains('country') &&
                        addressComponent.types.contains('political'),
                    orElse: () {
              print("No result");
              return null;
            }).shortName;

            return countryCode;
          }
        } else {
          showCupertinoDialog(
            context: context,
            builder: (BuildContext context) => CupertinoAlertDialog(
              content: Text(S.of(context).allowLocation),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(S.of(context).ok),
                ),
              ],
            ),
          );
        }
      } else {
        bool serviceStatusResult = await _locationService.requestService();
        print("Service status activated after request: $serviceStatusResult");
        if (serviceStatusResult) {
          return _getCountryCode();
        }
        return null;
      }
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            content: Text(S.of(context).allowLocation),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(S.of(context).ok),
              ),
            ],
          ),
        );
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            content: Text(S.of(context).enableLocationService),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(S.of(context).ok),
              ),
            ],
          ),
        );
      }
    }

    return null;
  }

  Future<String> _getCountryDialCode() async {
    final countryCode = await _getCountryCode();
    if (countryCode != null) {
      final country = COUNTRY_CODES.firstWhere(
          (countryData) => countryData['code'] == countryCode,
          orElse: () => null);
      if (country != null) {
        final countryDialCode = country['dial_code'];
        _dialCode = countryDialCode;
        return countryDialCode;
      }
    }
    return null;
  }

  Future<void> _checkRequestStatus() async {
    final countriesDocuments =
        await Firestore.instance.collection('countries').getDocuments();
    if (countriesDocuments != null && countriesDocuments.documents.isNotEmpty) {
      final countries =
          countriesDocuments.documents.map((DocumentSnapshot document) {
        final country = _Country(
          name: document.data['name_en'],
          text: document.data['name_en'],
          currency: document.data['currency'],
          dialCode: document.data['dial_code'],
        );

        return country;
      }).toList();
      countries.sort((a, b) => a.text.compareTo(b.text));

      _countries = countries;
    }

    final prefs = await SharedPreferences.getInstance();
    final joinRequestId = prefs.getString('joinRequestId');
    print(joinRequestId);

    if (joinRequestId == null) {
      _joinRequestStatus.complete(_JoinRequestStatus.none);
      return;
    }

    final requestDocument = await Firestore.instance
        .collection('joinRequests')
        .document(joinRequestId)
        .get();
    if (requestDocument == null || !requestDocument.exists) {
      _joinRequestStatus.complete(_JoinRequestStatus.none);
    } else {
      final requestStatusCode = requestDocument.data['request_status'] ?? 0;

      switch (requestStatusCode) {
        case 0:
          _joinRequestStatus.complete(_JoinRequestStatus.none);
          break;

        case 1:
          _joinRequestStatus.complete(_JoinRequestStatus.pending);
          break;

        case 2:
          _joinRequestStatus.complete(_JoinRequestStatus.approved);
          prefs.remove('joinRequestId');
          break;

        case 3:
          _joinRequestStatus.complete(_JoinRequestStatus.declined);
          prefs.remove('joinRequestId');
          break;
      }
    }
  }

  bool _saveForm() {
    final formState = _formKey.currentState;
    formState.save();
    return formState.validate();
  }

  Future<void> _send() async {
    if (_saveForm()) {
      setState(() {
        _isLoading = true;
      });

      final existingAccount = await Firestore.instance
          .collection('users')
          .where('phone_number', isEqualTo: _phoneNumber)
          .getDocuments();
      if (existingAccount != null && existingAccount.documents.isNotEmpty) {
        setState(() {
          _isLoading = false;
        });

        showCupertinoDialog(
          context: context,
          builder: (context) => CupertinoAlertDialog(
            content: Text(S.of(context).phoneNumberExists),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(S.of(context).ok),
              ),
            ],
          ),
        );

        return;
      }

      final reqDocuments = await Firestore.instance
          .collection('joinRequests')
          .where('email', isEqualTo: _email)
          .getDocuments();
      if (reqDocuments != null && reqDocuments.documents.isNotEmpty) {
        setState(() {
          _isLoading = false;
        });

        final prefs = await SharedPreferences.getInstance();
        prefs.setString('joinRequestId', reqDocuments.documents[0].documentID);

        String message;
        switch (reqDocuments.documents[0].data['request_status'] as int) {
          case 0:
          case 1:
            message = S.of(context).alreadySentRequest;
            break;

          case 2:
            message = S.of(context).requestApprovedMessage;
            break;

          case 3:
            message = S.of(context).requestDeclinedMessage;
            break;

          default:
            message = S.of(context).alreadySentRequest;
        }

        showCupertinoDialog(
          context: context,
          builder: (context) => CupertinoAlertDialog(
            content: Text(
              message,
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () {
                  if ((reqDocuments.documents[0].data['request_status']
                          as int) ==
                      2) {
                    Navigator.of(context).pushReplacementNamed('/login');
                  } else {
                    Navigator.of(context).pop();
                  }
                },
                child: Text(S.of(context).ok),
              ),
            ],
          ),
        );

        return;
      }

      final status = {
        _Status.company: 'company',
        _Status.individual: 'individual',
      };

      final formData = {
        'company_name': _companyName,
        'phone_number': _dialCode + _phoneNumber,
        'email': _email,
        'country': _country.name,
        'currency': _country.currency,
        'status': status[_status],
        'request_status': 1,
      };

      final document =
          await Firestore.instance.collection('joinRequests').add(formData);

      setState(() {
        _isLoading = false;
      });

      final prefs = await SharedPreferences.getInstance();
      prefs.setString('joinRequestId', document.documentID);

      showCupertinoDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(S.of(context).success),
          content: Text(
            S.of(context).requestSubmitted,
          ),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pushNamed('/'),
              child: Text(S.of(context).ok),
            ),
          ],
        ),
      );
    }
  }
}

@immutable
class _Tuple<T> {
  final T a;
  final T b;

  const _Tuple(this.a, this.b);
}

enum _Status {
  individual,
  company,
}

enum _JoinRequestStatus {
  none,
  pending,
  approved,
  declined,
}

@immutable
class _Country {
  final String name;
  final String text;
  final String currency;
  final String dialCode;

  _Country(
      {@required this.name,
      @required this.text,
      @required this.currency,
      @required this.dialCode});

  @override
  String toString() => text;
}
