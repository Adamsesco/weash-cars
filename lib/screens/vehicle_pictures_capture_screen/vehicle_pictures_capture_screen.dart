import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/user_role.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/stores/employee_store.dart';
import 'package:weash_cars/util/watermark_image.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';
import 'package:weash_cars/widgets/employee_drawer.dart';

class VehiclePicturesCaptureScreen extends StatefulWidget {
  Map<String, dynamic> order;

  VehiclePicturesCaptureScreen({Key key, this.order}) : super(key: key);

  @override
  _VehiclePicturesCaptureScreenState createState() =>
      _VehiclePicturesCaptureScreenState();
}

class _VehiclePicturesCaptureScreenState
    extends State<VehiclePicturesCaptureScreen> with WidgetsBindingObserver {
  ApplicationStore _application;
  EmployeeStore _employee;
  CompanyStore _company;
  Map<String, dynamic> _order;
  List<File> _imageFiles = [null, null, null, null];
  List<StorageUploadTask> _uploadTasks = [null, null, null, null];
  List<StorageTaskSnapshot> _imageSnapshots = [null, null, null, null];
  List<String> _imageDownloadUrls = [null, null, null, null];
  List<double> _uploadProgress = [0, 0, 0, 0];
  bool _isDialogVisible = false;
  bool _isLoading = false;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void didChangeDependencies() async {
    _application = Provider.of<ApplicationStore>(context);

    switch (_application.userRole) {
      case UserRole.employee:
        _employee = _application.employeeStore;
        break;

      case UserRole.company:
        _company = _application.companyStore;
        break;
      default:
    }
    _order = widget.order;

    if (mounted) {
      setState(() {});
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    _application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => _employee != null
            ? EmployeeDrawer(
                onCloseDrawer: () {
                  setState(() => _isDialogVisible = false);
                },
                avatarUrl: _employee.avatarUrl,
              )
            : _company != null
                ? CompanyDrawer(
                    onCloseDrawer: () {
                      setState(() => _isDialogVisible = false);
                    },
                    avatarUrl: _company.avatarUrl,
                  )
                : Container(),
      ),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Image(
              image:
                  AssetImage('assets/images/employee-screens-background.png'),
              fit: BoxFit.cover,
            ),
          ),
          Positioned.fill(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Observer(
                  builder: (BuildContext context) => CustomAppBar2(
                    onOpenDrawer: () {
                      setState(() {
                        _isDialogVisible = true;
                        Scaffold.of(context).openDrawer();
                      });
                    },
                    title: S.of(context).dashboard,
                    transparentBackground: true,
                    titleColor: Color(0xFFA0AEE6),
                    buttonColor: Color(0xFF0C2461),
                    nbNotifications: 0,
                    avatar: _employee != null
                        ? _employee.avatarUrl == null
                            ? null
                            : NetworkImage(_employee.avatarUrl)
                        : _company != null
                            ? _company.avatarUrl == null
                                ? null
                                : NetworkImage(_company.avatarUrl)
                            : null,
                  ),
                ),
                if (_order == null)
                  Expanded(
                    child: Center(
                      child: CupertinoActivityIndicator(),
                    ),
                  ),
                if (_order != null) ...[
                  SizedBox(height: ScreenUtil().setHeight(61.5)),
                  Align(
                    alignment: _application.locale == 'ar'
                        ? Alignment.topRight
                        : Alignment.topLeft,
                    child: Transform.translate(
                      offset: Offset(
                          ScreenUtil()
                              .setWidth(_application.locale == 'ar' ? -32 : 32),
                          0),
                      child: Text(
                        S.of(context).uploadVehiclePictures,
                        style: TextStyle(
                          fontFamily: _application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Display',
                          fontFamilyFallback: <String>['SF Pro Text'],
                          fontSize: 24,
                          fontWeight: FontWeight.w900,
                          color: Color(0xFFA0AEE6),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(41)),
                  Container(
                    width: ScreenUtil().setWidth(285),
                    height: ScreenUtil().setHeight(285),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            _buildPictureCard(0),
                            _buildPictureCard(1),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            _buildPictureCard(2),
                            _buildPictureCard(3),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(25)),
                  Text(
                    S.of(context).takeAtLeast,
                    style: TextStyle(
                      fontFamily:
                          _application.locale == 'ar' ? 'Cairo' : 'SF Pro Text',
                      fontFamilyFallback: <String>['SF Pro Text'],
                      fontSize: 11,
                      fontWeight: FontWeight.w300,
                      fontStyle: FontStyle.italic,
                      color: Color(0xFF021A89),
                    ),
                  ),
                  // SizedBox(height: ScreenUtil().setHeight(110)),
                  Spacer(),
                  InkWell(
                    onTap: _done,
                    borderRadius: BorderRadius.circular(20),
                    child: Container(
                      width: ScreenUtil().setWidth(116),
                      height: ScreenUtil().setHeight(43),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        gradient: LinearGradient(
                          colors: <Color>[Color(0xff031C8D), Color(0xff0A2FAF)],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                        ),
                      ),
                      child: Center(
                        child: _isLoading
                            ? CupertinoActivityIndicator()
                            : Text(
                                S.of(context).DONE,
                                style: TextStyle(
                                  fontFamily: _application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF UI Display',
                                  fontSize: 11,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xfffcfcfc),
                                ),
                              ),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(40)),
                ],
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPictureCard(int index) {
    final application = Provider.of<ApplicationStore>(context);

    final imageFile = _imageFiles[index];
    final imageSnapshot = _imageSnapshots[index];
    final uploadTask = _uploadTasks[index];

    return InkWell(
      onTap: () {
        if (imageFile == null) {
          _pickImage(index);
        }
      },
      child: Stack(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(130),
            height: ScreenUtil().setHeight(130),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: imageFile == null
                  ? DecorationImage(
                      image: AssetImage('assets/images/dashed-border.png'),
                      fit: BoxFit.fill,
                    )
                  : DecorationImage(
                      image: FileImage(imageFile),
                      fit: BoxFit.cover,
                    ),
            ),
            child: imageFile == null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setHeight(27)),
                      uploadTask == null
                          ? Image(
                              image: AssetImage(
                                  'assets/images/add-picture-icon.png'),
                              width: ScreenUtil().setWidth(50.94),
                              height: ScreenUtil().setHeight(56.31),
                              matchTextDirection: true,
                            )
                          : Image(
                              image: AssetImage(
                                  'assets/images/uploading-icon.png'),
                              width: ScreenUtil().setWidth(50.94),
                              height: ScreenUtil().setHeight(56.31),
                              matchTextDirection: true,
                            ),
                      SizedBox(height: ScreenUtil().setHeight(24.7)),
                      Visibility(
                        visible: uploadTask != null,
                        maintainSize: true,
                        maintainAnimation: true,
                        maintainState: true,
                        child: RotatedBox(
                          quarterTurns: application.locale == 'ar' ? 2 : 0,
                          child: Container(
                            width: ScreenUtil().setWidth(96.93),
                            height: ScreenUtil().setHeight(4),
                            alignment: Alignment.topLeft,
                            decoration: BoxDecoration(
                              color: Color(0xFFEFEFEF),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: FractionallySizedBox(
                              widthFactor: _uploadProgress[index],
                              child: Container(
                                height: ScreenUtil().setHeight(4),
                                decoration: BoxDecoration(
                                  color: Color(0xFF07D970),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(16)),
                    ],
                  )
                : Container(),
          ),
          Visibility(
            visible: _uploadProgress[index] == 1.0,
            child: Positioned(
              right: 0,
              child: Transform.translate(
                offset: Offset(ScreenUtil().setWidth(27.18 / 3),
                    -ScreenUtil().setWidth(27.18 / 3)),
                child: InkWell(
                  onTap: () {
                    // TODO: delete image from firebase storage
                    setState(() {
                      _imageFiles[index] = null;
                      _uploadTasks[index] = null;
                      _imageSnapshots[index] = null;
                      _imageDownloadUrls[index] = null;
                      _uploadProgress[index] = 0;
                    });
                  },
                  child: Image(
                    image: AssetImage('assets/images/remove-picture-icon.png'),
                    width: ScreenUtil().setWidth(27.18),
                    height: ScreenUtil().setWidth(27.18),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _pickImage(int index) async {
    final newImageFile =
        await ImagePicker.pickImage(source: ImageSource.camera);
    if (newImageFile != null) {
      final data = await newImageFile.readAsBytes();
      final completer = new Completer<ui.Image>();

      ui.decodeImageFromList(data, (ui.Image image) {
        completer.complete(image);
      });

      final image = await completer.future;
      final watermarkedImage = new WatermarkImage(
          image: image, waterMark: Text('test'), waterMarkOffset: Offset(0, 0));
      final byteData = await watermarkedImage.byteData;

      final directory = await getApplicationDocumentsDirectory();
      final path = directory.path;
      final file = new File("$path/${_timestamp()}.png");
      await file.writeAsBytes(byteData);

      await _uploadPicture(file, index);
      setState(() {
        _imageFiles[index] = newImageFile;
      });
    }
  }

  Future<void> _done() async {
    if (_order == null) {
      return;
    }

    setState(() {
      _isLoading = true;
    });

    await Firestore.instance
        .collection('orders')
        .document(_order['order_id'])
        .updateData({
      'pictures_urls': _imageDownloadUrls,
      'status': 20,
    });

    setState(() {
      _isLoading = false;
    });

    switch (_application.userRole) {
      case UserRole.employee:
        Navigator.pushNamed(context, '/dashboard/employee');
        break;

      case UserRole.company:
        Navigator.pushNamed(context, '/dashboard/company');
        break;

      default:
    }
  }

  Future<void> _uploadPicture(File picture, int index) async {
    // TODO: make picture accessible only for the employee and the customer.
    if (picture == null) return null;

    final userId =
        _employee != null ? _employee.employeeId : _company.companyId;
    final filename = "$userId.${_timestamp()}";
    final extension = picture.path
        .substring(picture.path.lastIndexOf('.'))
        .replaceAll('.', '');
    final storageRef = FirebaseStorage.instance
        .ref()
        .child('company_images_gallery')
        .child("$filename.$extension");

    final StorageUploadTask uploadTask = storageRef.putFile(
      picture,
      StorageMetadata(
        contentType: "image/$extension",
        customMetadata: <String, String>{
          'type': 'company_images_gallery',
          'user_id': userId,
        },
      ),
    );
    setState(() {
      _uploadTasks[index] = uploadTask;
    });
    uploadTask.events.listen((event) {
      setState(() {
        _uploadProgress[index] = event.snapshot.bytesTransferred.toDouble() /
            event.snapshot.totalByteCount.toDouble();
      });
    }).onError((error) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(error.toString()),
        backgroundColor: Colors.red,
      ));
    });

    final downloadUrl = await uploadTask.onComplete;
    final pictureUrl = await downloadUrl.ref.getDownloadURL();
    setState(() {
      _imageSnapshots[index] = downloadUrl;
      _imageDownloadUrls[index] = pictureUrl;
    });
    // return pictureUrl;
  }

  String _timestamp() => DateTime.now().millisecondsSinceEpoch.toString();
}
