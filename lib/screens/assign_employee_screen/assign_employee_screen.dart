import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/payment_method.dart';
import 'package:weash_cars/models/vehicle_type.dart';
import 'package:weash_cars/screens/company_dashboard_screen/widgets/incomes_chart.dart';
import 'package:weash_cars/screens/take_order_screen/take_order_screen.dart';
import 'package:weash_cars/services/IncomesService.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/balance_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/stores/orders_store.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class AssignEmployeeScreen extends StatefulWidget {
  Map<String, dynamic> order;

  AssignEmployeeScreen({Key key, this.order}) : super(key: key);

  @override
  _AssignEmployeeScreenState createState() => _AssignEmployeeScreenState();
}

class _AssignEmployeeScreenState extends State<AssignEmployeeScreen> {
  CompanyStore _company;
  List<Map<String, dynamic>> _employees;

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  GlobalKey _dropdownButtonKey = new GlobalKey();
  Offset _dropdownButtonOffset = new Offset(0, 0);

  Map<String, dynamic> _selectedEmployee;
  int _selectedEmployeeIndex;
  bool _assignedToMe = false;
  bool _isDialogVisible = false;
  bool _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_company == null) {
      final applicationStore = Provider.of<ApplicationStore>(context);
      _company = applicationStore.companyStore;
      _employees = _company.employees == null
          ? []
          : _company.employees
              .where((employee) => (employee['is_available'] as bool) ?? true)
              .toList();
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CompanyDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _company.avatarUrl,
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image(
                image:
                    AssetImage('assets/images/company-screens-background.png'),
                matchTextDirection: true,
                fit: BoxFit.fill),
          ),
          Positioned(
            top: 0,
            child: Observer(
              builder: (BuildContext context) => CustomAppBar2(
                onOpenDrawer: () {
                  setState(() {
                    _isDialogVisible = true;
                    Scaffold.of(context).openDrawer();
                  });
                },
                title: S.of(context).dashboard,
                transparentBackground: true,
                titleColor: Color(0xFF91A6D9),
                buttonColor: Color(0xFF0930C3),
                nbNotifications: 0,
                avatar: _company.avatarUrl == null
                    ? null
                    : NetworkImage(_company.avatarUrl),
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().setHeight(140),
            bottom: 0,
            left: ScreenUtil().setWidth(30),
            right: ScreenUtil().setWidth(30),
            child: Builder(
              builder: (BuildContext context) {
                final order = widget.order;

                final orderId = order['order_id'];
                final orderStatus = order['status'];
                final orderLocality = order['locality'] ?? ' ';
                final orderAddress = order['address'];
                final orderDate = order['date'] as DateTime;
                final orderNumber = order['order_number'] as int;
                final orderNumberStr =
                    List.generate(6 - orderNumber.toString().length, (_) => '0')
                            .join() +
                        orderNumber.toString();

                final customer = order['customer'];
                final customerAvatarUrl = customer['avatar_url'];
                final customerFirstName = customer['first_name'];
                final customerLastName = customer['last_name'];
                final customerPhoneNumber = customer['phone_number'];

                final package = order['package'];
                final packageTitle = package['title'];
                final packagePrice = package['price'];
                final packageCurrency = _company.currency;
                final vehicleTypes = {
                  "sedan": S.of(context).sedan,
                  "motorcycle": S.of(context).motorcycle,
                  "suv": S.of(context).suv,
                  "boat": S.of(context).boat,
                };
                final vehicleType = vehicleTypes[package['vehicle_type']];

                final paymentMethodStrs = {
                  PaymentMethod.cash: S.of(context).onDelivery,
                  PaymentMethod.visa: S.of(context).online,
                };
                final paymentMethod =
                    paymentMethodStrs[order['payment_method'] as PaymentMethod];

                if (orderStatus >= 10) {
                  final assignEmployeeId = order['assigned_employee_id'];
                  for (var i = 0; i < _employees.length; i++) {
                    final employee = _employees[i];
                    if (employee['employee_id'] == assignEmployeeId) {
                      _selectedEmployeeIndex = i;
                      _selectedEmployee = _employees[i];
                      break;
                    }
                  }
                }

                return ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          width: ScreenUtil().setWidth(64.41),
                          height: ScreenUtil().setWidth(64.41),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: customerAvatarUrl == null
                                  ? AssetImage('assets/images/avatar-user.png')
                                  : NetworkImage(customerAvatarUrl),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(12.8)),
                        Column(
                          children: <Widget>[
                            Text(
                              "$customerFirstName $customerLastName",
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 13,
                                fontWeight: FontWeight.w700,
                                color: Color(0xFF021A89),
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(1)),
                            Text(
                              orderLocality,
                              style: TextStyle(
                                fontFamily: 'SF Pro Text',
                                fontSize: 13,
                                fontWeight: FontWeight.w300,
                                color: Color(0xFF021A89),
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(4)),
                            InkWell(
                              onTap: () {
                                launch("tel://$customerPhoneNumber");
                              },
                              child: Text(
                                customerPhoneNumber,
                                style: TextStyle(
                                  fontFamily: 'SF Pro Text',
                                  fontSize: 13,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xFF021A89),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(33.5)),
                    Text(
                      "${S.of(context).orderN}       N$orderNumberStr",
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Display',
                        fontFamilyFallback: <String>['SF Pro Text'],
                        fontSize: 17,
                        fontWeight: FontWeight.w900,
                        color: Color(0xFF91A6D9),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(22)),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "${S.of(context).location} : ",
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Compact Display',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                                color: Color(0xFF021A89),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                orderAddress,
                                style: TextStyle(
                                  fontFamily: 'SF Compact Display',
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xFF021A89),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(11)),
                        Row(
                          children: <Widget>[
                            Text(
                              S.of(context).vehicleType,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Compact Display',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                                color: Color(0xFF021A89),
                              ),
                            ),
                            Text(
                              vehicleType,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Compact Display',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                color: Color(0xFF021A89),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(11)),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              S.of(context).dateAndTime,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Compact Display',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                                color: Color(0xFF021A89),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                DateFormat('EEEE d MMMM').format(orderDate) +
                                    " ${S.of(context).at} " +
                                    DateFormat('Hm').format(orderDate),
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Compact Display',
                                  fontFamilyFallback: <String>['SF Pro Text'],
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xFF021A89),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(11)),
                        Row(
                          children: <Widget>[
                            Text(
                              S.of(context).package,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Compact Display',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                                color: Color(0xFF021A89),
                              ),
                            ),
                            Text(
                              packageTitle,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Compact Display',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                color: Color(0xFF021A89),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(11)),
                        Row(
                          children: <Widget>[
                            Text(
                              S.of(context).payment,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Compact Display',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                                color: Color(0xFF021A89),
                              ),
                            ),
                            Text(
                              paymentMethod,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Compact Display',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                color: Color(0xFF021A89),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(48)),
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        S.of(context).assignEmployee,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Text',
                          fontFamilyFallback: <String>['SF Pro Text'],
                          fontSize: 13,
                          fontWeight: FontWeight.w800,
                          color: Color(0xFF021A89),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(15)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        InkWell(
                          onTap: _showDropdownItems,
                          key: _dropdownButtonKey,
                          child: Container(
                            width: ScreenUtil().setWidth(285),
                            height: ScreenUtil().setHeight(52),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x44666666),
                                  blurRadius: 10,
                                ),
                              ],
                            ),
                            child: Builder(
                              builder: (BuildContext context) {
                                final hintWidget = Row(
                                  children: <Widget>[
                                    SizedBox(
                                        width: ScreenUtil().setWidth(24.2)),
                                    Image(
                                      image: AssetImage(
                                          'assets/images/non-user-icon.png'),
                                      width: ScreenUtil().setWidth(18.18),
                                      color: Color(0xFFDBDADA),
                                    ),
                                    SizedBox(
                                        width: ScreenUtil().setWidth(24.2)),
                                    Text(
                                      S.of(context).notAssigned,
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'SF Pro Text',
                                        fontSize: 13,
                                        fontWeight: FontWeight.w700,
                                        color: Color(0xFFDBDADA),
                                      ),
                                    ),
                                    Expanded(
                                      child: Align(
                                        alignment: application.locale == 'ar'
                                            ? Alignment.centerLeft
                                            : Alignment.centerRight,
                                        child: Image(
                                          image: AssetImage(
                                              'assets/images/dropdown-button-icon.png'),
                                          width: ScreenUtil().setWidth(11.37),
                                          color: Color(0xFFDBDADA),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                        width: ScreenUtil().setWidth(27.8)),
                                  ],
                                );

                                String avatarUrl;
                                String firstName;
                                String lastName;
                                String employeeType;
                                int jobsDoneCount;

                                if (_assignedToMe) {
                                  avatarUrl = _company.avatarUrl;
                                  firstName = _company.companyName;
                                  lastName = '';
                                  jobsDoneCount = _company.jobsDoneCount;
                                } else {
                                  if (_selectedEmployee != null) {
                                    avatarUrl = _selectedEmployee['avatar_url'];
                                    firstName = _selectedEmployee['first_name'];
                                    lastName = _selectedEmployee['last_name'];
                                    employeeType = _selectedEmployee['type'];
                                    jobsDoneCount =
                                        _selectedEmployee['jobs_done'];
                                  }
                                }

                                final selectedWidget = Container(
                                  width: ScreenUtil().setWidth(272),
                                  height: ScreenUtil().setHeight(52),
                                  margin: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(7),
                                    // vertical: ScreenUtil().setHeight(3),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(16),
                                    // vertical: ScreenUtil().setHeight(5),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                        width: ScreenUtil().setHeight(41),
                                        height: ScreenUtil().setHeight(41),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: avatarUrl == null
                                                ? AssetImage(
                                                    'assets/images/avatar-user.png')
                                                : NetworkImage(avatarUrl),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                          width: ScreenUtil().setWidth(43)),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            firstName == null
                                                ? ' '
                                                : "$firstName $lastName",
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'Helvetica Rounded LT',
                                              fontSize: 13,
                                              fontWeight: FontWeight.w700,
                                              color: Color(0xFF021A89),
                                            ),
                                          ),
                                          Text(
                                            jobsDoneCount == null
                                                ? ' '
                                                : "$jobsDoneCount ${S.of(context).jobsDone}",
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Compact Rounded',
                                              fontSize: 10,
                                              fontWeight: FontWeight.w400,
                                              color: Color(0xFF021A89),
                                            ),
                                          ),
                                        ],
                                      ),
                                      if (employeeType == 'smart') ...[
                                        SizedBox(
                                            width: ScreenUtil().setWidth(30)),
                                        Icon(
                                          Icons.phone_android,
                                          size: ScreenUtil().setHeight(20),
                                          color: Color(0xff03c428),
                                        ),
                                      ],
                                      Expanded(
                                        child: Align(
                                          alignment: application.locale == 'ar'
                                              ? Alignment.centerLeft
                                              : Alignment.centerRight,
                                          child: Image(
                                            image: AssetImage(
                                                'assets/images/checkmark-icon.png'),
                                            width: ScreenUtil().setWidth(10.71),
                                            height:
                                                ScreenUtil().setHeight(7.05),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );

                                if (_selectedEmployee == null &&
                                    !_assignedToMe) {
                                  return hintWidget;
                                }
                                return selectedWidget;
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(20)),
                    Align(
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: _isLoading
                            ? null
                            : () {
                                if (_selectedEmployeeIndex != null ||
                                    _assignedToMe) {
                                  String employeeId;
                                  if (_selectedEmployee != null &&
                                      !_assignedToMe) {
                                    employeeId =
                                        _employees[_selectedEmployeeIndex]
                                            ['employee_id'];
                                  } else {
                                    employeeId = _company.companyId;
                                  }
                                  _assign(orderId, employeeId);
                                }
                              },
                        borderRadius: BorderRadius.circular(20),
                        child: Container(
                          width: ScreenUtil().setWidth(170.16),
                          height: ScreenUtil().setHeight(40.2),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            gradient: LinearGradient(
                              colors: <Color>[
                                Color(0xff031C8D),
                                Color(0xff0A2FAF)
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                            ),
                          ),
                          child: _isLoading
                              ? CupertinoActivityIndicator(
                                  animating: true,
                                )
                              : Text(
                                  S.of(context).proceed,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF UI Display',
                                    fontFamilyFallback: <String>['SF Pro Text'],
                                    fontSize: ScreenUtil().setSp(11),
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xfffcfcfc),
                                  ),
                                ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(20)),
                  ],
                );
              },
            ),
          ),
          Visibility(
            visible: _isDialogVisible,
            child: Positioned(
              top: _dropdownButtonOffset.dy,
              left: _dropdownButtonOffset.dx,
              child: Container(
                width: ScreenUtil().setWidth(285),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x44666666),
                      blurRadius: 10,
                    ),
                  ],
                ),
                child: LimitedBox(
                  maxWidth: ScreenUtil().setWidth(285),
                  maxHeight: MediaQuery.of(context).size.height -
                      _dropdownButtonOffset.dy -
                      ScreenUtil().setHeight(30),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      InkWell(
                        onTap: _showDropdownItems,
                        child: SizedBox(
                          width: ScreenUtil().setWidth(285),
                          height: ScreenUtil().setHeight(52),
                          child: Row(
                            children: <Widget>[
                              SizedBox(width: ScreenUtil().setWidth(24.2)),
                              Image(
                                image: AssetImage(
                                    'assets/images/non-user-icon.png'),
                                width: ScreenUtil().setWidth(18.18),
                                color: Color(0xFFDBDADA),
                              ),
                              SizedBox(width: ScreenUtil().setWidth(24.2)),
                              Text(
                                S.of(context).notAssigned,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontSize: 13,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xFFDBDADA),
                                ),
                              ),
                              Expanded(
                                child: Align(
                                  alignment: application.locale == 'ar'
                                      ? Alignment.centerLeft
                                      : Alignment.centerRight,
                                  child: Image(
                                    image: AssetImage(
                                        'assets/images/dropdown-button-icon.png'),
                                    width: ScreenUtil().setWidth(11.37),
                                    color: Color(0xFFDBDADA),
                                  ),
                                ),
                              ),
                              SizedBox(width: ScreenUtil().setWidth(27.8)),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(12)),
                      Flexible(
                        fit: FlexFit.loose,
                        child: ListView.builder(
                          scrollDirection: Axis.vertical,
                          padding: EdgeInsets.all(0),
                          shrinkWrap: true,
                          itemCount: _employees.length + 1,
                          itemBuilder: (BuildContext context, int index) {
                            Map<String, dynamic> employee;
                            String employeeId;
                            String employeeAvatarUrl;
                            String employeeFirstName;
                            String employeeLastName;
                            String employeeType;
                            int jobsDoneCount;

                            if (index == 0) {
                              employeeId = _company.companyId;
                              employeeAvatarUrl = _company.avatarUrl;
                              employeeFirstName = _company.companyName;
                              employeeLastName = '';
                              jobsDoneCount = _company.jobsDoneCount;
                            } else {
                              employee = _employees[index - 1];
                              employeeId = employee['employee_id'];
                              employeeAvatarUrl = employee['avatar_url'];
                              employeeFirstName = employee['first_name'];
                              employeeLastName = employee['last_name'];
                              employeeType = employee['type'];
                              jobsDoneCount = employee['jobs_done'] as int;
                            }

                            return InkWell(
                              onTap: () => setState(() {
                                if (index == 0) {
                                  _assignedToMe = true;
                                  _selectedEmployeeIndex = null;
                                  _selectedEmployee = null;
                                  _isDialogVisible = false;
                                } else {
                                  _assignedToMe = false;
                                  _selectedEmployeeIndex = index - 1;
                                  _selectedEmployee = employee;
                                  _isDialogVisible = false;
                                }
                              }),
                              child: Container(
                                width: ScreenUtil().setWidth(272),
                                margin: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(7),
                                  vertical: ScreenUtil().setHeight(3),
                                ),
                                padding: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(16),
                                  vertical: ScreenUtil().setHeight(5),
                                ),
                                decoration: index - 1 ==
                                            _selectedEmployeeIndex ||
                                        index == 0 && _assignedToMe
                                    ? BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Color(0x44888888),
                                            blurRadius: 10,
                                          ),
                                        ],
                                      )
                                    : null,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    CircleAvatar(
                                        radius: ScreenUtil().setWidth(41) / 2,
                                        backgroundImage: employeeAvatarUrl ==
                                                null
                                            ? AssetImage(
                                                'assets/images/avatar-user.png')
                                            : NetworkImage(employeeAvatarUrl),
                                        backgroundColor: Colors.transparent),
                                    SizedBox(width: ScreenUtil().setWidth(41)),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "$employeeFirstName $employeeLastName",
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Text',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: 13,
                                            fontWeight: FontWeight.w700,
                                            color: Color(0xFF021A89),
                                          ),
                                        ),
                                        Text(
                                          "$jobsDoneCount ${S.of(context).jobsDone}",
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Text',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: 10,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xFF021A89),
                                          ),
                                        ),
                                      ],
                                    ),
                                    if (employeeType == 'smart') ...[
                                      SizedBox(
                                          width: ScreenUtil().setWidth(30)),
                                      Icon(
                                        Icons.phone_android,
                                        size: ScreenUtil().setHeight(20),
                                        color: Color(0xff03c428),
                                      ),
                                    ],
                                    Expanded(
                                      child: Align(
                                        alignment: application.locale == 'ar'
                                            ? Alignment.centerLeft
                                            : Alignment.centerRight,
                                        child: Visibility(
                                          visible: index - 1 ==
                                                  _selectedEmployeeIndex ||
                                              index == 0 && _assignedToMe,
                                          maintainSize: true,
                                          maintainAnimation: true,
                                          maintainState: true,
                                          child: Image(
                                            image: AssetImage(
                                                'assets/images/checkmark-icon.png'),
                                            width: ScreenUtil().setWidth(10.71),
                                            height:
                                                ScreenUtil().setHeight(7.05),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(12)),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _showDropdownItems() {
    final RenderBox renderBox =
        _dropdownButtonKey.currentContext.findRenderObject();
    final position = renderBox.localToGlobal(Offset.zero);
    setState(() {
      _dropdownButtonOffset = position;
      _isDialogVisible = !_isDialogVisible;
    });
  }

  Future<void> _assign(String orderId, String employeeId) async {
    setState(() {
      _isLoading = true;
    });

    await Firestore.instance.collection('orders').document(orderId).updateData({
      'status': 10,
      'assigned_employee_id': employeeId,
    });

    setState(() {
      _isLoading = false;
    });

    if (_assignedToMe) {
      Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => TakeOrderScreen(
            order: widget.order,
          ),
        ),
      );
    } else {
      final employee = _employees[_selectedEmployeeIndex];

      final theEmployee = S.of(context).theEmployee;
      final isNowAssigned = S.of(context).isNowAssigned;

      await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(S.of(context).success),
          content: RichText(
            text: TextSpan(
              style: TextStyle(
                color: Colors.black,
              ),
              children: [
                TextSpan(text: theEmployee),
                TextSpan(
                  text: "${employee['first_name']} ${employee['last_name']}",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                TextSpan(text: isNowAssigned),
              ],
            ),
          ),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).ok),
            ),
          ],
        ),
      );

      Navigator.pushNamed(context, '/dashboard/company');
    }
  }
}
