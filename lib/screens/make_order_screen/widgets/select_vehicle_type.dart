import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:weash_cars/models/vehicle_type.dart';

typedef void OnSelected(VehicleType vehicleType, DateTime selectedDate);

class SelectVehicleType extends StatefulWidget {
  final OnSelected _onSelected;

  SelectVehicleType(this._onSelected);

  @override
  _SelectVehicleTypeState createState() => _SelectVehicleTypeState();
}

class _SelectVehicleTypeState extends State<SelectVehicleType> {
  VehicleType _vehicleType;
  DateTime _firstDate;
  DateTime _lastDate;
  DateTime _selectedDate;
  TimeOfDay _selectedTime;

  @override
  void initState() {
    super.initState();

    final now = DateTime.now();
    _firstDate = new DateTime(now.year, now.month, now.day);
    _lastDate = new DateTime(now.year + 1, now.month, now.day);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: <Widget>[
              FlatButton(
                onPressed: () => setState(() => _vehicleType = VehicleType.car),
                color: _vehicleType == VehicleType.car ? Colors.cyan : null,
                child: Row(
                  children: <Widget>[
                    Icon(Icons.directions_car),
                    const SizedBox(width: 10),
                    Text('CAR'),
                  ],
                ),
              ),
              FlatButton(
                onPressed: () =>
                    setState(() => _vehicleType = VehicleType.motorcycle),
                color:
                    _vehicleType == VehicleType.motorcycle ? Colors.cyan : null,
                child: Row(
                  children: <Widget>[
                    Icon(Icons.motorcycle),
                    const SizedBox(width: 10),
                    Text('MOTORCYCLE'),
                  ],
                ),
              ),
              FlatButton(
                onPressed: () =>
                    setState(() => _vehicleType = VehicleType.truck),
                color: _vehicleType == VehicleType.truck ? Colors.cyan : null,
                child: Row(
                  children: <Widget>[
                    Icon(Icons.directions_bus),
                    const SizedBox(width: 10),
                    Text('TRUCK'),
                  ],
                ),
              ),
              FlatButton(
                onPressed: () => setState(() => _vehicleType = VehicleType.van),
                color: _vehicleType == VehicleType.van ? Colors.cyan : null,
                child: Row(
                  children: <Widget>[
                    Icon(Icons.directions_subway),
                    const SizedBox(width: 10),
                    Text('VAN'),
                  ],
                ),
              ),
              FlatButton(
                onPressed: () =>
                    setState(() => _vehicleType = VehicleType.boat),
                color: _vehicleType == VehicleType.boat ? Colors.cyan : null,
                child: Row(
                  children: <Widget>[
                    Icon(Icons.directions_boat),
                    const SizedBox(width: 10),
                    Text('BOAT'),
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.cyan),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: FlatButton(
                  shape: RoundedRectangleBorder(),
                  splashColor: Colors.transparent,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.calendar_today),
                      const SizedBox(width: 6),
                      Text(
                        _selectedDate == null
                            ? 'Select date'
                            : DateFormat('EEE, d MMM').format(_selectedDate),
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                  onPressed: () async {
                    final selectedDate = await showDatePicker(
                      context: context,
                      initialDate:
                          _selectedDate == null ? _firstDate : _selectedDate,
                      firstDate: _firstDate,
                      lastDate: _lastDate,
                    );

                    if (selectedDate != null && selectedDate != _selectedDate) {
                      setState(() {
                        _selectedDate = selectedDate;
                      });
                    }
                  },
                ),
              ),
              const SizedBox(width: 30),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.cyan),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: FlatButton(
                  shape: RoundedRectangleBorder(),
                  splashColor: Colors.transparent,
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.access_time),
                      const SizedBox(width: 6),
                      Text(
                        _selectedTime == null
                            ? 'Select time'
                            : _selectedTime.format(context),
                        style: TextStyle(fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                  onPressed: () async {
                    final selectedTime = await showTimePicker(
                      context: context,
                      initialTime: _selectedTime == null
                          ? TimeOfDay.now()
                          : _selectedTime,
                    );

                    if (selectedTime != null && selectedTime != _selectedTime) {
                      setState(() {
                        _selectedTime = selectedTime;
                      });
                    }
                  },
                ),
              ),
            ],
          ),
          Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(20),
            ),
            child: FlatButton(
              padding: null,
              onPressed: () {
                if (_vehicleType == null) {
                  _showDialog('Vehicle type', 'Please select a vehicle type');
                  return;
                }
                if (_selectedDate == null) {
                  _showDialog('Date', 'Please select a date');
                  return;
                }
                if (_selectedTime == null) {
                  _showDialog('Time', 'Please select a time');
                  return;
                }

                final selectedDateTime = new DateTime(
                    _selectedDate.year,
                    _selectedDate.month,
                    _selectedDate.day,
                    _selectedTime.hour,
                    _selectedTime.minute);

                widget._onSelected(_vehicleType, selectedDateTime);
              },
              child: Text('Next'),
            ),
          ),
        ],
      ),
    );
  }

  void _showDialog(String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      },
    );
  }
}
