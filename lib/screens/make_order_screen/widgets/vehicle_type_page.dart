import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:quiver/time.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/vehicle_type.dart';
import 'package:weash_cars/stores/application_store.dart';

typedef OnSelect(
    VehicleType vehicleType, DateTime dateTime, bool isFlexibleTiming);

typedef OnBack();

class VehicleTypePage extends StatefulWidget {
  final OnSelect onSelect;
  final OnBack onBack;

  VehicleTypePage({Key key, @required this.onSelect, @required this.onBack})
      : super(key: key);

  @override
  _VehicleTypePageState createState() => _VehicleTypePageState();
}

class _VehicleTypePageState extends State<VehicleTypePage> {
  PageController _vehiclesPageController;
  PageController _daysPageController;
  PageController _monthsPageController;
  PageController _hoursPageController;
  ScrollController _daysScrollController;
  ScrollController _monthsScrollController;
  ScrollController _hoursScrollController;

  int _currentVehicleIndex = 1;
  double _currentVehiclesPageValue = 1.0;
  double _currentDaysPageValue;
  double _currentMonthsPageValue;
  double _currentHoursPageValue;

  List<VehicleType> _vehicleTypes;
  List<String> _vehicleTypeImages;

  int _selectedDay;
  int _selectedMonth;
  int _selectedHour;
  List<int> _monthDays;
  List<String> _months;
  List<String> _hours;

  bool _flexibleTiming = true;

  @override
  void initState() {
    super.initState();

    _vehicleTypes = [
      VehicleType.motorcycle,
      VehicleType.sedan,
      VehicleType.suv,
      VehicleType.boat,
    ];
    _vehicleTypeImages = [
      'assets/images/moto.jpg',
      'assets/images/sedan.jpg',
      'assets/images/suv.jpg',
      'assets/images/yacht.jpg',
    ];

    final now = DateTime.now();
    _selectedDay = now.day - 1;
    _selectedMonth = now.month - 1;
    _selectedHour = now.hour + 1;

    _monthDays = [
      31,
      isLeapYear(_selectedHour) ? 29 : 28,
      31,
      30,
      31,
      30,
      31,
      31,
      30,
      31,
      30,
      31,
    ];
    _hours = List<String>.generate(24, (int index) {
      return (index >= 10 ? index.toString() : "0$index") + ' 00';
    });

    _vehiclesPageController = new PageController(
      viewportFraction: 0.6,
      initialPage: _currentVehicleIndex,
    );
    _vehiclesPageController.addListener(() {
      setState(() => _currentVehiclesPageValue = _vehiclesPageController.page);
    });

    _daysScrollController =
        new FixedExtentScrollController(initialItem: _selectedDay);
    _currentDaysPageValue = _selectedDay.toDouble();
    _daysPageController = new PageController(
      initialPage: _selectedDay,
      viewportFraction: 0.20,
    );
    _daysPageController.addListener(() {
      setState(() => _currentDaysPageValue = _daysPageController.page);
    });

    _monthsScrollController =
        new FixedExtentScrollController(initialItem: _selectedMonth);
    _currentMonthsPageValue = _selectedMonth.toDouble();
    _monthsPageController = new PageController(
      initialPage: _selectedMonth,
      viewportFraction: 0.20,
    );
    _monthsPageController.addListener(() {
      setState(() => _currentMonthsPageValue = _monthsPageController.page);
    });

    _hoursScrollController =
        new FixedExtentScrollController(initialItem: _selectedHour);
    _currentHoursPageValue = _selectedHour.toDouble();
    _hoursPageController = new PageController(
      initialPage: _selectedHour,
      viewportFraction: 0.20,
    );
    _hoursPageController.addListener(() {
      setState(() => _currentHoursPageValue = _hoursPageController.page);
    });
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);
    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    _months = [
      S.of(context).january,
      S.of(context).february,
      S.of(context).march,
      S.of(context).april,
      S.of(context).may,
      S.of(context).june,
      S.of(context).july,
      S.of(context).august,
      S.of(context).september,
      S.of(context).october,
      S.of(context).november,
      S.of(context).december,
    ];

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Positioned.fill(
          child: Image(
            image:
                AssetImage('assets/images/select-vehicle-type-background.png'),
            fit: BoxFit.cover,
            matchTextDirection: true,
          ),
        ),
        Positioned(
          top: ScreenUtil().setHeight(55),
          left: application.locale == 'ar' ? null : ScreenUtil().setWidth(33),
          right: application.locale == 'ar' ? ScreenUtil().setWidth(33) : null,
          child: InkWell(
            onTap: widget.onBack,
            enableFeedback: false,
            child: Container(
              width: ScreenUtil().setWidth(35),
              height: ScreenUtil().setWidth(35),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
              ),
              child: Center(
                child: Image(
                  image: AssetImage('assets/images/back-button-icon.png'),
                  width: ScreenUtil().setWidth(17.39),
                  height: ScreenUtil().setHeight(12.15),
                  matchTextDirection: true,
                ),
              ),
            ),
          ),
        ),
        Positioned(
          top: ScreenUtil().setHeight(63),
          child: Text(
            S.of(context).chooseVehicleType,
            style: TextStyle(
              fontFamily:
                  application.locale == 'ar' ? 'Cairo' : 'SF Pro Dispalay',
              fontSize: 15,
              fontWeight: FontWeight.w900,
              color: Color(0xFF91A6D9),
            ),
          ),
        ),
        Positioned(
          top: ScreenUtil().setHeight(139),
          width: MediaQuery.of(context).size.width,
          height: ScreenUtil().setHeight(185),
          child: PageView.builder(
            controller: _vehiclesPageController,
            physics: BouncingScrollPhysics(),
            reverse: application.locale == 'ar' ? true : false,
            onPageChanged: (int index) =>
                setState(() => _currentVehicleIndex = index),
            itemCount: _vehicleTypeImages.length,
            itemBuilder: (BuildContext context, int index) {
              final imagePath = _vehicleTypeImages[index];
              final isCurrentSlide = index == _currentVehicleIndex;
              return Transform.scale(
                alignment: _currentVehicleIndex > index
                    ? Alignment.centerRight
                    : Alignment.centerLeft,
                scale: isCurrentSlide
                    ? 1.0
                    : 1.5 - (index - _currentVehiclesPageValue).abs(),
                child: Opacity(
                  opacity: isCurrentSlide
                      ? 1.0
                      : 1.5 - (index - _currentVehiclesPageValue).abs(),
                  child: Container(
                    width: ScreenUtil().setWidth(220),
                    height: ScreenUtil().setHeight(193),
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(4),
                      vertical: ScreenUtil().setHeight(4),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: isCurrentSlide
                          ? BorderRadius.circular(14)
                          : BorderRadius.circular(0),
                      border: isCurrentSlide
                          ? Border.all(
                              color: Colors.white,
                            )
                          : null,
                    ),
                    child: ClipRRect(
                      borderRadius: isCurrentSlide
                          ? BorderRadius.circular(14)
                          : BorderRadius.circular(0),
                      child: Image(
                        image: AssetImage(imagePath),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
        Positioned(
          top: ScreenUtil().setHeight(366),
          bottom: ScreenUtil().setHeight(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                S.of(context).pickDateAndTime,
                style: TextStyle(
                  fontFamily:
                      application.locale == 'ar' ? 'Cairo' : 'SF Pro Text',
                  fontSize: 11,
                  fontWeight: FontWeight.w700,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(42)),
              Expanded(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(30),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Flexible(
                        fit: FlexFit.loose,
                        child: CupertinoPicker.builder(
                          scrollController: _daysScrollController,
                          onSelectedItemChanged: (int index) =>
                              setState(() => _selectedDay = index),
                          backgroundColor: Colors.transparent,
                          magnification: 1.08,
                          squeeze: 1.25,
                          itemExtent: 32.0,
                          itemBuilder: (BuildContext context, int index) {
                            final day =
                                (index % _monthDays[_selectedMonth % 12]) + 1;
                            return Container(
                              alignment: application.locale == 'ar'
                                  ? Alignment.centerRight
                                  : Alignment.centerLeft,
                              child: Text(
                                day.toString(),
                                style: TextStyle(
                                  fontFamily: 'SF Pro Text',
                                  fontSize: 21,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white.withOpacity(index ==
                                          _selectedDay
                                      ? 1.0
                                      : min(0.5,
                                          1 / (index - _selectedDay).abs())),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      Flexible(
                        fit: FlexFit.loose,
                        child: CupertinoPicker.builder(
                          scrollController: _monthsScrollController,
                          onSelectedItemChanged: (int index) => setState(() {
                                _selectedMonth = index;
                              }),
                          backgroundColor: Colors.transparent,
                          magnification: 1.08,
                          squeeze: 1.25,
                          itemExtent: 32.0,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              alignment: Alignment.center,
                              child: Text(
                                _months[index % 12],
                                style: TextStyle(
                                  fontFamily: 'SF Pro Text',
                                  fontSize: 21,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white.withOpacity(
                                    index == _selectedMonth
                                        ? 1.0
                                        : min(0.5,
                                            1 / (index - _selectedMonth).abs()),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      Flexible(
                        fit: FlexFit.loose,
                        child: CupertinoPicker.builder(
                          scrollController: _hoursScrollController,
                          onSelectedItemChanged: (int index) =>
                              setState(() => _selectedHour = index),
                          backgroundColor: Colors.transparent,
                          magnification: 1.08,
                          squeeze: 1.25,
                          itemExtent: 32.0,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              alignment: application.locale == 'ar'
                                  ? Alignment.centerLeft
                                  : Alignment.centerRight,
                              child: Text(
                                _hours[index % 24],
                                textDirection: TextDirection.ltr,
                                style: TextStyle(
                                  fontFamily: 'SF Pro Text',
                                  fontSize: 21,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white.withOpacity(
                                    index == _selectedHour
                                        ? 1.0
                                        : min(0.5,
                                            1 / (index - _selectedHour).abs()),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(31.6)),
              Row(
                children: <Widget>[
                  SizedBox(
                    child: Transform.scale(
                      scale: 0.5,
                      alignment: application.locale == 'ar'
                          ? Alignment.centerRight
                          : Alignment.centerLeft,
                      child: CupertinoSwitch(
                        onChanged: (bool value) =>
                            setState(() => _flexibleTiming = value),
                        value: _flexibleTiming,
                        activeColor: Color(0xff00cb65),
                      ),
                    ),
                  ),
                  // SizedBox(width: ScreenUtil().setWidth(10)),
                  Transform.translate(
                    offset: Offset(
                        ScreenUtil()
                            .setWidth(application.locale == 'ar' ? 15 : -15),
                        0),
                    child: Text(
                      S.of(context).flexibleTiming,
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Text',
                        fontSize: 9,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  CircleAvatar(
                    backgroundColor: Colors.white.withOpacity(0.73),
                    radius: ScreenUtil().setWidth(13.24 / 2),
                    child: Text(
                      S.of(context).questionMark,
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'Lucida Grande',
                        fontSize: 8,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF455367),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: ScreenUtil().setHeight(59)),
              Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: () {
                    final vehicleType = _vehicleTypes[_currentVehicleIndex];
                    final dateTime = new DateTime(
                        DateTime.now().year,
                        (_selectedMonth % 12) + 1,
                        (_selectedDay % _monthDays[_selectedMonth % 12]) + 1,
                        _selectedHour % 24);
                    widget.onSelect(vehicleType, dateTime, _flexibleTiming);
                  },
                  borderRadius: BorderRadius.circular(20),
                  child: Container(
                    width: ScreenUtil().setWidth(111),
                    height: ScreenUtil().setHeight(41),
                    decoration: BoxDecoration(
                      border: Border.all(color: Color(0x120d2e00)),
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          color: Colors.black.withOpacity(0.4),
                          offset: Offset(0, ScreenUtil().setHeight(3)),
                          blurRadius: 6,
                        ),
                      ],
                    ),
                    child: Row(
                      children: <Widget>[
                        SizedBox(width: ScreenUtil().setWidth(27)),
                        Text(
                          S.of(context).next,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 11,
                            fontWeight: FontWeight.w700,
                            color: Color(0xFF011e96),
                          ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(27.9)),
                        Image(
                          image: AssetImage('assets/images/next-icon.png'),
                          width: ScreenUtil().setWidth(8),
                          height: ScreenUtil().setHeight(14),
                          matchTextDirection: true,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
