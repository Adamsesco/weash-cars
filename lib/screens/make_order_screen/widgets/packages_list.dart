import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/vehicle_type.dart';
import 'package:weash_cars/stores/application_store.dart';

typedef OnSelect(Map<String, dynamic> company, Map<String, dynamic> package);
typedef OnBack();

class PackagesList extends StatefulWidget {
  final OnSelect onSelect;
  final OnBack onBack;
  final String companyName;
  final String avatarUrl;
  final double rating;
  final int reviewsCount;
  final String locality;
  final String distance;
  final Map<String, dynamic> company;
  final VehicleType vehicleType;

  PackagesList({
    Key key,
    @required this.onSelect,
    @required this.onBack,
    this.companyName,
    this.avatarUrl,
    this.rating,
    this.reviewsCount,
    this.locality,
    this.distance,
    this.company,
    this.vehicleType,
  }) : super(key: key);

  @override
  _PackagesListState createState() => _PackagesListState();
}

class _PackagesListState extends State<PackagesList> {
  int _currentPackageIndex = 0;
  Future<List<Map<String, dynamic>>> _packagesFuture;

  @override
  void initState() {
    super.initState();

    final companyId = widget.company['company_id'];
    _packagesFuture = _getPackages(companyId, widget.vehicleType);
  }

  Future<List<Map<String, dynamic>>> _getPackages(
      String companyId, VehicleType vehicleType) async {
    final vehicleTypeStr =
        vehicleType.toString().replaceFirst(new RegExp(r'^.*\.'), '');

    print(companyId);

    final companyPackagesDocuments = await Firestore.instance
        .collection('packages')
        .where('company_id', isEqualTo: companyId)
        .where('vehicle_type', isEqualTo: vehicleTypeStr)
        .getDocuments();

    if (companyPackagesDocuments == null ||
        companyPackagesDocuments.documents.isEmpty) return null;

    final companyPackages = companyPackagesDocuments.documents
        .map((DocumentSnapshot packageDocument) {
      final package = <String, dynamic>{
        'package_id': packageDocument.documentID,
        'company_id': packageDocument.data['company_id'],
        'title': packageDocument.data['title'],
        'services': List<String>.of(packageDocument.data['services']
                .map((service) => "$service")
                .toList()
                .cast<String>() ??
            []),
        'price': (packageDocument.data['price'] as num).toDouble(),
        'old_price': packageDocument.data['old_price'] == null
            ? null
            : (packageDocument.data['old_price'] as num).toDouble(),
        'duration': packageDocument.data['duration'] == null
            ? null
            : (packageDocument.data['duration'] as int),
        'currency': packageDocument.data['currency'],
        'vehicle_type': vehicleType,
      };

      return package;
    }).toList();

    final packages = List<Map<String, dynamic>>.from(companyPackages)
      ..sort((packag1, package2) {
        return (packag1['price'] as double)
            .compareTo(package2['price'] as double);
      });

    return packages;
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: false)
          ..init(context);

    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned.fill(
            child: Image(
              image: AssetImage('assets/images/select-package-background.png'),
              fit: BoxFit.cover,
              matchTextDirection: true,
            ),
          ),
          Positioned(
            top: ScreenUtil().setHeight(55),
            left: application.locale == 'ar' ? null : ScreenUtil().setWidth(33),
            right:
                application.locale == 'ar' ? ScreenUtil().setWidth(33) : null,
            child: InkWell(
              onTap: () => Navigator.of(context).pop(),
              enableFeedback: false,
              child: Container(
                width: ScreenUtil().setWidth(35),
                height: ScreenUtil().setWidth(35),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                child: Center(
                  child: Image(
                    image: AssetImage('assets/images/back-button-icon.png'),
                    width: ScreenUtil().setWidth(17.39),
                    height: ScreenUtil().setHeight(12.15),
                    matchTextDirection: true,
                  ),
                ),
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().setHeight(65),
            child: Text(
              S.of(context).selectPackage,
              style: TextStyle(
                fontFamily:
                    application.locale == 'ar' ? 'Cairo' : 'SF Pro Display',
                fontSize: 15,
                fontWeight: FontWeight.w900,
                color: Color(0xFF91A6D9),
              ),
            ),
          ),
          Positioned.fill(
            top: ScreenUtil().setHeight(103.4),
            child: FutureBuilder<List<Map<String, dynamic>>>(
              future: _packagesFuture,
              builder: (context, snapshot) {
                if (snapshot.hasError || !snapshot.hasData) {
                  return Container();
                }

                switch (snapshot.connectionState) {
                  case ConnectionState.active:
                  case ConnectionState.waiting:
                    return CupertinoActivityIndicator();
                  case ConnectionState.none:
                    return Container();
                  case ConnectionState.done:
                    final packages = snapshot.data;
                    if (packages == null) {
                      return Container();
                    }

                    final packageDuration =
                        packages[_currentPackageIndex]['duration'] as int;
                    int durationHours;
                    int durationMinutes;
                    if (packageDuration != null) {
                      durationHours = packageDuration ~/ 100;
                      durationMinutes = packageDuration % 100;
                    }

                    return Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(13),
                        vertical: ScreenUtil().setHeight(13.4),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(19),
                        color: Colors.white,
                      ),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: ScreenUtil().setHeight(18.6),
                                horizontal: ScreenUtil().setWidth(26.5),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Image(
                                        image: NetworkImage(widget.avatarUrl),
                                        width: ScreenUtil().setWidth(64),
                                        height: ScreenUtil().setHeight(64),
                                      ),
                                      Spacer(),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            widget.companyName,
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontSize: 19,
                                              fontWeight: FontWeight.w800,
                                              color: Color(0xFF021a89),
                                              decoration: TextDecoration.none,
                                            ),
                                          ),
                                          Row(
                                            children: <Widget>[
                                              StarRating(
                                                size: 9,
                                                starCount: 5,
                                                rating: widget.rating,
                                                color: Color(0xffffc300),
                                                borderColor: Color(0xffffc300),
                                              ),
                                              SizedBox(
                                                  width:
                                                      ScreenUtil().setWidth(7)),
                                              Text(
                                                "${widget.reviewsCount} ${S.of(context).reviews}",
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'SF Pro Text',
                                                  fontSize: 9,
                                                  fontWeight: FontWeight.w500,
                                                  color: Color(0xFF021a89),
                                                  decoration:
                                                      TextDecoration.none,
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                              height:
                                                  ScreenUtil().setHeight(8)),
                                          Row(
                                            children: <Widget>[
                                              Image(
                                                image: AssetImage(
                                                    'assets/images/location-icon.png'),
                                                width: ScreenUtil().setWidth(7),
                                              ),
                                              SizedBox(
                                                  width:
                                                      ScreenUtil().setWidth(5)),
                                              Text(
                                                widget.locality ?? '',
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'SF Pro Text',
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.w700,
                                                  color: Color(0xFF021a89),
                                                  decoration:
                                                      TextDecoration.none,
                                                ),
                                              ),
                                            ],
                                          ),
                                          Text(
                                            widget.distance,
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontSize: 9,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0xFF021a89),
                                              decoration: TextDecoration.none,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Spacer(),
                                    ],
                                  ),
                                  SizedBox(
                                      height: ScreenUtil().setHeight(23.7)),
                                  Container(
                                    height: 1,
                                    color: Color(0xFF808FCC).withOpacity(0.24),
                                  ),
                                  SizedBox(
                                      height: ScreenUtil().setHeight(26.5)),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: List<Widget>.generate(
                                      packages.length,
                                      (int index) => Row(
                                        children: <Widget>[
                                          CircleAvatar(
                                            backgroundColor:
                                                index == _currentPackageIndex
                                                    ? Color(0xff011f99)
                                                    : Color(0xff011f99)
                                                        .withOpacity(0.5),
                                            radius: index ==
                                                    _currentPackageIndex
                                                ? ScreenUtil().setWidth(11) / 2
                                                : ScreenUtil().setWidth(9) / 2,
                                          ),
                                          SizedBox(
                                              width: ScreenUtil().setWidth(10)),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: ScreenUtil().setHeight(21)),
                                  Flexible(
                                    fit: FlexFit.loose,
                                    child: PageView.builder(
                                      onPageChanged: (int index) => setState(
                                          () => _currentPackageIndex = index),
                                      itemCount: packages.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        final packageName =
                                            packages[index]['title'];
                                        final services = List<String>.of(
                                            packages[index]['services']
                                                    .map(
                                                        (service) => "$service")
                                                    .toList()
                                                    .cast<String>() ??
                                                []);
                                        return ListView(
                                          // crossAxisAlignment: CrossAxisAlignment.start,
                                          // mainAxisAlignment:
                                          //     MainAxisAlignment.spaceBetween,
                                          padding: EdgeInsets.zero,
                                          shrinkWrap: true,
                                          children: <Widget>[
                                            Text(
                                              packageName ?? '',
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Pro Text',
                                                fontSize: 24,
                                                fontWeight: FontWeight.w800,
                                                color: Color(0xFF011f99),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                            for (var i = 0;
                                                i < services.length;
                                                i += 2)
                                              Builder(
                                                builder: (context) {
                                                  String service1;
                                                  String service2;

                                                  service1 = services[i];
                                                  if (i <=
                                                      services.length - 2) {
                                                    service2 = services[i + 1];
                                                  }

                                                  return Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 1,
                                                        child: Text(
                                                          "- $service1",
                                                          style: TextStyle(
                                                            fontFamily: application
                                                                        .locale ==
                                                                    'ar'
                                                                ? 'Cairo'
                                                                : 'SF Pro Text',
                                                            fontSize: 12,
                                                            fontWeight:
                                                                FontWeight.w400,
                                                            color: Color(
                                                                0xFF011f99),
                                                            decoration:
                                                                TextDecoration
                                                                    .none,
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                          width: ScreenUtil()
                                                              .setWidth(39)),
                                                      Expanded(
                                                        flex: 1,
                                                        child: service2 == null
                                                            ? Container()
                                                            : Text(
                                                                "- $service2",
                                                                style:
                                                                    TextStyle(
                                                                  fontFamily: application
                                                                              .locale ==
                                                                          'ar'
                                                                      ? 'Cairo'
                                                                      : 'SF Pro Text',
                                                                  fontSize: 12,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  color: Color(
                                                                      0xFF011f99),
                                                                  decoration:
                                                                      TextDecoration
                                                                          .none,
                                                                ),
                                                              ),
                                                      ),
                                                    ],
                                                  );
                                                },
                                              ),
                                          ],
                                        );
                                      },
                                    ),
                                  ),
                                  SizedBox(height: ScreenUtil().setHeight(21)),
                                  if (durationHours != null) ...[
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            "${S.of(context).Duration}:",
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontSize: 14,
                                              fontWeight: FontWeight.w700,
                                              color: Color(0xFF021a89),
                                              decoration: TextDecoration.none,
                                            ),
                                          ),
                                          Text(
                                            "$durationHours ${S.of(context).hourShort} $durationMinutes ${S.of(context).minuteShort}",
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontSize: 11,
                                              fontWeight: FontWeight.w500,
                                              color: Color(0xFF021a89),
                                              decoration: TextDecoration.none,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                        height: ScreenUtil().setHeight(20.7)),
                                  ],
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () => widget.onSelect(
                                widget.company, packages[_currentPackageIndex]),
                            borderRadius: BorderRadius.vertical(
                                bottom: Radius.circular(19)),
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                horizontal: ScreenUtil().setWidth(30),
                                vertical: ScreenUtil().setHeight(11.3),
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.vertical(
                                    bottom: Radius.circular(19)),
                                color: Color(0xFF03C428),
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    S.of(context).bookThis,
                                    style: TextStyle(
                                      fontFamily: 'SF Pro Text',
                                      fontSize: 19,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Builder(
                                    builder: (BuildContext context) {
                                      final package =
                                          packages[_currentPackageIndex];
                                      final price = package['price'] as double;
                                      final nat = price.toInt();
                                      final oldPrice =
                                          package['old_price'] as double;
                                      final currency = package['currency'];

                                      return Column(
                                        children: <Widget>[
                                          Row(
                                            textDirection: TextDirection.ltr,
                                            children: <Widget>[
                                              Text(
                                                nat.toString(),
                                                style: TextStyle(
                                                  fontFamily: 'SF Pro Text',
                                                  fontSize: 45,
                                                  fontWeight: FontWeight.w800,
                                                  color: Colors.white,
                                                ),
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    application.locale == 'ar'
                                                        ? CrossAxisAlignment.end
                                                        : CrossAxisAlignment
                                                            .start,
                                                children: <Widget>[
                                                  Text(
                                                    price
                                                        .toStringAsFixed(3)
                                                        .replaceFirst(
                                                            RegExp(r'.*\.'),
                                                            ''),
                                                    textDirection:
                                                        TextDirection.ltr,
                                                    style: TextStyle(
                                                      fontFamily: 'SF Pro Text',
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                  Text(
                                                    " $currency",
                                                    textDirection:
                                                        TextDirection.ltr,
                                                    style: TextStyle(
                                                      fontFamily:
                                                          application.locale ==
                                                                  'ar'
                                                              ? 'Cairo'
                                                              : 'SF Pro Text',
                                                      fontSize: 15,
                                                      fontWeight:
                                                          FontWeight.w800,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                          if (oldPrice != null) ...[
                                            Text(
                                              "${oldPrice.toStringAsFixed(3)} $currency",
                                              style: TextStyle(
                                                fontFamily: 'SF Pro Text',
                                                fontSize: 15,
                                                fontWeight: FontWeight.w800,
                                                color: Colors.white,
                                                decoration:
                                                    TextDecoration.lineThrough,
                                                decorationThickness: 2,
                                                decorationColor: Colors.white,
                                              ),
                                            ),
                                          ],
                                        ],
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                }

                return Container();
              },
            ),
          ),
        ],
      ),
    );
  }
}
