import 'package:flutter/material.dart';

typedef void OnSelected(int index);

class SelectArea extends StatelessWidget {
  final List<Map<String, String>> _areas;
  final OnSelected _onSelected;

  SelectArea(this._areas, this._onSelected);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _areas.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          margin: EdgeInsets.all(4),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
            ),
          ),
          child: FlatButton(
            onPressed: () => _onSelected(index),
            highlightColor: Colors.cyan,
            splashColor: Colors.cyan,
            child: Text(_areas[index]['name'], style: TextStyle(color: Colors.grey[700]),),
          ),
        );
      },
    );
  }
}
