import 'package:flutter/material.dart';
import 'package:flutter_rating/flutter_rating.dart';

typedef void OnSelected(int companyIndex, int packageIndex);

class SelectCompanyAndPackage extends StatelessWidget {
  final List<Map<String, dynamic>> _companies;
  final OnSelected _onSelected;

  SelectCompanyAndPackage(this._companies, this._onSelected);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _companies.length,
      itemBuilder: (BuildContext context, int companyIndex) {
        final companyName = _companies[companyIndex]['company_name'].toString();
        final avatarUrl = _companies[companyIndex]['avatar_url'].toString();
        final coverPhotoUrl = _companies[companyIndex]['cover_photo_url'].toString();
        final rating = _companies[companyIndex]['rating'].toDouble() as double;
        final packages =
            _companies[companyIndex]['packages'] as List<Map<String, dynamic>>;

        return Container(
          margin: const EdgeInsets.symmetric(vertical: 20),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
          ),
          child: Column(
            children: <Widget>[
              Column(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(coverPhotoUrl),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          top: 0,
                          right: 0,
                          bottom: 0,
                          left: 0,
                          child: Container(
                            color: Color(0x88000000),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              CircleAvatar(
                                backgroundImage: NetworkImage(avatarUrl),
                                radius: 32,
                              ),
                              const SizedBox(height: 10),
                              Text(
                                companyName,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              const SizedBox(height: 10),
                              StarRating(
                                size: 16,
                                starCount: 5,
                                rating: rating,
                                color: Colors.yellow,
                                borderColor: Colors.yellow,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      children: packages
                          .asMap()
                          .map((int packageIndex, Map<String, dynamic> package) {
                            final packageTitle = package['title'];
                            final packageDescription = package['description'];
                            final packagePrice = package['price'];
                            final packageCurrency = package['currency'];

                            return MapEntry(
                                packageIndex,
                                FlatButton(
                                  onPressed: () => _onSelected(companyIndex, packageIndex),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            packageTitle,
                                            style: TextStyle(
fontFamilyFallback: <String>[ 'SF Pro Text' ], fontSize: 20,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Text(
                                                "$packagePrice $packageCurrency",
                                                style: TextStyle(
                                                  color: Colors.blue,
fontFamilyFallback: <String>[ 'SF Pro Text' ], fontSize: 14,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 8),
                                      Text(
                                        packageDescription,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontWeight: FontWeight.normal,
fontFamilyFallback: <String>[ 'SF Pro Text' ], fontSize: 14,
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                    ],
                                  ),
                                ));
                          })
                          .values
                          .toList(),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
