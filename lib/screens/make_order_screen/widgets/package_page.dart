import 'dart:math';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_maps_webservice/distance.dart' as distanceApi;
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/dist_duration.dart';
import 'package:weash_cars/models/vehicle_type.dart';
import 'package:weash_cars/screens/make_order_screen/widgets/packages_list.dart';
import 'package:weash_cars/services/CompaniesService.dart';
import 'package:weash_cars/stores/application_store.dart';

typedef OnSelect(Map<String, dynamic> company, Map<String, dynamic> package);
typedef OnBack();

class PackagePage extends StatefulWidget {
  final String locality;
  final String country;
  final VehicleType vehicleType;
  final DateTime dateTime;
  final GeoPoint location;
  final bool flexibleTiming;
  final OnSelect onSelect;
  final OnBack onBack;

  PackagePage(
      {Key key,
      @required this.locality,
      @required this.country,
      @required this.vehicleType,
      @required this.dateTime,
      @required this.location,
      @required this.flexibleTiming,
      @required this.onSelect,
      @required this.onBack})
      : super(key: key);

  @override
  _PackagePageState createState() => _PackagePageState();
}

class _PackagePageState extends State<PackagePage> {
  final distanceApi.GoogleDistanceMatrix _distanceApi =
      new distanceApi.GoogleDistanceMatrix(
          apiKey: 'AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I');

  Future<List<Map<String, dynamic>>> _companies;
  Future<Map<String, dynamic>> _adFuture;
  Map<String, dynamic> _selectedCompany;
  Map<String, dynamic> _selectedPackage;
  String _vehicleTypeStr;

  double _blurValue = 0.0;
  bool _isDialogVisible = false;
  _Filter _selectedFilter = _Filter.jobsDoneDesc;

  int _currentPackageIndex = 0;

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    _vehicleTypeStr =
        widget.vehicleType.toString().replaceFirst(new RegExp(r'^.*\.'), '');
  }

  @override
  void dispose() {
    _distanceApi.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);
    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: false)
          ..init(context);

    if (_companies == null) {
      _companies = _getCompanies();
    }
    if (_adFuture == null) {
      _adFuture = _getAd();
    }

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Positioned.fill(
          child: Image(
            image: AssetImage('assets/images/select-package-background.png'),
            fit: BoxFit.cover,
            matchTextDirection: true,
          ),
        ),
        Positioned(
          top: ScreenUtil().setHeight(55),
          left: application.locale == 'ar' ? null : ScreenUtil().setWidth(33),
          right: application.locale == 'ar' ? ScreenUtil().setWidth(33) : null,
          child: InkWell(
            onTap: widget.onBack,
            enableFeedback: false,
            child: Container(
              width: ScreenUtil().setWidth(35),
              height: ScreenUtil().setWidth(35),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
              ),
              child: Center(
                child: Image(
                  image: AssetImage('assets/images/back-button-icon.png'),
                  width: ScreenUtil().setWidth(17.39),
                  height: ScreenUtil().setHeight(12.15),
                  matchTextDirection: true,
                ),
              ),
            ),
          ),
        ),
        Positioned(
          top: ScreenUtil().setHeight(65),
          child: Text(
            S.of(context).selectCompany,
            style: TextStyle(
              fontFamily:
                  application.locale == 'ar' ? 'Cairo' : 'SF Pro Display',
              fontSize: 15,
              fontWeight: FontWeight.w900,
              color: Color(0xFF91A6D9),
            ),
          ),
        ),
        FutureBuilder(
          future: _companies,
          builder: (BuildContext context,
              AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.active:
              case ConnectionState.waiting:
              case ConnectionState.none:
                return Container();
              case ConnectionState.done:
                final companies = snapshot.data;

                if (companies == null || companies.isEmpty) {
                  return Container();
                }

                return Positioned(
                  top: ScreenUtil().setHeight(126),
                  right: application.locale == 'ar'
                      ? null
                      : ScreenUtil().setWidth(32),
                  left: application.locale == 'ar'
                      ? ScreenUtil().setWidth(32)
                      : null,
                  child: Row(
                    children: <Widget>[
                      Visibility(
                        visible: false,
                        child: Text(
                          S.of(context).filters,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'Helvetica Rounded LT',
                            fontSize: 11,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(16)),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () async {
                            final filter =
                                await _showFiltersDialog(_selectedFilter);
                            if (filter != null) {
                              setState(() {
                                _selectedFilter = filter;

                                switch (filter) {
                                  case _Filter.jobsDoneDesc:
                                    companies.sort((company1, company2) {
                                      final jobsDone1 =
                                          company1['jobs_done'] as int ?? 0;
                                      final jobsDone2 =
                                          company2['jobs_done'] as int ?? 0;
                                      return jobsDone1.compareTo(jobsDone2);
                                    });
                                    break;

                                  case _Filter.distance:
                                    companies.sort((company1, company2) {
                                      final distance1 = company1['distance']
                                              as DistDuration ??
                                          DistDuration(
                                              0,
                                              0,
                                              application.locale == 'ar'
                                                  ? TextDirection.rtl
                                                  : TextDirection.ltr);
                                      final distance2 = company2['distance']
                                              as DistDuration ??
                                          DistDuration(
                                              0,
                                              0,
                                              application.locale == 'ar'
                                                  ? TextDirection.rtl
                                                  : TextDirection.ltr);
                                      return distance1.distance
                                          .compareTo(distance2.distance);
                                    });
                                    break;

                                  case _Filter.rating:
                                    companies.sort((company1, company2) {
                                      final rating1 =
                                          company1['rating'] as double ?? 0.0;
                                      final rating2 =
                                          company2['rating'] as double ?? 0.0;
                                      return rating1.compareTo(rating2);
                                    });
                                    break;

                                  case _Filter.priceAsc:
                                    companies.sort((company1, company2) {
                                      final price1 = (company1['min_prices']
                                                  [_vehicleTypeStr] as num)
                                              ?.toDouble() ??
                                          0.0;
                                      final price2 = (company2['min_prices']
                                                  [_vehicleTypeStr] as num)
                                              ?.toDouble() ??
                                          0.0;
                                      return price1.compareTo(price2);
                                    });
                                    break;

                                  case _Filter.priceDesc:
                                    companies.sort((company1, company2) {
                                      final price1 = (company1['min_prices']
                                                  [_vehicleTypeStr] as num)
                                              ?.toDouble() ??
                                          0.0;
                                      final price2 = (company2['min_prices']
                                                  [_vehicleTypeStr] as num)
                                              ?.toDouble() ??
                                          0.0;
                                      return price2.compareTo(price1);
                                    });
                                    break;
                                }
                              });
                            }
                          },
                          child: Image(
                            image: AssetImage('assets/images/filters-icon.png'),
                            width: ScreenUtil().setWidth(27),
                            height: ScreenUtil().setHeight(23),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
            }

            return Container();
          },
        ),
        FutureBuilder(
          future: _companies,
          builder: (BuildContext context,
              AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
              case ConnectionState.active:
                return CupertinoActivityIndicator();
              case ConnectionState.none:
                return Center(
                  child: Text(
                    S.of(context).noResult,
                    style: TextStyle(
                      fontFamily:
                          application.locale == 'ar' ? 'Cairo' : 'SF Pro Text',
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Colors.white.withOpacity(0.9),
                    ),
                  ),
                );
              case ConnectionState.done:
                if (snapshot.hasError ||
                    !snapshot.hasData ||
                    snapshot.data == null ||
                    snapshot.data.isEmpty) {
                  return Center(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(40),
                      ),
                      child: Text(
                        S.of(context).noResult,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Text',
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.white.withOpacity(0.9),
                        ),
                      ),
                    ),
                  );
                }

                final allCompanies = snapshot.data;
                final availableCompanies = allCompanies.where(
                    (company) => company['is_available'] as bool ?? true);
                final unAvailableCompanies = allCompanies.where(
                    (company) => !(company['is_available'] as bool ?? true));
                final zip = [...availableCompanies, ...unAvailableCompanies];

                final companies = zip
                    .where((company) => company['is_sponsored'] != true)
                    .toList();

                return Positioned(
                  top: ScreenUtil().setHeight(174),
                  bottom: ScreenUtil().setHeight(34),
                  child: Column(
                    children: <Widget>[
                      FutureBuilder<Map<String, dynamic>>(
                        future: _adFuture,
                        builder: (context, snapshot) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.none:
                            case ConnectionState.waiting:
                            case ConnectionState.active:
                              return Container();
                            case ConnectionState.done:
                              if (snapshot.hasError ||
                                  !snapshot.hasData ||
                                  snapshot.data == null) {
                                return Container();
                              }

                              final sponsoredCompany = snapshot.data;
                              return Transform.translate(
                                offset: Offset(0, ScreenUtil().setHeight(-11)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Transform.translate(
                                      offset: Offset(
                                          ScreenUtil().setWidth(
                                              application.locale == 'ar'
                                                  ? 17
                                                  : -17),
                                          0),
                                      child: Text(
                                        S.of(context).sponsored,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Compact Display',
                                          fontSize: 13,
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFFE4E4E4)
                                              .withOpacity(0.26),
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: ScreenUtil().setHeight(2)),
                                    InkWell(
                                      onTap: sponsoredCompany['link'] == null
                                          ? null
                                          : () async {
                                              final link =
                                                  sponsoredCompany['link'];
                                              final canlaunch =
                                                  await canLaunch(link);
                                              if (canlaunch) {
                                                launch(link);
                                              }
                                            },
                                      child: Container(
                                        width: ScreenUtil().setWidth(344),
//                                        height: ScreenUtil().setHeight(82),
                                        padding: EdgeInsets.all(3),
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                            image: AssetImage(
                                                'assets/images/sponsored-banner-dashed-border.png'),
                                            fit: BoxFit.fitWidth,
                                          ),
                                        ),
                                        child: Image(
                                          image: NetworkImage(
                                              sponsoredCompany['banner_url']),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                        height: ScreenUtil().setHeight(11)),
                                  ],
                                ),
                              );
                              break;
                          }

                          return Container();
                        },
                      ),
                      Expanded(
                        child: SizedBox(
                          width: ScreenUtil().setWidth(312),
                          child: ListView.separated(
                            padding: EdgeInsets.zero,
                            separatorBuilder: (BuildContext context,
                                    int index) =>
                                SizedBox(height: ScreenUtil().setHeight(11)),
                            itemCount: companies == null ? 0 : companies.length,
                            itemBuilder: (BuildContext context, int index) {
                              final company = companies[index];

                              final avatarUrl = company['avatar_url'];
                              final companyName = company['company_name'];
                              final rating = company['rating'] as double;
                              final reviewsCount =
                                  (company['total_reviews'] as int) ?? 0;
                              final companyLocality = company['locality'];
                              final distance =
                                  company['distance'] as DistDuration;
                              final minPrice = (company['min_prices']
                                          [_vehicleTypeStr] as num)
                                      .toDouble() ??
                                  0.0;
                              final isAvailable =
                                  company['is_available'] as bool ?? true;

                              return InkWell(
                                onTap: isAvailable
                                    ? () {
                                        setState(() {
                                          _selectedCompany = company;
                                        });
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    PackagesList(
                                                      onSelect: widget.onSelect,
                                                      onBack: widget.onBack,
                                                      companyName: companyName,
                                                      avatarUrl: avatarUrl,
                                                      rating: rating,
                                                      reviewsCount:
                                                          reviewsCount,
                                                      locality: companyLocality,
                                                      distance:
                                                          distance.toString(),
                                                      company: company,
                                                      vehicleType:
                                                          widget.vehicleType,
                                                    )));
                                      }
                                    : null,
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                        vertical: ScreenUtil().setHeight(11),
                                        horizontal: ScreenUtil().setWidth(15),
                                      ),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Image(
                                            image: NetworkImage(avatarUrl),
                                            width: ScreenUtil().setWidth(64),
                                            height: ScreenUtil().setHeight(64),
                                          ),
                                          Spacer(
                                            flex: 2,
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                companyName,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'SF Pro Text',
                                                  fontSize: 19,
                                                  fontWeight: FontWeight.w800,
                                                  color: Color(0xFF021a89),
                                                ),
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  StarRating(
                                                    size: 9,
                                                    starCount: 5,
                                                    rating: rating,
                                                    color: Color(0xffffc300),
                                                    borderColor:
                                                        Color(0xffffc300),
                                                  ),
                                                  SizedBox(
                                                      width: ScreenUtil()
                                                          .setWidth(7)),
                                                  Text(
                                                    "$reviewsCount ${S.of(context).reviews}",
                                                    style: TextStyle(
                                                      fontFamily:
                                                          application.locale ==
                                                                  'ar'
                                                              ? 'Cairo'
                                                              : 'SF Pro Text',
                                                      fontSize: 9,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      color: Color(0xFF021a89),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                  height: ScreenUtil()
                                                      .setHeight(10)),
                                              if (companyLocality != null)
                                                Row(
                                                  children: <Widget>[
                                                    Image(
                                                      image: AssetImage(
                                                          'assets/images/location-icon.png'),
                                                      width: ScreenUtil()
                                                          .setWidth(7),
                                                    ),
                                                    SizedBox(
                                                        width: ScreenUtil()
                                                            .setWidth(5)),
                                                    Text(
                                                      companyLocality ?? '',
                                                      style: TextStyle(
                                                        fontFamily: application
                                                                    .locale ==
                                                                'ar'
                                                            ? 'Cairo'
                                                            : 'SF Pro Text',
                                                        fontSize: 10,
                                                        fontWeight:
                                                            FontWeight.w700,
                                                        color:
                                                            Color(0xFF021a89),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              Text(
                                                distance.toString(),
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'SF Pro Text',
                                                  fontSize: 9,
                                                  fontWeight: FontWeight.w500,
                                                  color: Color(0xFF021a89),
                                                ),
                                              ),
                                              SizedBox(
                                                  height: ScreenUtil()
                                                      .setHeight(7)),
                                              RichText(
                                                textAlign: TextAlign.center,
                                                text: TextSpan(
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                      text: S
                                                          .of(context)
                                                          .startsFrom,
                                                      style: TextStyle(
                                                        fontFamily: application
                                                                    .locale ==
                                                                'ar'
                                                            ? 'Cairo'
                                                            : 'SF Pro Text',
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        color:
                                                            Color(0xFF021a89),
                                                      ),
                                                    ),
                                                    TextSpan(text: ' '),
                                                    TextSpan(
                                                      text:
                                                          "$minPrice ${company['currency']}",
                                                      style: TextStyle(
                                                        fontFamily: application
                                                                    .locale ==
                                                                'ar'
                                                            ? 'Cairo'
                                                            : 'SF Pro Text',
                                                        fontSize: 12,
                                                        fontWeight:
                                                            FontWeight.w800,
                                                        color:
                                                            Color(0xFF021a89),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          Spacer(
                                            flex: 1,
                                          ),
                                          Padding(
                                            padding: EdgeInsets.symmetric(
                                              horizontal:
                                                  ScreenUtil().setWidth(6),
                                              vertical:
                                                  ScreenUtil().setHeight(4),
                                            ),
                                            child: Image(
                                              image: AssetImage(
                                                  'assets/images/next-icon.png'),
                                              width: ScreenUtil().setWidth(12),
                                              matchTextDirection: true,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    if (!isAvailable)
                                      Positioned.fill(
                                        child: Container(
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                            color:
                                                Colors.black.withOpacity(0.40),
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.black
                                                      .withOpacity(0.8),
                                                  blurRadius: 60,
                                                ),
                                              ],
                                            ),
                                            child: Text(
                                              S.of(context).busy.toUpperCase(),
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Pro Text',
                                                fontSize: 20,
                                                fontWeight: FontWeight.w800,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                );
            }

            return Container();
          },
        ),
        // Visibility(
        //   visible: _isLoading,
        //   child: CupertinoActivityIndicator(
        //     animating: true,
        //   ),
        // ),
        Visibility(
          visible: _isDialogVisible,
          child: Positioned.fill(
            child: BackdropFilter(
              // filter: ImageFilter.blur(sigmaX: _blurValue, sigmaY: _blurValue),
              filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
              child: Container(
                color: Colors.grey.withOpacity(0.2),
              ),
            ),
          ),
        ),
      ],
    );
  }

  _showPackagesDialog(
      String companyName,
      String avatarUrl,
      double rating,
      int reviewsCount,
      String locality,
      String distance,
      Map<String, dynamic> company,
      List<Map<String, dynamic>> packages) {
    _currentPackageIndex = 0;

    showGeneralDialog(
        context: context,
        barrierDismissible: false,
        transitionDuration: Duration(milliseconds: 200),
        pageBuilder: (BuildContext context, Animation<double> animation1,
            Animation<double> animation2) {
          animation1.addListener(() {
            setState(() {
              _blurValue = animation1.value * 5;
              if (animation1.value > 0) {
                _isDialogVisible = true;
              }
              if (animation1.value == 0) {
                _isDialogVisible = false;
              }
            });
          });
          return _PackagesListDialog(
              companyName,
              avatarUrl,
              rating,
              reviewsCount,
              locality,
              distance,
              company,
              packages,
              widget.onSelect);
        });
  }

  Future<_Filter> _showFiltersDialog(_Filter selectedFilter) {
    return showGeneralDialog<_Filter>(
        context: context,
        barrierDismissible: true,
        barrierLabel: '',
        transitionDuration: Duration(milliseconds: 200),
        pageBuilder: (BuildContext context, Animation<double> animation1,
            Animation<double> animation2) {
          animation1.addListener(() {
            setState(() {
              if (animation1.value > 0) {
                _isDialogVisible = true;
              }
              if (animation1.value == 0) {
                _isDialogVisible = false;
              }
            });
          });
          return ValueListenableBuilder(
            valueListenable: animation1,
            builder: (BuildContext context, double animationValue, _) =>
                _FiltersDialog(selectedFilter, animationValue),
          );
        });
  }

  Future<List<Map<String, dynamic>>> _getCompanies() async {
    setState(() {
      _isLoading = true;
    });

    final companies = await CompaniesService.getCompanies(
        widget.location,
        30,
        widget.vehicleType,
        widget.dateTime,
        widget.flexibleTiming,
        Directionality.of(context));

    if (companies != null && companies.isNotEmpty) {
      companies.sort((company1, company2) {
        final jobsDone1 = company1['jobs_done'] as int ?? 0;
        final jobsDone2 = company2['jobs_done'] as int ?? 0;
        return jobsDone2.compareTo(jobsDone1);
      });
    }

    setState(() {
      _isLoading = false;
    });

    return companies;
  }

  Future<Map<String, dynamic>> _getAd() async {
    final adFuture = CompaniesService.getAd(
        GeoPoint(widget.location.latitude, widget.location.longitude));
    return adFuture;
  }
}

class _PackagesListDialog extends StatefulWidget {
  final String companyName;
  final String avatarUrl;
  final double rating;
  final int reviewsCount;
  final String locality;
  final String distance;
  final Map<String, dynamic> company;
  final List<Map<String, dynamic>> packages;
  final OnSelect onSelect;

  _PackagesListDialog(
      this.companyName,
      this.avatarUrl,
      this.rating,
      this.reviewsCount,
      this.locality,
      this.distance,
      this.company,
      this.packages,
      this.onSelect,
      {Key key})
      : super(key: key);

  @override
  _PackagesListDialogState createState() => _PackagesListDialogState();
}

class _PackagesListDialogState extends State<_PackagesListDialog> {
  int _currentPackageIndex = 0;

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    final packageDuration =
        widget.packages[_currentPackageIndex]['duration'] as int;
    int durationHours;
    int durationMinutes;
    if (packageDuration != null) {
      durationHours = packageDuration ~/ 60;
      durationMinutes = packageDuration % 60;
    }

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Positioned(
          top: (MediaQuery.of(context).size.height -
                      ScreenUtil().setHeight(418)) /
                  2 -
              ScreenUtil().setHeight(50),
          child: Container(
            width: ScreenUtil().setWidth(346),
            // height: ScreenUtil().setHeight(418),
            padding: EdgeInsets.symmetric(
              vertical: ScreenUtil().setHeight(19),
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(19),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Color(0x29000000),
                  offset: Offset(0, ScreenUtil().setHeight(3)),
                ),
              ],
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(35),
                  ),
                  child: Row(
                    children: <Widget>[
                      Image(
                        image: NetworkImage(widget.avatarUrl),
                        width: ScreenUtil().setWidth(64),
                        height: ScreenUtil().setHeight(64),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(43)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                widget.companyName,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontSize: 19,
                                  fontWeight: FontWeight.w800,
                                  color: Color(0xFF021a89),
                                  decoration: TextDecoration.none,
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  StarRating(
                                    size: 9,
                                    starCount: 5,
                                    rating: widget.rating,
                                    color: Color(0xffffc300),
                                    borderColor: Color(0xffffc300),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(7)),
                                  Text(
                                    "${widget.reviewsCount} ${S.of(context).reviews}",
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontSize: 9,
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xFF021a89),
                                      decoration: TextDecoration.none,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: ScreenUtil().setHeight(8)),
                              Row(
                                children: <Widget>[
                                  Image(
                                    image: AssetImage(
                                        'assets/images/location-icon.png'),
                                    width: ScreenUtil().setWidth(7),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(5)),
                                  Text(
                                    widget.locality,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontSize: 10,
                                      fontWeight: FontWeight.w700,
                                      color: Color(0xFF021a89),
                                      decoration: TextDecoration.none,
                                    ),
                                  ),
                                ],
                              ),
                              Text(
                                widget.distance,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontSize: 9,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xFF021a89),
                                  decoration: TextDecoration.none,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(19)),
                Divider(
                  color: Color(0xffd8d8d8),
                  height: 0,
                ),
                Container(
                  width: ScreenUtil().setWidth(282),
                  height: ScreenUtil().setHeight(285),
                  margin: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(35),
                    vertical: ScreenUtil().setHeight(21),
                  ),
                  child: PageView.builder(
                    onPageChanged: (int index) =>
                        setState(() => _currentPackageIndex = index),
                    itemCount: widget.packages.length,
                    itemBuilder: (BuildContext context, int index) {
                      final packageName = widget.packages[index]['title'];
                      final services = List<String>.of(widget.packages[index]
                                  ['services']
                              .map((service) => "$service")
                              .toList()
                              .cast<String>() ??
                          []);
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            packageName,
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: 24,
                              fontWeight: FontWeight.w800,
                              color: Color(0xFF011f99),
                              decoration: TextDecoration.none,
                            ),
                          ),
                          // SizedBox(height: ScreenUtil().setHeight(22)),
                          for (var i = 0; i < services.length; i += 2)
                            Builder(
                              builder: (context) {
                                String service1;
                                String service2;

                                service1 = services[i];
                                if (i <= services.length - 2) {
                                  service2 = services[i + 1];
                                }

                                return Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Text(
                                        "- $service1",
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Pro Text',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xFF011f99),
                                          decoration: TextDecoration.none,
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: ScreenUtil().setWidth(39)),
                                    Expanded(
                                      flex: 1,
                                      child: service2 == null
                                          ? Container()
                                          : Text(
                                              "- $service2",
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Pro Text',
                                                fontSize: 12,
                                                fontWeight: FontWeight.w400,
                                                color: Color(0xFF011f99),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                    ),
                                  ],
                                );
                              },
                            ),
                          // SizedBox(
                          //   width: ScreenUtil().setWidth(282),
                          //   height: ScreenUtil().setHeight(198),
                          //   child: Wrap(
                          //     direction: Axis.vertical,
                          //     runAlignment: WrapAlignment.start,
                          //     runSpacing: ScreenUtil().setWidth(39),
                          //     children: services
                          //         .map(
                          //           (String service) => SizedBox(
                          //             width:
                          //                 ScreenUtil().setWidth((282 - 39) / 2),
                          //             child: Text(
                          //               "- $service",
                          //               style: TextStyle(
                          //                 fontFamily: application.locale == 'ar'
                          //                     ? 'Cairo'
                          //                     : 'SF Pro Text',
                          //                 fontSize: 12,
                          //                 fontWeight: FontWeight.w400,
                          //                 color: Color(0xFF011f99),
                          //                 decoration: TextDecoration.none,
                          //               ),
                          //             ),
                          //           ),
                          //         )
                          //         .toList(),
                          //   ),
                          // ),
                        ],
                      );
                    },
                  ),
                ),
                if (durationHours != null) ...[
                  Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(35)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${S.of(context).Duration}:",
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: Color(0xFF021a89),
                            decoration: TextDecoration.none,
                          ),
                        ),
                        Text(
                          "$durationHours ${S.of(context).hourShort} $durationMinutes ${S.of(context).minuteShort}",
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 11,
                            fontWeight: FontWeight.w500,
                            color: Color(0xFF021a89),
                            decoration: TextDecoration.none,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(20)),
                ],
                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(35)),
                  child: Row(
                    children: List<Widget>.generate(
                      widget.packages.length,
                      (int index) => Row(
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: index == _currentPackageIndex
                                ? Color(0xff011f99)
                                : Color(0xff011f99).withOpacity(0.5),
                            radius: index == _currentPackageIndex
                                ? ScreenUtil().setWidth(11) / 2
                                : ScreenUtil().setWidth(9) / 2,
                          ),
                          SizedBox(width: ScreenUtil().setWidth(10)),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          top: (MediaQuery.of(context).size.height -
                      ScreenUtil().setHeight(418)) /
                  2 -
              ScreenUtil().setHeight(59),
          right: application.locale == 'ar' ? null : ScreenUtil().setWidth(6),
          left: application.locale == 'ar' ? ScreenUtil().setWidth(6) : null,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () => Navigator.of(context).pop(),
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              child: Container(
                width: ScreenUtil().setWidth(40),
                height: ScreenUtil().setHeight(40),
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(14),
                  vertical: ScreenUtil().setHeight(14),
                ),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xff011f99),
                ),
                child: Image(
                  image: AssetImage('assets/images/close-icon.png'),
                  width: ScreenUtil().setWidth(12),
                  height: ScreenUtil().setHeight(12),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          top: (MediaQuery.of(context).size.height -
                      ScreenUtil().setHeight(418)) /
                  2 +
              ScreenUtil().setHeight(418) -
              ScreenUtil().setHeight(116) +
              ScreenUtil().setHeight(110),
          right: application.locale == 'ar' ? null : ScreenUtil().setWidth(70),
          left: application.locale == 'ar' ? ScreenUtil().setWidth(70) : null,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () => widget.onSelect(
                  widget.company, widget.packages[_currentPackageIndex]),
              child: Container(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(12),
                  vertical: ScreenUtil().setHeight(17),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(13),
                  color: Color(0xff03c428),
                ),
                child: Builder(
                  builder: (BuildContext context) {
                    final package = widget.packages[_currentPackageIndex];
                    final price = package['price'] as double;
                    final nat = price.toInt();
                    final oldPrice = package['old_price'] as double;
                    final currency = package['currency'];

                    return Column(
                      children: <Widget>[
                        Row(
                          textDirection: TextDirection.ltr,
                          children: <Widget>[
                            Text(
                              nat.toString(),
                              style: TextStyle(
                                fontFamily: 'SF Pro Text',
                                fontSize: 51,
                                fontWeight: FontWeight.w800,
                                color: Colors.white,
                                decoration: TextDecoration.none,
                              ),
                            ),
                            Column(
                              crossAxisAlignment: application.locale == 'ar'
                                  ? CrossAxisAlignment.end
                                  : CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  price
                                      .toStringAsFixed(3)
                                      .replaceFirst(RegExp(r'.*\.'), ''),
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    fontFamily: 'SF Pro Text',
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white,
                                    decoration: TextDecoration.none,
                                  ),
                                ),
                                Text(
                                  " $currency",
                                  textDirection: TextDirection.ltr,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontSize: 18,
                                    fontWeight: FontWeight.w800,
                                    color: Colors.white,
                                    decoration: TextDecoration.none,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        if (oldPrice != null) ...[
                          Text(
                            "${oldPrice.toStringAsFixed(3)} $currency",
                            style: TextStyle(
                              fontFamily: 'SF Pro Text',
                              fontSize: 14,
                              fontWeight: FontWeight.w800,
                              color: Colors.white,
                              decoration: TextDecoration.lineThrough,
                              decorationThickness: 2,
                              decorationColor: Colors.white,
                            ),
                          ),
                          SizedBox(height: ScreenUtil().setHeight(8)),
                        ],
                        Text(
                          S.of(context).bookThis,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 15,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                            decoration: TextDecoration.none,
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _FiltersDialog extends StatefulWidget {
  final _Filter selectedFilter;
  final double animationValue;

  _FiltersDialog(this.selectedFilter, this.animationValue, {Key key})
      : super(key: key);

  @override
  __FiltersDialogState createState() => __FiltersDialogState();
}

class __FiltersDialogState extends State<_FiltersDialog> {
  _Filter _selectedFilter;

  @override
  void initState() {
    super.initState();
    _selectedFilter = widget.selectedFilter;
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);
    final animationEnded = widget.animationValue == 1.0;

    return Stack(
      children: <Widget>[
        Positioned(
          top: ScreenUtil().setHeight(100),
          right: application.locale == 'ar' ? null : ScreenUtil().setWidth(8),
          left: application.locale == 'ar' ? ScreenUtil().setWidth(8) : null,
          child: Container(
            width: max(ScreenUtil().setWidth(27.0 + 48.0),
                ScreenUtil().setWidth(223) * widget.animationValue),
            height: max(ScreenUtil().setHeight(23.0 + 52.0),
                ScreenUtil().setHeight(240) * widget.animationValue),
            padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(24),
              vertical: ScreenUtil().setHeight(26),
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    animationEnded
                        ? Text(
                            S.of(context).sortBy,
                            style: TextStyle(
                              fontFamily:
                                  application.locale == 'ar' ? 'Cairo' : 'Lato',
                              fontFamilyFallback: <String>['SF Pro Text'],
                              fontSize: ScreenUtil().setSp(14),
                              fontWeight: FontWeight.w900,
                              color: Color(0xff021a89),
                              decoration: TextDecoration.none,
                            ),
                          )
                        : SizedBox(width: 0, height: 0),
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () => Navigator.of(context).pop(),
                        highlightColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        child: Image(
                          image: AssetImage('assets/images/filters-icon.png'),
                          width: ScreenUtil().setWidth(27),
                          height: ScreenUtil().setHeight(23),
                          color: Color(0xffd2d2d6),
                        ),
                      ),
                    ),
                  ],
                ),
                animationEnded
                    ? SizedBox(height: ScreenUtil().setHeight(28))
                    : SizedBox(width: 0, height: 0),
                animationEnded
                    ? Expanded(
                        child: Material(
                          color: Colors.transparent,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              FractionallySizedBox(
                                widthFactor: 1,
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      _selectedFilter = _Filter.distance;
                                      Navigator.of(context)
                                          .pop(_Filter.distance);
                                    });
                                  },
                                  highlightColor: Colors.transparent,
                                  splashColor: Colors.transparent,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      RichText(
                                        text: TextSpan(
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: S.of(context).distance,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'Lato',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: ScreenUtil().setSp(9),
                                                fontWeight: FontWeight.w900,
                                                color: Color(0xff011b7b),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                            TextSpan(text: ' '),
                                            TextSpan(
                                              text: S.of(context).nearestFirst,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'Lato',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: ScreenUtil().setSp(9),
                                                fontWeight: FontWeight.w500,
                                                color: Color(0xff011b7b),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      _selectedFilter == _Filter.distance
                                          ? Image(
                                              image: AssetImage(
                                                  'assets/images/checkmark-icon.png'),
                                              width: ScreenUtil().setWidth(12),
                                              height: ScreenUtil().setHeight(8),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                              ),
                              FractionallySizedBox(
                                widthFactor: 1,
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      _selectedFilter = _Filter.rating;
                                      Navigator.of(context).pop(_Filter.rating);
                                    });
                                  },
                                  highlightColor: Colors.transparent,
                                  splashColor: Colors.transparent,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      RichText(
                                        text: TextSpan(
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: S.of(context).rating,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'Lato',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: ScreenUtil().setSp(9),
                                                fontWeight: FontWeight.w900,
                                                color: Color(0xff011b7b),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                            TextSpan(text: ' '),
                                            TextSpan(
                                              text: S.of(context).highestFirst,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'Lato',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: ScreenUtil().setSp(9),
                                                fontWeight: FontWeight.w500,
                                                color: Color(0xff011b7b),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      _selectedFilter == _Filter.rating
                                          ? Image(
                                              image: AssetImage(
                                                  'assets/images/checkmark-icon.png'),
                                              width: ScreenUtil().setWidth(12),
                                              height: ScreenUtil().setHeight(8),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                              ),
                              FractionallySizedBox(
                                widthFactor: 1,
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      _selectedFilter = _Filter.priceAsc;
                                      Navigator.of(context)
                                          .pop(_Filter.priceAsc);
                                    });
                                  },
                                  highlightColor: Colors.transparent,
                                  splashColor: Colors.transparent,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      RichText(
                                        text: TextSpan(
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: S.of(context).price,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'Lato',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: ScreenUtil().setSp(9),
                                                fontWeight: FontWeight.w900,
                                                color: Color(0xff011b7b),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                            TextSpan(text: ' '),
                                            TextSpan(
                                              text: S.of(context).lowestFirst,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'Lato',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: ScreenUtil().setSp(9),
                                                fontWeight: FontWeight.w500,
                                                color: Color(0xff011b7b),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      _selectedFilter == _Filter.priceAsc
                                          ? Image(
                                              image: AssetImage(
                                                  'assets/images/checkmark-icon.png'),
                                              width: ScreenUtil().setWidth(12),
                                              height: ScreenUtil().setHeight(8),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                              ),
                              FractionallySizedBox(
                                widthFactor: 1,
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      _selectedFilter = _Filter.priceDesc;
                                      Navigator.of(context)
                                          .pop(_Filter.priceDesc);
                                    });
                                  },
                                  highlightColor: Colors.transparent,
                                  splashColor: Colors.transparent,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      RichText(
                                        text: TextSpan(
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: S.of(context).price,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'Lato',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: ScreenUtil().setSp(9),
                                                fontWeight: FontWeight.w900,
                                                color: Color(0xff011b7b),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                            TextSpan(text: ' '),
                                            TextSpan(
                                              text: S.of(context).highestFirst,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'Lato',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: ScreenUtil().setSp(9),
                                                fontWeight: FontWeight.w500,
                                                color: Color(0xff011b7b),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      _selectedFilter == _Filter.priceDesc
                                          ? Image(
                                              image: AssetImage(
                                                  'assets/images/checkmark-icon.png'),
                                              width: ScreenUtil().setWidth(12),
                                              height: ScreenUtil().setHeight(8),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    : SizedBox(width: 0, height: 0),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

enum _Filter {
  distance,
  rating,
  priceAsc,
  priceDesc,
  jobsDoneDesc,
}
