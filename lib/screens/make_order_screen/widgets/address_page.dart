import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/stores/application_store.dart';

typedef OnSelect(String address);

typedef OnBack();

class AddressPage extends StatefulWidget {
  final String address;
  final GeoPoint location;
  final OnSelect onSelect;
  final OnBack onBack;

  AddressPage(
      {Key key,
      @required this.address,
      @required this.location,
      @required this.onSelect,
      @required this.onBack})
      : super(key: key);

  @override
  _AddressPageState createState() => _AddressPageState();
}

class _AddressPageState extends State<AddressPage> {
  String _address;
  TextEditingController _addressController;
  Geoflutterfire _geo = Geoflutterfire();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    _address = widget.address;
    _addressController = TextEditingController(text: widget.address ?? '');
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);
    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Color(0xFF0A33CC), Color(0xFF01114E)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Column(
        children: <Widget>[
          SizedBox(height: ScreenUtil().setHeight(55)),
          Padding(
            padding:
                EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(33)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: widget.onBack,
                  enableFeedback: false,
                  child: Container(
                    width: ScreenUtil().setWidth(35),
                    height: ScreenUtil().setWidth(35),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    child: Center(
                      child: Image(
                        image: AssetImage('assets/images/back-button-icon.png'),
                        width: ScreenUtil().setWidth(17.39),
                        height: ScreenUtil().setHeight(12.15),
                        matchTextDirection: true,
                      ),
                    ),
                  ),
                ),
                Text(
                  S.of(context).addYourAddress,
                  style: TextStyle(
                    fontFamily: application.locale == 'ar'
                        ? 'Cairo'
                        : 'SF Pro Dispalay',
                    fontSize: 15,
                    fontWeight: FontWeight.w900,
                    color: Color(0xFF91A6D9),
                  ),
                ),
                SizedBox(
                  width: ScreenUtil().setWidth(35),
                  height: ScreenUtil().setWidth(35),
                ),
              ],
            ),
          ),
          Spacer(),
          SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: Container(
              margin: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(27),
              ),
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(15.2),
                right: ScreenUtil().setWidth(11),
                bottom: ScreenUtil().setHeight(30.2),
                left: ScreenUtil().setWidth(11),
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.35),
                    blurRadius: 12,
                    offset: Offset(0, ScreenUtil().setHeight(8)),
                  ),
                ],
              ),
              child: Column(
                children: <Widget>[
                  Image(
                    image: AssetImage('assets/images/location-icon.png'),
                    height: ScreenUtil().setHeight(37.55),
                    fit: BoxFit.fitHeight,
                  ),
                  SizedBox(height: ScreenUtil().setHeight(15)),
                  Text(
                    S.of(context).theSelectedLocationIs,
                    style: TextStyle(
                      fontFamily: application.locale == 'ar'
                          ? 'Cairo'
                          : 'SF Pro Dispalay',
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                      letterSpacing: 0.50,
                      color: Color(0xFFA0AEE6),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(5)),
                  SizedBox(
                    width: ScreenUtil().setWidth(176),
                    child: Text(
                      widget.address ?? '',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Dispalay',
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 0,
                        color: Color(0xFF021A89).withOpacity(0.98),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(24.9)),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(15),
                    ),
                    child: Row(
                      children: <Widget>[
                        Text(
                          S.of(context).pleaseAddYourAddess,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Dispalay',
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.50,
                            color: Color(0xFFA0AEE6),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(15)),
                  Container(
                    width: ScreenUtil().setWidth(299),
                    height: ScreenUtil().setHeight(82),
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(13),
                      vertical: ScreenUtil().setHeight(14),
                    ),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/dashed-border2.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                    child: TextField(
                      controller: _addressController,
                      onChanged: (String value) {
                        _address = value;
                      },
                      maxLines: 4,
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Dispalay',
                        fontSize: 12,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 0,
                        color: Color(0xFF021A89).withOpacity(0.98),
                      ),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.zero,
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        focusedErrorBorder: InputBorder.none,
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(21.6)),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(24),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        InkWell(
                          onTap: () async {
                            FocusScope.of(context).requestFocus(FocusNode());
                            if (_address == null || _address.trim().isEmpty) {
                              showCupertinoDialog(
                                context: context,
                                builder: (BuildContext context) =>
                                    CupertinoAlertDialog(
                                  content: Text(
                                    S.of(context).writeAddress,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  actions: <Widget>[
                                    CupertinoDialogAction(
                                      onPressed: () =>
                                          Navigator.of(context).pop(),
                                      child: Text(S.of(context).ok),
                                    ),
                                  ],
                                ),
                              );
                            } else {
                              await _addAdress(_address, widget.location);
                              widget.onSelect(_address);
                            }
                          },
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          child: Container(
                            width: ScreenUtil().setWidth(115),
                            height: ScreenUtil().setHeight(40),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: Color(0xFF021A89),
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: _isLoading
                                ? CupertinoActivityIndicator()
                                : Text(
                                    S.of(context).add,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Dispalay',
                                      fontSize: 11,
                                      fontWeight: FontWeight.w700,
                                      letterSpacing: 0.20,
                                      color: Colors.white,
                                    ),
                                  ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            FocusScope.of(context).requestFocus(FocusNode());
                            widget.onBack();
                          },
                          child: Container(
                            width: ScreenUtil().setWidth(115),
                            height: ScreenUtil().setHeight(40),
                            alignment: Alignment.center,
                            child: Text(
                              S.of(context).cancel,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Dispalay',
                                fontSize: 11,
                                fontWeight: FontWeight.w700,
                                letterSpacing: 0.20,
                                color: Color(0xFF021A89),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Spacer(flex: 2),
        ],
      ),
    );
  }

  Future<void> _addAdress(String address, GeoPoint location) async {
    setState(() {
      _isLoading = true;
    });

    final application = Provider.of<ApplicationStore>(context);
    final customer = application.customerStore;
    final newOrder = customer.newOrder;

    newOrder.address = address;
    newOrder.location = location;
    customer.setData(
      address: address,
      location: location,
    );

    if (application.isSignedIn) {
      final location = widget.location == null
          ? null
          : _geo.point(
              latitude: widget.location.latitude,
              longitude: widget.location.longitude);

      await Firestore.instance
          .collection('users')
          .document(customer.customerId)
          .updateData({
        'address': address,
        'location': location?.data,
      });
    }

    setState(() {
      _isLoading = false;
    });
  }
}
