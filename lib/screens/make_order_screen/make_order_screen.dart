import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/models/user_role.dart';
import 'package:weash_cars/models/vehicle_type.dart';
import 'package:weash_cars/screens/make_order_screen/widgets/address_page.dart';
import 'package:weash_cars/screens/make_order_screen/widgets/location_page.dart';
import 'package:weash_cars/screens/make_order_screen/widgets/package_page.dart';
import 'package:weash_cars/screens/make_order_screen/widgets/vehicle_type_page.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/customer_store.dart';
import 'package:weash_cars/stores/order_store.dart';

class MakeOrderScreen extends StatefulWidget {
  final bool addressPickerOnly;

  MakeOrderScreen({Key key, this.addressPickerOnly = false}) : super(key: key);

  @override
  _MakeOrderScreenState createState() => _MakeOrderScreenState();
}

class _MakeOrderScreenState extends State<MakeOrderScreen> {
  CustomerStore _customer;
  GeoPoint _selectedLocation;
  String _selectedAddress;
  String _selectedLocality;
  String _selectedCountry;
  VehicleType _selectedVehicleType;
  DateTime _selectedDate;
  bool _isFlexibleTiming;
  Map<String, dynamic> _selectedCompany;
  Map<String, dynamic> _selectedPackage;
  PageController _pageController;
  OrderStore _order;

  @override
  void initState() {
    super.initState();

    _pageController = new PageController();
  }

  @override
  void didChangeDependencies() {
    final application = Provider.of<ApplicationStore>(context);
    _customer = application.customerStore;
    _order = _customer.newOrder;

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: () {
          if (_pageController.page >= 1.0) {
            _pageController.previousPage(
                duration: Duration(milliseconds: 300), curve: Curves.linear);
            return Future.value(false);
          }
          return Future.value(true);
        },
        child: Stack(
          children: <Widget>[
            PageView(
              controller: _pageController,
              physics: NeverScrollableScrollPhysics(),
              children: <Widget>[
                VehicleTypePage(
                  onSelect: (VehicleType vehicleType, DateTime dateTime,
                      bool isFlexibleTiming) {
                    setState(() {
                      _selectedVehicleType = vehicleType;
                      _selectedDate = dateTime;
                      _isFlexibleTiming = isFlexibleTiming;
                      _pageController.nextPage(
                          duration: Duration(milliseconds: 300),
                          curve: Curves.linear);
                    });
                  },
                  onBack: () => Navigator.of(context).pop(),
                ),
                LocationPage(
                  onSelect: (GeoPoint location, String address, String locality,
                      String country) {
                    setState(() {
                      _selectedLocation = location;
                      _selectedAddress = address;
                      _selectedLocality = locality;
                      _selectedCountry = country;

                      if ((address == null || address.isEmpty) ||
                          (address != _customer.address &&
                              location.latitude !=
                                  _customer.location?.latitude &&
                              location.longitude !=
                                  _customer.location?.longitude)) {
                        _pageController.nextPage(
                            duration: Duration(milliseconds: 300),
                            curve: Curves.linear);
                      } else {
                        _pageController.animateToPage(2,
                            duration: Duration(milliseconds: 300),
                            curve: Curves.linear);
                      }
                    });
                  },
                  onBack: () => _pageController.previousPage(
                      duration: Duration(milliseconds: 300),
                      curve: Curves.linear),
                ),
                AddressPage(
                  address: _selectedAddress,
                  location: _selectedLocation,
                  onSelect: (String address) {
                    setState(() {
                      _selectedAddress = address;
                    });

                    if (widget.addressPickerOnly) {
                      Navigator.of(context).pop<Map<String, dynamic>>({
                        'address': _selectedAddress,
                        'location': _selectedLocation
                      });
                    } else {
                      _pageController.nextPage(
                          duration: Duration(milliseconds: 300),
                          curve: Curves.linear);
                    }
                  },
                  onBack: () => _pageController.previousPage(
                      duration: Duration(milliseconds: 300),
                      curve: Curves.linear),
                ),
                PackagePage(
                  locality: _selectedLocality,
                  country: _selectedCountry,
                  vehicleType: _selectedVehicleType,
                  dateTime: _selectedDate,
                  location: _selectedLocation,
                  flexibleTiming: _isFlexibleTiming,
                  onSelect: (Map<String, dynamic> company,
                      Map<String, dynamic> package) async {
                    setState(() {
                      _selectedCompany = company;
                      _selectedPackage = package;
                    });

                    _order.setOrder(
                      company: _selectedCompany,
                      package: _selectedPackage,
                      vehicleType: _selectedVehicleType,
                      address: _selectedAddress,
                      location: _selectedLocation,
                      country: _selectedCountry,
                      locality: _selectedLocality,
                      date: _selectedDate,
                    );

                    final application = Provider.of<ApplicationStore>(context);
                    if (application.isSignedIn &&
                        application.userRole == UserRole.customer) {
                      Navigator.pushNamed(context, '/dashboard/customer');
                    } else {
                      Navigator.pushNamed(context, '/login');
                    }
                  },
                  onBack: () => _pageController.previousPage(
                      duration: Duration(milliseconds: 300),
                      curve: Curves.linear),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
