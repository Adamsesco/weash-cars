import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/customer_store.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class CustomerOrderTrackScreen extends StatefulWidget {
  final String orderId;

  CustomerOrderTrackScreen(this.orderId);

  @override
  _CustomerOrderTrackScreenState createState() =>
      _CustomerOrderTrackScreenState();
}

class _CustomerOrderTrackScreenState extends State<CustomerOrderTrackScreen> {
  CustomerStore _customer;
  Map<String, dynamic> _order;
  String _backgroundImagePath;
  String _statusIconPath;
  String _statusTitle;
  String _statusDescription;

  @override
  void didChangeDependencies() {
    final application = Provider.of<ApplicationStore>(context);
    _customer = application.customerStore;
    _order = _customer.orders.orders
        .firstWhere((order) => order['order_id'] == widget.orderId, orElse: () {
      print("No result");
      return null;
    });

    // TODO: finish all the orders status
    switch (_order['status'] as int) {
      case 11:
        _backgroundImagePath = 'assets/images/on-the-way-background.png';
        _statusIconPath = 'assets/images/on-the-way-icon.png';
        _statusTitle = 'On the way !';
        _statusDescription = 'Our employee is on\nhis way to your location.';
        break;

      case 12:
        _backgroundImagePath = 'assets/images/working-on-background.png';
        _statusIconPath = 'assets/images/working-on-icon.png';
        _statusTitle = 'Working on !';
        _statusDescription = 'Vehicle is being cleaned\nright now';
        break;

      case 20:
        _backgroundImagePath = 'assets/images/done-background.png';
        _statusIconPath = 'assets/images/done-icon.png';
        break;
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Image(
              image: AssetImage(_backgroundImagePath),
              fit: BoxFit.cover,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CustomAppBar2(
                transparentBackground: true,
                title: 'TRACKING ORDER',
                nbNotifications: 0,
                avatar: _customer.avatarUrl == null
                    ? null
                    : NetworkImage(_customer.avatarUrl),
                popupMenuItems: <PopupMenuEntry<String>>[
                  PopupMenuItem<String>(
                    value: '/profile/company/edit',
                    child: Text('Edit info'),
                  ),
                  PopupMenuItem<String>(
                    value: '/packages/add',
                    child: Text('Add package'),
                  ),
                  PopupMenuItem(
                    value: '/employees/manage',
                    child: Text('Manage employees'),
                  ),
                  PopupMenuItem<String>(
                    value: '/logout',
                    child: Text('Logout'),
                  ),
                ],
              ),
              SizedBox(height: ScreenUtil().setHeight(118)),
              Text(
                _statusTitle,
                style: TextStyle(
                  fontFamily:
                      application.locale == 'ar' ? 'Cairo' : 'Montserrat',
                  fontFamilyFallback: <String>['SF Pro Text'],
                  fontSize: ScreenUtil().setSp(27),
                  fontWeight: FontWeight.w700,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(63)),
              Image(
                image: AssetImage(_statusIconPath),
                width: ScreenUtil().setWidth(213),
                height: ScreenUtil().setWidth(213),
              ),
              SizedBox(height: ScreenUtil().setHeight(76)),
              Text(
                _statusDescription,
                style: TextStyle(
                  fontFamily:
                      application.locale == 'ar' ? 'Cairo' : 'Montserrat',
                  fontFamilyFallback: <String>['SF Pro Text'],
                  fontSize: ScreenUtil().setSp(21),
                  fontWeight: FontWeight.w700,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
