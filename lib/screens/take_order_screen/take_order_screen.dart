import 'dart:async';
import 'dart:math';

import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_webservice/distance.dart' as distanceApi;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart' as intl;
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/user_role.dart';
import 'package:weash_cars/screens/vehicle_pictures_capture_screen/vehicle_pictures_capture_screen.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/stores/employee_store.dart';

class TakeOrderScreen extends StatefulWidget {
  Map<String, dynamic> order;

  TakeOrderScreen({Key key, this.order}) : super(key: key);

  @override
  _TakeOrderScreenState createState() => _TakeOrderScreenState();
}

class _TakeOrderScreenState extends State<TakeOrderScreen> {
  final distanceApi.GoogleDistanceMatrix _distanceApi =
      new distanceApi.GoogleDistanceMatrix(
          apiKey: 'AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I');

  Location _locationService;
  Completer<GoogleMapController> _mapController = Completer();
  GoogleMapController _googleMapController;
  CompanyStore _company;
  EmployeeStore _employee;
  Map<String, dynamic> _order;
  LatLng _orderLocation;
  LatLng _currentLocation;
  Future<_DistDuration> _distanceFuture;
  Set<Marker> _markers = {};
  final Set<Polyline> _polylines = {};
  bool _isNavigating = false;
  StreamSubscription<LocationData> _locationSubscription;
  Future _navigationFuture;

  @override
  void initState() {
    super.initState();

    _locationService = new Location();
    _navigationFuture = Future.delayed(Duration(milliseconds: 500));
  }

  @override
  void didChangeDependencies() async {
    if (_employee == null && _company == null) {
      final application = Provider.of<ApplicationStore>(context);
      _order = widget.order;
      if (application.userRole == UserRole.employee) {
        _employee = application.employeeStore;
      } else if (application.userRole == UserRole.company) {
        _company = application.companyStore;
        if (mounted) {
          setState(() {});
        }
      }

      _setOrderLocation();
    }

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    if (_locationSubscription != null) {
      _locationSubscription.cancel();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    if (_order == null) {
      return Scaffold(
        body: Container(
          alignment: Alignment.center,
          child: CupertinoActivityIndicator(
            animating: true,
          ),
        ),
      );
    }

    final orderId = _order['order_id'];
    final orderAddress = _order['address'];
    final orderDate = _order['date'] as DateTime;

    final customer = _order['customer'];
    final customerFirstName = customer['first_name'];
    final customerLastName = customer['last_name'];
    final customerPhoneNumber = customer['phone_number'];

    final package = _order['package'];
    final packageTitle = package['title'];
    final vehicleType = package['vehicle_type'];
    final vehicleTypeStrs = {
      'motorcycle': S.of(context).motorcycle,
      'sedan': S.of(context).sedan,
      'suv': S.of(context).suv,
      'boat': S.of(context).boat,
    };

    return Scaffold(
      body: Container(
        child: Stack(
          alignment: Alignment.bottomLeft,
          children: <Widget>[
            FutureBuilder<Object>(
              future: _navigationFuture,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return Positioned.fill(
                    child: GoogleMap(
                      onMapCreated: (GoogleMapController controller) {
                        _mapController.complete(controller);
                        _googleMapController = controller;
                      },
                      mapType: MapType.normal,
                      initialCameraPosition: CameraPosition(
                        target: _orderLocation,
                        zoom: 14.4746,
                      ),
                      compassEnabled: true,
                      markers: _markers,
                      polylines: _polylines,
                    ),
                  );
                }

                return Container();
              },
            ),
            Positioned(
              top: ScreenUtil().setHeight(55),
              left:
                  application.locale == 'ar' ? null : ScreenUtil().setWidth(33),
              right:
                  application.locale == 'ar' ? ScreenUtil().setWidth(33) : null,
              child: InkWell(
                onTap: () {
                  _locationSubscription.cancel();
                  Navigator.of(context).pop();
                },
                child: Container(
                  width: ScreenUtil().setWidth(35),
                  height: ScreenUtil().setWidth(35),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                  ),
                  child: Center(
                    child: Image(
                      image: AssetImage('assets/images/back-button-icon.png'),
                      width: ScreenUtil().setWidth(17.39),
                      height: ScreenUtil().setHeight(12.15),
                      matchTextDirection: true,
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(
                  right: ScreenUtil().setWidth(26.3),
                  left: ScreenUtil().setWidth(25),
                  bottom: ScreenUtil().setHeight(40.8),
                ),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image:
                        AssetImage('assets/images/take-order-background.png'),
                    fit: BoxFit.fill,
                  ),
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: ScreenUtil().setHeight(51.3)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "$customerFirstName $customerLastName",
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 18,
                                fontWeight: FontWeight.w800,
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(8)),
                            Text(
                              S.of(context).location,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 13,
                                fontWeight: FontWeight.w800,
                                color: Colors.white,
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(1.9)),
                            SizedBox(
                              width: ScreenUtil().setWidth(200),
                              child: Text(
                                orderAddress,
                                style: TextStyle(
                                  fontFamily: 'SF Pro Text',
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(18.8)),
                            RichText(
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                    text: S.of(context).vehicleType,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: 13,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.white,
                                    ),
                                  ),
                                  TextSpan(
                                    text: vehicleTypeStrs[vehicleType],
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: 13,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(4)),
                            RichText(
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                    text: S.of(context).package,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: 13,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.white,
                                    ),
                                  ),
                                  TextSpan(
                                    text: packageTitle,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: 13,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(4)),
                            RichText(
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                    text: S.of(context).time,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: 13,
                                      fontWeight: FontWeight.w800,
                                      color: Colors.white,
                                    ),
                                  ),
                                  TextSpan(
                                    text:
                                        intl.DateFormat('Hm').format(orderDate),
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: 13,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        InkWell(
                          onTap: () => launch("tel://$customerPhoneNumber"),
                          child: Container(
                            width: ScreenUtil().setWidth(57.7),
                            height: ScreenUtil().setWidth(57.7),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                            ),
                            child: Center(
                              child: Image(
                                image: AssetImage(
                                    'assets/images/phone-call-icon.png'),
                                width: ScreenUtil().setWidth(31.82),
                                height: ScreenUtil().setHeight(31.88),
                                matchTextDirection: true,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(32)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Flexible(
                          fit: FlexFit.loose,
                          child: FutureBuilder(
                            future: _distanceFuture,
                            builder: (BuildContext context,
                                AsyncSnapshot<_DistDuration> snapshot) {
                              switch (snapshot.connectionState) {
                                case ConnectionState.none:
                                case ConnectionState.active:
                                case ConnectionState.waiting:
                                  return Container();
                                case ConnectionState.done:
                                  final distDuration = snapshot.data;
                                  return Row(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    // textDirection: TextDirection.ltr,
                                    children: <Widget>[
                                      Text(
                                        distDuration.hours == 0
                                            ? ''
                                            : "${distDuration.hours} ",
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Pro Text',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Text(
                                        distDuration.hours == 0
                                            ? ''
                                            : "${S.of(context).hourShort} ",
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Pro Text',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: 10,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Text(
                                        "${distDuration.minutes} ",
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Pro Text',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Text(
                                        "${S.of(context).minuteShort}  ",
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Pro Text',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: 10,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Image(
                                        image: AssetImage(
                                            'assets/images/time-icon.png'),
                                        width: ScreenUtil().setWidth(13.33),
                                        height: ScreenUtil().setWidth(13.33),
                                        color: Colors.white,
                                      ),
                                      SizedBox(
                                          width: ScreenUtil().setWidth(12.7)),
                                      Text(
                                        distDuration.kilometers.toString(),
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Pro Text',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Text(
                                        "${S.of(context).kilometerShort}  ",
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Pro Text',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: 10,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Image(
                                        image: AssetImage(
                                            'assets/images/distance-icon.png'),
                                        width: ScreenUtil().setWidth(17.48),
                                        height: ScreenUtil().setWidth(16.31),
                                        color: Colors.white,
                                      ),
                                    ],
                                  );
                              }
                            },
                          ),
                        ),
                        InkWell(
                          onTap: _isNavigating
                              ? () async {
                                  _locationSubscription.cancel();
                                  await Firestore.instance
                                      .collection('orders')
                                      .document(_order['order_id'])
                                      .updateData({
                                    'status': 12,
                                  });
                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          VehiclePicturesCaptureScreen(
                                              order: _order),
                                    ),
                                  );
                                }
                              : _navigate,
                          child: Container(
                            width: ScreenUtil().setWidth(170.16),
                            height: ScreenUtil().setHeight(40.2),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  _isNavigating
                                      ? S.of(context).start
                                      : S.of(context).navigate,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontFamilyFallback: <String>['SF Pro Text'],
                                    fontSize: 11,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFF011E96),
                                  ),
                                ),
                                if (!_isNavigating) ...[
                                  SizedBox(width: ScreenUtil().setWidth(12.2)),
                                  Image(
                                    image: AssetImage(
                                        'assets/images/navigate-icon.png'),
                                    width: ScreenUtil().setWidth(14.72),
                                    height: ScreenUtil().setWidth(14.72),
                                  ),
                                ],
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _setOrderLocation() async {
    final orderLocation = _order['location'] as GeoPoint;
    _orderLocation =
        new LatLng(orderLocation.latitude, orderLocation.longitude);
    _markers.add(Marker(
      markerId: MarkerId(_order['order_id']),
      position: _orderLocation,
      icon: await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
          devicePixelRatio: MediaQuery.of(context).devicePixelRatio,
        ),
        'assets/images/default-marker-icon.png',
      ),
    ));

    if (mounted) {
      setState(() {});
    }

    _getCurrentLocation();
  }

  Future<void> _getCurrentLocation() async {
    await _locationService.changeSettings(
        accuracy: LocationAccuracy.NAVIGATION);

    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      bool serviceStatus = await _locationService.serviceEnabled();
      if (serviceStatus) {
        final permission = await _locationService.requestPermission();
        if (permission) {
          final defaultMarker = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(
              devicePixelRatio: MediaQuery.of(context).devicePixelRatio,
            ),
            'assets/images/order-location-marker-icon.png',
          );
          final orderMarker = await BitmapDescriptor.fromAssetImage(
            ImageConfiguration(
              devicePixelRatio: MediaQuery.of(context).devicePixelRatio,
            ),
            'assets/images/default-marker-icon.png',
          );

          _locationSubscription = _locationService
              .onLocationChanged()
              .listen((LocationData currentLocation) async {
            final latlng =
                new LatLng(currentLocation.latitude, currentLocation.longitude);

            final marker = new Marker(
              markerId: MarkerId('me'),
              position: latlng,
              icon: defaultMarker,
            );

            if (_currentLocation == null ||
                currentLocation.latitude != _currentLocation.latitude ||
                currentLocation.longitude != _currentLocation.longitude) {
              setState(() {
                if (_googleMapController != null) {
                  _currentLocation = latlng;
                  _markers.clear();
                  final orderLocation = _order['location'] as GeoPoint;
                  _orderLocation = new LatLng(
                      orderLocation.latitude, orderLocation.longitude);
                  _markers.add(Marker(
                    markerId: MarkerId(_order['order_id']),
                    position: _orderLocation,
                    icon: orderMarker,
                  ));
                  _markers.add(marker);

                  if (_isNavigating) {
                    _googleMapController
                        .animateCamera(CameraUpdate.newLatLng(latlng));
                  }
                }

                if (_distanceFuture == null) {
                  _distanceFuture = _getDistance(
                      GeoPoint(
                          currentLocation.latitude, currentLocation.longitude),
                      GeoPoint(
                          _orderLocation.latitude, _orderLocation.longitude));
                }
              });
            }
          });
        }
      } else {
        bool serviceStatusResult = await _locationService.requestService();
        print("Service status activated after request: $serviceStatusResult");
        if (serviceStatusResult) {
          _getCurrentLocation();
        }
        return null;
      }
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        _showDialog(
            'Permission denied', "Could not get your current location.");
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        _showDialog('Location service status error', e.message);
      }
    }

    return null;
  }

  Future<void> _navigate() async {
    final destination = _order['location'] as GeoPoint;
    final polylinePoints = PolylinePoints();
    final points = await polylinePoints.getRouteBetweenCoordinates(
        'AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I',
        _currentLocation.latitude,
        _currentLocation.longitude,
        destination.latitude,
        destination.longitude);
    final polyline = new Polyline(
      polylineId: PolylineId(points.hashCode.toString()),
      points: points
          .map((PointLatLng point) => LatLng(point.latitude, point.longitude))
          .toList(),
      color: Colors.blueAccent,
    );

    await Firestore.instance
        .collection('orders')
        .document(_order['order_id'])
        .updateData({
      'status': 11,
    });

    setState(() {
      _isNavigating = true;
      _polylines.clear();
      _polylines.add(polyline);

      if (_googleMapController != null) {
        _googleMapController.animateCamera(CameraUpdate.newLatLng(
            LatLng(_currentLocation.latitude, _currentLocation.longitude)));
      }
    });
  }

  double _haversine(double lat1, double lon1, double lat2, double lon2) {
    final dLat = _toRadians(lat2 - lat1);
    final dLon = _toRadians(lon2 - lon1);
    lat1 = _toRadians(lat1);
    lat2 = _toRadians(lat2);
    final a = (pow(sin(dLat / 2), 2) +
        pow(sin(dLon / 2), 2) * cos(lat1) * cos(lat2)) as double;
    final c = 2 * asin(sqrt(a));
    return 6372800 * c;
  }

  double _toRadians(double degree) {
    return degree * pi / 180;
  }

  Future<_DistDuration> _getDistance(GeoPoint p1, GeoPoint p2) async {
    final distance =
        _haversine(p1.latitude, p1.longitude, p2.latitude, p2.longitude);
    final duration = distance.toInt() ~/ 30;
    return _DistDuration(distance, duration);

//    final origin = <distanceApi.Location>[
//      distanceApi.Location(p1.latitude, p1.longitude)
//    ];
//    final destionation = <distanceApi.Location>[
//      distanceApi.Location(p2.latitude, p2.longitude)
//    ];
//
//    final distanceResponse = await _distanceApi.distanceWithLocation(
//        origin, destionation,
//        unit: distanceApi.Unit.metric);
//    if (distanceResponse.isOkay &&
//        distanceResponse.results.length > 0 &&
//        distanceResponse.results[0].elements.length > 0 &&
//        distanceResponse.results[0].elements[0].distance != null &&
//        distanceResponse.results[0].elements[0].duration != null) {
//      final distance =
//          distanceResponse.results[0].elements[0].distance.value.toInt();
//      final duration =
//          distanceResponse.results[0].elements[0].duration.value.toInt();
//      final distDuration = new _DistDuration(distance, duration);
//
//      return distDuration;
//    }
//
//    return _DistDuration(0, 0);
  }

  void _showDialog(String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      },
    );
  }
}

class _DistDuration {
  final double distance;
  final int duration;
  double kilometers;
  int hours;
  int minutes;
  String distanceUnit;
  String durationUnit;

  _DistDuration(this.distance, this.duration) {
    kilometers = distance / 1000;
    hours = duration ~/ 3600;
    minutes = (duration % 3600) ~/ 60;
  }
}
