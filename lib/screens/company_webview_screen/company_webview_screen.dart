import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CompanyWebviewScreen extends StatefulWidget {
  final String url;

  CompanyWebviewScreen({this.url});

  @override
  _CompanyWebviewScreenState createState() => _CompanyWebviewScreenState();
}

class _CompanyWebviewScreenState extends State<CompanyWebviewScreen> {
  CompanyStore _company;
  bool _isDialogVisible = false;

  @override
  void didChangeDependencies() {
    if (_company == null) {
      final applicationStore = Provider.of<ApplicationStore>(context);
      _company = applicationStore.companyStore;
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CompanyDrawer(
              onCloseDrawer: () {
                setState(() => _isDialogVisible = false);
              },
              avatarUrl: _company.avatarUrl,
            ),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Observer(
              builder: (BuildContext context) => CustomAppBar2(
                    onOpenDrawer: () {
                      setState(() {
                        _isDialogVisible = true;
                        Scaffold.of(context).openDrawer();
                      });
                    },
                    title: S.of(context).dashboard,
                    transparentBackground: true,
                    titleColor: Color(0xFF91A6D9),
                    buttonColor: Color(0xFF0930C3),
                    nbNotifications: 0,
                    avatar: _company.avatarUrl == null
                        ? null
                        : NetworkImage(_company.avatarUrl),
                  ),
            ),
            SizedBox(height: ScreenUtil().setHeight(29.5)),
            Expanded(
              child: WebView(
                initialUrl: widget.url,
                navigationDelegate: (NavigationRequest request) {
                  if (request.url == widget.url) {
                    return NavigationDecision.navigate;
                  }

                  canLaunch(request.url).then((value) {
                    if (value) {
                      launch(request.url);
                    }
                  });

                  return NavigationDecision.prevent;
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
