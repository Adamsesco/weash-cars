import 'dart:collection';
import 'dart:io';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/screens/company_settings_screen/widgets/location_picker.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class CompanySettingsScreen extends StatefulWidget {
  @override
  _CompanySettingsState createState() => _CompanySettingsState();
}

class _CompanySettingsState extends State<CompanySettingsScreen>
    with SingleTickerProviderStateMixin {
  CompanyStore _company;

  PageController _pageController = new PageController(keepPage: true);
  GlobalKey<FormState> _formKey = new GlobalKey();
  FocusNode _companyNameFocusNode = new FocusNode();
  FocusNode _emailFocusNode = new FocusNode();
  FocusNode _phoneNumberFocusNode = new FocusNode();

  String _companyName;
  String _email;
  String _phoneNumber;
  File _avatarFile;
  Map<String, dynamic> _locationData;
  Geoflutterfire _geo = Geoflutterfire();
  Map<int, Map<String, dynamic>> _schedule;
  String _locale;

  int _page = 0;
  bool _isDialogVisible = false;
  bool _isLoading = false;

  AnimationController _blurAnimationController;
  Animation<double> _blurAnimation;

  @override
  void initState() {
    super.initState();

    _blurAnimationController = new AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 100),
    );
    _blurAnimation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(_blurAnimationController);

    _blurAnimation.addListener(() {
      setState(() {});
    });
  }

  @override
  void didChangeDependencies() {
    if (_company == null) {
      final applicationStore = Provider.of<ApplicationStore>(context);
      _company = applicationStore.companyStore;
      _schedule = _company.schedule;
      if (_schedule != null) {
        final sorted = SplayTreeMap<int, Map<String, dynamic>>.from(
            _schedule, (a, b) => a.compareTo(b));
        _schedule = sorted;
      }
      _locationData = {
        'location': _company.location,
        'address': _company.address,
        'locality': _company.locality,
        'country': _company.country,
      };
      _locale = applicationStore.locale;
    }

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _blurAnimationController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CompanyDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _company.avatarUrl,
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image(
                image:
                    AssetImage('assets/images/company-screens-background.png'),
                matchTextDirection: true,
                fit: BoxFit.fill),
          ),
          Positioned(
            top: 0,
            child: Observer(
              builder: (BuildContext context) => CustomAppBar2(
                onOpenDrawer: () {
                  setState(() {
                    _isDialogVisible = true;
                    Scaffold.of(context).openDrawer();
                  });
                },
                title: S.of(context).dashboard,
                transparentBackground: true,
                titleColor: Color(0xFF91A6D9),
                buttonColor: Color(0xFF0930C3),
                nbNotifications: 0,
                avatar: _company.avatarUrl == null
                    ? null
                    : NetworkImage(_company.avatarUrl),
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().setHeight(144),
            bottom: 0,
            child: SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height -
                //     ScreenUtil().setHeight(184),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Transform.translate(
                      offset: Offset(
                          ScreenUtil()
                              .setWidth(application.locale == 'ar' ? -32 : 32),
                          0),
                      child: Align(
                        alignment: application.locale == 'ar'
                            ? Alignment.topRight
                            : Alignment.topLeft,
                        child: Text(
                          S.of(context).editSettings,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Display',
                            fontFamilyFallback: <String>['SF Pro Text'],
                            fontSize: 24,
                            fontWeight: FontWeight.w900,
                            color: Color(0xFFA0AEE6),
                          ),
                        ),
                      ),
                    ),
                    LimitedBox(
                      maxHeight: ScreenUtil().setHeight(620),
//                      maxHeight: application.locale == 'ar'
//                          ? 600
//                          : 600,
                      child: PageView(
                        onPageChanged: (int page) =>
                            setState(() => _page = page),
                        children: <Widget>[
                          // Profile view
                          Column(
                            children: <Widget>[
                              SizedBox(height: ScreenUtil().setHeight(30)),
                              Transform.translate(
                                offset: Offset(
                                    ScreenUtil().setWidth(
                                        application.locale == 'ar' ? -45 : 45),
                                    0),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    InkWell(
                                      onTap: _pickPhoto,
                                      highlightColor: Colors.transparent,
                                      splashColor: Colors.transparent,
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          CircleAvatar(
                                            radius: ScreenUtil().setWidth(45),
                                            backgroundColor: Color(0xFFF0F0F0),
                                            backgroundImage: _avatarFile == null
                                                ? _company.avatarUrl == null
                                                    ? null
                                                    : NetworkImage(
                                                        _company.avatarUrl)
                                                : FileImage(_avatarFile),
                                          ),
                                          Transform.translate(
                                            offset: Offset(
                                                ScreenUtil().setWidth(
                                                    application.locale == 'ar'
                                                        ? 30
                                                        : -30),
                                                ScreenUtil().setHeight(6)),
                                            child: Container(
                                              width: ScreenUtil().setWidth(35),
                                              height: ScreenUtil().setWidth(35),
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/camera-icon-background.png'),
                                                ),
                                              ),
                                              child: Image(
                                                image: AssetImage(
                                                    'assets/images/camera-white-icon.png'),
                                                width: ScreenUtil()
                                                    .setWidth(14.25),
                                                height: ScreenUtil()
                                                    .setHeight(12.04),
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        SizedBox(
                                            height: ScreenUtil().setHeight(20)),
                                        Text(
                                          _company.companyName,
                                          style: TextStyle(
                                            fontFamily: 'SF Pro Text',
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            color: Color(0xFF021A89),
                                          ),
                                        ),
                                        Text(
                                          "${_company.jobsDoneCount} ${S.of(context).jobsDone}",
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Text',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: 10,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xFF021A89),
                                          ),
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(8)),
                                        Row(
                                          children: <Widget>[
                                            Transform.scale(
                                              scale: 0.6,
                                              alignment: Alignment.centerLeft,
                                              child: CupertinoSwitch(
                                                value: _company.isAvailable,
                                                onChanged: (bool value) {
                                                  setState(() {
                                                    _company.isAvailable =
                                                        value;
                                                  });
                                                  Firestore.instance
                                                      .collection('users')
                                                      .document(
                                                          _company.companyId)
                                                      .updateData({
                                                    'is_available': value,
                                                  });
                                                },
                                                activeColor: Color(0xFF00CB65),
                                              ),
                                            ),
                                            Text(
                                              (_company.isAvailable
                                                      ? S.of(context).available
                                                      : S
                                                          .of(context)
                                                          .unavailable)
                                                  .toUpperCase(),
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                fontSize: 10,
                                                fontWeight: FontWeight.w700,
                                                color: _company.isAvailable
                                                    ? Color(0xFF00CB65)
                                                    : Color(0xFFC1C1C1),
                                              ),
                                            ),
                                            if (!_company.isAvailable) ...[
                                              SizedBox(
                                                  width: ScreenUtil()
                                                      .setWidth(7.6)),
                                              Image(
                                                image: AssetImage(
                                                    'assets/images/attention-icon.png'),
                                                width: ScreenUtil()
                                                    .setWidth(14.63),
                                                height: ScreenUtil()
                                                    .setHeight(13.2),
                                              ),
                                            ]
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(35)),
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(44)),
                                child: Form(
                                  key: _formKey,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        S.of(context).companyName,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Pro Text',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff021A89),
                                        ),
                                      ),
                                      SizedBox(
                                          height: ScreenUtil().setHeight(6)),
                                      TextFormField(
                                        keyboardType: TextInputType.text,
                                        textInputAction: TextInputAction.next,
                                        textCapitalization:
                                            TextCapitalization.words,
                                        initialValue:
                                            _company.companyName ?? '',
                                        style: TextStyle(
                                          fontFamily: 'SF Pro Text',
                                          fontWeight: FontWeight.w700,
                                          color: Color(0xff021A89),
                                        ),
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.only(
                                            left: ScreenUtil().setWidth(0),
                                            right: ScreenUtil().setWidth(0),
                                            bottom: ScreenUtil().setHeight(1.5),
                                          ),
                                          border: UnderlineInputBorder(
                                            borderSide: BorderSide.none,
                                          ),
                                        ),
                                        validator: (String val) {
                                          if (val.length < 2) {
                                            return S
                                                .of(context)
                                                .companyNameError;
                                          }
                                        },
                                        onFieldSubmitted: (_) =>
                                            FocusScope.of(context)
                                                .requestFocus(_emailFocusNode),
                                        onSaved: (String val) =>
                                            _companyName = val,
                                      ),
                                      FractionallySizedBox(
                                        widthFactor: 1,
                                        child: Container(
                                          height: ScreenUtil().setHeight(0.3),
                                          color: Color(0xFFC5D4F8),
                                        ),
                                      ),
                                      SizedBox(
                                          height: ScreenUtil().setHeight(23.5)),
                                      Text(
                                        S.of(context).email,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Pro Text',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff021A89),
                                        ),
                                      ),
                                      SizedBox(
                                          height: ScreenUtil().setHeight(6)),
                                      TextFormField(
                                        focusNode: _emailFocusNode,
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        textInputAction: TextInputAction.next,
                                        textDirection: TextDirection.ltr,
                                        initialValue: _company.email ?? '',
                                        style: TextStyle(
                                          fontFamily: 'SF Pro Text',
                                          fontWeight: FontWeight.w700,
                                          color: Color(0xff021A89),
                                        ),
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.only(
                                            left: ScreenUtil().setWidth(0),
                                            right: ScreenUtil().setWidth(0),
                                            bottom: ScreenUtil().setHeight(1.5),
                                          ),
                                          border: UnderlineInputBorder(
                                            borderSide: BorderSide.none,
                                          ),
                                        ),
                                        validator: (String val) {
                                          var pattern =
                                              r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
                                          var regexp = new RegExp(pattern);
                                          if (!regexp.hasMatch(val)) {
                                            return S.of(context).emailError;
                                          }
                                        },
                                        onFieldSubmitted: (_) =>
                                            FocusScope.of(context).requestFocus(
                                                _phoneNumberFocusNode),
                                        onSaved: (String val) => _email = val,
                                      ),
                                      FractionallySizedBox(
                                        widthFactor: 1,
                                        child: Container(
                                          height: ScreenUtil().setHeight(0.3),
                                          color: Color(0xFFC5D4F8),
                                        ),
                                      ),
                                      SizedBox(
                                          height: ScreenUtil().setHeight(23.5)),
                                      Text(
                                        S.of(context).phoneNumber,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Pro Text',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: 13,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff021A89),
                                        ),
                                      ),
                                      SizedBox(
                                          height: ScreenUtil().setHeight(6)),
                                      TextFormField(
                                        focusNode: _phoneNumberFocusNode,
                                        keyboardType: TextInputType.phone,
                                        textInputAction: TextInputAction.done,
                                        textDirection: TextDirection.ltr,
                                        initialValue:
                                            _company.phoneNumber ?? '',
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Pro Text',
                                          fontWeight: FontWeight.w700,
                                          color: Color(0xff021A89),
                                        ),
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.only(
                                            left: ScreenUtil().setWidth(0),
                                            right: ScreenUtil().setWidth(0),
                                            bottom: ScreenUtil().setHeight(1.5),
                                          ),
                                          border: UnderlineInputBorder(
                                            borderSide: BorderSide.none,
                                          ),
                                        ),
                                        validator: (String val) {
                                          var pattern = r"^\+[0-9]{10,}$";
                                          var regexp = new RegExp(pattern);
                                          if (!regexp.hasMatch(val)) {
                                            return S
                                                .of(context)
                                                .phoneNumberError;
                                          }
                                        },
                                        onFieldSubmitted: (_) {
                                          _save();
                                          FocusScope.of(context)
                                              .requestFocus(FocusNode());
                                        },
                                        onSaved: (String val) =>
                                            _phoneNumber = val,
                                      ),
                                      FractionallySizedBox(
                                        widthFactor: 1,
                                        child: Container(
                                          height: ScreenUtil().setHeight(0.3),
                                          color: Color(0xFFC5D4F8),
                                        ),
                                      ),
                                      SizedBox(
                                          height: ScreenUtil().setHeight(29.5)),
                                      Align(
                                        alignment: application.locale == 'ar'
                                            ? Alignment.topRight
                                            : Alignment.topLeft,
                                        child: Text(
                                          S.of(context).language,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Text',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: 13,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xFF021A89),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.symmetric(
                                            horizontal:
                                                ScreenUtil().setWidth(35)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          textDirection: TextDirection.ltr,
                                          children: <Widget>[
                                            InkWell(
                                              onTap: () => setState(
                                                  () => _locale = 'en'),
                                              enableFeedback: false,
                                              child: Row(
                                                textDirection:
                                                    TextDirection.ltr,
                                                children: <Widget>[
                                                  Container(
                                                    width: ScreenUtil()
                                                        .setWidth(12),
                                                    height: ScreenUtil()
                                                        .setWidth(12),
                                                    alignment: Alignment.center,
                                                    decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      border: Border.all(
                                                        color:
                                                            Color(0xFF021A89),
                                                        width: 1,
                                                      ),
                                                    ),
                                                    child: Visibility(
                                                      visible: _locale == 'en',
                                                      child: Container(
                                                        width: ScreenUtil()
                                                            .setWidth(7),
                                                        height: ScreenUtil()
                                                            .setWidth(7),
                                                        decoration:
                                                            BoxDecoration(
                                                          shape:
                                                              BoxShape.circle,
                                                          color:
                                                              Color(0xFF07D80F),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                      width: ScreenUtil()
                                                          .setWidth(5)),
                                                  Text(
                                                    'English',
                                                    style: TextStyle(
                                                      fontFamily: 'SF Pro Text',
                                                      fontSize: 15,
                                                      fontWeight:
                                                          FontWeight.w700,
                                                      color: Color(0xFF021A89),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            InkWell(
                                              onTap: () => setState(
                                                  () => _locale = 'ar'),
                                              enableFeedback: false,
                                              child: Row(
                                                textDirection:
                                                    TextDirection.rtl,
                                                children: <Widget>[
                                                  Container(
                                                    width: ScreenUtil()
                                                        .setWidth(12),
                                                    height: ScreenUtil()
                                                        .setWidth(12),
                                                    alignment: Alignment.center,
                                                    decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      border: Border.all(
                                                        color:
                                                            Color(0xFF021A89),
                                                        width: 1,
                                                      ),
                                                    ),
                                                    child: Visibility(
                                                      visible: _locale == 'ar',
                                                      child: Container(
                                                        width: ScreenUtil()
                                                            .setWidth(7),
                                                        height: ScreenUtil()
                                                            .setWidth(7),
                                                        decoration:
                                                            BoxDecoration(
                                                          shape:
                                                              BoxShape.circle,
                                                          color:
                                                              Color(0xFF07D80F),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                      width: ScreenUtil()
                                                          .setWidth(5)),
                                                  Text(
                                                    'العربية',
                                                    style: TextStyle(
                                                      fontFamily:
                                                          application.locale ==
                                                                  'ar'
                                                              ? 'Cairo'
                                                              : 'SF Pro Text',
                                                      fontFamilyFallback: <
                                                          String>[
                                                        'SF Pro Text'
                                                      ],
                                                      fontSize: 15,
                                                      fontWeight:
                                                          FontWeight.w700,
                                                      color: Color(0xFF021A89),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      FractionallySizedBox(
                                        widthFactor: 1,
                                        child: Container(
                                          height: ScreenUtil().setHeight(0.3),
                                          color: Color(0xFFC5D4F8),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          // Schedule view
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(30),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                // SizedBox(height: ScreenUtil().setHeight(37)),
                                Spacer(),
                                Align(
                                  alignment: application.locale == 'ar'
                                      ? Alignment.topRight
                                      : Alignment.topLeft,
                                  child: Text(
                                    S.of(context).yourLocation,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Display',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: 15,
                                      fontWeight: FontWeight.w900,
                                      color: Color(0xFFA0AEE6),
                                    ),
                                  ),
                                ),
                                // SizedBox(height: ScreenUtil().setHeight(15)),
                                Spacer(),
                                InkWell(
                                  onTap: () async {
                                    final locationData = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              LocationPicker(),
                                        ));
                                    if (locationData != null) {
                                      setState(() {
                                        _locationData = locationData
                                            as Map<String, dynamic>;
                                      });
                                    }
                                  },
                                  enableFeedback: false,
                                  child: _locationData == null ||
                                          _locationData['location'] == null
                                      ? Container(
                                          width: ScreenUtil().setWidth(315),
                                          height: ScreenUtil().setHeight(70),
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: AssetImage(
                                                  'assets/images/pick-location-border.png'),
                                              fit: BoxFit.fill,
                                            ),
                                          ),
                                          child: Center(
                                            child: Image(
                                              image: AssetImage(
                                                  'assets/images/pick-location-icon.png'),
                                              width:
                                                  ScreenUtil().setWidth(36.74),
                                              height:
                                                  ScreenUtil().setWidth(36.74),
                                            ),
                                          ),
                                        )
                                      : Container(
                                          width: ScreenUtil().setWidth(315),
                                          // height: ScreenUtil().setHeight(70),
                                          padding: EdgeInsets.only(
                                            top: ScreenUtil().setHeight(17.4),
                                            right: ScreenUtil().setWidth(48),
                                            bottom: ScreenUtil().setHeight(18),
                                            left: ScreenUtil().setWidth(25.7),
                                          ),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Color(0xFFA0AEE6)
                                                    .withOpacity(0.23),
                                                offset: Offset(0,
                                                    ScreenUtil().setHeight(10)),
                                                blurRadius: 11,
                                              ),
                                            ],
                                          ),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Image(
                                                image: AssetImage(
                                                    'assets/images/default-marker-icon.png'),
                                                width: ScreenUtil()
                                                    .setWidth(20.69),
                                                height: ScreenUtil()
                                                    .setHeight(31.55),
                                                color: Color(0xFFA0AEE6),
                                              ),
                                              SizedBox(
                                                  width: ScreenUtil()
                                                      .setWidth(44.6)),
                                              Expanded(
                                                child: Text(
                                                  _locationData['address'],
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    fontFamily: application
                                                                .locale ==
                                                            'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                    fontFamilyFallback: <
                                                        String>['SF Pro Text'],
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w700,
                                                    color: Color(0xFF021A89),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                ),
                                // SizedBox(height: ScreenUtil().setHeight(29)),
                                Spacer(),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      S.of(context).workingHours,
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'SF Pro Display',
                                        fontFamilyFallback: <String>[
                                          'SF Pro Text'
                                        ],
                                        fontSize: 15,
                                        fontWeight: FontWeight.w900,
                                        color: Color(0xFFA0AEE6),
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () async {
                                        final hs =
                                            await _showScheduleDialog(-1, {
                                          'from': 800,
                                          'to': 1630,
                                          'is_day_off': false,
                                        });
                                        if (hs != null) {
                                          setState(() {
                                            _schedule.values.forEach(
                                                (Map<String, dynamic>
                                                    daySchedule) {
                                              daySchedule['from'] = hs['from'];
                                              daySchedule['to'] = hs['to'];
                                              daySchedule['is_day_off'] = false;
                                            });
                                            _company.setData(
                                                schedule: _schedule);
                                          });
                                        }
                                      },
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            S.of(context).evereyDay,
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Compact Display',
                                              fontFamilyFallback: <String>[
                                                'SF Pro Text'
                                              ],
                                              fontSize: 13,
                                              fontWeight: FontWeight.w600,
                                              color: Color(0xFFE4E4E4),
                                            ),
                                          ),
                                          SizedBox(
                                              width: ScreenUtil().setWidth(11)),
                                          Image(
                                            image: AssetImage(
                                                'assets/images/edit-icon.png'),
                                            width: ScreenUtil().setWidth(18),
                                            height: ScreenUtil().setWidth(18),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                // SizedBox(
                                //     height:
                                //         ScreenUtil().setHeight(27 - 17 / 2)),
                                Spacer(),
                                Expanded(
                                  flex: 14,
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: ScreenUtil().setWidth(16),
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        for (final day in _schedule.keys)
                                          Builder(
                                            builder: (BuildContext context) {
                                              final dayNames = [
                                                S.of(context).monday,
                                                S.of(context).tuesday,
                                                S.of(context).wednesday,
                                                S.of(context).thursday,
                                                S.of(context).friday,
                                                S.of(context).saturday,
                                                S.of(context).sunday,
                                              ];
                                              final dayName = dayNames[day];
                                              final daySchedule =
                                                  _schedule[day];
                                              final hourFrom =
                                                  daySchedule['from'] ~/ 100;
                                              final minuteFrom =
                                                  daySchedule['from'] % 100;
                                              final hourTo =
                                                  daySchedule['to'] ~/ 100;
                                              final minuteTo =
                                                  daySchedule['to'] % 100;

                                              final hourFromStr = hourFrom >= 10
                                                  ? hourFrom.toString()
                                                  : "0$hourFrom";
                                              final minuteFromStr =
                                                  minuteFrom >= 10
                                                      ? minuteFrom.toString()
                                                      : "0$minuteFrom";
                                              final hourToStr = hourTo >= 10
                                                  ? hourTo.toString()
                                                  : "0$hourFrom";
                                              final minuteToStr = minuteTo >= 10
                                                  ? minuteTo.toString()
                                                  : "0$minuteTo";

                                              final isDayOff =
                                                  daySchedule['is_day_off']
                                                      as bool;

                                              return InkWell(
                                                onTap: () async {
                                                  final hs =
                                                      await _showScheduleDialog(
                                                          day, daySchedule);
                                                  if (hs != null) {
                                                    setState(() {
                                                      _schedule[day] = hs;
                                                      _company.setData(
                                                          schedule: _schedule);
                                                    });
                                                  }
                                                },
                                                child: Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      // vertical: ScreenUtil()
                                                      //     .setHeight(17 / 2),
                                                      ),
                                                  child: Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 1,
                                                        child: Text(
                                                          dayName,
                                                          style: TextStyle(
                                                            fontFamily: application
                                                                        .locale ==
                                                                    'ar'
                                                                ? 'Cairo'
                                                                : 'SF Compact Display',
                                                            fontSize:
                                                                ScreenUtil()
                                                                    .setSp(13),
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            color: isDayOff
                                                                ? Color(0xFFA0AEE6)
                                                                    .withOpacity(
                                                                        0.35)
                                                                : Color(
                                                                    0xFF7888D5),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: Center(
                                                          child: Container(
                                                            width: ScreenUtil()
                                                                .setWidth(82),
                                                            height: ScreenUtil()
                                                                .setHeight(0.3),
                                                            color: Color(
                                                                0xFFA0AEE6),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: Row(
                                                          mainAxisAlignment: isDayOff
                                                              ? MainAxisAlignment
                                                                  .center
                                                              : MainAxisAlignment
                                                                  .end,
                                                          children: <Widget>[
                                                            if (isDayOff)
                                                              Text(
                                                                S
                                                                    .of(context)
                                                                    .dayOff,
                                                                style:
                                                                    TextStyle(
                                                                  fontFamily: application
                                                                              .locale ==
                                                                          'ar'
                                                                      ? 'Cairo'
                                                                      : 'SF Compact Display',
                                                                  fontFamilyFallback: <
                                                                      String>[
                                                                    'SF Pro Text'
                                                                  ],
                                                                  fontSize: 13,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300,
                                                                  color: Color(
                                                                          0xFFA0AEE6)
                                                                      .withOpacity(
                                                                          0.35),
                                                                ),
                                                              ),
                                                            if (!isDayOff) ...[
                                                              Text(
                                                                "$hourFromStr:$minuteFromStr",
                                                                style:
                                                                    TextStyle(
                                                                  fontFamily: application
                                                                              .locale ==
                                                                          'ar'
                                                                      ? 'Cairo'
                                                                      : 'SF Compact Display',
                                                                  fontFamilyFallback: <
                                                                      String>[
                                                                    'SF Pro Text'
                                                                  ],
                                                                  fontSize: 13,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300,
                                                                  color: Color(
                                                                      0xFF01156B),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                  width: ScreenUtil()
                                                                      .setWidth(
                                                                          3.8)),
                                                              Image(
                                                                image: AssetImage(
                                                                    'assets/images/arrow-icon.png'),
                                                                width: ScreenUtil()
                                                                    .setWidth(
                                                                        8.6),
                                                                height: ScreenUtil()
                                                                    .setHeight(
                                                                        6.01),
                                                                matchTextDirection:
                                                                    true,
                                                              ),
                                                              SizedBox(
                                                                  width: ScreenUtil()
                                                                      .setWidth(
                                                                          3.8)),
                                                              Text(
                                                                "$hourToStr:$minuteToStr",
                                                                style:
                                                                    TextStyle(
                                                                  fontFamily: application
                                                                              .locale ==
                                                                          'ar'
                                                                      ? 'Cairo'
                                                                      : 'SF Compact Display',
                                                                  fontFamilyFallback: <
                                                                      String>[
                                                                    'SF Pro Text'
                                                                  ],
                                                                  fontSize: 13,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w300,
                                                                  color: Color(
                                                                      0xFF01156B),
                                                                ),
                                                              ),
                                                            ]
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                      ],
                                    ),
                                  ),
                                ),
                                // SizedBox(height: ScreenUtil().setHeight(18.4)),
                                Spacer(),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List<Widget>.generate(
                          2,
                          (int index) => Row(
                                children: <Widget>[
                                  CircleAvatar(
                                    radius: ScreenUtil()
                                            .setWidth(_page == index ? 11 : 9) /
                                        2,
                                    backgroundColor: _page == index
                                        ? Color(0xFF011F99)
                                        : Color(0xFF011F99).withOpacity(0.5),
                                  ),
                                  SizedBox(
                                      width: ScreenUtil()
                                          .setHeight(index == 1 ? 0 : 10)),
                                ],
                              )),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(31)),
                    InkWell(
                      onTap: _isLoading
                          ? null
                          : () {
                              _save();
                              // FocusScope.of(context).requestFocus(FocusNode());
                            },
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                        width: ScreenUtil().setWidth(170.16),
                        height: ScreenUtil().setHeight(40.2),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          gradient: LinearGradient(
                            colors: <Color>[
                              Color(0xff031C8D),
                              Color(0xff0A2FAF)
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                          ),
                        ),
                        child: _isLoading
                            ? CupertinoActivityIndicator(
                                animating: true,
                              )
                            : Text(
                                S.of(context).saveChanges,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF UI Display',
                                  fontFamilyFallback: <String>['SF Pro Text'],
                                  fontSize: ScreenUtil().setSp(11),
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xfffcfcfc),
                                ),
                              ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(30)),
                  ],
                ),
              ),
            ),
          ),
          Visibility(
            visible: _isDialogVisible,
            child: Positioned.fill(
              child: BackdropFilter(
                filter: ImageFilter.blur(
                    sigmaX: _blurAnimation.value * 5,
                    sigmaY: _blurAnimation.value * 5),
                child: Container(
                  color: Colors.grey.withOpacity(0.2),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _pickPhoto() async {
    final imageFile = await showCupertinoModalPopup<File>(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        title: Text(S.of(context).changeProfilePicture),
        actions: <Widget>[
          CupertinoActionSheetAction(
            onPressed: () async {
              final file =
                  await ImagePicker.pickImage(source: ImageSource.camera);
              Navigator.of(context).pop(file);
            },
            child: Text(S.of(context).takeNewPhoto),
          ),
          CupertinoActionSheetAction(
            onPressed: () async {
              final file =
                  await ImagePicker.pickImage(source: ImageSource.gallery);
              Navigator.of(context).pop(file);
            },
            child: Text(S.of(context).chooseExistingPhoto),
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(
            S.of(context).Cancel,
            style: TextStyle(
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
    );

    setState(() {
      _avatarFile = imageFile;
    });
  }

  bool _saveForm() {
    final form = _formKey.currentState;
    if (form == null) {
      return true;
    }
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> _save() async {
    if (_saveForm()) {
      setState(() {
        _isLoading = true;
      });

      if (_formKey.currentState != null && _avatarFile != null) {
        final extension =
            _avatarFile.path.substring(_avatarFile.path.lastIndexOf('.'));
        final storageRef = FirebaseStorage.instance
            .ref()
            .child('profile_pictures')
            .child("${_company.companyId}.$extension");

        final StorageUploadTask uploadTask = storageRef.putFile(
          _avatarFile,
          StorageMetadata(
            contentType: "image/$extension",
            customMetadata: <String, String>{
              'type': 'Profile Picture',
              'user_id': _company.companyId,
            },
          ),
        );

        final downloadUrl = await uploadTask.onComplete;
        final imageUrl = await downloadUrl.ref.getDownloadURL();
        _company.setData(avatarUrl: imageUrl);
      }

      final myLocation = _locationData['location'] as GeoPoint;
      GeoFirePoint location;
      if (myLocation != null) {
        location = _geo.point(
            latitude: myLocation.latitude, longitude: myLocation.longitude);
      }

      _company.setData(
        companyName: _companyName,
        email: _email,
        phoneNumber: _phoneNumber,
        location: myLocation,
        address: _locationData['address'],
        locality: _locationData['locality'],
        country: _locationData['country'],
        schedule: _schedule,
        locale: _locale,
      );
      final application = Provider.of<ApplicationStore>(context);
      application.localeStream.add(_locale);
      await Firestore.instance
          .collection('users')
          .document(_company.companyId)
          .updateData({
        'company_name': _company.companyName,
        'email': _company.email,
        'phone_number': _company.phoneNumber,
        'avatar_url': _company.avatarUrl,
        'location': location == null ? null : location.data,
        'address': _locationData['address'],
        'locality': _locationData['locality'],
        'country': _locationData['country'],
        'schedule': _schedule.map((int day, Map<String, dynamic> daySchedule) =>
            MapEntry(day.toString(), daySchedule)),
        'locale': _locale,
      });

      setState(() {
        _isLoading = false;
      });

      showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(S.of(context).success),
          content: Text(S.of(context).changesSaved),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).ok),
            ),
          ],
        ),
      );
    }
  }

  Future<Map<String, dynamic>> _showScheduleDialog(
      int day, Map<String, dynamic> daySchedule) {
    return showGeneralDialog<Map<String, dynamic>>(
        context: context,
        barrierDismissible: true,
        barrierLabel: 'Dismiss',
        transitionDuration: Duration(milliseconds: 200),
        pageBuilder: (BuildContext context, Animation<double> animation1,
            Animation<double> animation2) {
          animation1.addListener(() {
            setState(() {
              if (animation1.value == 1 &&
                  !_blurAnimationController.isCompleted) {
                _isDialogVisible = true;
                _blurAnimationController.forward();
              }
              if (animation1.value == 0) {
                _isDialogVisible = false;
                _blurAnimationController.reset();
              }
            });
          });
          return ValueListenableBuilder(
            valueListenable: animation1,
            builder: (BuildContext context, double animationValue, _) =>
                _ScheduleDialog(
                    day: day,
                    daySchedule: daySchedule,
                    animationValue: animationValue),
          );
        });
  }
}

class _ScheduleDialog extends StatefulWidget {
  final int day;
  final Map<String, dynamic> daySchedule;
  final double animationValue;

  _ScheduleDialog({Key key, this.day, this.daySchedule, this.animationValue})
      : super(key: key);

  @override
  __ScheduleDialogState createState() => __ScheduleDialogState();
}

class __ScheduleDialogState extends State<_ScheduleDialog>
    with SingleTickerProviderStateMixin {
  int _hourFrom;
  int _minuteFrom;
  int _hourTo;
  int _minuteTo;
  bool _isDayOff;
  bool _isEveryday;
  String _hourFromStr;
  String _minuteFromStr;
  String _hourToStr;
  String _minuteToStr;

  TextEditingController _hourFromController;
  TextEditingController _minuteFromController;
  TextEditingController _hourToController;
  TextEditingController _minuteToController;

  AnimationController _animationController;
  Animation<double> _animation;

  @override
  void initState() {
    super.initState();

    _animationController = new AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 200),
    );
    _animation = Tween(
      begin: 0.8,
      end: 1.0,
    ).animate(_animationController);

    _hourFrom = widget.daySchedule['from'] ~/ 100;
    _minuteFrom = widget.daySchedule['from'] % 100;
    _hourTo = widget.daySchedule['to'] ~/ 100;
    _minuteTo = widget.daySchedule['to'] % 100;
    _isDayOff = widget.daySchedule['is_day_off'] as bool;
    _isEveryday = widget.day == -1;

    _hourFromStr = _hourFrom >= 10 ? _hourFrom.toString() : "0$_hourFrom";
    _minuteFromStr =
        _minuteFrom >= 10 ? _minuteFrom.toString() : "0$_minuteFrom";
    _hourToStr = _hourTo >= 10 ? _hourTo.toString() : "0$_hourTo";
    _minuteToStr = _minuteTo >= 10 ? _minuteTo.toString() : "0$_minuteTo";

    _hourFromController = new TextEditingController(text: _hourFromStr);
    _minuteFromController = new TextEditingController(text: _minuteFromStr);
    _hourToController = new TextEditingController(text: _hourToStr);
    _minuteToController = new TextEditingController(text: _minuteToStr);
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    final dayNames = [
      S.of(context).monday,
      S.of(context).tuesday,
      S.of(context).wednesday,
      S.of(context).thursday,
      S.of(context).friday,
      S.of(context).saturday,
      S.of(context).sunday,
    ];

    final textSpan = TextSpan(
        text: '00',
        style: TextStyle(
          fontFamily:
              application.locale == 'ar' ? 'Cairo' : 'SF Compact Display',
          fontFamilyFallback: <String>['SF Pro Text'],
          fontSize: 22,
          fontWeight: FontWeight.w600,
        ));
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout();
    final textFieldWidth = textPainter.width;

    _animationController.forward();

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: ScaleTransition(
          scale: _animation,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(20),
                      right: ScreenUtil().setWidth(46),
                      bottom: ScreenUtil().setHeight(10 + 43 / 2),
                      left: ScreenUtil().setWidth(46),
                    ),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          _isEveryday
                              ? S.of(context).evereyDay
                              : dayNames[widget.day],
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Display',
                            fontFamilyFallback: <String>['SF Pro Text'],
                            fontSize: 15,
                            fontWeight: FontWeight.w900,
                            color: Color(0xFFA0AEE6),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(7)),
                        Flexible(
                          fit: FlexFit.loose,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Flexible(
                                fit: FlexFit.loose,
                                child: Row(
                                  textDirection: TextDirection.ltr,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    SizedBox(
                                      width: textFieldWidth,
                                      child: Transform.translate(
                                        offset: Offset(
                                            0, ScreenUtil().setHeight(5)),
                                        child: TextField(
                                          controller: _hourFromController,
                                          buildCounter: (BuildContext context,
                                                  {currentLength,
                                                  maxLength,
                                                  isFocused}) =>
                                              Container(),
                                          keyboardType: TextInputType.number,
                                          maxLength: 2,
                                          // maxLines: null,
                                          // maxLengthEnforced: true,
                                          // initialValue: _hourFromStr,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Compact Display',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: 22,
                                            fontWeight: FontWeight.w600,
                                            color: Color(0xFF01156B),
                                          ),
                                          decoration: InputDecoration.collapsed(
                                            hintText: '00',
                                            hintStyle: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Compact Display',
                                              fontFamilyFallback: <String>[
                                                'SF Pro Text'
                                              ],
                                              fontSize: 22,
                                              fontWeight: FontWeight.w600,
                                              color: Color(0xFF01156B),
                                            ),
                                            border: UnderlineInputBorder(
                                              borderSide: BorderSide.none,
                                            ),
                                          ),
                                          onChanged: (String value) {
                                            final intValue =
                                                int.tryParse(value);
                                            if (intValue != null &&
                                                intValue <= 23) {
                                              _hourFrom = intValue;
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                    Text(
                                      ':',
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'SF Compact Display',
                                        fontFamilyFallback: <String>[
                                          'SF Pro Text'
                                        ],
                                        fontSize: 22,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xFF01156B),
                                      ),
                                    ),
                                    SizedBox(
                                      width: textFieldWidth,
                                      child: Transform.translate(
                                        offset: Offset(
                                            0, ScreenUtil().setHeight(5)),
                                        child: TextField(
                                          controller: _minuteFromController,
                                          buildCounter: (BuildContext context,
                                                  {currentLength,
                                                  maxLength,
                                                  isFocused}) =>
                                              Container(),
                                          keyboardType: TextInputType.number,
                                          maxLength: 2,
                                          maxLengthEnforced: true,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Compact Display',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: 22,
                                            fontWeight: FontWeight.w600,
                                            color: Color(0xFF01156B),
                                          ),
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(0),
                                            border: UnderlineInputBorder(
                                              borderSide: BorderSide.none,
                                            ),
                                          ),
                                          onChanged: (String value) {
                                            final intValue =
                                                int.tryParse(value);
                                            if (intValue != null &&
                                                intValue <= 59) {
                                              _minuteFrom = intValue;
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(width: ScreenUtil().setWidth(9.2)),
                              Image(
                                image:
                                    AssetImage('assets/images/arrow-icon.png'),
                                width: ScreenUtil().setWidth(10.38),
                                height: ScreenUtil().setHeight(7.26),
                                matchTextDirection: true,
                              ),
                              SizedBox(width: ScreenUtil().setWidth(9.2)),
                              Flexible(
                                fit: FlexFit.loose,
                                child: Row(
                                  textDirection: TextDirection.ltr,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    SizedBox(
                                      width: textFieldWidth,
                                      child: Transform.translate(
                                        offset: Offset(
                                            0, ScreenUtil().setHeight(5)),
                                        child: TextField(
                                          controller: _hourToController,
                                          buildCounter: (BuildContext context,
                                                  {currentLength,
                                                  maxLength,
                                                  isFocused}) =>
                                              Container(),
                                          keyboardType: TextInputType.number,
                                          maxLength: 2,
                                          maxLengthEnforced: true,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Compact Display',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: 22,
                                            fontWeight: FontWeight.w600,
                                            color: Color(0xFF01156B),
                                          ),
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(0),
                                            border: UnderlineInputBorder(
                                              borderSide: BorderSide.none,
                                            ),
                                          ),
                                          onChanged: (String value) {
                                            final intValue =
                                                int.tryParse(value);
                                            if (intValue != null &&
                                                intValue <= 23) {
                                              _hourTo = intValue;
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                    Text(
                                      ':',
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'SF Compact Display',
                                        fontFamilyFallback: <String>[
                                          'SF Pro Text'
                                        ],
                                        fontSize: 22,
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xFF01156B),
                                      ),
                                    ),
                                    SizedBox(
                                      width: textFieldWidth,
                                      child: Transform.translate(
                                        offset: Offset(
                                            0, ScreenUtil().setHeight(5)),
                                        child: TextField(
                                          controller: _minuteToController,
                                          buildCounter: (BuildContext context,
                                                  {currentLength,
                                                  maxLength,
                                                  isFocused}) =>
                                              Container(),
                                          keyboardType: TextInputType.number,
                                          maxLength: 2,
                                          maxLengthEnforced: true,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Compact Display',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: 22,
                                            fontWeight: FontWeight.w600,
                                            color: Color(0xFF01156B),
                                          ),
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.all(0),
                                            border: UnderlineInputBorder(
                                              borderSide: BorderSide.none,
                                            ),
                                          ),
                                          onChanged: (String value) {
                                            final intValue =
                                                int.tryParse(value);
                                            if (intValue != null &&
                                                intValue <= 59) {
                                              _minuteTo = intValue;
                                            }
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Visibility(
                          visible: !_isEveryday,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Transform.scale(
                                scale: 0.6,
                                child: CupertinoSwitch(
                                  onChanged: (bool value) =>
                                      setState(() => _isDayOff = value),
                                  value: _isDayOff,
                                  activeColor: Color(0xFF00CB65),
                                ),
                              ),
                              SizedBox(width: ScreenUtil().setWidth(11)),
                              Text(
                                S.of(context).dayOff,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Compact Display',
                                  fontFamilyFallback: <String>['SF Pro Text'],
                                  fontSize: 13,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xFFA0AEE6),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Transform.translate(
                offset: Offset(0, -ScreenUtil().setHeight(43 / 2)),
                child: InkWell(
                  onTap: () {
                    // FocusScope.of(context).requestFocus(FocusNode());
                    final timeFrom = _hourFrom * 100 + _minuteFrom;
                    final timeTo = _hourTo * 100 + _minuteTo;
                    Navigator.of(context).pop({
                      'from': timeFrom,
                      'to': timeTo,
                      'is_day_off': _isDayOff,
                    });
                  },
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  child: Container(
                    width: ScreenUtil().setWidth(116),
                    height: ScreenUtil().setHeight(43),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      gradient: LinearGradient(
                        colors: <Color>[Color(0xff031C8D), Color(0xff0A2FAF)],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                      ),
                    ),
                    child: Text(
                      S.of(context).DONE,
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Text',
                        fontFamilyFallback: <String>['SF Pro Text'],
                        fontSize: 11,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
