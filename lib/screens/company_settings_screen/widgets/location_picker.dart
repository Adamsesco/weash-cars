import 'dart:async';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import "package:google_maps_webservice/geocoding.dart" as geocoding;
import "package:google_maps_webservice/places.dart" as places;
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/stores/application_store.dart';

class LocationPicker extends StatefulWidget {
  @override
  _LocationPickerState createState() => _LocationPickerState();
}

class _LocationPickerState extends State<LocationPicker> {
  final Location _locationService = Location();
  final geocoding.GoogleMapsGeocoding _geocoder =
      new geocoding.GoogleMapsGeocoding(
          apiKey: 'AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I');
  final places.GoogleMapsPlaces _places = new places.GoogleMapsPlaces(
      apiKey: 'AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I');
  GeoPoint _location;
  String _address;
  String _locality;
  String _country;

  TextEditingController _addressController = new TextEditingController();
  FocusNode _addressFocusNode = new FocusNode();
  List<String> _suggestedAddresses = [];

  Completer<GoogleMapController> _mapController = Completer();
  GoogleMapController _googleMapController;
  final Set<Marker> _markers = {};

  Timer _autocompleteTimer;
  bool _isAutocompleteLoading = false;
  bool _isMapLoading = false;

  @override
  void dispose() {
    _geocoder.dispose();
    _places.dispose();
    _autocompleteTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Material(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned.fill(
            child: GoogleMap(
              onMapCreated: (GoogleMapController controller) {
                if (_googleMapController == null) {
                  _mapController.complete(controller);
                  _googleMapController = controller;
                  _getCurrentLocation();
                }
              },
              onTap: (LatLng location) async {
                setState(() {
                  _isMapLoading = true;
                });

                if (_location == null) {
                  _googleMapController.moveCamera(CameraUpdate.newLatLng(
                      LatLng(location.latitude, location.longitude)));
                } else {
                  _googleMapController.animateCamera(CameraUpdate.newLatLng(
                      LatLng(location.latitude, location.longitude)));
                }

                setState(() {
                  _location =
                      new GeoPoint(location.latitude, location.longitude);

                  _markers.clear();
                  _markers.add(Marker(
                    markerId: MarkerId('me'),
                    position: LatLng(location.latitude, location.longitude),
                    icon: BitmapDescriptor.defaultMarker,
                  ));
                });

                final addressResult = await _geocoder.searchByLocation(
                    new geocoding.Location(
                        location.latitude, location.longitude));
                if (addressResult.isOkay && addressResult.results.length > 0) {
                  final result = addressResult.results.firstWhere((result) {
                    final components = result.addressComponents;
                    if (components == null || components.length == 0)
                      return false;
                    final hasLocality = components.any(
                        (component) => component.types.contains('locality'));
                    final hasCountry = components.any(
                        (component) => component.types.contains('country'));
                    return hasLocality && hasCountry;
                  }, orElse: () {
                    print("No result");
                    return null;
                  });

                  if (result != null) {
                    final address = result.formattedAddress;
                    final locality = result.addressComponents.firstWhere(
                        (addressComponent) =>
                            addressComponent.types.contains('locality'),
                        orElse: () {
                      print("No result");
                      return null;
                    }).longName;

                    final country = result.addressComponents.firstWhere(
                        (addressComponent) =>
                            addressComponent.types.contains('country'),
                        orElse: () {
                      print("No result");
                      return null;
                    }).shortName;

                    setState(() {
                      _address = address;
                      _locality = locality;
                      _country = country;
                      _addressController.text = address;
                    });
                  }

                  setState(() {
                    _isMapLoading = false;
                  });
                }
              },
              mapType: MapType.normal,
              initialCameraPosition: CameraPosition(
                target: _location == null
                    ? LatLng(0, 0)
                    : LatLng(_location.latitude, _location.longitude),
                zoom: 17,
              ),
              markers: _markers,
            ),
          ),
          if (_isMapLoading)
            Positioned.fill(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
                child: Container(
                  color: Colors.white.withOpacity(0.5),
                ),
              ),
            ),
          if (_isMapLoading)
            CupertinoActivityIndicator(
              animating: true,
            ),
          Positioned(
            top: ScreenUtil().setHeight(71),
            left: ScreenUtil().setWidth(30),
            right: ScreenUtil().setWidth(30),
            child: Stack(
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        width: ScreenUtil().setWidth(45),
                        height: ScreenUtil().setWidth(45),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Color(0x4d000000),
                              offset: Offset(0, ScreenUtil().setHeight(3)),
                              blurRadius: 4,
                            ),
                          ],
                        ),
                        child: Center(
                          child: Image(
                            image: AssetImage(
                                'assets/images/back-button-icon.png'),
                            width: ScreenUtil().setWidth(22.36),
                            height: ScreenUtil().setHeight(15.63),
                            color: Color(0xFF9AA1A8),
                            matchTextDirection: true,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: ScreenUtil().setWidth(12.1)),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(
                          bottom: ScreenUtil().setHeight(
                              (_addressFocusNode.hasFocus &&
                                          _suggestedAddresses.isNotEmpty) ||
                                      _isAutocompleteLoading
                                  ? 6
                                  : 0),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Color(0x4d000000),
                              offset: Offset(0, ScreenUtil().setHeight(3)),
                              blurRadius: 4,
                            ),
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            TextField(
                              controller: _addressController,
                              focusNode: _addressFocusNode,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(0),
                                  vertical: ScreenUtil().setHeight(0),
                                ),
                                prefixIcon: InkWell(
                                  onTap: _getCurrentLocation,
                                  child: Image(
                                    image: AssetImage(
                                        'assets/images/current-location-icon.png'),
                                    color: Color(0xFF9AA1A8),
                                  ),
                                ),
                                suffixIcon: InkWell(
                                  onTap: () => setState(
                                      () => _addressController.clear()),
                                  child: Image(
                                    image: AssetImage(
                                        'assets/images/close-icon.png'),
                                    color: Color(0xFF9AA1A8),
                                  ),
                                ),
                                hintText: S.of(context).selectRegion,
                                hintStyle: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xFF3A4655).withOpacity(0.4),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: BorderSide.none,
                                ),
                              ),
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                                color: Color(0xFF3A4655),
                              ),
                              onChanged: (String text) {
                                _getAddressSuggestions(text);
                              },
                            ),
                            Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: _addressFocusNode.hasFocus
                                      ? _suggestedAddresses
                                          .map((String address) {
                                          return InkWell(
                                            onTap: () async {
                                              setState(() {
                                                _isMapLoading = true;
                                              });

                                              FocusScope.of(context)
                                                  .requestFocus(
                                                      new FocusNode());

                                              final addressLocationResponse =
                                                  await _geocoder
                                                      .searchByAddress(address);
                                              if (addressLocationResponse
                                                      .isOkay &&
                                                  addressLocationResponse
                                                          .results.length >
                                                      0) {
                                                final addressLocation =
                                                    addressLocationResponse
                                                        .results[0]
                                                        .geometry
                                                        .location;
                                                final locality = addressLocationResponse
                                                    .results[0]
                                                    .addressComponents
                                                    .firstWhere(
                                                        (addressComponent) =>
                                                            addressComponent
                                                                .types
                                                                .contains(
                                                                    'locality') &&
                                                            addressComponent
                                                                .types
                                                                .contains(
                                                                    'political'),
                                                        orElse: () {
                                                  print("No result");
                                                  return null;
                                                }).longName;
                                                final country = addressLocationResponse
                                                    .results[0]
                                                    .addressComponents
                                                    .firstWhere(
                                                        (addressComponent) =>
                                                            addressComponent
                                                                .types
                                                                .contains(
                                                                    'country') &&
                                                            addressComponent
                                                                .types
                                                                .contains(
                                                                    'political'),
                                                        orElse: () {
                                                  print("No result");
                                                  return null;
                                                }).shortName;
                                                setState(() {
                                                  _location = new GeoPoint(
                                                      addressLocation.lat,
                                                      addressLocation.lng);
                                                  _address = address;
                                                  _locality = locality;
                                                  _country = country;
                                                  _addressController.text =
                                                      address;

                                                  _markers.clear();
                                                  _markers.add(Marker(
                                                    markerId:
                                                        MarkerId(_address),
                                                    position: LatLng(
                                                        addressLocation.lat,
                                                        addressLocation.lng),
                                                    icon: BitmapDescriptor
                                                        .defaultMarker,
                                                  ));

                                                  _isMapLoading = false;

                                                  _googleMapController
                                                      .animateCamera(CameraUpdate
                                                          .newLatLng(LatLng(
                                                              addressLocation
                                                                  .lat,
                                                              addressLocation
                                                                  .lng)));
                                                });
                                              }
                                            },
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  ScreenUtil().setWidth(60),
                                              padding: EdgeInsets.symmetric(
                                                horizontal:
                                                    ScreenUtil().setWidth(20),
                                                vertical:
                                                    ScreenUtil().setHeight(10),
                                              ),
                                              child: Text(
                                                address,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'SF Pro Text',
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500,
                                                  color: Color(0xFF3A4655),
                                                ),
                                              ),
                                            ),
                                          );
                                        }).toList()
                                      : <Widget>[],
                                ),
                                if (_isAutocompleteLoading)
                                  Positioned.fill(
                                    child: ClipRect(
                                      child: BackdropFilter(
                                        filter: ImageFilter.blur(
                                            sigmaX: 2, sigmaY: 2),
                                        child: Container(
                                          color: Colors.white.withOpacity(0.5),
                                        ),
                                      ),
                                    ),
                                  ),
                                if (_isAutocompleteLoading)
                                  Container(
                                    width: MediaQuery.of(context).size.width -
                                        ScreenUtil().setWidth(60),
                                    alignment: Alignment.center,
                                    child: CupertinoActivityIndicator(
                                      animating: true,
                                    ),
                                  ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            bottom: ScreenUtil().setHeight(68),
            left: ScreenUtil().setWidth(102),
            right: ScreenUtil().setWidth(102),
            child: InkWell(
              onTap: () {
                if (_location != null && _address != null) {
                  Navigator.of(context).pop({
                    'location': _location,
                    'address': _address,
                    'locality': _locality,
                    'country': _country,
                  });
                }
              },
              borderRadius: BorderRadius.circular(20),
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(
                  vertical: ScreenUtil().setHeight(14),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  gradient: LinearGradient(
                    colors: [Color(0xFF0026A1), Color(0xFF000F66)],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Color(0x66000000),
                      offset: Offset(0, 3),
                      blurRadius: 4,
                    ),
                  ],
                ),
                child: Text(
                  S.of(context).confirmLocation,
                  style: TextStyle(
                    fontFamily:
                        application.locale == 'ar' ? 'Cairo' : 'SF UI Display',
                    fontSize: ScreenUtil().setSp(11),
                    fontWeight: FontWeight.w700,
                    color: Color(0xfffcfcfc),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _getCurrentLocation() async {
    await _locationService.changeSettings(accuracy: LocationAccuracy.HIGH);
    LocationData location;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      location = await _locationService.getLocation();

      setState(() {
        if (_location == null) {
          _googleMapController.moveCamera(CameraUpdate.newLatLng(
              LatLng(location.latitude, location.longitude)));
        } else {
          _googleMapController.animateCamera(CameraUpdate.newLatLng(
              LatLng(location.latitude, location.longitude)));
        }
        _location = new GeoPoint(location.latitude, location.longitude);

        _markers.clear();
        _markers.add(Marker(
          markerId: MarkerId('me'),
          position: LatLng(location.latitude, location.longitude),
          icon: BitmapDescriptor.defaultMarker,
        ));
      });

      final addressResult = await _geocoder.searchByLocation(
          new geocoding.Location(location.latitude, location.longitude));
      if (addressResult.isOkay && addressResult.results.length > 0) {
        final result = addressResult.results.firstWhere((result) {
          final components = result.addressComponents;
          if (components == null || components.length == 0) return false;
          final hasLocality = components
              .any((component) => component.types.contains('locality'));
          final hasCountry = components
              .any((component) => component.types.contains('country'));
          return hasLocality && hasCountry;
        }, orElse: () {
          print("No result");
          return null;
        });

        if (result != null) {
          final address = result.formattedAddress;
          final locality = result.addressComponents.firstWhere(
              (addressComponent) => addressComponent.types.contains('locality'),
              orElse: () {
            print("No result");
            return null;
          }).longName;

          final country = result.addressComponents.firstWhere(
              (addressComponent) => addressComponent.types.contains('country'),
              orElse: () {
            print("No result");
            return null;
          }).shortName;

          setState(() {
            _address = address;
            _locality = locality;
            _country = country;
            _addressController.text = address;
          });
        }
      }
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            content: Text(S.of(context).allowLocation),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(S.of(context).ok),
              ),
            ],
          ),
        );
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            content: Text(S.of(context).enableLocationService),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(S.of(context).ok),
              ),
            ],
          ),
        );
      }
    }
  }

  Future<void> _getAddressSuggestions(String partialAddress) async {
    if (_autocompleteTimer != null) {
      _autocompleteTimer.cancel();
    }

    if (partialAddress.isEmpty) {
      setState(() {
        _isAutocompleteLoading = false;
      });
      return;
    }

    setState(() {
      _isAutocompleteLoading = true;
    });

    _autocompleteTimer = new Timer(const Duration(seconds: 2), () async {
      final response = await _places.queryAutocomplete(partialAddress);
      if (response.isOkay && response.predictions.length > 0) {
        final suggestedAddresses =
            response.predictions.map((p) => p.description).toList();
        setState(() {
          _suggestedAddresses = suggestedAddresses;
          _isAutocompleteLoading = false;
        });
      }
    });
  }
}
