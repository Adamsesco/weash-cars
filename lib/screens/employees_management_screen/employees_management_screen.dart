import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/screens/company_dashboard_screen/widgets/incomes_chart.dart';
import 'package:weash_cars/services/IncomesService.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/balance_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/stores/orders_store.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class EmployeesManagementScreen extends StatefulWidget {
  @override
  _EmployeesManagementScreenState createState() =>
      _EmployeesManagementScreenState();
}

class _EmployeesManagementScreenState extends State<EmployeesManagementScreen> {
  CompanyStore _company;

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  bool _isDialogVisible = false;
  double _price = 12.0;

  @override
  void didChangeDependencies() {
    final applicationStore = Provider.of<ApplicationStore>(context);
    _company = applicationStore.companyStore;

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CompanyDrawer(
              onCloseDrawer: () {
                setState(() => _isDialogVisible = false);
              },
              avatarUrl: _company.avatarUrl,
            ),
      ),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Image(
                image:
                    AssetImage('assets/images/company-screens-background.png'),
                matchTextDirection: true,
                fit: BoxFit.fill),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Observer(
                builder: (BuildContext context) => CustomAppBar2(
                      onOpenDrawer: () {
                        setState(() {
                          _isDialogVisible = true;
                          Scaffold.of(context).openDrawer();
                        });
                      },
                      title: S.of(context).dashboard,
                      transparentBackground: true,
                      titleColor: Color(0xFF91A6D9),
                      buttonColor: Color(0xFF0930C3),
                      nbNotifications: 0,
                      avatar: _company.avatarUrl == null
                          ? null
                          : NetworkImage(_company.avatarUrl),
                    ),
              ),
              SizedBox(height: ScreenUtil().setHeight(58.5)),
              Align(
                alignment: application.locale == 'ar'
                    ? Alignment.topRight
                    : Alignment.topLeft,
                child: Container(
                  margin: EdgeInsets.only(
                    left: application.locale == 'ar'
                        ? 0
                        : ScreenUtil().setWidth(25),
                    right: application.locale == 'ar'
                        ? ScreenUtil().setWidth(25)
                        : 0,
                  ),
                  child: Text(
                    S.of(context).employees.toUpperCase(),
                    style: prefix0.TextStyle(
                      fontFamily: application.locale == 'ar'
                          ? 'Cairo'
                          : 'SF Pro Display',
                      fontFamilyFallback: <String>['SF Pro Text'],
                      fontSize: 24,
                      fontWeight: FontWeight.w900,
                      color: Color(0xFFA0AEE6),
                    ),
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(57)),
              Expanded(
                child: Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(20)),
                  child: ListView.separated(
                    physics: BouncingScrollPhysics(),
                    itemCount: _company.employees == null
                        ? 0
                        : _company.employees.length,
                    separatorBuilder: (BuildContext context, int index) =>
                        SizedBox(height: ScreenUtil().setHeight(12)),
                    itemBuilder: (BuildContext context, int index) {
                      final employee = _company.employees[index];
                      final employeeId = employee['employee_id'];
                      final employeeAvatarUrl = employee['avatar_url'];
                      final employeeFirstName = employee['first_name'];
                      final employeeLastName = employee['last_name'];
                      final jobsDoneCount = employee['jobs_done'] as int;
                      final creationDate = employee['created_at'] as DateTime;
                      final employeeSince =
                          DateFormat('d MMMM y').format(creationDate);

                      return Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => Navigator.pushNamed(
                              context, "/employees/edit/$employeeId"),
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Container(
                            height: ScreenUtil().setHeight(110),
                            padding: EdgeInsets.only(
                              left: ScreenUtil().setWidth(
                                  application.locale == 'ar' ? 13.2 : 0),
                              top: ScreenUtil().setHeight(10),
                              right: ScreenUtil().setWidth(
                                  application.locale == 'ar' ? 0 : 13.2),
                              bottom: ScreenUtil().setHeight(10),
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.horizontal(
                                right: application.locale == 'ar'
                                    ? Radius.circular(
                                        ScreenUtil().setHeight(110) / 2)
                                    : Radius.circular(8),
                                left: application.locale == 'ar'
                                    ? Radius.circular(8)
                                    : Radius.circular(
                                        ScreenUtil().setHeight(110) / 2),
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x44999999),
                                  blurRadius: 8,
                                ),
                              ],
                            ),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  width: ScreenUtil().setWidth(87),
                                  height: ScreenUtil().setHeight(87),
                                  padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(5),
                                    vertical: ScreenUtil().setHeight(5),
                                  ),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0x88999999),
                                        blurRadius: 8,
                                      ),
                                    ],
                                  ),
                                  child: ClipPath(
                                    clipper: ShapeBorderClipper(
                                      shape: CircleBorder(),
                                    ),
                                    child: Image(
                                      image: employeeAvatarUrl == null
                                          ? AssetImage(
                                              'assets/images/avatar-user.png')
                                          : NetworkImage(employeeAvatarUrl),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                SizedBox(width: ScreenUtil().setWidth(19)),
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "$employeeFirstName $employeeLastName",
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'SF Pro Text',
                                        fontFamilyFallback: <String>[
                                          'SF Pro Text'
                                        ],
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700,
                                        color: Color(0xFF021A89),
                                      ),
                                    ),
                                    Text(
                                      "$jobsDoneCount ${S.of(context).jobsDone}",
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'SF Pro Text',
                                        fontFamilyFallback: <String>[
                                          'SF Pro Text'
                                        ],
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xFF021A89),
                                      ),
                                    ),
                                    // SizedBox(height: ScreenUtil().setHeight(7)),
                                    Text(
                                      "${S.of(context).employeeSince} $employeeSince",
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'SF Pro Text',
                                        fontFamilyFallback: <String>[
                                          'SF Pro Text'
                                        ],
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xFF021A89),
                                      ),
                                    ),
                                  ],
                                ),
                                Expanded(
                                  child: Align(
                                    alignment: application.locale == 'ar'
                                        ? Alignment.topLeft
                                        : Alignment.topRight,
                                    child: Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                        child: Image(
                                          image: AssetImage(
                                              'assets/images/edit-icon.png'),
                                          width: ScreenUtil().setWidth(18),
                                          height: ScreenUtil().setWidth(18),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(20)),
              IconButton(
                onPressed: () => Navigator.pushNamed(context, '/employees/add'),
                iconSize: ScreenUtil().setWidth(61.17),
                icon: Image(
                  image: AssetImage('assets/images/new-order-icon.png'),
                  width: ScreenUtil().setWidth(61.17),
                  height: ScreenUtil().setWidth(61.17),
                ),
              ),
            ],
          ),
          // Visibility(
          //   visible: _isDialogVisible,
          //   child: Positioned.fill(
          //     child: BackdropFilter(
          //       filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          //       child: Container(
          //         color: Colors.grey.withOpacity(0.2),
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
