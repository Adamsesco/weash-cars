import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/vehicle_type.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class EditPackageScreen extends StatefulWidget {
  final String packageId;
  final VehicleType vehicleType;

  EditPackageScreen({Key key, this.packageId, this.vehicleType})
      : super(key: key);

  @override
  _EditPackageScreenState createState() => _EditPackageScreenState();
}

class _EditPackageScreenState extends State<EditPackageScreen> {
  CompanyStore _company;
  List<Map<String, dynamic>> _packages;
  Map<String, dynamic> _selectedPackage;

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  GlobalKey _dropdownButtonKey = new GlobalKey();
  Offset _dropdownButtonOffset = new Offset(0, 0);

  String _packageTitle;
  List<TextEditingController> _controllers;
  TextEditingController _priceController;
  TextEditingController _durationHoursController;
  TextEditingController _durationMinutesController;
  FocusNode _hoursFocusNode = new FocusNode();
  FocusNode _minutesFocusNode = new FocusNode();
  double _price;
  double _oldPrice;
  int _duration;
  bool _isDialogVisible = false;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    final applicationStore =
        Provider.of<ApplicationStore>(context, listen: false);
    _company = applicationStore.companyStore;

    _packages = _company.packages
        .where((package) => package['vehicle_type'] == widget.vehicleType)
        .toList();
    _selectedPackage = _packages.firstWhere(
        (package) => package['package_id'] == widget.packageId, orElse: () {
      print("No result");
      return null;
    });

    if (_selectedPackage != null) {
      _setPackage(_selectedPackage);
    }
  }

  @override
  void dispose() {
    _controllers.forEach((controller) => controller?.dispose());

    super.dispose();
  }

  void _setPackage(Map<String, dynamic> package) {
    _selectedPackage = package;
    _packageTitle = _selectedPackage['title'];

    final services =
        List<String>.from(_selectedPackage['services'] as List<String>);
    _controllers = services.map((String service) {
      final controller = TextEditingController(text: service);
      controller.addListener(() => _serviceListener(controller));
      return controller;
    }).toList();

    _price = _selectedPackage['price'] as double;
    _oldPrice = _selectedPackage['old_price'] as double;
    _duration = _selectedPackage['duration'] as int;

    _priceController = TextEditingController(text: _price.toString());
    _durationHoursController = TextEditingController(
        text: _duration == null ? '' : (_duration ~/ 100).toString());
    _durationMinutesController = TextEditingController(
        text: _duration == null ? '' : (_duration % 100).toString());
  }

  void _serviceListener(TextEditingController controller) {
    final value = controller.text;

    if (!value.contains('\n')) return;

    var services = <String>[''];

    if (value != null && value.isNotEmpty) {
      services = value.trim().split('\n');
      services = services.map((service) => service.trim()).toList();
    }

    final index = _controllers.indexOf(controller);
    if (index == -1) return;

    setState(() {
      controller.text = services.first;

      if (services.length > 1) {
        final rest = services.sublist(1);
        final restControllers = rest.map((service) {
          final cntrl = TextEditingController(text: service);
          cntrl.addListener(() => _serviceListener(cntrl));
          return cntrl;
        });

        _controllers.insertAll(index + 1, restControllers);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CompanyDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _company.avatarUrl,
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image(
                image:
                    AssetImage('assets/images/company-screens-background.png'),
                matchTextDirection: true,
                fit: BoxFit.fill),
          ),
          Positioned(
            top: 0,
            child: Builder(
              builder: (BuildContext context) => CustomAppBar2(
                onOpenDrawer: () {
                  setState(() {
                    // _isDialogVisible = true;
                    Scaffold.of(context).openDrawer();
                  });
                },
                title: S.of(context).dashboard,
                transparentBackground: true,
                titleColor: Color(0xFF91A6D9),
                buttonColor: Color(0xFF0930C3),
                nbNotifications: 0,
                avatar: _company.avatarUrl == null
                    ? null
                    : NetworkImage(_company.avatarUrl),
              ),
            ),
          ),
          Positioned.fill(
            top: ScreenUtil().setHeight(184),
            right: ScreenUtil().setWidth(25),
            left: ScreenUtil().setWidth(25),
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: application.locale == 'ar'
                        ? Alignment.topRight
                        : Alignment.topLeft,
                    child: SizedBox(
                      height: ScreenUtil().setHeight(53),
                      child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: Text(
                          S.of(context).editPackages,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Display',
                            fontWeight: FontWeight.w900,
                            color: Color(0xFFA0AEE6),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(50)),
                  InkWell(
                    onTap: _showDropdownItems,
                    key: _dropdownButtonKey,
                    child: Container(
                      width: ScreenUtil().setWidth(285),
                      height: ScreenUtil().setHeight(52),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                            color: Color(0x44666666),
                            blurRadius: 10,
                          ),
                        ],
                      ),
                      child: Builder(
                        builder: (BuildContext context) {
                          final selectedWidget = Container(
                            width: ScreenUtil().setWidth(272),
                            height: ScreenUtil().setHeight(52),
                            margin: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(7),
                              vertical: ScreenUtil().setHeight(3),
                            ),
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(16),
                              vertical: ScreenUtil().setHeight(5),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image(
                                  image: AssetImage(
                                      'assets/images/packages-icon.png'),
                                  width: ScreenUtil().setWidth(20.54),
                                  height: ScreenUtil().setHeight(23.38),
                                  color: Color(0xFF021A89),
                                ),
                                Expanded(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: SizedBox(
                                      height: ScreenUtil().setHeight(20),
                                      child: FittedBox(
                                        fit: BoxFit.fitHeight,
                                        child: Text(
                                          _packageTitle,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'Helvetica Rounded LT',
                                            fontWeight: FontWeight.w700,
                                            color: Color(0xFF021A89),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Image(
                                  image: AssetImage(
                                      'assets/images/dropdown-button-icon.png'),
                                  width: ScreenUtil().setWidth(11.37),
                                  height: ScreenUtil().setHeight(5.69),
                                ),
                              ],
                            ),
                          );

                          return selectedWidget;
                        },
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(22.9)),
                  Container(
                    width: ScreenUtil().setWidth(311),
                    // height: ScreenUtil().setHeight(312),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x44666666),
                          blurRadius: 10,
                        ),
                      ],
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox(height: ScreenUtil().setHeight(30)),
                        SizedBox(
                          width: ScreenUtil().setWidth(233.5),
                          height: ScreenUtil().setHeight(180),
                          child: ListView.separated(
                            itemCount: _controllers.length,
                            padding: EdgeInsets.zero,
                            separatorBuilder:
                                (BuildContext context, int index) => SizedBox(
                                    height: ScreenUtil().setHeight(
                                        index == _controllers.length
                                            ? 17.5
                                            : 5.5)),
                            itemBuilder: (BuildContext context, int index) {
                              return Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      InkWell(
                                        onTap: () => setState(() {
                                          if (_controllers.length > 1) {
                                            setState(() {
                                              _controllers.removeAt(index);
                                            });
                                          }
                                        }),
                                        child: Image(
                                          image: AssetImage(
                                              'assets/images/minus-icon.png'),
                                          width: ScreenUtil().setWidth(16),
                                        ),
                                      ),
                                      SizedBox(
                                          width: ScreenUtil().setWidth(16)),
                                      Expanded(
                                        child: TextField(
                                          controller: _controllers[index],
                                          minLines: 1,
                                          maxLines: 2,
                                          textCapitalization:
                                              TextCapitalization.sentences,
                                          decoration: InputDecoration(
                                            border: UnderlineInputBorder(
                                                borderSide: BorderSide.none),
                                            contentPadding: EdgeInsets.all(0),
                                          ),
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Text',
                                            fontSize: 16,
                                            fontWeight: FontWeight.w700,
                                            color: Color(0xFF021A89),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: ScreenUtil().setHeight(5.5)),
                                  Container(
                                    width: ScreenUtil().setWidth(203),
                                    height: 0.3,
                                    margin: EdgeInsets.only(
                                        left: ScreenUtil().setWidth(15.9)),
                                    decoration: BoxDecoration(
                                      color: Color(0xFF91A6D9),
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(20)),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(45),
                          ),
                          child: InkWell(
                            onTap: () {
                              final controller =
                                  TextEditingController(text: '');
                              controller.addListener(
                                  () => _serviceListener(controller));

                              setState(() {
                                _controllers.add(controller);
                              });
                            },
                            child: Row(
                              children: <Widget>[
                                SizedBox(width: ScreenUtil().setWidth(17)),
                                Image(
                                  image:
                                      AssetImage('assets/images/plus-icon.png'),
                                  width: ScreenUtil().setWidth(10),
                                  color: Color(0xFF91A6D9),
                                ),
                                SizedBox(width: ScreenUtil().setWidth(9.6)),
                                Text(
                                  S.of(context).addService,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFF91A6D9),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(20)),
                        Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                SizedBox(width: ScreenUtil().setWidth(45)),
                                Expanded(
                                  flex: 1,
                                  child: Text(
                                    S.of(context).Price,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontSize: 19,
                                      fontWeight: FontWeight.w800,
                                      color: Color(0xFF00218C),
                                    ),
                                  ),
                                ),
                                SizedBox(width: ScreenUtil().setWidth(15)),
                                Expanded(
                                  flex: 2,
                                  child: Builder(
                                    builder: (BuildContext context) {
                                      final textSpan = TextSpan(
                                        text: '00.000',
                                        style: TextStyle(
                                          fontFamily: 'SF Pro Text',
                                          fontSize: 24,
                                          fontWeight: FontWeight.w800,
                                          color: Color(0xFF00218C),
                                        ),
                                      );

                                      final textPainter = TextPainter(
                                        text: textSpan,
                                        textDirection: TextDirection.ltr,
                                      );
                                      textPainter.layout();

                                      final width = textPainter.width;

                                      return Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          SizedBox(
                                            width: width,
                                            child: TextField(
                                              controller: _priceController,
                                              textDirection: TextDirection.ltr,
                                              keyboardType: TextInputType
                                                  .numberWithOptions(
                                                      decimal: true),
                                              maxLength: 5,
                                              buildCounter:
                                                  (BuildContext context,
                                                          {int currentLength,
                                                          int maxLength,
                                                          bool isFocused}) =>
                                                      Container(),
                                              inputFormatters: <
                                                  TextInputFormatter>[
                                                WhitelistingTextInputFormatter(
                                                    RegExp('[0-9.]')),
                                              ],
                                              cursorWidth: 1,
                                              decoration: InputDecoration(
                                                contentPadding: EdgeInsets.zero,
                                                border: UnderlineInputBorder(
                                                    borderSide:
                                                        BorderSide.none),
                                              ),
                                              style: TextStyle(
                                                fontFamily: 'SF Pro Text',
                                                fontSize: 24,
                                                fontWeight: FontWeight.w800,
                                                color: Color(0xFF00218C),
                                              ),
                                              onChanged: (String value) {
                                                final price =
                                                    double.tryParse(value);
                                                if (price != null) {
                                                  setState(() {
                                                    _price = price;
                                                  });
                                                }
                                              },
                                            ),
                                          ),
                                          Container(
                                            width: width + 4,
                                            height: 0.3,
                                            color: Color(0xFF91A6D9),
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: ScreenUtil().setHeight(12)),
                            Row(
                              children: <Widget>[
                                SizedBox(width: ScreenUtil().setWidth(45)),
                                Expanded(
                                  flex: 1,
                                  child: Text(
                                    S.of(context).Duration,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontSize: 19,
                                      fontWeight: FontWeight.w800,
                                      color: Color(0xFF00218C),
                                    ),
                                  ),
                                ),
                                SizedBox(width: ScreenUtil().setWidth(15)),
                                Expanded(
                                  flex: 2,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Builder(builder: (context) {
                                        final textSpan = TextSpan(
                                          text: '0',
                                          style: TextStyle(
                                            fontFamily: 'SF Pro Text',
                                            fontSize: 24,
                                            fontWeight: FontWeight.w800,
                                            color: Color(0xFF00218C),
                                          ),
                                        );

                                        final textPainter = TextPainter(
                                          text: textSpan,
                                          textDirection: TextDirection.ltr,
                                        );
                                        textPainter.layout();

                                        final width = textPainter.width;

                                        return Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            SizedBox(
                                              width: width,
                                              child: TextField(
                                                controller:
                                                    _durationHoursController,
                                                focusNode: _hoursFocusNode,
                                                textDirection:
                                                    TextDirection.ltr,
                                                keyboardType:
                                                    TextInputType.number,
                                                textInputAction:
                                                    TextInputAction.next,
                                                maxLength: 1,
                                                buildCounter:
                                                    (BuildContext context,
                                                            {int currentLength,
                                                            int maxLength,
                                                            bool isFocused}) =>
                                                        Container(),
                                                inputFormatters: <
                                                    TextInputFormatter>[
                                                  WhitelistingTextInputFormatter
                                                      .digitsOnly,
                                                ],
                                                cursorWidth: 1,
                                                decoration: InputDecoration(
                                                  contentPadding:
                                                      EdgeInsets.zero,
                                                  border: UnderlineInputBorder(
                                                      borderSide:
                                                          BorderSide.none),
                                                ),
                                                style: TextStyle(
                                                  fontFamily: 'SF Pro Text',
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.w800,
                                                  color: Color(0xFF00218C),
                                                ),
                                                onChanged: (String value) {
                                                  final hours =
                                                      int.tryParse(value);
                                                  if (hours != null) {
                                                    setState(() {
                                                      if (_duration == null) {
                                                        _duration = hours * 100;
                                                      } else {
                                                        _duration =
                                                            _duration % 100;
                                                        _duration +=
                                                            hours * 100;
                                                      }
                                                    });
                                                  }
                                                },
                                                onSubmitted: (String value) =>
                                                    FocusScope.of(context)
                                                        .requestFocus(
                                                            _minutesFocusNode),
                                              ),
                                            ),
                                            Container(
                                              width: width + 4,
                                              height: 0.3,
                                              color: Color(0xFF91A6D9),
                                            ),
                                          ],
                                        );
                                      }),
                                      SizedBox(
                                          width: ScreenUtil().setWidth(10)),
                                      Text(
                                        S.of(context).hourShort,
                                        style: TextStyle(
                                          fontFamily: 'SF Pro Text',
                                          fontSize: 18,
                                          fontWeight: FontWeight.w800,
                                          color: Color(0xFF00218C),
                                        ),
                                      ),
                                      SizedBox(
                                          width: ScreenUtil().setWidth(10)),
                                      Builder(builder: (context) {
                                        final textSpan = TextSpan(
                                          text: '00',
                                          style: TextStyle(
                                            fontFamily: 'SF Pro Text',
                                            fontSize: 24,
                                            fontWeight: FontWeight.w800,
                                            color: Color(0xFF00218C),
                                          ),
                                        );

                                        final textPainter = TextPainter(
                                          text: textSpan,
                                          textDirection: TextDirection.ltr,
                                        );
                                        textPainter.layout();

                                        final width = textPainter.width;

                                        return Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            SizedBox(
                                              width: width,
                                              child: TextField(
                                                controller:
                                                    _durationMinutesController,
                                                focusNode: _minutesFocusNode,
                                                textDirection:
                                                    TextDirection.ltr,
                                                keyboardType:
                                                    TextInputType.number,
                                                maxLength: 2,
                                                buildCounter:
                                                    (BuildContext context,
                                                            {int currentLength,
                                                            int maxLength,
                                                            bool isFocused}) =>
                                                        Container(),
                                                inputFormatters: <
                                                    TextInputFormatter>[
                                                  WhitelistingTextInputFormatter
                                                      .digitsOnly,
                                                ],
                                                cursorWidth: 1,
                                                decoration: InputDecoration(
                                                  contentPadding:
                                                      EdgeInsets.zero,
                                                  border: UnderlineInputBorder(
                                                      borderSide:
                                                          BorderSide.none),
                                                ),
                                                style: TextStyle(
                                                  fontFamily: 'SF Pro Text',
                                                  fontSize: 24,
                                                  fontWeight: FontWeight.w800,
                                                  color: Color(0xFF00218C),
                                                ),
                                                onChanged: (String value) {
                                                  final minutes =
                                                      int.tryParse(value);
                                                  if (minutes != null) {
                                                    setState(() {
                                                      if (_duration == null) {
                                                        _duration = minutes;
                                                      } else {
                                                        _duration ~/= 100;
                                                        _duration *= 100;
                                                        _duration += minutes;
                                                      }
                                                    });
                                                  }
                                                },
                                              ),
                                            ),
                                            Container(
                                              width: width + 4,
                                              height: 0.3,
                                              color: Color(0xFF91A6D9),
                                            ),
                                          ],
                                        );
                                      }),
                                      SizedBox(
                                          width: ScreenUtil().setWidth(10)),
                                      Text(
                                        S.of(context).minuteShort,
                                        style: TextStyle(
                                          fontFamily: 'SF Pro Text',
                                          fontSize: 18,
                                          fontWeight: FontWeight.w800,
                                          color: Color(0xFF00218C),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(18.8)),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(39)),
                  InkWell(
                    onTap: _isLoading ? null : _editPackage,
                    borderRadius: BorderRadius.circular(20),
                    child: Container(
                      width: ScreenUtil().setWidth(170.16),
                      height: ScreenUtil().setHeight(40.2),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        gradient: LinearGradient(
                          colors: <Color>[Color(0xff031C8D), Color(0xff0A2FAF)],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                        ),
                      ),
                      child: _isLoading
                          ? CupertinoActivityIndicator(
                              animating: true,
                            )
                          : Text(
                              S.of(context).saveChanges,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF UI Display',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: ScreenUtil().setSp(11),
                                fontWeight: FontWeight.w700,
                                color: Color(0xfffcfcfc),
                              ),
                            ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(39)),
                ],
              ),
            ),
          ),
          Visibility(
            visible: _isDialogVisible,
            child: Positioned(
              top: _dropdownButtonOffset.dy,
              left: _dropdownButtonOffset.dx,
              child: Container(
                width: ScreenUtil().setWidth(285),
                // height: ScreenUtil().setHeight(297),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0x44666666),
                      blurRadius: 10,
                    ),
                  ],
                ),
                child: Column(
                  children: <Widget>[
                    InkWell(
                      onTap: _showDropdownItems,
                      child: Container(
                        width: ScreenUtil().setWidth(272),
                        height: ScreenUtil().setHeight(52),
                        margin: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(7),
                        ),
                        padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(16),
                          vertical: ScreenUtil().setHeight(5),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image(
                              image:
                                  AssetImage('assets/images/packages-icon.png'),
                              width: ScreenUtil().setWidth(20.54),
                              height: ScreenUtil().setHeight(23.38),
                              color: Color(0xFF021A89),
                            ),
                            Expanded(
                              child: Align(
                                alignment: Alignment.center,
                                child: SizedBox(
                                  height: ScreenUtil().setHeight(20),
                                  child: FittedBox(
                                    fit: BoxFit.fitHeight,
                                    child: Text(
                                      _packageTitle,
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'Helvetica Rounded LT',
                                        fontWeight: FontWeight.w700,
                                        color: Color(0xFF021A89),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Image(
                              image: AssetImage(
                                  'assets/images/dropdown-button-icon.png'),
                              width: ScreenUtil().setWidth(11.37),
                              height: ScreenUtil().setHeight(5.69),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(24)),
                    Column(
                      children: _packages.map((Map<String, dynamic> package) {
                        final isSelected = package['package_id'] ==
                            _selectedPackage['package_id'];

                        return InkWell(
                          onTap: () => setState(() {
                            _setPackage(package);
                            _isDialogVisible = false;
                          }),
                          child: Container(
                            width: ScreenUtil().setWidth(272),
                            height: ScreenUtil().setHeight(52),
                            margin: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(7),
                              vertical: ScreenUtil().setHeight(3),
                            ),
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(16),
                              vertical: ScreenUtil().setHeight(5),
                            ),
                            decoration: isSelected
                                ? BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0x44888888),
                                        blurRadius: 10,
                                      ),
                                    ],
                                  )
                                : null,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(width: ScreenUtil().setWidth(65)),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SizedBox(
                                      height: ScreenUtil().setHeight(16),
                                      child: FittedBox(
                                        fit: BoxFit.fitHeight,
                                        child: Text(
                                          package['title'],
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'Helvetica Rounded LT',
                                            fontWeight: FontWeight.w700,
                                            color: Color(0xFF021A89),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: ScreenUtil().setHeight(12),
                                      child: FittedBox(
                                        fit: BoxFit.fitHeight,
                                        child: Text(
                                          "${(package['price'] as double).toStringAsFixed(3)} ${package['currency']}",
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'Helvetica Rounded LT',
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xFF021A89),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Expanded(
                                  child: Align(
                                    alignment: application.locale == 'ar'
                                        ? Alignment.centerLeft
                                        : Alignment.centerRight,
                                    child: Visibility(
                                      visible: isSelected,
                                      maintainSize: true,
                                      maintainAnimation: true,
                                      maintainState: true,
                                      child: Image(
                                        image: AssetImage(
                                            'assets/images/checkmark-icon.png'),
                                        width: ScreenUtil().setWidth(10.71),
                                        height: ScreenUtil().setHeight(7.05),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }).toList(),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(24)),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _showDropdownItems() {
    final RenderBox renderBox =
        _dropdownButtonKey.currentContext.findRenderObject();
    final position = renderBox.localToGlobal(Offset.zero);
    setState(() {
      _dropdownButtonOffset = position;
      _isDialogVisible = !_isDialogVisible;
    });
  }

  Future<void> _editPackage() async {
    setState(() {
      _isLoading = true;
    });

    if (_price < (_selectedPackage['price'] as double)) {
      _oldPrice = _selectedPackage['price'] as double;
    } else {
      _oldPrice = null;
    }

    if (_controllers == null ||
        _controllers.isEmpty ||
        _controllers[0] == null ||
        _controllers[0].text.isEmpty) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          content: Text(S.of(context).enterServices),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).ok),
            ),
          ],
        ),
      );

      setState(() {
        _isLoading = false;
      });

      return;
    }

    if (_price == null || _price <= 0) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          content: Text(S.of(context).enterPrice),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).ok),
            ),
          ],
        ),
      );

      setState(() {
        _isLoading = false;
      });

      return;
    }

    if (_duration == null || _price <= 0) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          content: Text(S.of(context).enterDuration),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).ok),
            ),
          ],
        ),
      );

      setState(() {
        _isLoading = false;
      });

      return;
    }

    final services = _controllers.map((controller) => controller.text).toList();
    await Firestore.instance
        .collection('packages')
        .document(widget.packageId)
        .updateData({
      'services': services,
      'price': _price,
      'old_price': _oldPrice,
      'duration': _duration,
    });

    final companyPackage = _company.packages.firstWhere(
        (p) => p['package_id'] == widget.packageId,
        orElse: () => null);
    if (companyPackage != null) {
      companyPackage['services'] = services;
      companyPackage['price'] = _price;
      companyPackage['old_price'] = _oldPrice;
      companyPackage['duration'] = _duration;
    }

    setState(() {
      _isLoading = false;
    });

    await showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(S.of(context).success),
        content: Text(S.of(context).changesSaved),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () => Navigator.of(context).pop(),
            child: Text(S.of(context).ok),
          ),
        ],
      ),
    );

    Navigator.pushNamed(context, '/packages/manage');
  }
}
