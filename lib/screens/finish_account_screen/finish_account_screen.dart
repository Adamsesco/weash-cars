import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/customer_store.dart';

class FinishAccountScreen extends StatefulWidget {
  @override
  _FinishAccountScreenState createState() => _FinishAccountScreenState();
}

class _FinishAccountScreenState extends State<FinishAccountScreen> {
  CustomerStore _customer;
  GlobalKey<FormState> _formKey;
  TextEditingController _firstNameController;
  TextEditingController _lastNameController;
  TextEditingController _emailController;
  FocusNode _lastNameFocusNode;
  FocusNode _emailFocusNode;

  String _firstName;
  String _lastName;
  String _email;

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _formKey = new GlobalKey<FormState>();
    _firstNameController = new TextEditingController();
    _lastNameController = new TextEditingController();
    _emailController = new TextEditingController();
    _lastNameFocusNode = new FocusNode();
    _emailFocusNode = new FocusNode();
  }

  @override
  void didChangeDependencies() {
    final applicationStore = Provider.of<ApplicationStore>(context);
    _customer = applicationStore.customerStore;

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Positioned.fill(
          child: Image(
            image: AssetImage('assets/images/lowest-price-background.png'),
            fit: BoxFit.cover,
          ),
        ),
        Positioned(
          left: ScreenUtil().setWidth(39),
          child: SizedBox(
            width: ScreenUtil().setWidth(274),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Finish your account !',
                    style: TextStyle(
                      fontFamily:
                          application.locale == 'ar' ? 'Cairo' : 'SF Pro Text',
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: Colors.white.withOpacity(0.65),
                      decoration: TextDecoration.none,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(49)),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Row(
                          children: <Widget>[
                            Text(
                              S.of(context).firstName.toUpperCase(),
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontSize: 13,
                                fontWeight: FontWeight.w600,
                                color: Colors.white.withOpacity(0.65),
                                decoration: TextDecoration.none,
                              ),
                            ),
                            SizedBox(width: ScreenUtil().setWidth(3)),
                            Text(
                              '*',
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Display',
                                fontSize: 25,
                                fontWeight: FontWeight.w400,
                                color: Colors.red,
                                decoration: TextDecoration.none,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(62)),
                      Expanded(
                        flex: 1,
                        child: Material(
                          color: Colors.transparent,
                          child: TextFormField(
                            controller: _firstNameController,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            textCapitalization: TextCapitalization.words,
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: 11,
                              fontWeight: FontWeight.w300,
                              color: Colors.white.withOpacity(0.90),
                            ),
                            decoration: InputDecoration(
                              hintText: S.of(context).firstName,
                              hintStyle: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontSize: 11,
                                fontWeight: FontWeight.w300,
                                color: Colors.white.withOpacity(0.65),
                              ),
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide.none),
                            ),
                            validator: (String val) {
                              if (val.length < 3) {
                                return S.of(context).firstNameError;
                              }
                            },
                            onSaved: (String val) {
                              setState(() {
                                _firstName = val;
                              });
                            },
                            onFieldSubmitted: (String val) {
                              FocusScope.of(context)
                                  .requestFocus(_lastNameFocusNode);
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(10)),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Row(
                          children: <Widget>[
                            Text(
                              S.of(context).lastName.toUpperCase(),
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontSize: 13,
                                fontWeight: FontWeight.w600,
                                color: Colors.white.withOpacity(0.65),
                                decoration: TextDecoration.none,
                              ),
                            ),
                            SizedBox(width: ScreenUtil().setWidth(3)),
                            Text(
                              '*',
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Display',
                                fontSize: 25,
                                fontWeight: FontWeight.w400,
                                color: Colors.red,
                                decoration: TextDecoration.none,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(62)),
                      Expanded(
                        flex: 1,
                        child: Material(
                          color: Colors.transparent,
                          child: TextFormField(
                            focusNode: _lastNameFocusNode,
                            controller: _lastNameController,
                            keyboardType: TextInputType.text,
                            textInputAction: TextInputAction.next,
                            textCapitalization: TextCapitalization.words,
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: 11,
                              fontWeight: FontWeight.w300,
                              color: Colors.white.withOpacity(0.90),
                            ),
                            decoration: InputDecoration(
                              hintText: S.of(context).lastName,
                              hintStyle: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontSize: 11,
                                fontWeight: FontWeight.w300,
                                color: Colors.white.withOpacity(0.65),
                              ),
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide.none),
                            ),
                            validator: (String val) {
                              if (val.length < 3) {
                                return S.of(context).lastNameError;
                              }
                            },
                            onSaved: (String val) {
                              setState(() {
                                _lastName = val;
                              });
                            },
                            onFieldSubmitted: (String val) {
                              FocusScope.of(context)
                                  .requestFocus(_emailFocusNode);
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(10)),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Text(
                          S.of(context).email.toUpperCase(),
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 13,
                            fontWeight: FontWeight.w600,
                            color: Colors.white.withOpacity(0.65),
                            decoration: TextDecoration.none,
                          ),
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(69)),
                      Expanded(
                        flex: 1,
                        child: Material(
                          color: Colors.transparent,
                          child: TextFormField(
                            controller: _emailController,
                            focusNode: _emailFocusNode,
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.done,
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: 11,
                              fontWeight: FontWeight.w300,
                              color: Colors.white.withOpacity(0.90),
                            ),
                            decoration: InputDecoration(
                              hintText: S.of(context).email,
                              hintStyle: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontSize: 11,
                                fontWeight: FontWeight.w300,
                                color: Colors.white.withOpacity(0.65),
                              ),
                              border: UnderlineInputBorder(
                                  borderSide: BorderSide.none),
                            ),
                            // validator: (String val) {
                            //   var pattern =
                            //       r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
                            //   var regexp = new RegExp(pattern);
                            //   if (!regexp.hasMatch(val)) {
                            //     return 'Invalid e-mail address';
                            //   }
                            // },
                            onSaved: (String val) {
                              setState(() {
                                _email = val;
                              });
                            },
                            onFieldSubmitted: (_) => _submit(),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(41)),
                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: _isLoading ? null : _submit,
                      borderRadius: BorderRadius.circular(22),
                      child: Container(
                        width: ScreenUtil().setWidth(116),
                        height: ScreenUtil().setHeight(43),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0x120d2e00)),
                          borderRadius: BorderRadius.circular(22),
                          color: Colors.white,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Color(0x66000000),
                              offset: Offset(0, ScreenUtil().setHeight(3)),
                            ),
                          ],
                        ),
                        child: Text(
                          S.of(context).submit,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF UI Display',
                            fontFamilyFallback: <String>['SF Pro Text'],
                            fontSize: ScreenUtil().setSp(11),
                            fontWeight: FontWeight.w700,
                            color: Color(0xff011e96),
                            letterSpacing: 0.22,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(30)),
                  Text(
                    S.of(context).byContinuing,
                    style: TextStyle(
                      fontFamily: application.locale == 'ar'
                          ? 'Cairo'
                          : 'SF UI Display',
                      fontFamilyFallback: <String>['SF Pro Text'],
                      fontSize: ScreenUtil().setSp(12),
                      fontWeight: FontWeight.w500,
                      color: Colors.white.withOpacity(0.65),
                      decoration: TextDecoration.none,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  bool _saveForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> _submit() async {
    if (_saveForm()) {
      setState(() {
        _isLoading = true;
      });

      final userId = _customer.customerId;
      await Firestore.instance.collection('users').document(userId).setData({
        'first_name': _firstName,
        'last_name': _lastName,
        'email': _email,
        'phone_number': _customer.phoneNumber,
        'role': 'customer',
      });

      setState(() {
        _isLoading = false;
      });

      _customer.setData(
          firstName: _firstName, lastName: _lastName, email: _email);
      Navigator.pushNamed(context, '/dashboard/customer');
    }
  }
}
