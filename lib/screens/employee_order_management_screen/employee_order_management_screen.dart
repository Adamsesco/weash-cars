import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/screens/vehicle_pictures_capture_screen/vehicle_pictures_capture_screen.dart';
import 'package:weash_cars/services/DirectionsService.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/employee_store.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class EmployeeOrderManagementScreen extends StatefulWidget {
  final String orderId;

  EmployeeOrderManagementScreen({Key key, this.orderId}) : super(key: key);

  @override
  _EmployeeOrderManagementScreenState createState() =>
      _EmployeeOrderManagementScreenState();
}

class _EmployeeOrderManagementScreenState
    extends State<EmployeeOrderManagementScreen> {
  Completer<GoogleMapController> _mapController = Completer();
  GoogleMapController _googleMapController;
  EmployeeStore _employee;
  Map<String, dynamic> _order;
  Map<String, dynamic> _customer;
  Map<String, dynamic> _package;
  DateTime _orderDate;
  LatLng _orderLocation;
  GeoPoint _currentLocation;
  StreamSubscription<Position> _locationStream;
  final Set<Marker> _markers = {};
  final Set<Polyline> _polylines = {};
  int _orderStatus;
  Color _orderStatusColor;
  bool _isTakeOrderLoading = false;
  bool _isStartLoading = false;
  bool _isDoneLoading = false;

  @override
  void didChangeDependencies() {
    final application = Provider.of<ApplicationStore>(context);
    _employee = application.employeeStore;
    _order = _employee.orders.orders
        .where(
            (Map<String, dynamic> order) => order['order_id'] == widget.orderId)
        .toList()[0];
    _customer = _order['customer'];
    _package = _order['package'];
    _orderDate = _order['date'] as DateTime;
    final orderLocation = _order['location'] as GeoPoint;
    _orderLocation =
        new LatLng(orderLocation.latitude, orderLocation.longitude);
    _markers.add(Marker(
      markerId: MarkerId(_order['order_id']),
      position: _orderLocation,
      icon: BitmapDescriptor.defaultMarker,
    ));
    _orderStatus = _order['status'];
    _orderStatusColor = [
      Colors.grey,
      Colors.blue,
      Colors.green,
      Colors.red,
    ][_orderStatus ~/ 10];

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    if (_locationStream != null) {
      _locationStream.cancel();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        context,
        nbNotifications: 0,
        avatarUrl: _employee.avatarUrl,
        popupMenuItems: <PopupMenuEntry<String>>[
          PopupMenuItem<String>(
            value: '/profile/employee/edit',
            child: Text('Edit info'),
          ),
          PopupMenuItem<String>(
            value: '/logout',
            child: Text('Logout'),
          ),
        ],
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: _buildHeader(),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundImage: NetworkImage(
                          _order['customer']['avatar_url'] ??
                              'http://www.personalbrandingblog.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png',
                        ),
                      ),
                      const SizedBox(width: 14),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "${_customer['username']}",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 4),
                          Text(
                            "${_orderDate.day}/${_orderDate.month}/${_orderDate.year}",
                            style: TextStyle(
                              fontFamilyFallback: <String>['SF Pro Text'],
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  _orderStatus == 0
                      ? Container(
                          decoration: BoxDecoration(
                            color: Colors.blue,
                          ),
                          child: FlatButton(
                            onPressed: null,
                            child: Text(
                              'Send message',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        )
                      : Row(
                          children: <Widget>[
                            Text(
                              _package['title'].toString(),
                              style: TextStyle(
                                color: _orderStatusColor,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            const SizedBox(width: 14),
                            Container(
                              width: 10,
                              height: 10,
                              decoration: BoxDecoration(
                                color: _orderStatusColor,
                                shape: BoxShape.circle,
                              ),
                            ),
                          ],
                        ),
                ],
              ),
            ),
            Divider(),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6, vertical: 10),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        'Package : ',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(_package['title']),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 300,
              child: GoogleMap(
                onMapCreated: (GoogleMapController controller) {
                  _mapController.complete(controller);
                  _googleMapController = controller;
                },
                mapType: MapType.normal,
                initialCameraPosition: CameraPosition(
                  target: _orderLocation,
                  zoom: 14.4746,
                ),
                compassEnabled: true,
                markers: _markers,
                polylines: _polylines,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildHeader() {
    switch (_orderStatus) {
      case 10:
        return Container(
          color: Colors.blue,
          child: FlatButton(
            onPressed: _takeOrder,
            child: _isTakeOrderLoading
                ? CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    strokeWidth: 2,
                  )
                : Text(
                    'Take order',
                    style: TextStyle(color: Colors.white),
                  ),
          ),
        );
      case 11:
        return Container(
          color: Colors.blue,
          child: FlatButton(
            onPressed: _start,
            child: _isStartLoading
                ? CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    strokeWidth: 2,
                  )
                : Text(
                    'Start',
                    style: TextStyle(color: Colors.white),
                  ),
          ),
        );
      case 12:
        return Container(
          color: Colors.green,
          child: FlatButton(
            onPressed: _done,
            child: _isDoneLoading
                ? CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    strokeWidth: 2,
                  )
                : Text(
                    'Done',
                    style: TextStyle(color: Colors.white),
                  ),
          ),
        );

      case 20:
        return Text('Pending customer\'s approval.');
      case 3:
      default:
        return Container();
    }
  }

  Future<void> _takeOrder() async {
    setState(() {
      _isTakeOrderLoading = true;
    });

    final geolocator = new Geolocator();
    final locationOptions =
        new LocationOptions(accuracy: LocationAccuracy.bestForNavigation);
    _locationStream = geolocator
        .getPositionStream(locationOptions)
        .listen((Position position) {
      if (position != null) {
        setState(() {
          _currentLocation =
              new GeoPoint(position.latitude, position.longitude);

          if (_googleMapController != null) {
            final latlng = new LatLng(position.latitude, position.longitude);
            _markers.remove((Marker m) => m.markerId == MarkerId('me'));
            _markers.add(new Marker(
              markerId: MarkerId('me'),
              position: latlng,
              icon: BitmapDescriptor.defaultMarkerWithHue(
                  BitmapDescriptor.hueAzure),
            ));
            _googleMapController.animateCamera(CameraUpdate.newLatLng(latlng));
          }
        });
      }
    });

    final currentPosition = await Geolocator().getCurrentPosition(
        desiredAccuracy: LocationAccuracy.bestForNavigation);
    final destination = _order['location'] as GeoPoint;
    final polylinePoints = PolylinePoints();
    final points = await polylinePoints.getRouteBetweenCoordinates(
        'AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I',
        currentPosition.latitude,
        currentPosition.longitude,
        destination.latitude,
        destination.longitude);
    final polyline = new Polyline(
      polylineId: PolylineId(points.hashCode.toString()),
      points: points
          .map((PointLatLng point) => LatLng(point.latitude, point.longitude))
          .toList(),
      color: Colors.blueAccent,
    );

    await Firestore.instance
        .collection('orders')
        .document(_order['order_id'])
        .updateData({
      'status': 11,
    });

    setState(() {
      _isTakeOrderLoading = false;
      _orderStatus = 11;

      _polylines.clear();
      _polylines.add(polyline);
    });
  }

  Future<void> _start() async {
    setState(() {
      _isStartLoading = true;
    });

    DirectionsService.dispose();

    await Firestore.instance
        .collection('orders')
        .document(_order['order_id'])
        .updateData({
      'status': 12,
    });

    setState(() {
      _isStartLoading = false;
      _orderStatus = 12;
    });
  }

  Future<void> _done() async {
    setState(() {
      _isDoneLoading = true;
    });

    // await Firestore.instance
    //     .collection('orders')
    //     .document(_order['order_id'])
    //     .updateData({
    //   'status': 20,
    // });

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => VehiclePicturesCaptureScreen(order: _order),
      ),
    );

    setState(() {
      _isDoneLoading = false;
      _orderStatus = 20;
    });
  }
}
