import 'dart:ui' as prefix0;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import "package:google_maps_webservice/geocoding.dart" as geocoding;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weash_cars/consts/country_codes.dart';
import 'package:weash_cars/generated/i18n.dart';

import 'package:weash_cars/models/user_role.dart';
import 'package:weash_cars/models/vehicle_type.dart';
import 'package:weash_cars/screens/company_dashboard_screen/company_dashboard_screen.dart';
import 'package:weash_cars/screens/customer_dashboard_screen/customer_dashboard_screen.dart';
import 'package:weash_cars/screens/employee_dashboard_screen/employee_dashboard_screen.dart';
import 'package:weash_cars/screens/login_screen/widgets/otp_text_field.dart';
import 'package:weash_cars/services/UsersService.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/stores/customer_store.dart';
import 'package:weash_cars/stores/employee_store.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  geocoding.GoogleMapsGeocoding _geocoder;
  Location _locationService;
  UsersService _usersService;
  CompanyStore _company;
  CustomerStore _customer;
  EmployeeStore _employee;
  UserRole _userRole;
  Future<String> _countryDialCodeFuture;
  String _phoneNumber;
  String _verificationId;
  String _smsCode;

  PageController _pageController;
  TextEditingController _phoneNumberController;
  List<TextEditingController> _verificationCodeControllers;
  List<FocusNode> _verificationCodeFocusNodes;

  bool _isSigningIn = false;

  @override
  void initState() {
    super.initState();

    _geocoder = new geocoding.GoogleMapsGeocoding(
        apiKey: 'AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I');
    _locationService = new Location();
    _usersService = new UsersService();
    _phoneNumberController = new TextEditingController();
    _verificationCodeControllers = List<TextEditingController>.generate(
        6, (i) => TextEditingController(text: '*'));
    _verificationCodeFocusNodes =
        List<FocusNode>.generate(6, (i) => FocusNode());
    _pageController = new PageController();
    _countryDialCodeFuture = _getCountryDialCode();
  }

  @override
  void didChangeDependencies() {
    final applicationStore = Provider.of<ApplicationStore>(context);
    _company = applicationStore.companyStore;
    _customer = applicationStore.customerStore;
    _employee = applicationStore.employeeStore;

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    if (_isSigningIn) {
      FocusScope.of(context).requestFocus(FocusNode());
    }

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned.fill(
            child: Image(
              image: AssetImage('assets/images/we-come-to-you-background.png'),
              fit: BoxFit.cover,
              matchTextDirection: true,
            ),
          ),
          PageView(
            controller: _pageController,
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Positioned.fill(
                    child: FutureBuilder<String>(
                      future: _countryDialCodeFuture,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.none:
                          case ConnectionState.active:
                          case ConnectionState.waiting:
                            return Center(
                              child: CupertinoActivityIndicator(
                                animating: true,
                              ),
                            );
                          case ConnectionState.done:
                            final countryDialCode = snapshot.data.toString();
                            // final countryDialCode = '+965';
                            if (countryDialCode != null) {
                              return SingleChildScrollView(
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(59)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(
                                        height: ScreenUtil().setHeight(340)),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      textDirection: TextDirection.ltr,
                                      children: <Widget>[
                                        Text(
                                          countryDialCode,
                                          textDirection: TextDirection.ltr,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Text',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.white,
                                            decoration: TextDecoration.none,
                                          ),
                                        ),
                                        SizedBox(
                                            width: ScreenUtil().setWidth(20)),
                                        Expanded(
                                          // width: ScreenUtil().setWidth(205),
                                          child: TextField(
                                            controller: _phoneNumberController,
                                            autofocus: true,
                                            keyboardType: TextInputType.number,
                                            textDirection: TextDirection.ltr,
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontSize: 14,
                                              fontWeight: FontWeight.w300,
                                              color: Colors.white,
                                              decoration: TextDecoration.none,
                                            ),
                                            decoration: InputDecoration(
                                              hintText: S
                                                  .of(context)
                                                  .enterMobileNumber,
                                              hintStyle: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Pro Text',
                                                fontSize: 14,
                                                fontWeight: FontWeight.w300,
                                                color: Colors.white
                                                    .withOpacity(0.34),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                            onSubmitted: (String phoneNumber) {
                                              _phoneNumber =
                                                  "$countryDialCode${_phoneNumberController.text}";
                                              _verifyPhoneNumber();
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                        height: ScreenUtil().setHeight(74)),
                                    Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                        onTap: () {
                                          _phoneNumber =
                                              "$countryDialCode${_phoneNumberController.text}";
                                          _verifyPhoneNumber();
                                        },
                                        borderRadius: BorderRadius.circular(20),
                                        child: Container(
                                          width: ScreenUtil().setWidth(93),
                                          height: ScreenUtil().setHeight(43),
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Color(0x120d2e00)),
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            color: Colors.white,
                                            boxShadow: <BoxShadow>[
                                              BoxShadow(
                                                color: Color(0x66000000),
                                                offset: Offset(0,
                                                    ScreenUtil().setHeight(3)),
                                              ),
                                            ],
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                S.of(context).send,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'SF Pro Text',
                                                  fontSize: 11,
                                                  fontWeight: FontWeight.w700,
                                                  color: Color(0xFF011e96),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                        height: ScreenUtil().setHeight(30)),
                                    Text(
                                      S.of(context).sendSixDigits,
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'SF Pro Text',
                                        fontSize: 12,
                                        fontWeight: FontWeight.w300,
                                        color: Colors.white.withOpacity(0.65),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }
                        }
                        return Container();
                      },
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setHeight(
                        MediaQuery.of(context).padding.top +
                            ScreenUtil().setHeight(20)),
                    left: application.locale == 'ar'
                        ? null
                        : ScreenUtil().setWidth(20),
                    right: application.locale == 'ar'
                        ? ScreenUtil().setWidth(20)
                        : null,
                    child: InkWell(
                      onTap: () => Navigator.of(context).pop(),
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      child: Container(
                        width: ScreenUtil().setWidth(35),
                        height: ScreenUtil().setWidth(35),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.black.withOpacity(0.16),
                              blurRadius: 11,
                            ),
                          ],
                        ),
                        child: Center(
                          child: Image(
                            image: AssetImage(
                                'assets/images/back-button-icon.png'),
                            width: ScreenUtil().setWidth(17.39),
                            height: ScreenUtil().setHeight(12.15),
                            matchTextDirection: true,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Positioned(
                    top: ScreenUtil().setHeight(
                        MediaQuery.of(context).padding.top +
                            ScreenUtil().setHeight(20)),
                    left: application.locale == 'ar'
                        ? null
                        : ScreenUtil().setWidth(20),
                    right: application.locale == 'ar'
                        ? ScreenUtil().setWidth(20)
                        : null,
                    child: InkWell(
                      onTap: () => _pageController.animateToPage(0,
                          duration: Duration(milliseconds: 300),
                          curve: Curves.linear),
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      child: Container(
                        width: ScreenUtil().setWidth(35),
                        height: ScreenUtil().setWidth(35),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.black.withOpacity(0.16),
                              blurRadius: 11,
                            ),
                          ],
                        ),
                        child: Center(
                          child: Image(
                            image: AssetImage(
                                'assets/images/back-button-icon.png'),
                            width: ScreenUtil().setWidth(17.39),
                            height: ScreenUtil().setHeight(12.15),
                            matchTextDirection: true,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setHeight(205),
                    left: ScreenUtil().setWidth(20),
                    right: ScreenUtil().setWidth(20),
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Container(
                          // width: ScreenUtil().setWidth(306),
                          // height: ScreenUtil().setHeight(262),
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(30),
                            vertical: ScreenUtil().setHeight(28),
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                color: Color(0x66000000),
                                offset: Offset(0, 0),
                              ),
                            ],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                S.of(context).otpVerification,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontSize: 17,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xFF455367),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(18)),
                              Text(
                                S.of(context).enterSixDigits,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontSize: 10,
                                  fontWeight: FontWeight.w300,
                                  color: Color(0xFF455367),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(36)),
                              Center(
                                child: OTPTextField(
                                  width: MediaQuery.of(context).size.width -
                                      ScreenUtil().setWidth(100),
                                  isSigningIn: _isSigningIn,
                                  onChanged: (String value) {
                                    if (value != null && value.length == 6) {
                                      _smsCode = value;
                                      _signInWithPhoneNumber();
                                    }
                                  },
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(36)),
                              Row(
                                children: <Widget>[
                                  Visibility(
                                    visible: false,
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          S.of(context).resendOtp,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'Proxima Nova Alt',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: ScreenUtil().setSp(13),
                                            fontWeight: FontWeight.w700,
                                            color: Color(0xff021a89),
                                          ),
                                        ),
                                        SizedBox(
                                            width: ScreenUtil().setWidth(4)),
                                        RotatedBox(
                                          quarterTurns:
                                              application.locale == 'ar'
                                                  ? 2
                                                  : 0,
                                          child: Image(
                                            image: AssetImage(
                                                'assets/images/resend-icon.png'),
                                            width: ScreenUtil().setWidth(8),
                                            height: ScreenUtil().setHeight(9),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Spacer(),
                                  InkWell(
                                    onTap: _signInWithPhoneNumber,
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    child: Container(
                                      width: ScreenUtil().setWidth(30),
                                      height: ScreenUtil().setWidth(30),
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Color(0xff03c428),
                                      ),
                                      child: RotatedBox(
                                        quarterTurns: 2,
                                        child: Image(
                                          image: AssetImage(
                                              'assets/images/back-button-icon.png'),
                                          color: Colors.white,
                                          width: ScreenUtil()
                                              .setWidth(17.39 * 0.8),
                                          height: ScreenUtil()
                                              .setHeight(12.15 * 0.8),
                                          matchTextDirection: true,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        if (_isSigningIn) ...[
                          Positioned.fill(
                            child: ClipRect(
                              child: BackdropFilter(
                                filter: prefix0.ImageFilter.blur(
                                    sigmaX: 3, sigmaY: 3),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.grey.withOpacity(0.5),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          CupertinoActivityIndicator(),
                        ],
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _verifyPhoneNumber() {
    print('Verifying $_phoneNumber ... ');
    setState(() {
      _isSigningIn = false;
    });
    _pageController.animateToPage(1,
        duration: Duration(milliseconds: 300), curve: Curves.linear);
    FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: _phoneNumber,
        timeout: const Duration(seconds: 120),
        verificationCompleted: _verificationCompleted,
        verificationFailed: _verificationFailed,
        codeSent: _codeSent,
        codeAutoRetrievalTimeout: _codeAutoRetrievalTimeout);
  }

  void _verificationCompleted(AuthCredential credential) async {
    print('Verification complete!');
    if (!_isSigningIn) {
      setState(() {
        _isSigningIn = true;
      });
      final user = await FirebaseAuth.instance.signInWithCredential(credential);
      _signIn(user);
    }
  }

  void _verificationFailed(AuthException authException) {
    setState(() {
      _isSigningIn = false;
    });
    print(authException.message);
    _showDialog('Phone number verification failed',
        'Invalid phone verification code. Please try again.');
    // _pageController.animateToPage(0,
    //     duration: Duration(milliseconds: 300), curve: Curves.linear);
  }

  void _codeSent(String verificationId, [int forceResendingToken]) {
    print("Code sent. $verificationId");
    _verificationId = verificationId;
  }

  void _codeAutoRetrievalTimeout(String verificationId) {
    setState(() {
      _isSigningIn = false;
    });
    print("Timeout. $verificationId");
    _verificationId = verificationId;
    // _pageController.animateToPage(0,
    //     duration: Duration(milliseconds: 300), curve: Curves.linear);
  }

  Future<String> _getCountryCode() async {
    await _locationService.changeSettings(accuracy: LocationAccuracy.HIGH);

    LocationData location;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      bool serviceStatus = await _locationService.serviceEnabled();
      if (serviceStatus) {
        final permission = await _locationService.requestPermission();
        if (permission) {
          location = await _locationService.getLocation();

          final addressResult = await _geocoder.searchByLocation(
              new geocoding.Location(location.latitude, location.longitude));
          if (addressResult.isOkay && addressResult.results.length > 0) {
            final countryCode = addressResult.results[0].addressComponents
                .firstWhere(
                    (addressComponent) =>
                        addressComponent.types.contains('country') &&
                        addressComponent.types.contains('political'),
                    orElse: () {
              print("No result");
              return null;
            }).shortName;

            return countryCode;
          }
        } else {
          showCupertinoDialog(
            context: context,
            builder: (BuildContext context) => CupertinoAlertDialog(
              content: Text(S.of(context).allowLocation),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(S.of(context).ok),
                ),
              ],
            ),
          );
        }
      } else {
        bool serviceStatusResult = await _locationService.requestService();
        print("Service status activated after request: $serviceStatusResult");
        if (serviceStatusResult) {
          return _getCountryCode();
        }
        return null;
      }
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            content: Text(S.of(context).allowLocation),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(S.of(context).ok),
              ),
            ],
          ),
        );
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            content: Text(S.of(context).enableLocationService),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(S.of(context).ok),
              ),
            ],
          ),
        );
      }
    }

    return null;
  }

  Future<String> _getCountryDialCode() async {
    final countryCode = await _getCountryCode();
    if (countryCode != null) {
      final country = COUNTRY_CODES.firstWhere(
          (countryData) => countryData['code'] == countryCode,
          orElse: () => null);
      if (country != null) {
        final countryDialCode = country['dial_code'];
        return countryDialCode;
      }
    }
    return null;
  }

  Future<void> _signInWithPhoneNumber() async {
    if (!_isSigningIn) {
      setState(() {
        _isSigningIn = true;
      });

      // _smsCode = _verificationCodeControllers
      //     .where((controller) =>
      //         controller.text != null &&
      //         controller.text.isNotEmpty &&
      //         controller.text.contains(RegExp('[0-9]')))
      //     .map((controller) => controller.text)
      //     .join();
      if (_smsCode.length < 6) {
        setState(() {
          _isSigningIn = false;
        });
        return;
      }

      print(_smsCode);

      try {
        final user = await _usersService.signInWithPhoneNumber(
            _verificationId, _smsCode);
        _signIn(user);
      } catch (ex) {
        setState(() {
          _isSigningIn = false;
        });
        _showDialog('Phone number verification failed',
            'Invalid phone verification code. Please try again.');
      }
    }
  }

  Future<void> _signIn(FirebaseUser user) async {
    final application = Provider.of<ApplicationStore>(context);
    final prefs = await SharedPreferences.getInstance();

    Firestore.instance
        .collection('users')
        .where('phone_number', isEqualTo: user.phoneNumber)
        .getDocuments()
        .then((QuerySnapshot qs) {
      if (qs != null && qs.documents != null && qs.documents.length == 1) {
        final ds = qs.documents[0];
        final userData = ds.data;
        final userRole = userData['role'];

        switch (userRole) {
          case 'customer':
            _userRole = UserRole.customer;
            break;

          case 'employee':
            _userRole = UserRole.employee;
            break;

          case 'company':
            _userRole = UserRole.company;
            break;

          case 'self_employed':
            _userRole = UserRole.selfEmployed;
            break;
        }
        application.setUserRole(_userRole);

        if (userData['locale'] != null &&
            ['ar', 'en'].contains(userData['locale'])) {
          application.localeStream.add(userData['locale']);
        }

        if (_userRole == UserRole.company ||
            _userRole == UserRole.selfEmployed) {
          if (userData['is_activated'] == false) {
            _showDialog('Sign-in error', 'Your account is not yet activated.');
          } else {
            Firestore.instance
                .collection('users')
                .where('role', isEqualTo: 'employee')
                .where('company_id', isEqualTo: ds.documentID)
                .getDocuments()
                .then((QuerySnapshot employeesDocuments) async {
              final employees = new List<Map<String, dynamic>>();

              if (_userRole == UserRole.company) {
                await Future.forEach(employeesDocuments.documents,
                    (DocumentSnapshot employeeDocument) {
                  final employee = <String, dynamic>{
                    'employee_id': employeeDocument.documentID,
                    'company_id': employeeDocument.data['company_id'],
                    'first_name': employeeDocument.data['first_name'],
                    'last_name': employeeDocument.data['last_name'],
                    'phone_number': employeeDocument.data['phone_number'],
                    'avatar_url': employeeDocument.data['avatar_url'],
                    'jobs_done': employeeDocument.data['jobs_done'] as int,
                    'type': employeeDocument.data['type'] ?? 'smart',
                    'created_at':
                        (employeeDocument.data['created_at'] as Timestamp)
                            .toDate(),
                    'is_available':
                        employeeDocument.data['is_available'] ?? true,
                  };

                  employees.add(employee);
                });
              }

              final schedule = (userData['schedule'] as Map<dynamic, dynamic>)
                  .map((day, daySchedule) {
                final dayNumber = int.tryParse(day as String);
                final timeFrom = daySchedule['from'] as int;
                final timeTo = daySchedule['to'] as int;
                final isDayOff = daySchedule['is_day_off'] as bool;

                return MapEntry(dayNumber, {
                  'from': timeFrom,
                  'to': timeTo,
                  'is_day_off': isDayOff,
                });
              });

              _company.setData(
                companyId: ds.documentID,
                role: _userRole,
                email: userData['email'],
                phoneNumber: userData['phone_number'],
                fcmToken: userData['fcm_token'],
                companyName: userData['company_name'],
                descripton: userData['description'],
                location: userData['location'] == null
                    ? null
                    : userData['location']['geopoint'] as GeoPoint,
                address: userData['address'],
                locality: userData['locality'],
                country: userData['country'],
                avatarUrl: userData['avatar_url'],
                rating: userData['rating'].toDouble() as double,
                ordersCount: userData['total_orders'] as int,
                packagesCount: userData['total_packages'] as int,
                isAvailable: userData['is_available'] as bool,
                schedule: schedule,
                daysUnavailable: userData['days_unavailable'] == null
                    ? new List<DateTime>()
                    : new List<DateTime>.from(userData['days_unavailable']
                        .map((date) => (date as Timestamp).toDate())),
                dateCreated: (userData['date_created'] as Timestamp).toDate(),
                dateActivated: userData['date_activated'] == null
                    ? null
                    : (userData['date_activated'] as Timestamp).toDate(),
                employees: employees,
                jobsDoneCount: userData['jobs_done'] as int,
                locale: userData['locale'],
                currency: userData['currency'],
              );
              setState(() {
                _isSigningIn = false;
              });
              application.setIsSignedIn(true);
              prefs.setString('user_id', ds.documentID);
              Navigator.pushNamed(context, '/dashboard/company');
            });
          }
        } else if (_userRole == UserRole.customer) {
          _customer.setData(
            customerId: ds.documentID,
            phoneNumber: userData['phone_number'],
            fullName: userData['full_name'],
            firstName: userData['first_name'],
            lastName: userData['last_name'],
            email: userData['email'],
            avatarUrl: userData['avatar_url'],
            locale: userData['locale'],
            fcmToken: userData['fcm_token'],
          );
          setState(() {
            _isSigningIn = false;
          });
          application.setIsSignedIn(true);
          prefs.setString('user_id', ds.documentID);
          print(ds.documentID);
          Navigator.pushNamed(context, '/dashboard/customer');
        } else if (_userRole == UserRole.employee) {
          _employee.setData(
            employeeId: ds.documentID,
            companyId: userData['company_id'],
            phoneNumber: userData['phone_number'],
            firstName: userData['first_name'],
            lastName: userData['last_name'],
            avatarUrl: userData['avatar_url'],
            jobsDoneCount: userData['jobs_done'] as int,
            locale: userData['locale'],
            fcmToken: userData['fcm_token'],
          );
          setState(() {
            _isSigningIn = false;
          });
          application.setIsSignedIn(true);
          prefs.setString('user_id', ds.documentID);
          Navigator.pushNamed(context, '/dashboard/employee');
        }
      } else {
        _customer.setData(customerId: user.uid, phoneNumber: _phoneNumber);
        setState(() {
          _isSigningIn = false;
        });
        Navigator.pushNamed(context, '/signup');
      }
    });
  }

  void _showDialog(String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      },
    );
  }
}
