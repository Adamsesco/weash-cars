import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:simple_animations/simple_animations.dart';

typedef OnChanged(String value);

class OTPTextField extends StatefulWidget {
  final OnChanged onChanged;
  final bool isSigningIn;
  final double width;

  OTPTextField(
      {Key key, @required this.width, this.isSigningIn, this.onChanged})
      : super(key: key);

  @override
  _OTPTextFieldState createState() => _OTPTextFieldState();
}

class _OTPTextFieldState extends State<OTPTextField> {
  double _characterWidth;
  double _characterHeight;
  ScrollController _textFieldScrollController;
  TextEditingController _textEditingController;
  FocusNode _focusNode;
  bool _grabFocus;

  @override
  void initState() {
    super.initState();

    _grabFocus = true;

    _textEditingController = TextEditingController();
    _focusNode = FocusNode();

    _textFieldScrollController =
        ScrollController(initialScrollOffset: 0, keepScrollOffset: false);
    _textFieldScrollController.addListener(() {
      _textFieldScrollController.jumpTo(0);
    });

    final textSpan = TextSpan(
      text: '0',
      style: TextStyle(
        fontFamily: 'Lato',
        fontSize: ScreenUtil().setSp(25),
        fontWeight: FontWeight.w400,
        color: Color(0xFF455367),
        decoration: TextDecoration.none,
      ),
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout();
    _characterWidth = textPainter.width;
    _characterHeight = textPainter.height;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isSigningIn && _grabFocus == true) {
      _grabFocus = false;
    }

    if (!_focusNode.hasFocus && _grabFocus) {
      FocusScope.of(context).requestFocus(_focusNode);
    }

    return SizedBox(
      // width: ScreenUtil().setWidth(227.68),
      width: widget.width,
      // height: ScreenUtil().setHeight(50),
      child: Stack(
        children: <Widget>[
          Positioned(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              textDirection: TextDirection.ltr,
              children: List.generate(6, (int index) {
                return Container(
                  // width: _characterWidth + ScreenUtil().setWidth(8) * 2,
                  // height: ScreenUtil().setHeight(50),
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(10),
                    vertical: ScreenUtil().setHeight(6),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(0xFF91A6D9).withOpacity(0.27),
                  ),
                  child: Transform.translate(
                    offset: Offset(0, ScreenUtil().setHeight(8.5)),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text(
                          '*',
                          style: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: ScreenUtil().setSp(30),
                            fontWeight: FontWeight.w900,
                            color: _textEditingController.text != null &&
                                    _textEditingController.text.length <
                                        index + 1
                                ? Color(0xFF455367).withOpacity(0.4)
                                : Colors.transparent,
                            decoration: TextDecoration.none,
                          ),
                        ),
                        // SizedBox(height: ScreenUtil().setHeight(1)),
                        Transform.translate(
                          offset: Offset(0, -ScreenUtil().setHeight(9)),
                          child: Visibility(
                            visible: (_textEditingController.text != null &&
                                    _textEditingController.text.length ==
                                        index) ||
                                ((_textEditingController.text == null) &&
                                    index == 0),
                            // visible: true,
                            maintainAnimation: true,
                            maintainSize: true,
                            maintainState: true,
                            child: Builder(builder: (context) {
                              final tween = MultiTrackTween([
                                Track('opacity')
                                    .add(Duration(milliseconds: 300),
                                        Tween(begin: 0.0, end: 1.0))
                                    .add(Duration(milliseconds: 300),
                                        ConstantTween(1.0))
                                    .add(Duration(milliseconds: 200),
                                        ConstantTween(0.0)),
                              ]);

                              return ControlledAnimation(
                                playback: Playback.LOOP,
                                duration: tween.duration,
                                tween: tween,
                                builder: (context, animation) {
                                  return Container(
                                    width: _characterWidth,
                                    height: 2,
                                    color: Colors.blue
                                        .withOpacity(animation['opacity']),
                                  );
                                },
                              );
                            }),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              }),
            ),
          ),
          Positioned(
            // left: -ScreenUtil().setWidth(12),
            left: -((widget.width / 6) - _characterWidth) / 2 +
                ScreenUtil().setWidth(8),
            child: Container(
              width: widget.width,
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(
                vertical: ScreenUtil().setHeight(8),
              ),
              child: SizedBox(
                height: _characterHeight + ScreenUtil().setHeight(4),
                child: TextField(
                  onChanged: (String value) {
                    if (widget.onChanged != null) {
                      widget.onChanged(value);
                    }
                    setState(() {});
                  },
                  controller: _textEditingController,
                  focusNode: _focusNode,
                  autofocus: true,
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    WhitelistingTextInputFormatter.digitsOnly,
                  ],
                  maxLength: 6,
                  maxLengthEnforced: true,
                  textDirection: TextDirection.ltr,
                  showCursor: false,
                  scrollPhysics: NeverScrollableScrollPhysics(),
                  // scrollPadding: EdgeInsets.zero,
                  scrollController: _textFieldScrollController,
                  decoration: InputDecoration(
                    // contentPadding: EdgeInsets.only(
                    //   left: ScreenUtil().setWidth(0),
                    //   right: ScreenUtil().setWidth(0),
                    //   bottom: ScreenUtil().setWidth(0),
                    //   top: ScreenUtil().setHeight(4),
                    //   // vertical: ScreenUtil().setHeight(5),
                    // ),
                    contentPadding: EdgeInsets.zero,
                    border: InputBorder.none,
                    counter: Container(height: 0),
                    isDense: true,
                  ),
                  style: TextStyle(
                    fontFamily: 'Lato',
                    fontSize: ScreenUtil().setSp(25),
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF455367),
                    letterSpacing: ((widget.width / 6) - _characterWidth) +
                        ScreenUtil().setWidth(3),
                    decoration: TextDecoration.none,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
