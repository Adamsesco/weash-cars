import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

typedef OnBack();
typedef OnSubmit(String password);

class PasswordPage extends StatefulWidget {
  final OnBack onBack;
  final OnSubmit onSubmit;
  final bool isLoading;

  PasswordPage(
      {Key key,
      @required this.onBack,
      @required this.onSubmit,
      @required this.isLoading})
      : super(key: key);

  @override
  _PasswordPageState createState() => _PasswordPageState();
}

class _PasswordPageState extends State<PasswordPage> {
  GlobalKey<FormState> _formKey = GlobalKey();
  String _password;

  @override
  Widget build(BuildContext context) {
    final textDirection = Directionality.of(context);

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Positioned.directional(
          textDirection: textDirection,
          top: ScreenUtil().setHeight(
              MediaQuery.of(context).padding.top + ScreenUtil().setHeight(20)),
          start: ScreenUtil().setWidth(20),
          child: InkWell(
            onTap: widget.onBack,
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            child: Container(
              width: ScreenUtil().setWidth(35),
              height: ScreenUtil().setWidth(35),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors.black.withOpacity(0.16),
                    blurRadius: 11,
                  ),
                ],
              ),
              child: Center(
                child: Image(
                  image: AssetImage('assets/images/back-button-icon.png'),
                  width: ScreenUtil().setWidth(17.39),
                  height: ScreenUtil().setHeight(12.15),
                  matchTextDirection: true,
                ),
              ),
            ),
          ),
        ),
        Positioned.fill(
          top: ScreenUtil().setHeight(278),
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(57),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Enter your password',
                  style: TextStyle(
                    fontFamily: textDirection == TextDirection.rtl
                        ? 'Cairo'
                        : 'SF Pro Text',
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 0.20,
                    color: Colors.white.withOpacity(0.65),
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(49)),
                Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextFormField(
                        autofocus: true,
                        obscureText: true,
                        textInputAction: TextInputAction.done,
                        style: TextStyle(
                          fontFamily: textDirection == TextDirection.rtl
                              ? 'Cairo'
                              : 'SF Pro Text',
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          letterSpacing: 1,
                          color: Colors.white.withOpacity(0.65),
                        ),
                        decoration: InputDecoration(
                          hintText: 'Your password',
                          hintStyle: TextStyle(
                            fontFamily: textDirection == TextDirection.rtl
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 13,
                            fontWeight: FontWeight.w300,
                            letterSpacing: 1,
                            color: Colors.white.withOpacity(0.5),
                          ),
                        ),
                        validator: (String value) {
                          if (value == null || value.length < 6) {
                            return 'The password must contain at least 6 chracters';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          _password = value;
                        },
                        onFieldSubmitted: (_) {
                          _submit();
                        },
                      ),
                      SizedBox(height: ScreenUtil().setHeight(44)),
                      InkWell(
                        onTap: _submit,
                        highlightColor: Colors.transparent,
                        splashColor: Colors.transparent,
                        child: Container(
                          width: ScreenUtil().setWidth(116),
                          height: ScreenUtil().setHeight(43),
                          alignment: Alignment.center,
                          decoration: ShapeDecoration(
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(22)),
                            shadows: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.30),
                                blurRadius: 10,
                                offset: Offset(0, ScreenUtil().setHeight(6)),
                              ),
                            ],
                          ),
                          child: widget.isLoading
                              ? CupertinoActivityIndicator()
                              : Text(
                                  'Login',
                                  style: TextStyle(
                                    fontFamily:
                                        textDirection == TextDirection.rtl
                                            ? 'Cairo'
                                            : 'SF Pro Text',
                                    fontSize: 11,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFF021A89),
                                  ),
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void _submit() {
    if (_validate()) {
      widget.onSubmit(_password);
    }
  }

  bool _validate() {
    final formState = _formKey.currentState;
    if (formState.validate()) {
      formState.save();
      return true;
    }
    return false;
  }
}
