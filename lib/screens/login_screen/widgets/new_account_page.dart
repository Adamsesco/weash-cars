import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weash_cars/generated/i18n.dart';

typedef OnBack();
typedef OnSubmit(
    String firstName, String lastName, String email, String password);

class NewAccountPage extends StatefulWidget {
  final OnBack onBack;
  final OnSubmit onSubmit;
  final bool isLoading;

  NewAccountPage(
      {Key key,
      @required this.onBack,
      @required this.onSubmit,
      @required this.isLoading})
      : super(key: key);

  @override
  _NewAccountPageState createState() => _NewAccountPageState();
}

class _NewAccountPageState extends State<NewAccountPage> {
  GlobalKey<FormState> _formKey = GlobalKey();
  FocusNode _lastNameFocusNode = FocusNode();
  FocusNode _emailFocusNode = FocusNode();
  FocusNode _passwordFocusNode = FocusNode();
  FocusNode _confirmPasswordFocusNode = FocusNode();
  String _firstName;
  String _lastName;
  String _email;
  String _password;
  String _tmpPassword;

  @override
  Widget build(BuildContext context) {
    final textDirection = Directionality.of(context);

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Positioned.directional(
          textDirection: textDirection,
          top: ScreenUtil().setHeight(
              MediaQuery.of(context).padding.top + ScreenUtil().setHeight(20)),
          start: ScreenUtil().setWidth(20),
          child: InkWell(
            onTap: widget.onBack,
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            child: Container(
              width: ScreenUtil().setWidth(35),
              height: ScreenUtil().setWidth(35),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors.black.withOpacity(0.16),
                    blurRadius: 11,
                  ),
                ],
              ),
              child: Center(
                child: Image(
                  image: AssetImage('assets/images/back-button-icon.png'),
                  width: ScreenUtil().setWidth(17.39),
                  height: ScreenUtil().setHeight(12.15),
                  matchTextDirection: true,
                ),
              ),
            ),
          ),
        ),
        Positioned.fill(
          top: ScreenUtil().setHeight(278),
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(57),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Finish your account !',
                    style: TextStyle(
                      fontFamily: textDirection == TextDirection.rtl
                          ? 'Cairo'
                          : 'SF Pro Text',
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 0.20,
                      color: Colors.white.withOpacity(0.65),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(49)),
                  Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            SizedBox(
                              width: ScreenUtil().setWidth(120),
                              child: TextFormField(
                                autofocus: true,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                style: TextStyle(
                                  fontFamily: textDirection == TextDirection.rtl
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1,
                                  color: Colors.white.withOpacity(0.65),
                                ),
                                decoration: InputDecoration(
                                  hintText: 'First name',
                                  hintStyle: TextStyle(
                                    fontFamily:
                                        textDirection == TextDirection.rtl
                                            ? 'Cairo'
                                            : 'SF Pro Text',
                                    fontSize: 13,
                                    fontWeight: FontWeight.w300,
                                    letterSpacing: 1,
                                    color: Colors.white.withOpacity(0.5),
                                  ),
                                ),
                                validator: (String value) {
                                  if (value == null || value.length < 2) {
                                    return S.of(context).firstNameError;
                                  }
                                  return null;
                                },
                                onFieldSubmitted: (_) {
                                  FocusScope.of(context)
                                      .requestFocus(_lastNameFocusNode);
                                },
                                onSaved: (String value) {
                                  _firstName = value;
                                },
                              ),
                            ),
                            SizedBox(
                              width: ScreenUtil().setWidth(120),
                              child: TextFormField(
                                focusNode: _lastNameFocusNode,
                                textCapitalization: TextCapitalization.words,
                                textInputAction: TextInputAction.next,
                                style: TextStyle(
                                  fontFamily: textDirection == TextDirection.rtl
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 1,
                                  color: Colors.white.withOpacity(0.65),
                                ),
                                decoration: InputDecoration(
                                  hintText: 'Last name',
                                  hintStyle: TextStyle(
                                    fontFamily:
                                        textDirection == TextDirection.rtl
                                            ? 'Cairo'
                                            : 'SF Pro Text',
                                    fontSize: 13,
                                    fontWeight: FontWeight.w300,
                                    letterSpacing: 1,
                                    color: Colors.white.withOpacity(0.5),
                                  ),
                                ),
                                validator: (String value) {
                                  if (value == null || value.length < 2) {
                                    return S.of(context).lastNameError;
                                  }
                                  return null;
                                },
                                onFieldSubmitted: (_) {
                                  FocusScope.of(context)
                                      .requestFocus(_emailFocusNode);
                                },
                                onSaved: (String value) {
                                  _lastName = value;
                                },
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(17)),
                        TextFormField(
                          focusNode: _emailFocusNode,
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(
                            fontFamily: textDirection == TextDirection.rtl
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1,
                            color: Colors.white.withOpacity(0.65),
                          ),
                          decoration: InputDecoration(
                            hintText: 'Email',
                            hintStyle: TextStyle(
                              fontFamily: textDirection == TextDirection.rtl
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: 13,
                              fontWeight: FontWeight.w300,
                              letterSpacing: 1,
                              color: Colors.white.withOpacity(0.5),
                            ),
                          ),
                          validator: (String value) {
                            final pattern =
                                r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
                            final regexp = new RegExp(pattern);
                            if (value != null &&
                                value.isNotEmpty &&
                                !regexp.hasMatch(value)) {
                              return S.of(context).emailError;
                            }
                            return null;
                          },
                          onFieldSubmitted: (_) {
                            FocusScope.of(context)
                                .requestFocus(_passwordFocusNode);
                          },
                          onSaved: (String value) {
                            _email = value;
                          },
                        ),
                        SizedBox(height: ScreenUtil().setHeight(17)),
                        TextFormField(
                          focusNode: _passwordFocusNode,
                          obscureText: true,
                          textInputAction: TextInputAction.next,
                          style: TextStyle(
                            fontFamily: textDirection == TextDirection.rtl
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1,
                            color: Colors.white.withOpacity(0.65),
                          ),
                          decoration: InputDecoration(
                            hintText: 'Password',
                            hintStyle: TextStyle(
                              fontFamily: textDirection == TextDirection.rtl
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: 13,
                              fontWeight: FontWeight.w300,
                              letterSpacing: 1,
                              color: Colors.white.withOpacity(0.5),
                            ),
                          ),
                          validator: (String value) {
                            if (value == null || value.length < 6) {
                              return 'The password must contain at least 6 characters';
                            }
                            return null;
                          },
                          onChanged: (String value) {
                            _tmpPassword = value;
                          },
                          onFieldSubmitted: (_) {
                            FocusScope.of(context)
                                .requestFocus(_confirmPasswordFocusNode);
                          },
                          onSaved: (String value) {
                            _password = value;
                          },
                        ),
                        SizedBox(height: ScreenUtil().setHeight(17)),
                        TextFormField(
                          focusNode: _confirmPasswordFocusNode,
                          obscureText: true,
                          textInputAction: TextInputAction.done,
                          style: TextStyle(
                            fontFamily: textDirection == TextDirection.rtl
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1,
                            color: Colors.white.withOpacity(0.65),
                          ),
                          decoration: InputDecoration(
                            hintText: 'Confirm password',
                            hintStyle: TextStyle(
                              fontFamily: textDirection == TextDirection.rtl
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: 13,
                              fontWeight: FontWeight.w300,
                              letterSpacing: 1,
                              color: Colors.white.withOpacity(0.5),
                            ),
                          ),
                          validator: (String value) {
                            if (value != _tmpPassword) {
                              return "The passwords don't match";
                            }
                            return null;
                          },
                          onFieldSubmitted: (_) {
                            _submit();
                          },
                        ),
                        SizedBox(height: ScreenUtil().setHeight(44)),
                        InkWell(
                          onTap: _submit,
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Container(
                            width: ScreenUtil().setWidth(116),
                            height: ScreenUtil().setHeight(43),
                            alignment: Alignment.center,
                            decoration: ShapeDecoration(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(22)),
                              shadows: [
                                BoxShadow(
                                  color: Colors.black.withOpacity(0.30),
                                  blurRadius: 10,
                                  offset: Offset(0, ScreenUtil().setHeight(6)),
                                ),
                              ],
                            ),
                            child: widget.isLoading
                                ? CupertinoActivityIndicator()
                                : Text(
                                    'SAVE',
                                    style: TextStyle(
                                      fontFamily:
                                          textDirection == TextDirection.rtl
                                              ? 'Cairo'
                                              : 'SF Pro Text',
                                      fontSize: 11,
                                      fontWeight: FontWeight.w700,
                                      color: Color(0xFF021A89),
                                    ),
                                  ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  bool _validate() {
    final formState = _formKey.currentState;
    if (formState.validate()) {
      formState.save();
      return true;
    }
    return false;
  }

  void _submit() {
    if (_validate()) {
      widget.onSubmit(_firstName, _lastName, _email, _password);
    }
  }
}
