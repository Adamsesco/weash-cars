import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import "package:google_maps_webservice/geocoding.dart" as geocoding;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weash_cars/consts/country_codes.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:http/http.dart' as http;

import 'package:weash_cars/models/user_role.dart';
import 'package:weash_cars/screens/login_screen/widgets/new_account_page.dart';
import 'package:weash_cars/screens/login_screen/widgets/new_password_page.dart';
import 'package:weash_cars/screens/login_screen/widgets/password_page.dart';
import 'package:weash_cars/services/UsersService.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/stores/customer_store.dart';
import 'package:weash_cars/stores/employee_store.dart';

class LoginWithPasswordScreen extends StatefulWidget {
  @override
  _LoginWithPasswordScreenState createState() =>
      _LoginWithPasswordScreenState();
}

class _LoginWithPasswordScreenState extends State<LoginWithPasswordScreen> {
  geocoding.GoogleMapsGeocoding _geocoder;
  Location _locationService;
  UsersService _usersService;
  CompanyStore _company;
  CustomerStore _customer;
  EmployeeStore _employee;
  UserRole _userRole;
  String _countryDialCode;
  List<String> _countryDialCodes;
  Future<String> _countryDialCodeFuture;
  String _phoneNumber;

  PageController _pageController;
  TextEditingController _phoneNumberController;
  _PasswordPageType _passwordPageType;
  Widget _passwordPage = Container();
  DocumentSnapshot _userDocument;

  bool _isSettingNewPassword = false;
  bool _isSigningIn = false;
  bool _isSigningUp = false;

  @override
  void initState() {
    super.initState();

    final applicationStore =
        Provider.of<ApplicationStore>(context, listen: false);
    _company = applicationStore.companyStore;
    _customer = applicationStore.customerStore;
    _employee = applicationStore.employeeStore;

    _geocoder = new geocoding.GoogleMapsGeocoding(
        apiKey: 'AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I');
    _locationService = new Location();
    _usersService = new UsersService();
    _phoneNumberController = new TextEditingController();
    _pageController = new PageController();
    _countryDialCodes = [];
    _countryDialCodeFuture = _getCountryDialCode();

//    _countryDialCodes = [
//      '+20', // Egypt
//      '+44', // UK
//      '+212', // Morocco
//      '+962', // Jordan
//      '+965', // Kuwait
//      '+966', // KSA
//      '+968', // Oman
//      '+971', // UAE
//      '+973', // Bahrain
//      '+974', // Qatar
//    ];
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned.fill(
            child: Image(
              image: AssetImage('assets/images/we-come-to-you-background.png'),
              fit: BoxFit.cover,
              matchTextDirection: true,
            ),
          ),
          PageView(
            controller: _pageController,
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Positioned.fill(
                    child: FutureBuilder<String>(
                      future: _countryDialCodeFuture,
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.none:
                          case ConnectionState.active:
                          case ConnectionState.waiting:
                            return Center(
                              child: CupertinoActivityIndicator(),
                            );
                          case ConnectionState.done:
                            final countryDialCode = snapshot.data.toString();
                            // final countryDialCode = '+965';
                            if (countryDialCode != null) {
                              _countryDialCode =
                                  _countryDialCode ?? countryDialCode;

                              return SingleChildScrollView(
                                padding: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(59)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(
                                        height: ScreenUtil().setHeight(340)),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      textDirection: TextDirection.ltr,
                                      children: <Widget>[
                                        DropdownButton<String>(
                                          value: _countryDialCode,
                                          underline: Container(),
                                          isDense: true,
                                          selectedItemBuilder:
                                              (BuildContext context) {
                                            return _countryDialCodes
                                                .map<DropdownMenuItem<String>>(
                                                    (String value) {
                                              return DropdownMenuItem(
                                                value: value,
                                                child: Text(
                                                  value,
                                                  style: TextStyle(
                                                    fontFamily:
                                                        application.locale ==
                                                                'ar'
                                                            ? 'Cairo'
                                                            : 'SF Pro Text',
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w400,
                                                    color: Colors.white,
                                                    decoration:
                                                        TextDecoration.none,
                                                  ),
                                                ),
                                              );
                                            }).toList();
                                          },
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Text',
                                            fontSize: 18,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.black,
                                            decoration: TextDecoration.none,
                                          ),
                                          onChanged: (String value) {
                                            setState(() {
                                              _countryDialCode = value;
                                            });
                                          },
                                          items: _countryDialCodes
                                              .map<DropdownMenuItem<String>>(
                                                  (String value) {
                                            return DropdownMenuItem(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                        ),
                                        SizedBox(
                                            width: ScreenUtil().setWidth(20)),
                                        Expanded(
                                          // width: ScreenUtil().setWidth(205),
                                          child: TextField(
                                            controller: _phoneNumberController,
                                            autofocus: true,
                                            keyboardType: TextInputType.number,
                                            textDirection: TextDirection.ltr,
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontSize: 14,
                                              fontWeight: FontWeight.w300,
                                              color: Colors.white,
                                              decoration: TextDecoration.none,
                                            ),
                                            decoration: InputDecoration(
                                              hintText: S
                                                  .of(context)
                                                  .enterMobileNumber,
                                              hintStyle: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Pro Text',
                                                fontSize: 14,
                                                fontWeight: FontWeight.w300,
                                                color: Colors.white
                                                    .withOpacity(0.34),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                            onSubmitted: (String phoneNumber) {
                                              _phoneNumber =
                                                  "$_countryDialCode${_phoneNumberController.text}";
                                              _gotoPasswordPage();
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                        height: ScreenUtil().setHeight(74)),
                                    Material(
                                      color: Colors.transparent,
                                      child: InkWell(
                                        onTap: () {
                                          _phoneNumber =
                                              "$_countryDialCode${_phoneNumberController.text}";
                                          _gotoPasswordPage();
                                        },
                                        borderRadius: BorderRadius.circular(20),
                                        child: Container(
                                          width: ScreenUtil().setWidth(93),
                                          height: ScreenUtil().setHeight(43),
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Color(0x120d2e00)),
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            color: Colors.white,
                                            boxShadow: <BoxShadow>[
                                              BoxShadow(
                                                color: Color(0x66000000),
                                                offset: Offset(0,
                                                    ScreenUtil().setHeight(3)),
                                              ),
                                            ],
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Text(
                                                S.of(context).login,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'SF Pro Text',
                                                  fontSize: 11,
                                                  fontWeight: FontWeight.w700,
                                                  color: Color(0xFF011e96),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }
                        }
                        return Container();
                      },
                    ),
                  ),
                  Positioned(
                    top: ScreenUtil().setHeight(
                        MediaQuery.of(context).padding.top +
                            ScreenUtil().setHeight(20)),
                    left: application.locale == 'ar'
                        ? null
                        : ScreenUtil().setWidth(20),
                    right: application.locale == 'ar'
                        ? ScreenUtil().setWidth(20)
                        : null,
                    child: InkWell(
                      onTap: () => Navigator.of(context).pop(),
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      child: Container(
                        width: ScreenUtil().setWidth(35),
                        height: ScreenUtil().setWidth(35),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.black.withOpacity(0.16),
                              blurRadius: 11,
                            ),
                          ],
                        ),
                        child: Center(
                          child: Image(
                            image: AssetImage(
                                'assets/images/back-button-icon.png'),
                            width: ScreenUtil().setWidth(17.39),
                            height: ScreenUtil().setHeight(12.15),
                            matchTextDirection: true,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              if (_passwordPageType == _PasswordPageType.setPassword)
                NewPasswordPage(
                  isLoading: _isSettingNewPassword || _isSigningIn,
                  onSubmit: (String password) async {
                    setState(() {
                      _isSettingNewPassword = true;
                    });

                    final setPasswordRequest = await http.get(
                        "https://us-central1-weash-cars.cloudfunctions.net/setPassword?userId=${_userDocument.documentID}&password=$password");
                    if (setPasswordRequest.statusCode == 200) {
                      print('Set new password');
                      final encodedPhoneNumber =
                          Uri.encodeComponent(_phoneNumber);
                      final encodedPassword = Uri.encodeComponent(password);
                      final loginRequest = await http.get(
                          "https://us-central1-weash-cars.cloudfunctions.net/login?phoneNumber=$encodedPhoneNumber&password=$encodedPassword");
                      if (loginRequest.statusCode == 200) {
                        await _signIn(_userDocument);
                      } else {
                        print('Incorrect password');
                      }
                    }

                    setState(() {
                      _isSettingNewPassword = false;
                    });
                  },
                  onBack: () {
                    _pageController.animateToPage(0,
                        duration: Duration(milliseconds: 300),
                        curve: Curves.linear);
                  },
                ),
              if (_passwordPageType == _PasswordPageType.newAccount)
                NewAccountPage(
                  isLoading: _isSigningUp,
                  onSubmit: (String firstName, String lastName, String email,
                      String password) async {
                    setState(() {
                      _isSigningUp = true;
                    });

                    _userDocument = await (await Firestore.instance
                            .collection('users')
                            .add({
                      'first_name': firstName,
                      'last_name': lastName,
                      'email': email,
                      'phone_number': _phoneNumber,
                      'role': 'customer',
                    }))
                        .get();

                    final setPasswordRequest = await http.get(
                        "https://us-central1-weash-cars.cloudfunctions.net/setPassword?userId=${_userDocument.documentID}&password=$password");
                    if (setPasswordRequest.statusCode == 200) {
                      print('Set new password');
                      final encodedPhoneNumber =
                          Uri.encodeComponent(_phoneNumber);
                      final encodedPassword = Uri.encodeComponent(password);
                      final loginRequest = await http.get(
                          "https://us-central1-weash-cars.cloudfunctions.net/login?phoneNumber=$encodedPhoneNumber&password=$encodedPassword");
                      if (loginRequest.statusCode == 200) {
                        await _signIn(_userDocument);
                      } else {
                        print('Incorrect password');
                      }
                    }

                    setState(() {
                      _isSigningUp = false;
                    });
                  },
                  onBack: () {
                    _pageController.animateToPage(0,
                        duration: Duration(milliseconds: 300),
                        curve: Curves.linear);
                  },
                ),
              if (_passwordPageType == _PasswordPageType.signin)
                PasswordPage(
                  isLoading: _isSigningIn,
                  onSubmit: (String password) async {
                    setState(() {
                      _isSigningIn = true;
                    });

                    final encodedPhoneNumber =
                        Uri.encodeComponent(_phoneNumber);
                    final encodedPassword = Uri.encodeComponent(password);

                    final request = await http.get(
                        "https://us-central1-weash-cars.cloudfunctions.net/login?phoneNumber=$encodedPhoneNumber&password=$encodedPassword");
                    if (request.statusCode == 200) {
                      _signIn(_userDocument);
                    } else {
                      print('Incorrect password');
                    }

                    setState(() {
                      _isSigningIn = false;
                    });
                  },
                  onBack: () {
                    _pageController.animateToPage(0,
                        duration: Duration(milliseconds: 300),
                        curve: Curves.linear);
                  },
                ),
            ],
          ),
        ],
      ),
    );
  }

  Future<void> _gotoPasswordPage() async {
    _userDocument = await _getUser(_phoneNumber);
    if (_userDocument == null) {
      setState(() {
        _passwordPageType = _PasswordPageType.newAccount;
      });
    } else {
      final userData = _userDocument.data;
      if (userData['password'] == null) {
        setState(() {
          _passwordPageType = _PasswordPageType.setPassword;
        });
      } else {
        setState(() {
          _passwordPageType = _PasswordPageType.signin;
        });
      }
    }

    _pageController.animateToPage(1,
        duration: Duration(milliseconds: 300), curve: Curves.linear);
  }

  Future<String> _getCountryCode() async {
    await _locationService.changeSettings(accuracy: LocationAccuracy.HIGH);

    LocationData location;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      bool serviceStatus = await _locationService.serviceEnabled();
      if (serviceStatus) {
        final permission = await _locationService.requestPermission();
        if (permission) {
          location = await _locationService.getLocation();

          final addressResult = await _geocoder.searchByLocation(
              new geocoding.Location(location.latitude, location.longitude));
          if (addressResult.isOkay && addressResult.results.length > 0) {
            final countryCode = addressResult.results[0].addressComponents
                .firstWhere(
                    (addressComponent) =>
                        addressComponent.types.contains('country') &&
                        addressComponent.types.contains('political'),
                    orElse: () {
              print("No result");
              return null;
            }).shortName;

            return countryCode;
          }
        } else {
          showCupertinoDialog(
            context: context,
            builder: (BuildContext context) => CupertinoAlertDialog(
              content: Text(S.of(context).allowLocation),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(S.of(context).ok),
                ),
              ],
            ),
          );
        }
      } else {
        bool serviceStatusResult = await _locationService.requestService();
        print("Service status activated after request: $serviceStatusResult");
        if (serviceStatusResult) {
          return _getCountryCode();
        }
        return null;
      }
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            content: Text(S.of(context).allowLocation),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(S.of(context).ok),
              ),
            ],
          ),
        );
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        showCupertinoDialog(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            content: Text(S.of(context).enableLocationService),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(S.of(context).ok),
              ),
            ],
          ),
        );
      }
    }

    return null;
  }

  Future<String> _getCountryDialCode() async {
    final countriesDocuments =
        await Firestore.instance.collection('countries').getDocuments();
    if (countriesDocuments != null && countriesDocuments.documents.isNotEmpty) {
      for (var document in countriesDocuments.documents) {
        _countryDialCodes.add(document.data['dial_code']);
      }

      _countryDialCodes.sort((a, b) {
        final n1 = int.tryParse(a) ?? 0;
        final n2 = int.tryParse(b) ?? 0;
        return n1.compareTo(n2);
      });
    }

    final countryCode = await _getCountryCode();
    if (countryCode != null) {
      final country = COUNTRY_CODES.firstWhere(
          (countryData) => countryData['code'] == countryCode,
          orElse: () => null);
      if (country != null) {
        final countryDialCode = country['dial_code'];
        if (!_countryDialCodes.contains(countryDialCode)) {
          _countryDialCodes.add(countryDialCode);
          _countryDialCodes.sort((a, b) {
            final n1 = int.tryParse(a) ?? 0;
            final n2 = int.tryParse(b) ?? 0;
            return n1.compareTo(n2);
          });
        }
        return countryDialCode;
      }
    }
    return null;
  }

  Future<DocumentSnapshot> _getUser(String phoneNumber) async {
    final querySnapshot = await Firestore.instance
        .collection('users')
        .where('phone_number', isEqualTo: phoneNumber)
        .limit(1)
        .getDocuments();
    if (querySnapshot == null ||
        querySnapshot.documents == null ||
        querySnapshot.documents.isEmpty) {
      return null;
    }

    final document = querySnapshot.documents.first;

    return document;
  }

  Future<void> _signIn(DocumentSnapshot userDocument) async {
    final application = Provider.of<ApplicationStore>(context);

    final ds = userDocument;
    final userData = ds.data;
    final userRole = userData['role'];

    switch (userRole) {
      case 'customer':
        _userRole = UserRole.customer;
        break;

      case 'employee':
        _userRole = UserRole.employee;
        break;

      case 'company':
        _userRole = UserRole.company;
        break;

      case 'self_employed':
        _userRole = UserRole.selfEmployed;
        break;
    }
    application.setUserRole(_userRole);

    if (userData['locale'] != null &&
        ['ar', 'en'].contains(userData['locale'])) {
      application.localeStream.add(userData['locale']);
    }

    if (_userRole == UserRole.company || _userRole == UserRole.selfEmployed) {
      if (userData['is_activated'] == false) {
        _showDialog('Sign-in error', 'Your account is not yet activated.');
      } else {
        Firestore.instance
            .collection('users')
            .where('role', isEqualTo: 'employee')
            .where('company_id', isEqualTo: ds.documentID)
            .getDocuments()
            .then((QuerySnapshot employeesDocuments) async {
          final employees = new List<Map<String, dynamic>>();

          if (_userRole == UserRole.company) {
            await Future.forEach(employeesDocuments.documents,
                (DocumentSnapshot employeeDocument) {
              final employee = <String, dynamic>{
                'employee_id': employeeDocument.documentID,
                'company_id': employeeDocument.data['company_id'],
                'first_name': employeeDocument.data['first_name'],
                'last_name': employeeDocument.data['last_name'],
                'phone_number': employeeDocument.data['phone_number'],
                'avatar_url': employeeDocument.data['avatar_url'],
                'jobs_done': employeeDocument.data['jobs_done'] as int,
                'type': employeeDocument.data['type'] ?? 'smart',
                'created_at':
                    (employeeDocument.data['created_at'] as Timestamp).toDate(),
                'is_available': employeeDocument.data['is_available'] ?? true,
              };

              employees.add(employee);
            });
          }

          final schedule = (userData['schedule'] as Map<dynamic, dynamic>)
              .map((day, daySchedule) {
            final dayNumber = int.tryParse(day as String);
            final timeFrom = daySchedule['from'] as int;
            final timeTo = daySchedule['to'] as int;
            final isDayOff = daySchedule['is_day_off'] as bool;

            return MapEntry(dayNumber, {
              'from': timeFrom,
              'to': timeTo,
              'is_day_off': isDayOff,
            });
          });

          _company.setData(
            companyId: ds.documentID,
            role: _userRole,
            email: userData['email'],
            phoneNumber: userData['phone_number'],
            fcmToken: userData['fcm_token'],
            companyName: userData['company_name'],
            descripton: userData['description'],
            location: userData['location'] == null
                ? null
                : userData['location']['geopoint'] as GeoPoint,
            address: userData['address'],
            locality: userData['locality'],
            country: userData['country'],
            avatarUrl: userData['avatar_url'],
            rating: userData['rating'].toDouble() as double,
            ordersCount: userData['total_orders'] as int,
            packagesCount: userData['total_packages'] as int,
            isAvailable: userData['is_available'] as bool,
            schedule: schedule,
            daysUnavailable: userData['days_unavailable'] == null
                ? new List<DateTime>()
                : new List<DateTime>.from(userData['days_unavailable']
                    .map((date) => (date as Timestamp).toDate())),
            dateCreated: (userData['date_created'] as Timestamp).toDate(),
            dateActivated: userData['date_activated'] == null
                ? null
                : (userData['date_activated'] as Timestamp).toDate(),
            employees: employees,
            jobsDoneCount: userData['jobs_done'] as int,
            locale: userData['locale'],
            currency: userData['currency'],
          );
          setState(() {
            _isSigningIn = false;
          });
          application.setIsSignedIn(true);
          final prefs = await SharedPreferences.getInstance();
          prefs.setString('user_id', ds.documentID);
          Navigator.pushNamed(context, '/dashboard/company');
        });
      }
    } else if (_userRole == UserRole.customer) {
      _customer.setData(
        customerId: ds.documentID,
        phoneNumber: userData['phone_number'],
        fullName: userData['full_name'],
        firstName: userData['first_name'],
        lastName: userData['last_name'],
        email: userData['email'],
        avatarUrl: userData['avatar_url'],
        locale: userData['locale'],
        location: userData['location'] == null
            ? null
            : userData['location']['geopoint'] as GeoPoint,
        address: userData['address'],
        fcmToken: userData['fcm_token'],
      );
      setState(() {
        _isSigningIn = false;
      });
      application.setIsSignedIn(true);
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('user_id', ds.documentID);
      Navigator.pushNamed(context, '/dashboard/customer');
    } else if (_userRole == UserRole.employee) {
      _employee.setData(
        employeeId: ds.documentID,
        companyId: userData['company_id'],
        phoneNumber: userData['phone_number'],
        firstName: userData['first_name'],
        lastName: userData['last_name'],
        avatarUrl: userData['avatar_url'],
        jobsDoneCount: userData['jobs_done'] as int,
        locale: userData['locale'],
        fcmToken: userData['fcm_token'],
      );
      setState(() {
        _isSigningIn = false;
      });
      application.setIsSignedIn(true);
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('user_id', ds.documentID);
      Navigator.pushNamed(context, '/dashboard/employee');
    }
  }

  void _showDialog(String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      },
    );
  }
}

enum _PasswordPageType {
  setPassword,
  newAccount,
  signin,
}
