import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WheelList extends StatefulWidget {
  final List<Widget> children;

  WheelList({Key key, this.children}) : super(key: key);

  @override
  WheelListState createState() => WheelListState();
}

class WheelListState extends State<WheelList> {
  double _dy = 0.0;
  bool _isDraggingDown;
  List<Widget> _children;
  List<Widget> _childrenTmp;

  @override
  void didChangeDependencies() {
    _children = List<Widget>.of(widget.children ?? []);

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    if (_children == null || _children.isEmpty) {
      return Container();
    } else if (_children.length == 1) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ConstrainedBox(
            constraints: BoxConstraints.expand(
              width: ScreenUtil().setWidth(326),
              // height: height,
            ),
            child: _children[0],
          ),
        ],
      );
    }

    return GestureDetector(
      onVerticalDragStart: (DragStartDetails details) {
        setState(() {
          _childrenTmp = _children;
        });
      },
      onVerticalDragUpdate: (DragUpdateDetails details) {
        setState(() {
          _dy += details.primaryDelta / 5;

          if (_dy >= 0) {
            _isDraggingDown = true;
          } else {
            _isDraggingDown = false;
          }
        });
      },
      onVerticalDragEnd: (DragEndDetails details) {
        setState(() {
          _dy = 0;
          _children = _childrenTmp;
          if (_isDraggingDown) {
            _children.add(null);
            _children[_children.length - 1] = _children.first;
            _children.removeAt(0);
          } else {
            _children.insert(0, null);
            _children[0] = _children.last;
            _children.removeLast();
          }
        });
      },
      onVerticalDragCancel: () => setState(() => _dy = 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ConstrainedBox(
            constraints: BoxConstraints.expand(
              width: ScreenUtil().setWidth(326),
              // height: height,
            ),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                for (var i = _children.length ~/ 2 + 1;
                    i < _children.length &&
                        (i - (_children.length ~/ 2 + 1)) <= 2;
                    i++)
                  Builder(
                    builder: (BuildContext context) {
                      final index = min(2, _children.length ~/ 2) -
                          (i - (_children.length ~/ 2 + 1));
                      final offset = (_dy % ScreenUtil().setHeight(14.0)) /
                          ScreenUtil().setHeight(14.0);
                      var transform = 3.0 - (index + offset).abs();
                      transform = _mapToInterval(transform, 0.0, 3.0, 0.6, 1.0);
                      final opacity = pow(transform, 3);
                      final scale = transform;

                      return AnimatedPositioned(
                        top: ScreenUtil().setHeight((index + 2) * 14.0) +
                            _dy % ScreenUtil().setHeight(14.0),
                        duration: Duration(milliseconds: _dy == 0 ? 300 : 0),
                        curve: Curves.easeOut,
                        child: Opacity(
                          opacity: opacity,
                          child: Transform.scale(
                            scale: scale,
                            alignment: Alignment.bottomCenter,
                            child: _children[i],
                          ),
                        ),
                      );
                    },
                  ),
                for (var i = _children.length ~/ 2;
                    i >= 0 &&
                        _children.length > 0 &&
                        (_children.length ~/ 2 - i) <= 2;
                    i--)
                  Builder(
                    builder: (BuildContext context) {
                      final index = min(2, _children.length ~/ 2) -
                          (_children.length ~/ 2 - i);
                      final offset = (_dy % ScreenUtil().setHeight(14.0)) /
                          ScreenUtil().setHeight(14.0);
                      var transform = 3.0 - (index - offset).abs();
                      transform = _mapToInterval(transform, 0.0, 3.0, 0.6, 1.0);
                      final opacity = pow(transform, 3);
                      final scale = transform;

                      return AnimatedPositioned(
                        top: ScreenUtil().setHeight((2 - index) * 14.0) +
                            _dy % ScreenUtil().setHeight(14.0),
                        duration: Duration(milliseconds: _dy == 0 ? 300 : 0),
                        curve: Curves.easeOut,
                        child: Opacity(
                          opacity: opacity,
                          child: Transform.scale(
                            scale: scale,
                            alignment: Alignment.topCenter,
                            child: _children[i],
                          ),
                        ),
                      );
                    },
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  double _mapToInterval(
      double value, double a1, double b1, double a2, double b2) {
    final mapped = (value - a1) * (b2 - a2) / (b1 - a1) + a2;
    return mapped;
  }
}
