import 'dart:math';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/vehicle_type.dart';
import 'package:weash_cars/screens/packages_management_screen/widgets/wheel_list.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class PackagesManagementScreen extends StatefulWidget {
  @override
  _PackagesManagementScreenState createState() =>
      _PackagesManagementScreenState();
}

class _PackagesManagementScreenState extends State<PackagesManagementScreen> {
  CompanyStore _company;

  PageController _pagesController;
  int _page = 0;
  List<VehicleType> _vehicleTypes = [
    VehicleType.sedan,
    VehicleType.motorcycle,
    VehicleType.suv,
    VehicleType.boat,
  ];
  List<String> _types;
  bool _isDialogVisible = false;

  @override
  void initState() {
    super.initState();

    _pagesController = PageController();

    final applicationStore =
        Provider.of<ApplicationStore>(context, listen: false);
    _company = applicationStore.companyStore;

    if (_company.packages == null) {
      _getPackages();
    }
  }

  @override
  void dispose() {
    _pagesController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    _types = [
      S.of(context).carsPackages,
      S.of(context).motorcyclesPackages,
      S.of(context).suvsPackages,
      S.of(context).boatsPackages,
    ];

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CompanyDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _company.avatarUrl,
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image(
                image:
                    AssetImage('assets/images/company-screens-background.png'),
                matchTextDirection: true,
                fit: BoxFit.fill),
          ),
          Positioned(
            top: 0,
            child: Observer(
              builder: (BuildContext context) => CustomAppBar2(
                onOpenDrawer: () {
                  setState(() {
                    _isDialogVisible = true;
                    Scaffold.of(context).openDrawer();
                  });
                },
                title: S.of(context).dashboard,
                transparentBackground: true,
                titleColor: Color(0xFF91A6D9),
                buttonColor: Color(0xFF0930C3),
                nbNotifications: 0,
                avatar: _company.avatarUrl == null
                    ? null
                    : NetworkImage(_company.avatarUrl),
              ),
            ),
          ),
          Positioned.fill(
            top: ScreenUtil().setHeight(150),
            child: Column(
              children: <Widget>[
                Align(
                  alignment: application.locale == 'ar'
                      ? Alignment.topRight
                      : Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(25),
                    ),
                    child: Text(
                      _types[_page],
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Display',
                        fontSize: 24,
                        fontWeight: FontWeight.w900,
                        color: Color(0xFFA0AEE6),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(26)),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Flexible(
                        fit: FlexFit.loose,
                        child: PageView.builder(
                          controller: _pagesController,
                          scrollDirection: Axis.horizontal,
                          onPageChanged: (int page) =>
                              setState(() => _page = page),
                          itemCount: _vehicleTypes.length,
                          itemBuilder: (BuildContext context, int pageIndex) {
                            if (_company.packages == null ||
                                _company.packages.isEmpty) {
                              return Container();
                            }

                            return Builder(builder: (context) {
                              // final vehicleTypeStr = _vehicleTypes[pageIndex]
                              //     .toString()
                              //     .replaceFirst(new RegExp(r'^.*\.'), '');

                              final packages = _company.packages
                                  .where((package) =>
                                      package['vehicle_type'] ==
                                      _vehicleTypes[pageIndex])
                                  .toList();

                              return Container(
                                child: WheelList(
                                  key: ValueKey<int>(packages.length),
                                  children: packages?.map((package) {
                                    final packageId = package['package_id'];
                                    final packageTitle = package['title'];
                                    final packagePrice =
                                        package['price'] as double;
                                    final priceNat = packagePrice.toInt();
                                    final priceFrac = packagePrice
                                        .toStringAsFixed(3)
                                        .replaceFirst(
                                            new RegExp(r'[0-9]+\.'), '');
                                    final services = List<String>.from(
                                        package['services'] as List<String>);

                                    final servicesWidget = Align(
                                      alignment: application.locale == 'ar'
                                          ? Alignment.topRight
                                          : Alignment.topLeft,
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        padding: const EdgeInsets.all(0),
                                        child: Wrap(
                                          direction: Axis.vertical,
                                          runSpacing: ScreenUtil().setWidth(26),
                                          spacing: ScreenUtil().setHeight(6),
                                          children: services
                                              .map((String service) => Row(
                                                    children: <Widget>[
                                                      Text(
                                                        "- $service",
                                                        style: TextStyle(
                                                          fontFamily: application
                                                                      .locale ==
                                                                  'ar'
                                                              ? 'Cairo'
                                                              : 'SF Pro Text',
                                                          fontSize: 11,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          color: Colors.white,
                                                        ),
                                                      ),
                                                    ],
                                                  ))
                                              .toList(),
                                        ),
                                      ),
                                    );

                                    return Container(
                                      width: ScreenUtil().setWidth(326),
                                      // height: ScreenUtil()
                                      //     .setHeight(320),
                                      padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(27.5),
                                        vertical: ScreenUtil().setHeight(34),
                                      ),
                                      decoration: BoxDecoration(
                                        color: Color(0xFF11296F),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Image(
                                                image: AssetImage(
                                                    'assets/images/packages-icon.png'),
                                                width: ScreenUtil()
                                                    .setWidth(26.27),
                                                height: ScreenUtil()
                                                    .setHeight(29.91),
                                                color: Colors.white,
                                              ),
                                              SizedBox(
                                                  width: ScreenUtil()
                                                      .setWidth(3.7)),
                                              Text(
                                                packageTitle,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'SF Pro Text',
                                                  fontSize: 19,
                                                  fontWeight: FontWeight.w700,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                              height:
                                                  ScreenUtil().setHeight(38.1)),
                                          packages.length > 1
                                              ? SizedBox(
                                                  height: ScreenUtil()
                                                      .setHeight(103),
                                                  width: ScreenUtil()
                                                      .setWidth(272),
                                                  child: servicesWidget,
                                                )
                                              : Expanded(
                                                  child: servicesWidget,
                                                ),
                                          SizedBox(
                                              height:
                                                  ScreenUtil().setHeight(6.7)),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  InkWell(
                                                    onTap: () =>
                                                        Navigator.pushNamed(
                                                            context,
                                                            "/packages/edit/$packageId/${_vehicleTypes[pageIndex]}"),
                                                    child: Text(
                                                      S.of(context).edit,
                                                      style: TextStyle(
                                                        fontFamily: application
                                                                    .locale ==
                                                                'ar'
                                                            ? 'Cairo'
                                                            : 'SF Pro Text',
                                                        fontFamilyFallback: <
                                                            String>[
                                                          'SF Pro Text'
                                                        ],
                                                        fontSize: 11,
                                                        fontWeight:
                                                            FontWeight.w800,
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                      width: ScreenUtil()
                                                          .setWidth(27)),
                                                  InkWell(
                                                    onTap: () async {
                                                      await _deletePackage(
                                                          package);

                                                      setState(() {
                                                        _company.setData(
                                                            packages: _company
                                                                .packages);
                                                      });
                                                    },
                                                    child: Text(
                                                      S.of(context).delete,
                                                      style: TextStyle(
                                                        fontFamily: application
                                                                    .locale ==
                                                                'ar'
                                                            ? 'Cairo'
                                                            : 'SF Pro Text',
                                                        fontFamilyFallback: <
                                                            String>[
                                                          'SF Pro Text'
                                                        ],
                                                        fontSize: 11,
                                                        fontWeight:
                                                            FontWeight.w800,
                                                        color: Color(0xFFFAFAFA)
                                                            .withOpacity(0.5),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                textDirection:
                                                    TextDirection.ltr,
                                                children: <Widget>[
                                                  SizedBox(
                                                    height: ScreenUtil()
                                                        .setHeight(65),
                                                    child: FittedBox(
                                                      fit: BoxFit.fitHeight,
                                                      child: Text(
                                                        priceNat.toString(),
                                                        style: TextStyle(
                                                          fontFamily: application
                                                                      .locale ==
                                                                  'ar'
                                                              ? 'Cairo'
                                                              : 'SF Pro Text',
                                                          fontWeight:
                                                              FontWeight.w800,
                                                          color: Colors.white,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    crossAxisAlignment:
                                                        application.locale ==
                                                                'ar'
                                                            ? CrossAxisAlignment
                                                                .end
                                                            : CrossAxisAlignment
                                                                .start,
                                                    children: <Widget>[
                                                      SizedBox(
                                                        height: ScreenUtil()
                                                            .setHeight(21),
                                                        child: FittedBox(
                                                          fit: BoxFit.fitHeight,
                                                          child: Text(
                                                            priceFrac,
                                                            style: TextStyle(
                                                              fontFamily: application
                                                                          .locale ==
                                                                      'ar'
                                                                  ? 'Cairo'
                                                                  : 'SF Pro Text',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: ScreenUtil()
                                                            .setHeight(25),
                                                        child: FittedBox(
                                                          fit: BoxFit.fitHeight,
                                                          child: Text(
                                                            package['currency'],
                                                            style: TextStyle(
                                                              fontFamily: application
                                                                          .locale ==
                                                                      'ar'
                                                                  ? 'Cairo'
                                                                  : 'SF Pro Text',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w800,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    );
                                  })?.toList(),
                                ),
                              );
                            });
                          },
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(20)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: _vehicleTypes
                            .asMap()
                            .map((int index, VehicleType vehicleType) =>
                                MapEntry(
                                    index,
                                    Row(
                                      children: <Widget>[
                                        CircleAvatar(
                                          radius: ScreenUtil().setWidth(
                                                  _page == index ? 11 : 9) /
                                              2,
                                          backgroundColor: _page == index
                                              ? Color(0xFF011F99)
                                              : Color(0xFF011F99)
                                                  .withOpacity(0.5),
                                        ),
                                        SizedBox(
                                            width: ScreenUtil().setHeight(
                                                index ==
                                                        _vehicleTypes.length - 1
                                                    ? 0
                                                    : 10)),
                                      ],
                                    )))
                            .values
                            .toList(),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(25)),
                IconButton(
                  onPressed: () =>
                      Navigator.pushNamed(context, '/packages/add'),
                  iconSize: ScreenUtil().setWidth(62),
                  icon: Image(
                    image: AssetImage('assets/images/new-order-icon.png'),
                    width: ScreenUtil().setWidth(62),
                    height: ScreenUtil().setWidth(62),
                  ),
                ),
              ],
            ),
          ),
          // Visibility(
          //   visible: _isDialogVisible,
          //   child: Positioned.fill(
          //     child: BackdropFilter(
          //       filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          //       child: Container(
          //         color: Colors.grey.withOpacity(0.2),
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }

  Future<void> _getPackages() async {
    List<Map<String, dynamic>> packages;

    final packagesDocuments = await Firestore.instance
        .collection('packages')
        .where('company_id', isEqualTo: _company.companyId)
        .getDocuments();
    if (packagesDocuments == null || packagesDocuments.documents.length == 0) {
      packages = [];
    } else {
      final types = {
        'sedan': VehicleType.sedan,
        'motorcycle': VehicleType.motorcycle,
        'suv': VehicleType.suv,
        'boat': VehicleType.boat,
      };

      packages =
          packagesDocuments.documents.map((DocumentSnapshot packageDocument) {
        final package = <String, dynamic>{
          'package_id': packageDocument.documentID,
          'company_id': packageDocument.data['company_id'],
          'title': packageDocument.data['title'],
          'services': List<String>.of(packageDocument.data['services']
                  .map((service) => "$service")
                  .toList()
                  .cast<String>() ??
              []),
          'price': (packageDocument.data['price'] as num).toDouble(),
          'old_price': packageDocument.data['old_price'] == null
              ? null
              : (packageDocument.data['old_price'] as num).toDouble(),
          'duration': packageDocument.data['duration'] == null
              ? null
              : (packageDocument.data['duration'] as num).toInt(),
          'currency': packageDocument.data['currency'],
          'vehicle_type':
              types[packageDocument.data['vehicle_type'].toString()],
        };

        return package;
      }).toList();
    }

    setState(() {
      _company.setData(packages: packages);
    });
  }

  Future<void> _deletePackage(Map<String, dynamic> package) async {
    await showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(S.of(context).confirmAction),
        content: Text(S.of(context).deletePackageConfirmation),
        actions: <Widget>[
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text(S.of(context).Cancel),
            onPressed: () => Navigator.of(context).pop(),
          ),
          CupertinoDialogAction(
            isDestructiveAction: true,
            child: Text(S.of(context).Delete),
            onPressed: () async {
              await Firestore.instance
                  .collection('packages')
                  .document(package['package_id'])
                  .updateData({
                'vehicle_type': 'removed',
              });

              final packages = _company.packages;
              final pkg = packages.firstWhere(
                  (p) => p['package_id'] == package['package_id'],
                  orElse: () => null);
              if (pkg != null) {
                pkg['vehicle_type'] = 'removed';
                _company.setData(packages: packages);
              }

              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}
