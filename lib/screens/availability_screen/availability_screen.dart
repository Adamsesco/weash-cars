import 'dart:math' as math;
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class AvailabilityScreen extends StatefulWidget {
  @override
  _AvailabilityScreenState createState() => _AvailabilityScreenState();
}

class _AvailabilityScreenState extends State<AvailabilityScreen>
    with SingleTickerProviderStateMixin {
  CompanyStore _company;

  PageController _pageController;
  double _page;

  DateTime _currentDate;
  DateTime _selectedDate;
  DateTime _pageCurrentDate;
  Map<int, Map<String, dynamic>> _schedule;
  Future<Map<String, Map<DateTime, bool>>> _availability;

  bool _isDialogVisible = false;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    _page = 3.0;
    _pageController =
        PageController(initialPage: _page.toInt(), viewportFraction: 1.0 / 5.0)
          ..addListener(() {
            if (_pageController.page != null) {
              setState(() {
                _page = _pageController.page;
              });
            }
          });

    final now = DateTime.now();
    _currentDate = DateTime(now.year, now.month, now.day);
    _selectedDate = _currentDate;
    _pageCurrentDate = _selectedDate;

    final applicationStore =
        Provider.of<ApplicationStore>(context, listen: false);
    _company = applicationStore.companyStore;
    _schedule = _company.schedule;

    _availability = _getAvailability(_company.companyId, _currentDate);
  }

  @override
  void dispose() {
    _pageController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      backgroundColor: Colors.white,
      drawer: Observer(
        builder: (BuildContext context) => CompanyDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _company.avatarUrl,
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            top: 0,
            child: Observer(
              builder: (BuildContext context) => CustomAppBar2(
                onOpenDrawer: () {
                  setState(() {
                    _isDialogVisible = true;
                    Scaffold.of(context).openDrawer();
                  });
                },
                title: S.of(context).dashboard,
                transparentBackground: true,
                titleColor: Color(0xFF91A6D9),
                buttonColor: Color(0xFF0930C3),
                nbNotifications: 0,
                avatar: _company.avatarUrl == null
                    ? null
                    : NetworkImage(_company.avatarUrl),
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().setHeight(144),
            bottom: 0,
            child: Column(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(32),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        S.of(context).availability,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Display',
                          fontSize: 24,
                          fontWeight: FontWeight.w900,
                          color: Color(0xFFA0AEE6),
                        ),
                      ),
                    ],
                  ),
                ),
                Spacer(flex: 2),
                Text(
                  DateFormat('MMMM').format(_pageCurrentDate).toUpperCase(),
                  style: TextStyle(
                    fontFamily:
                        application.locale == 'ar' ? 'Cairo' : 'SF Pro Display',
                    fontSize: 15,
                    fontWeight: FontWeight.w900,
                    letterSpacing: 0.40,
                    color: Color(0xFF021A89),
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(12)),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: ScreenUtil().setHeight(50),
                  padding: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(19),
                  ),
                  child: PageView.builder(
                    controller: _pageController,
                    scrollDirection: Axis.horizontal,
                    onPageChanged: (int page) {
                      final date = _currentDate
                          .subtract(Duration(days: 3))
                          .add(Duration(days: page));

                      setState(() {
                        _pageCurrentDate = date;
                      });
                    },
                    itemBuilder: (BuildContext context, int index) {
                      final date = _currentDate
                          .subtract(Duration(days: 3))
                          .add(Duration(days: index));

                      final isSelected = date == _selectedDate;
                      final isDisabled = date.isBefore(_currentDate);
                      final color = isSelected
                          ? Color(0xFF021A89)
                          : isDisabled ? Color(0xFFA0AEE6) : Color(0xFF004AFF);

                      var opacity = 1.0;
                      if (!isSelected) {
                        final distance = math.max(1, (index - _page).abs());
                        opacity = 1 / distance;
                      }

                      return Opacity(
                        opacity: opacity,
                        child: InkWell(
                          onTap: isDisabled
                              ? null
                              : () {
                                  _pageController.animateToPage(
                                    index,
                                    duration: Duration(milliseconds: 200),
                                    curve: Curves.linear,
                                  );
                                  setState(() {
                                    _selectedDate = date;
                                  });
                                },
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: 14,
                            ),
                            child: DefaultTextStyle(
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Display',
                                fontWeight: FontWeight.w900,
                                letterSpacing: 0.40,
                                color: color,
                              ),
                              child: Column(
                                children: <Widget>[
                                  Expanded(
                                    child: FittedBox(
                                      fit: BoxFit.fitHeight,
                                      child: Text(
                                        DateFormat('d', 'en').format(date),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: FittedBox(
                                      fit: BoxFit.fitHeight,
                                      child: Text(
                                        DateFormat('EE')
                                            .format(date)
                                            .toUpperCase(),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    height: 2,
                                    decoration: BoxDecoration(
                                      color: isSelected
                                          ? color
                                          : Colors.transparent,
                                      borderRadius: BorderRadius.circular(3),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                Spacer(flex: 2),
                FutureBuilder<Map<String, Map<DateTime, bool>>>(
                    future: _availability,
                    builder: (BuildContext context,
                        AsyncSnapshot<Map<String, Map<DateTime, bool>>>
                            snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.waiting:
                        case ConnectionState.active:
                          return CupertinoActivityIndicator();

                        case ConnectionState.none:
                          return Container();

                        case ConnectionState.done:
                          if (!snapshot.hasData || snapshot.hasError) {
                            return Container();
                          }

                          final availability = snapshot.data;
                          final hoursAvailability = availability['hours'] ?? {};
                          final daysAvailability = availability['days'] ?? {};

                          final weekday = _selectedDate.weekday - 1;
                          final isDayOff =
                              _schedule[weekday]['is_day_off'] as bool;

                          return Expanded(
                            flex: 24,
                            child: Stack(
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Builder(
                                          builder: (BuildContext context) {
                                            final dayAvailability =
                                                daysAvailability[_selectedDate];

                                            return Transform.scale(
                                              scale: 0.7,
                                              child: CupertinoSwitch(
                                                value: dayAvailability ?? false,
                                                onChanged: (bool value) {
                                                  _setDayOff(
                                                      _selectedDate, value);
                                                },
                                              ),
                                            );
                                          },
                                        ),
                                        SizedBox(
                                            width: ScreenUtil().setWidth(14.5)),
                                        Text(
                                          S.of(context).makeWholeDayOff,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Display',
                                            fontSize: 15,
                                            fontWeight: FontWeight.w900,
                                            letterSpacing: 0.40,
                                            color: Color(0xFF021A89),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Spacer(flex: 1),
                                    Expanded(
                                      flex: 22,
                                      child: SizedBox(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Builder(builder: (context) {
                                          final weekday =
                                              _selectedDate.weekday - 1;
                                          final from = (_schedule[weekday]
                                                  ['from'] as int) ~/
                                              100;
                                          final to = (_schedule[weekday]['to']
                                                  as int) ~/
                                              100;

                                          return ListView.separated(
                                            padding: EdgeInsets.zero,
                                            itemCount: to - from + 2,
                                            separatorBuilder:
                                                (BuildContext context,
                                                    int index) {
                                              return Container(
                                                height: 0.3,
                                                color: Color(0xFFA0AEE6),
                                              );
                                            },
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              if (index == 0 || index == 25) {
                                                return Container();
                                              }

                                              final hour =
                                                  (index - 1 + from) % 24;
                                              final nextHour = (hour + 1) % 24;
                                              final date = DateTime(
                                                  _selectedDate.year,
                                                  _selectedDate.month,
                                                  _selectedDate.day,
                                                  hour);
                                              final dayAvailability =
                                                  daysAvailability[
                                                          _selectedDate] ??
                                                      false;
                                              final hourAvailability =
                                                  !dayAvailability &&
                                                      (hoursAvailability[
                                                              date] ??
                                                          true);

                                              return InkWell(
                                                onTap: !dayAvailability
                                                    ? () {
                                                        _setHourOff(date,
                                                            !hourAvailability);
                                                      }
                                                    : null,
                                                child: Padding(
                                                  padding: EdgeInsets.symmetric(
                                                    horizontal: ScreenUtil()
                                                        .setWidth(57),
                                                    vertical: ScreenUtil()
                                                        .setHeight(13.5),
                                                  ),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: <Widget>[
                                                      Row(
                                                        children: <Widget>[
                                                          Text(
                                                            "$hour:00",
                                                            style: TextStyle(
                                                              fontFamily:
                                                                  'SF Compact Display',
                                                              fontSize: 13,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              letterSpacing:
                                                                  0.50,
                                                              color: Color(
                                                                  0xFF021A89),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                              width:
                                                                  ScreenUtil()
                                                                      .setWidth(
                                                                          6)),
                                                          Image(
                                                            image: AssetImage(
                                                                'assets/images/arrow-icon.png'),
                                                            width: ScreenUtil()
                                                                .setWidth(8.6),
                                                            height: ScreenUtil()
                                                                .setHeight(
                                                                    6.01),
                                                            matchTextDirection:
                                                                true,
                                                          ),
                                                          SizedBox(
                                                              width:
                                                                  ScreenUtil()
                                                                      .setWidth(
                                                                          6)),
                                                          Text(
                                                            "$nextHour:00",
                                                            style: TextStyle(
                                                              fontFamily:
                                                                  'SF Compact Display',
                                                              fontSize: 13,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              letterSpacing:
                                                                  0.50,
                                                              color: Color(
                                                                  0xFF021A89),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      Text(
                                                        hourAvailability
                                                            ? S
                                                                .of(context)
                                                                .available
                                                            : S
                                                                .of(context)
                                                                .unavailable,
                                                        style: TextStyle(
                                                          fontFamily: application
                                                                      .locale ==
                                                                  'ar'
                                                              ? 'Cairo'
                                                              : 'SF Compact Display',
                                                          fontSize: 13,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          letterSpacing: 0.50,
                                                          color: hourAvailability
                                                              ? Color(
                                                                  0xFF02AC1C)
                                                              : Color(
                                                                  0xFFA0AEE6),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            },
                                          );
                                        }),
                                      ),
                                    ),
                                  ],
                                ),
                                if (isDayOff) ...[
                                  Positioned.fill(
                                    child: ClipRect(
                                      child: BackdropFilter(
                                        filter: ImageFilter.blur(
                                            sigmaX: 5, sigmaY: 5),
                                        child: Container(
                                          color: Colors.white.withOpacity(0.4),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Positioned.fill(
                                    child: Container(
                                      margin: EdgeInsets.only(
                                        bottom: ScreenUtil().setHeight(80),
                                      ),
                                      padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(20),
                                      ),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            S.of(context).dayOff,
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Display',
                                              fontSize: 24,
                                              fontWeight: FontWeight.w900,
                                              letterSpacing: 0.40,
                                              color: Color(0xFFA0AEE6),
                                            ),
                                          ),
                                          SizedBox(
                                              height:
                                                  ScreenUtil().setHeight(10)),
                                          Text(
                                            S.of(context).youCanChangeHours,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Display',
                                              fontSize: 14,
                                              fontWeight: FontWeight.w600,
                                              letterSpacing: 0.40,
                                              color:
                                                  Colors.black.withOpacity(0.6),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ],
                            ),
                          );
                      }

                      return Container();
                    }),
                Spacer(flex: 1),
                InkWell(
                  onTap: _save,
                  borderRadius: BorderRadius.circular(20),
                  child: Container(
                    width: ScreenUtil().setWidth(170.16),
                    height: ScreenUtil().setHeight(40.2),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      gradient: LinearGradient(
                        colors: <Color>[Color(0xff031C8D), Color(0xff0A2FAF)],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                      ),
                    ),
                    child: _isLoading
                        ? CupertinoActivityIndicator(
                            animating: true,
                          )
                        : Text(
                            S.of(context).saveChanges,
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF UI Display',
                              fontFamilyFallback: <String>['SF Pro Text'],
                              fontSize: ScreenUtil().setSp(11),
                              fontWeight: FontWeight.w700,
                              color: Color(0xfffcfcfc),
                            ),
                          ),
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(30)),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<Map<String, Map<DateTime, bool>>> _getAvailability(
      String companyId, DateTime currentDate) async {
    Map<String, Map<DateTime, bool>> availability = {'hours': {}, 'days': {}};

    final hoursQuerySnapshot = await Firestore.instance
        .collection('company_availability')
        .where('company_id', isEqualTo: companyId)
        .where('date', isGreaterThanOrEqualTo: currentDate.toUtc())
        .getDocuments();

    if (hoursQuerySnapshot != null && hoursQuerySnapshot.documents.isNotEmpty) {
      hoursQuerySnapshot.documents.forEach((DocumentSnapshot document) {
        final type = document.data['type'] as String;
        final date = (document.data['date'] as Timestamp).toDate();

        switch (type) {
          case 'day':
            final dayAvailability = document.data['is_day_off'] as bool;
            availability['days'][date] = dayAvailability;
            break;

          case 'hour':
            final hourAvailability = document.data['is_available'] as bool;
            availability['hours'][date] = hourAvailability;
            break;
        }
      });
    }

    return availability;
  }

  Future<void> _setDayOff(DateTime date, bool value) async {
    final availablity = await _availability;

    setState(() {
      availablity['days'][date] = value;
    });
  }

  Future<void> _setHourOff(DateTime date, bool value) async {
    final availablity = await _availability;

    setState(() {
      availablity['hours'][date] = value;
    });
  }

  Future<void> _save() async {
    setState(() {
      _isLoading = true;
    });

    final availability = await _availability;
    final daysAvailability = availability['days'];
    final hoursAvailability = availability['hours'];

    final collectionRef = Firestore.instance.collection('company_availability');

    await Future.forEach(daysAvailability.keys, (DateTime date) async {
      final value = daysAvailability[date];
      final documentId = "${_company.companyId}-${date.toUtc().toString()}-day";
      final document = collectionRef.document(documentId);

      final documentRef = await collectionRef.document(documentId).get();
      if (documentRef.exists) {
        await document.updateData({
          'is_day_off': value,
        });
      } else {
        await document.setData({
          'company_id': _company.companyId,
          'type': 'day',
          'date': date.toUtc(),
          'is_day_off': value,
        });
      }
    });

    await Future.forEach(hoursAvailability.keys, (DateTime date) async {
      final value = hoursAvailability[date];
      final documentId =
          "${_company.companyId}-${date.toUtc().toString()}-hour";
      final document = collectionRef.document(documentId);

      final documentRef = await collectionRef.document(documentId).get();
      if (documentRef.exists) {
        await document.updateData({
          'is_available': value,
        });
      } else {
        await document.setData({
          'company_id': _company.companyId,
          'type': 'hour',
          'date': date.toUtc(),
          'is_available': value,
        });
      }
    });

    setState(() {
      _isLoading = false;
    });

    showCupertinoDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(S.of(context).success),
        content: Text(S.of(context).changesSaved),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () => Navigator.of(context).pop(),
            child: Text(S.of(context).ok),
          ),
        ],
      ),
    );
  }
}
