import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/screens/home_screen/home_screen.dart';
import 'package:weash_cars/stores/application_store.dart';

class LanguageScreen extends StatefulWidget {
  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  ApplicationStore _application;

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    _application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/splash_sceen_background.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          children: <Widget>[
            Image(
              image: AssetImage('assets/images/logo.png'),
            ),
            Positioned(
              bottom: ScreenUtil().setHeight(139),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      _application.setLocale('ar');
                      _application.localeStream.sink.add('ar');
                      Navigator.pushNamed(context, '/intro');
                    },
                    enableFeedback: false,
                    child: Container(
                      width: ScreenUtil().setWidth(170.16),
                      height: ScreenUtil().setHeight(40.2),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        gradient: LinearGradient(
                          colors: [Color(0xFF0026A1), Color(0xFF000F66)],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.4),
                            offset: Offset(0, ScreenUtil().setHeight(3)),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                      child: Text(
                        'العربية',
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'Cairo',
                          fontFamilyFallback: <String>['SF Pro Text'],
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(17.8)),
                  InkWell(
                    onTap: () {
                      _application.setLocale('en');
                      _application.localeStream.sink.add('en');
                      Navigator.pushNamed(context, '/intro');
                    },
                    enableFeedback: false,
                    child: Container(
                      width: ScreenUtil().setWidth(170.16),
                      height: ScreenUtil().setHeight(40.2),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        gradient: LinearGradient(
                          colors: [Color(0xFF0026A1), Color(0xFF000F66)],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.4),
                            offset: Offset(0, ScreenUtil().setHeight(3)),
                            blurRadius: 6,
                          ),
                        ],
                      ),
                      child: Text(
                        'ENGLISH',
                        style: TextStyle(
                          fontFamily: 'SF Pro Text',
                          fontSize: 11,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
