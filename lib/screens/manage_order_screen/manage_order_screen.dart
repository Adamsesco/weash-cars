import 'dart:async';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/util/watermark_image.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class ManageOrderScreen extends StatefulWidget {
  final Map<String, dynamic> order;

  ManageOrderScreen({Key key, this.order}) : super(key: key);

  @override
  _ManageOrderScreenState createState() => _ManageOrderScreenState();
}

class _ManageOrderScreenState extends State<ManageOrderScreen> {
  CompanyStore _company;

  List<File> _imageFiles = [null, null, null, null];
  List<StorageUploadTask> _uploadTasks = [null, null, null, null];
  List<StorageTaskSnapshot> _imageSnapshots = [null, null, null, null];
  List<String> _imageDownloadUrls = [null, null, null, null];
  List<double> _uploadProgress = [0, 0, 0, 0];

  bool _isDialogVisible = false;
  bool _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_company == null) {
      final applicationStore = Provider.of<ApplicationStore>(context);
      _company = applicationStore.companyStore;
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CompanyDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _company.avatarUrl,
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image(
                image:
                    AssetImage('assets/images/company-screens-background.png'),
                matchTextDirection: true,
                fit: BoxFit.fill),
          ),
          Positioned(
            top: 0,
            child: Observer(
              builder: (BuildContext context) => CustomAppBar2(
                onOpenDrawer: () {
                  setState(() {
                    _isDialogVisible = true;
                    Scaffold.of(context).openDrawer();
                  });
                },
                title: S.of(context).dashboard,
                transparentBackground: true,
                titleColor: Color(0xFF91A6D9),
                buttonColor: Color(0xFF0930C3),
                nbNotifications: 0,
                avatar: _company.avatarUrl == null
                    ? null
                    : NetworkImage(_company.avatarUrl),
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().setHeight(159),
            bottom: 0,
            left: ScreenUtil().setWidth(20.7),
            right: ScreenUtil().setWidth(20.7),
            child: Builder(
              builder: (BuildContext context) {
                final order = widget.order;

                if (order == null) {
                  return CupertinoActivityIndicator(
                    animating: true,
                  );
                }

                final orderId = order['order_id'];
                final orderStatus = order['status'];

                final assignedEmployeeId = order['assigned_employee_id'];
                final assignedEmployee = _company.employees.firstWhere(
                    (Map<String, dynamic> employee) =>
                        employee['employee_id'] == assignedEmployeeId,
                    orElse: () {
                  print("No result");
                  return null;
                });
                String avatarUrl = ' ';
                String firstName = ' ';
                String lastName = ' ';
                int jobsDoneCount = 0;
                if (assignedEmployee != null) {
                  avatarUrl = assignedEmployee['avatar_url'];
                  firstName = assignedEmployee['first_name'];
                  lastName = assignedEmployee['last_name'];
                  jobsDoneCount = assignedEmployee['jobs_done'] as int;
                }

                final statusButtonIcons = {
                  10: 'assigned-status-button.png',
                  11: 'on-the-way-status-button.png',
                  12: 'started-working-status-button.png',
                  20: 'done-status-button.png',
                  21: 'done-status-button.png',
                };
                final statusDescriptions = {
                  10: S.of(context).Employeeassigned,
                  11: S.of(context).onTheWay,
                  12: S.of(context).workingOn,
                  20: S.of(context).done,
                  21: S.of(context).done,
                };
                final statusButtonIcon = statusButtonIcons[orderStatus];
                final statusDescription = statusDescriptions[orderStatus];

                return ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Text(
                        S.of(context).AssignedEmployee.toUpperCase(),
                        style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Display',
                            fontSize: 13,
                            fontWeight: FontWeight.w900,
                            color: Color(0xFFA0AEE6)),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(16)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: ScreenUtil().setWidth(272),
                          height: ScreenUtil().setHeight(52),
                          margin: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(7),
                            // vertical: ScreenUtil().setHeight(3),
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(16),
                            // vertical: ScreenUtil().setHeight(5),
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                color: Color(0xFF939393).withOpacity(0.25),
                                blurRadius: 30,
                              ),
                            ],
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: ScreenUtil().setHeight(41),
                                height: ScreenUtil().setHeight(41),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: avatarUrl == null
                                        ? AssetImage(
                                            'assets/images/avatar-user.png')
                                        : NetworkImage(avatarUrl),
                                  ),
                                ),
                              ),
                              SizedBox(width: ScreenUtil().setWidth(43)),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      firstName == null
                                          ? ' '
                                          : "$firstName $lastName",
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'Helvetica Rounded LT',
                                        fontSize: 13,
                                        fontWeight: FontWeight.w700,
                                        color: Color(0xFF021A89),
                                      ),
                                    ),
                                    Text(
                                      jobsDoneCount == null
                                          ? ' '
                                          : "$jobsDoneCount ${S.of(context).jobsDone}",
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'SF Compact Rounded',
                                        fontSize: 10,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xFF021A89),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Image(
                                image: AssetImage(
                                    'assets/images/checkmark-icon.png'),
                                width: ScreenUtil().setWidth(10.71),
                                height: ScreenUtil().setHeight(7.05),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(52)),
                    AnimatedSwitcher(
                      duration: const Duration(milliseconds: 500),
                      transitionBuilder:
                          (Widget child, Animation<double> animation) {
                        return FadeTransition(
                          child: child,
                          opacity: animation,
                        );
                      },
                      child: Column(
                        key: ValueKey<int>(orderStatus),
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: SizedBox(
                              width: ScreenUtil().setWidth(189.51),
                              child: Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  Image(
                                    image: AssetImage(
                                        "assets/images/$statusButtonIcon"),
                                    width: ScreenUtil().setWidth(189.51),
                                    height: ScreenUtil().setHeight(188.78),
                                  ),
                                  Positioned(
                                    top: 0,
                                    left: 0,
                                    child: InkWell(
                                      onTap: () {
                                        const nextStatus = 11;
                                        _updateOrderStatus(
                                            order, orderId, nextStatus);
                                      },
                                      child: Container(
                                        width: ScreenUtil().setWidth(89.08),
                                        height: ScreenUtil().setHeight(128.45),
                                        // color: Colors.orange.withOpacity(0.20),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    top: 0,
                                    left: ScreenUtil().setWidth(89.08 + 11.3),
                                    child: InkWell(
                                      onTap: () {
                                        const nextStatus = 12;
                                        _updateOrderStatus(
                                            order, orderId, nextStatus);
                                      },
                                      child: Container(
                                        width: ScreenUtil().setWidth(89.08),
                                        height: ScreenUtil().setHeight(128.45),
                                        // color: Colors.blue.withOpacity(0.20),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    top: ScreenUtil().setHeight(128.45 + 9),
                                    child: InkWell(
                                      onTap: () {
                                        const nextStatus = 20;
                                        _updateOrderStatus(
                                            order, orderId, nextStatus);
                                      },
                                      child: Container(
                                        width: ScreenUtil().setWidth(159.94),
                                        height: ScreenUtil().setHeight(51.33),
                                        // color: Colors.green.withOpacity(0.20),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    child: InkWell(
                                      onTap: () {
                                        if (orderStatus < 20) {
                                          int nextStatus;

                                          switch (orderStatus) {
                                            case 10:
                                              nextStatus = 11;
                                              break;
                                            case 11:
                                              nextStatus = 12;
                                              break;
                                            case 12:
                                              nextStatus = 20;
                                              break;
                                          }

                                          _updateOrderStatus(
                                              order, orderId, nextStatus);
                                        }
                                      },
                                      child: Container(
                                        width: ScreenUtil().setWidth(80),
                                        height: ScreenUtil().setHeight(70),
                                        // color: Colors.black.withOpacity(0.20),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: ScreenUtil().setHeight(25.2)),
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              statusDescription,
                              style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'Montserrat',
                                  fontSize: 22,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xFFA0AEE6)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(42)),
                    Text(
                      S.of(context).uploadVehiclePictures,
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Display',
                        fontSize: 15,
                        fontWeight: FontWeight.w900,
                        color: Color(0xFFA0AEE6),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(12)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        for (var i = 0; i < 4; i++)
                          _buildPictureCard(i, assignedEmployeeId),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(28.3)),
                    Align(
                      alignment: Alignment.center,
                      child: InkWell(
                        enableFeedback: false,
                        onTap: () async {
                          setState(() {
                            _isLoading = true;
                          });

                          await Firestore.instance
                              .collection('orders')
                              .document(orderId)
                              .updateData({
                            'pictures_urls': _imageDownloadUrls,
                          });

                          setState(() {
                            _isLoading = false;
                          });
                          Navigator.pushNamed(context, '/dashboard/company');
                        },
                        child: Container(
                          width: ScreenUtil().setWidth(170.16),
                          height: ScreenUtil().setHeight(40.2),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Color(0xFFDFDFDF),
                            borderRadius: BorderRadius.circular(20),
                            gradient: orderStatus >= 20
                                ? LinearGradient(
                                    colors: [
                                      Color(0xFF0026A1),
                                      Color(0xFF000F66)
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  )
                                : null,
                          ),
                          child: Text(
                            S.of(context).endOrder,
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: 11,
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(28.3)),
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _updateOrderStatus(
      Map<String, dynamic> order, String orderId, int nextStatus) async {
    setState(() {
      order['status'] = nextStatus;
    });

    await Firestore.instance.collection('orders').document(orderId).updateData({
      'status': nextStatus,
    });
  }

  Widget _buildPictureCard(int index, String employeeId) {
    final application = Provider.of<ApplicationStore>(context);

    final imageFile = _imageFiles[index];
    final imageSnapshot = _imageSnapshots[index];
    final uploadTask = _uploadTasks[index];

    return InkWell(
      onTap: () {
        if (imageFile == null) {
          _pickImage(index, employeeId);
        }
      },
      child: Stack(
        children: <Widget>[
          Container(
            width: ScreenUtil().setWidth(72.75),
            height: ScreenUtil().setHeight(72.75),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: imageFile == null
                  ? DecorationImage(
                      image: AssetImage('assets/images/dashed-border.png'),
                      fit: BoxFit.fill,
                    )
                  : DecorationImage(
                      image: FileImage(imageFile),
                      fit: BoxFit.cover,
                    ),
            ),
            child: imageFile == null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setHeight(16.2)),
                      uploadTask == null
                          ? Image(
                              image: AssetImage(
                                  'assets/images/add-picture-icon.png'),
                              // width: ScreenUtil().setWidth(28.51),
                              height: ScreenUtil().setHeight(31.52),
                              matchTextDirection: true,
                            )
                          : Image(
                              image: AssetImage(
                                  'assets/images/uploading-icon.png'),
                              // width: ScreenUtil().setWidth(28.51),
                              height: ScreenUtil().setHeight(31.52),
                              matchTextDirection: true,
                            ),
                      SizedBox(height: ScreenUtil().setHeight(11.6)),
                      Visibility(
                        visible: uploadTask != null,
                        maintainSize: true,
                        maintainAnimation: true,
                        maintainState: true,
                        child: RotatedBox(
                          quarterTurns: application.locale == 'ar' ? 2 : 0,
                          child: Container(
                            width: ScreenUtil().setWidth(54.24),
                            height: ScreenUtil().setHeight(2.24),
                            alignment: Alignment.topLeft,
                            decoration: BoxDecoration(
                              color: Color(0xFFEFEFEF),
                              borderRadius: BorderRadius.circular(2.5),
                            ),
                            child: FractionallySizedBox(
                              widthFactor: _uploadProgress[index],
                              child: Container(
                                height: ScreenUtil().setHeight(2.24),
                                decoration: BoxDecoration(
                                  color: Color(0xFF07D970),
                                  borderRadius: BorderRadius.circular(2.5),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(11)),
                    ],
                  )
                : Container(),
          ),
          Visibility(
            visible: _uploadProgress[index] == 1.0,
            child: Positioned(
              right: 0,
              child: Transform.translate(
                offset: Offset(ScreenUtil().setWidth(24 / 3),
                    -ScreenUtil().setWidth(24 / 3)),
                child: InkWell(
                  onTap: () {
                    // TODO: delete image from firebase storage
                    setState(() {
                      _imageFiles[index] = null;
                      _uploadTasks[index] = null;
                      _imageSnapshots[index] = null;
                      _imageDownloadUrls[index] = null;
                      _uploadProgress[index] = 0;
                    });
                  },
                  child: Image(
                    image: AssetImage('assets/images/remove-picture-icon.png'),
                    width: ScreenUtil().setWidth(24),
                    height: ScreenUtil().setWidth(24),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _pickImage(int index, String employeeId) async {
    final imageFile = await showCupertinoModalPopup<File>(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        title: Text(S.of(context).changeProfilePicture),
        actions: <Widget>[
          CupertinoActionSheetAction(
            onPressed: () async {
              final file =
                  await ImagePicker.pickImage(source: ImageSource.camera);
              Navigator.of(context).pop(file);
            },
            child: Text(S.of(context).takeNewPhoto),
          ),
          CupertinoActionSheetAction(
            onPressed: () async {
              final file =
                  await ImagePicker.pickImage(source: ImageSource.gallery);
              Navigator.of(context).pop(file);
            },
            child: Text(S.of(context).chooseExistingPhoto),
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(
            S.of(context).Cancel,
            style: TextStyle(
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
    );

    if (imageFile != null) {
      final data = await imageFile.readAsBytes();
      final completer = new Completer<ui.Image>();

      ui.decodeImageFromList(data, (ui.Image image) {
        completer.complete(image);
      });

      final image = await completer.future;
      final watermarkedImage = new WatermarkImage(
          image: image, waterMark: Text('test'), waterMarkOffset: Offset(0, 0));
      final byteData = await watermarkedImage.byteData;

      final directory = await getApplicationDocumentsDirectory();
      final path = directory.path;
      final file = new File("$path/${_timestamp()}.png");
      await file.writeAsBytes(byteData);

      await _uploadPicture(file, index, employeeId);
      setState(() {
        _imageFiles[index] = imageFile;
      });
    }
  }

  Future<void> _uploadPicture(
      File picture, int index, String employeeId) async {
    // TODO: make picture accessible only for the employee and the customer.
    if (picture == null) return null;

    final filename = "$employeeId.${_timestamp()}";
    final extension = picture.path
        .substring(picture.path.lastIndexOf('.'))
        .replaceAll('.', '');
    final storageRef = FirebaseStorage.instance
        .ref()
        .child('company_images_gallery')
        .child("$filename.$extension");

    final StorageUploadTask uploadTask = storageRef.putFile(
      picture,
      StorageMetadata(
        contentType: "image/$extension",
        customMetadata: <String, String>{
          'type': 'company_images_gallery',
          'user_id': employeeId,
        },
      ),
    );
    setState(() {
      _uploadTasks[index] = uploadTask;
    });
    uploadTask.events.listen((event) {
      setState(() {
        _uploadProgress[index] = event.snapshot.bytesTransferred.toDouble() /
            event.snapshot.totalByteCount.toDouble();
      });
    }).onError((error) {
      print(error.toString());
    });

    final downloadUrl = await uploadTask.onComplete;
    final pictureUrl = await downloadUrl.ref.getDownloadURL();
    setState(() {
      _imageSnapshots[index] = downloadUrl;
      _imageDownloadUrls[index] = pictureUrl;
    });
    // return pictureUrl;
  }

  String _timestamp() => DateTime.now().millisecondsSinceEpoch.toString();
}
