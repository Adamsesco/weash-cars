import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/customer_store.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';
import 'package:weash_cars/widgets/customer_drawer.dart';

class WorkingOnScreen extends StatefulWidget {
  @override
  _WorkingOnScreenState createState() => _WorkingOnScreenState();
}

class _WorkingOnScreenState extends State<WorkingOnScreen> {
  CustomerStore _customer;
  bool _isDialogVisible = false;

  @override
  void didChangeDependencies() {
    final application = Provider.of<ApplicationStore>(context);
    _customer = application.customerStore;

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CustomerDrawer(
              onCloseDrawer: () {
                setState(() => _isDialogVisible = false);
              },
              avatarUrl: _customer.avatarUrl,
            ),
      ),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Image(
              image: AssetImage('assets/images/working-on-background.png'),
              fit: BoxFit.cover,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Builder(
                builder: (BuildContext context) => CustomAppBar2(
                      onOpenDrawer: () {
                        setState(() {
                          _isDialogVisible = true;
                          Scaffold.of(context).openDrawer();
                        });
                      },
                      transparentBackground: true,
                      title: S.of(context).trackingOrder,
                      nbNotifications: 0,
                      avatar: _customer.avatarUrl == null
                          ? null
                          : NetworkImage(_customer.avatarUrl),
                    ),
              ),
              SizedBox(height: ScreenUtil().setHeight(118.5)),
              SizedBox(
                height: ScreenUtil().setHeight(33),
                child: FittedBox(
                  fit: BoxFit.fitHeight,
                  child: Text(
                    S.of(context).workingOn,
                    style: TextStyle(
                      fontFamily: application.locale == 'ar' ? 'Cairo' : 'Montserrat',
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(63)),
              Image(
                image: AssetImage('assets/images/working-on-icon.png'),
                width: ScreenUtil().setWidth(212.31),
                height: ScreenUtil().setWidth(212.31),
              ),
              SizedBox(height: ScreenUtil().setHeight(76.5)),
              SizedBox(
                height: ScreenUtil().setHeight(50),
                child: FittedBox(
                  fit: BoxFit.fitHeight,
                  child: Text(
                    S.of(context).vehicleBeingCleaned,
                    style: TextStyle(
                      fontFamily: application.locale == 'ar' ? 'Cairo' : 'Montserrat',
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(26)),
              SizedBox(
                height: ScreenUtil().setHeight(18),
                child: FittedBox(
                  fit: BoxFit.fitHeight,
                  child: Text(
                    "“${S.of(context).thankForPatience}”",
                    style: TextStyle(
                      fontFamily: application.locale == 'ar' ? 'Cairo' : 'Montserrat',
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Visibility(
            visible: _isDialogVisible,
            child: Positioned.fill(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                child: Container(
                  color: Colors.grey.withOpacity(0.2),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
