import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/customer_store.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';
import 'package:weash_cars/widgets/customer_drawer.dart';
import 'package:weash_cars/widgets/full_screen_image_view.dart';

class DoneScreen extends StatefulWidget {
  final String orderId;

  DoneScreen({Key key, this.orderId}) : super(key: key);

  @override
  _DoneScreenState createState() => _DoneScreenState();
}

class _DoneScreenState extends State<DoneScreen> {
  CustomerStore _customer;
  Future<List<Map<String, dynamic>>> _orders;
  bool _isDialogVisible = false;

  @override
  void didChangeDependencies() {
    final application = Provider.of<ApplicationStore>(context);
    _customer = application.customerStore;
    _orders = _customer.orders.ordersFuture;

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CustomerDrawer(
              onCloseDrawer: () {
                setState(() => _isDialogVisible = false);
              },
              avatarUrl: _customer.avatarUrl,
            ),
      ),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Image(
              image: AssetImage('assets/images/done-background.png'),
              fit: BoxFit.cover,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Builder(
                builder: (BuildContext context) => CustomAppBar2(
                      onOpenDrawer: () {
                        setState(() {
                          _isDialogVisible = true;
                          Scaffold.of(context).openDrawer();
                        });
                      },
                      transparentBackground: true,
                      title: S.of(context).trackingOrder,
                      nbNotifications: 0,
                      avatar: _customer.avatarUrl == null
                          ? null
                          : NetworkImage(_customer.avatarUrl),
                    ),
              ),
              Spacer(),
              Text(
                S.of(context).done,
                style: TextStyle(
                  fontFamily:
                      application.locale == 'ar' ? 'Cairo' : 'Montserrat',
                  fontSize: 27,
                  fontWeight: FontWeight.w700,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(32)),
              Image(
                image: AssetImage('assets/images/done-icon.png'),
                width: ScreenUtil().setWidth(189.51),
                height: ScreenUtil().setWidth(189.51),
              ),
              SizedBox(height: ScreenUtil().setHeight(33.2)),
              Text(
                S.of(context).vehicleIsClean,
                style: TextStyle(
                  fontFamily:
                      application.locale == 'ar' ? 'Cairo' : 'Montserrat',
                  fontSize: 19,
                  fontWeight: FontWeight.w700,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(33)),
              FutureBuilder<List<Map<String, dynamic>>>(
                future: _orders,
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                    case ConnectionState.active:
                      return CupertinoActivityIndicator(
                        animating: true,
                      );
                    case ConnectionState.none:
                      return Container();
                    case ConnectionState.done:
                      if (snapshot.hasError) {
                        return Container();
                      }

                      final orders = snapshot.data;
                      final order = orders.firstWhere(
                          (Map<String, dynamic> o) =>
                              o['order_id'] == widget.orderId,
                          orElse: () => null);

                      if (order == null) {
                        return Container();
                      }

                      final orderStatus = order['status'];
                      final picturesUrls = order['pictures_urls'] == null
                          ? <String>[]
                          : List<String>.from(order['pictures_urls'])
                              .where((url) => url != null)
                              .toList();

                      return Column(
                        children: <Widget>[
                          SizedBox(
                            width: ScreenUtil().setWidth(295.8),
                            height: ScreenUtil().setHeight(93.01),
                            child: ListView.separated(
                              scrollDirection: Axis.horizontal,
                              itemCount: picturesUrls.length,
                              separatorBuilder: (BuildContext context,
                                      int index) =>
                                  SizedBox(width: ScreenUtil().setWidth(8.4)),
                              itemBuilder: (BuildContext context, int index) {
                                final imageProvider =
                                    CachedNetworkImageProvider(
                                  picturesUrls[index],
                                  // 'http://placekitten.com/700/400'
                                );

                                return InkWell(
                                  onTap: () => _showFullScreenImage(
                                      imageProvider, picturesUrls[index]),
                                  enableFeedback: false,
                                  child: Container(
                                    width: ScreenUtil().setWidth(93.01),
                                    height: ScreenUtil().setHeight(93.01),
                                    alignment: Alignment.center,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Image(
                                        image: imageProvider,
                                        width: ScreenUtil().setWidth(93.01),
                                        height: ScreenUtil().setHeight(93.01),
                                        fit: BoxFit.cover,
                                        frameBuilder: (BuildContext context,
                                            Widget child,
                                            int frame,
                                            bool wasSyncronouslyLoaded) {
                                          if (frame != null) {
                                            return child;
                                          }

                                          return CupertinoActivityIndicator();
                                        },
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                          SizedBox(height: ScreenUtil().setHeight(38)),
                          Visibility(
                            visible: orderStatus == 20,
                            child: Column(
                              children: <Widget>[
                                Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: _confirm,
                                    borderRadius: BorderRadius.circular(20),
                                    child: Container(
                                      width: ScreenUtil().setWidth(110.26),
                                      height: ScreenUtil().setHeight(40.2),
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: Color(0x120d2e00)),
                                        borderRadius: BorderRadius.circular(20),
                                        color: Colors.white,
                                        boxShadow: <BoxShadow>[
                                          BoxShadow(
                                            color: Color(0x66000000),
                                            offset: Offset(
                                                0, ScreenUtil().setHeight(3)),
                                          ),
                                        ],
                                      ),
                                      child: Center(
                                        child: Text(
                                          S.of(context).confirm,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF UI Display',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: ScreenUtil().setSp(11),
                                            fontWeight: FontWeight.w700,
                                            color: Color(0xff011e96),
                                            letterSpacing: 0.22,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(20)),
                              ],
                            ),
                          ),
                        ],
                      );
                  }

                  return Container();
                },
              ),
            ],
          ),
          Visibility(
            visible: _isDialogVisible,
            child: Positioned.fill(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                child: Container(
                  color: Colors.grey.withOpacity(0.2),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _showFullScreenImage(ImageProvider image, String imageUrl) {
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: '',
      transitionDuration: Duration(milliseconds: 200),
      pageBuilder: (BuildContext context, Animation<double> animation1,
          Animation<double> animation2) {
        return ValueListenableBuilder(
          valueListenable: animation1,
          builder: (BuildContext context, double animationValue, _) =>
              FullScreenImageView(
                imageProvider: image,
                imageUrl: imageUrl,
                animationValue: animationValue,
              ),
        );
      },
    );
  }

  Future<void> _confirm() async {
    await Firestore.instance
        .collection('orders')
        .document(widget.orderId)
        .updateData({
      'status': 21,
    });

    await showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
            title: Text(S.of(context).confirmed),
            content: Text(S.of(context).orderWasConfirmed),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text(S.of(context).ok),
                onPressed: () =>
                    Navigator.pushNamed(context, '/dashboard/customer'),
              ),
            ],
          ),
    );

    Navigator.pushNamed(context, '/dashboard/customer');
  }
}
