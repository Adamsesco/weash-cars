import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/payment_method.dart';
import 'package:weash_cars/models/vehicle_type.dart';
import 'package:weash_cars/stores/order_store.dart';

import 'package:weash_cars/widgets/custom_app_bar.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/customer_store.dart';
import 'package:weash_cars/widgets/customer_drawer.dart';

class OrderSummary extends StatefulWidget {
  @override
  _OrderSummaryState createState() => _OrderSummaryState();
}

class _OrderSummaryState extends State<OrderSummary> {
  CustomerStore _customer;
  OrderStore _order;
  String _vehicleType;
  String _dateTime;
  String _companyName;
  String _packageTitle;
  String _duration;
  PaymentMethod _selectedPaymentMethod = PaymentMethod.cash;

  bool _isDialogVisible = false;

  @override
  void didChangeDependencies() {
    if (_customer == null) {
      final application = Provider.of<ApplicationStore>(context);
      _customer = application.customerStore;
      _order = _customer.newOrder;

      _dateTime = DateFormat('EEEE d MMMM').format(_order.date) +
          ' ' +
          S.of(context).at +
          ' ' +
          DateFormat('Hm').format(_order.date);
      _companyName = _order.company['company_name'];
      _packageTitle = _order.package['title'];
      final duration = _order.package['duration'] as int;
      if (duration != null) {
        _duration =
            "${duration ~/ 100}${S.of(context).hourShort} ${duration % 100}${S.of(context).minuteShort}";
      }

      switch (_order.vehicleType) {
        case VehicleType.sedan:
          _vehicleType = S.of(context).sedan.replaceFirst('s', 'S');
          break;

        case VehicleType.suv:
          _vehicleType = S.of(context).suv.toUpperCase();
          break;

        case VehicleType.motorcycle:
          _vehicleType = S.of(context).motorcycle.replaceFirst('m', 'M');
          break;

        case VehicleType.boat:
          _vehicleType = S.of(context).boat.replaceFirst('b', 'B');
          break;

        case VehicleType.car:
          _vehicleType = 'Car';
          break;

        case VehicleType.truck:
          _vehicleType = 'Truck';
          break;

        case VehicleType.van:
          _vehicleType = 'Van';
          break;
      }
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CustomerDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _customer.avatarUrl,
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/order-confirm-background.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Builder(
              builder: (BuildContext context) => CustomAppBar2(
                onOpenDrawer: () {
                  setState(() {
                    _isDialogVisible = true;
                    Scaffold.of(context).openDrawer();
                  });
                },
                title: S.of(context).dashboard,
                nbNotifications: 0,
                avatar: _customer.avatarUrl == null
                    ? null
                    : NetworkImage(_customer.avatarUrl),
              ),
            ),
            Spacer(flex: 2),
            Align(
              alignment: application.locale == 'ar'
                  ? Alignment.topRight
                  : Alignment.topLeft,
              child: Transform.translate(
                offset: Offset(
                    ScreenUtil()
                        .setWidth(application.locale == 'ar' ? -28 : 28),
                    0),
                child: Text(
                  S.of(context).orderSummary,
                  style: TextStyle(
                    fontFamily:
                        application.locale == 'ar' ? 'Cairo' : 'SF Pro Display',
                    fontSize: 21,
                    fontWeight: FontWeight.w900,
                    color: Color(0xFF91A6D9),
                  ),
                ),
              ),
            ),
            Spacer(flex: 2),
            Container(
              width: ScreenUtil().setWidth(320),
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(25.7),
                right: ScreenUtil().setWidth(35),
                bottom: ScreenUtil().setHeight(23.2),
                left: ScreenUtil().setWidth(35),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(
                  colors: <Color>[Color(0xff011fa6), Color(0xff011053)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Colors.black.withOpacity(0.35),
                    offset: Offset(0, 8),
                    blurRadius: 12,
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Image(
                    image: AssetImage('assets/images/location-icon.png'),
                    height: ScreenUtil().setHeight(18.51),
                    fit: BoxFit.fill,
                    color: Colors.white,
                  ),
                  SizedBox(height: ScreenUtil().setHeight(3)),
                  Text(
                    S.of(context).vehicleLocated,
                    style: TextStyle(
                      fontFamily:
                          application.locale == 'ar' ? 'Cairo' : 'SF Pro Text',
                      fontSize: 12,
                      fontWeight: FontWeight.w700,
                      color: Colors.white.withOpacity(0.98),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(3)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          _order.address,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Colors.white.withOpacity(0.98),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(14)),
                  Row(
                    children: <Widget>[
                      Image(
                        image:
                            AssetImage('assets/images/vehicle-type-icon.png'),
                        width: ScreenUtil().setWidth(15.78),
                        height: ScreenUtil().setWidth(15.78),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(15.8)),
                      Text(
                        S.of(context).vehicleType,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Text',
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                          color: Colors.white.withOpacity(0.98),
                        ),
                      ),
                      Text(
                        _vehicleType,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Text',
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: Colors.white.withOpacity(0.98),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(6)),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/images/calendar-icon.png'),
                        width: ScreenUtil().setWidth(15.78),
                        height: ScreenUtil().setWidth(15.78),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(15.8)),
                      Text(
                        S.of(context).dateAndTime,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Text',
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                          color: Colors.white.withOpacity(0.98),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          _dateTime,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Colors.white.withOpacity(0.98),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(6)),
                  Row(
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/images/company-icon.png'),
                        width: ScreenUtil().setWidth(15.78),
                        height: ScreenUtil().setWidth(15.78),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(15.8)),
                      Text(
                        S.of(context).washCompany,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Text',
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                          color: Colors.white.withOpacity(0.98),
                        ),
                      ),
                      Text(
                        _companyName,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Text',
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: Colors.white.withOpacity(0.98),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(6)),
                  Row(
                    children: <Widget>[
                      Image(
                        image: AssetImage('assets/images/package-icon.png'),
                        width: ScreenUtil().setWidth(15.78),
                        height: ScreenUtil().setWidth(15.78),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(15.8)),
                      Text(
                        S.of(context).package,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Text',
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                          color: Colors.white.withOpacity(0.98),
                        ),
                      ),
                      Text(
                        _packageTitle,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Text',
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                          color: Colors.white.withOpacity(0.98),
                        ),
                      ),
                    ],
                  ),
                  if (_duration != null) ...[
                    SizedBox(height: ScreenUtil().setHeight(6)),
                    Row(
                      children: <Widget>[
                        Image(
                          image: AssetImage('assets/images/time-icon.png'),
                          width: ScreenUtil().setWidth(15.78),
                          height: ScreenUtil().setWidth(15.78),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(15.8)),
                        Text(
                          "${S.of(context).Duration} : ",
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 12,
                            fontWeight: FontWeight.w700,
                            color: Colors.white.withOpacity(0.98),
                          ),
                        ),
                        Text(
                          _duration,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Colors.white.withOpacity(0.98),
                          ),
                        ),
                      ],
                    ),
                  ],
                  SizedBox(height: ScreenUtil().setHeight(20)),
                  Flexible(
                    fit: FlexFit.loose,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, '/orders/new');
                            },
                            borderRadius: BorderRadius.circular(20),
                            child: Container(
                              width: ScreenUtil().setWidth(115),
                              height: ScreenUtil().setHeight(40),
                              decoration: BoxDecoration(
                                border: Border.all(color: Color(0x120d2e00)),
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.white,
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                    color: Color(0x66000000),
                                    offset:
                                        Offset(0, ScreenUtil().setHeight(3)),
                                  ),
                                ],
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    S.of(context).edit,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontSize: 11,
                                      fontWeight: FontWeight.w700,
                                      color: Color(0xff011e96),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              _order.company = null;
                              Navigator.pushNamed(
                                  context, '/dashboard/customer');
                            },
                            child: Container(
                              width: ScreenUtil().setWidth(115),
                              height: ScreenUtil().setHeight(40),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    S.of(context).cancel,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontSize: 11,
                                      fontWeight: FontWeight.w700,
                                      color: Color(0xfff8f8f8),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Spacer(flex: 2),
            Text(
              S.of(context).paymentMethods,
              style: TextStyle(
                fontFamily:
                    application.locale == 'ar' ? 'Cairo' : 'SF Pro Display',
                fontSize: 11,
                fontWeight: FontWeight.w700,
                color: Color(0xff3a4655),
              ),
            ),
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                // Stack(
                //   children: <Widget>[
                //     Material(
                //       color: Colors.transparent,
                //       child: InkWell(
                //         onTap: () => setState(() =>
                //             _selectedPaymentMethod =
                //                 PaymentMethod.visa),
                //         borderRadius: BorderRadius.circular(5),
                //         child: Container(
                //           width: ScreenUtil().setWidth(123),
                //           height: ScreenUtil().setHeight(56),
                //           decoration: BoxDecoration(
                //             borderRadius: BorderRadius.circular(5),
                //             color: Color(0xff91a6d9),
                //             gradient: _selectedPaymentMethod ==
                //                     PaymentMethod.visa
                //                 ? LinearGradient(
                //                     colors: <Color>[
                //                       Color(0xff052299),
                //                       Color(0xff0a2faf)
                //                     ],
                //                     begin: Alignment.topCenter,
                //                     end: Alignment.bottomCenter,
                //                   )
                //                 : null,
                // boxShadow: <BoxShadow>[
                //                   BoxShadow(
                //                     color: Color(0xFF004AFF).withOpacity(0.17),
                //                     offset: Offset(0, 6),
                //                     blurRadius: 10,
                //                   ),
                //                 ],
                //           ),
                //           child: Center(
                //             child: Image(
                //               image: AssetImage(
                //                   'assets/images/visa-logo.png'),
                //               width: ScreenUtil().setWidth(62),
                //               height: ScreenUtil().setHeight(21),
                //             ),
                //           ),
                //         ),
                //       ),
                //     ),
                //     Positioned(
                //       top: 0,
                //       right: 0,
                //       child: Transform.translate(
                //         offset: Offset(ScreenUtil().setWidth(10),
                //             -ScreenUtil().setHeight(10)),
                //         child: _selectedPaymentMethod ==
                //                 PaymentMethod.visa
                //             ? Image(
                //                 image: AssetImage(
                //                     'assets/images/selected-icon.png'),
                //                 width: ScreenUtil().setWidth(32),
                //                 height: ScreenUtil().setHeight(32),
                //               )
                //             : SizedBox(
                //                 width: ScreenUtil().setWidth(32),
                //                 height: ScreenUtil().setHeight(32),
                //               ),
                //       ),
                //     ),
                //   ],
                // ),
                // SizedBox(width: ScreenUtil().setWidth(14)),
                Stack(
                  children: <Widget>[
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () => setState(
                            () => _selectedPaymentMethod = PaymentMethod.cash),
                        borderRadius: BorderRadius.circular(5),
                        child: Container(
                          width: ScreenUtil().setWidth(123),
                          height: ScreenUtil().setHeight(64),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xff91a6d9),
                            gradient:
                                _selectedPaymentMethod == PaymentMethod.cash
                                    ? LinearGradient(
                                        colors: <Color>[
                                          Color(0xff052299),
                                          Color(0xff0a2faf)
                                        ],
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                      )
                                    : null,
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                color: Color(0xFF004AFF).withOpacity(0.17),
                                offset: Offset(0, 6),
                                blurRadius: 10,
                              ),
                            ],
                          ),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  S.of(context).cashOn,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontSize: 13,
                                    fontWeight: FontWeight.w800,
                                    color: Colors.white,
                                  ),
                                ),
                                Text(
                                  S.of(context).delivery,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontSize: 11,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.white,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 0,
                      right: 0,
                      child: Transform.translate(
                        offset: Offset(ScreenUtil().setWidth(10),
                            -ScreenUtil().setHeight(10)),
                        child: _selectedPaymentMethod == PaymentMethod.cash
                            ? Image(
                                image: AssetImage(
                                    'assets/images/selected-icon.png'),
                                width: ScreenUtil().setWidth(32),
                                height: ScreenUtil().setHeight(32),
                              )
                            : SizedBox(
                                width: ScreenUtil().setWidth(32),
                                height: ScreenUtil().setHeight(32),
                              ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Spacer(flex: 2),
            Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () async {
                  await _makeOrder();
                  _customer.newOrder = new OrderStore();
                  Navigator.pushNamed(context, '/dashboard/customer');
                },
                borderRadius: BorderRadius.circular(20),
                child: Container(
                  width: ScreenUtil().setWidth(171),
                  height: ScreenUtil().setHeight(41),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    gradient: LinearGradient(
                      colors: <Color>[
                        Color(0xFF0026A1),
                        Color(0xFF000F66),
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        color: Color(0x66000000),
                        offset: Offset(0, ScreenUtil().setHeight(3)),
                        blurRadius: 6,
                      ),
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        S.of(context).orderNow,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF UI Display',
                          fontFamilyFallback: <String>['SF Pro Text'],
                          fontSize: ScreenUtil().setSp(11),
                          fontWeight: FontWeight.w700,
                          color: Color(0xfffcfcfc),
                          letterSpacing: 0.22,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Spacer(flex: 20),
          ],
        ),
      ),
    );
  }

  Future<void> _makeOrder() async {
    final paymentMethodStrs = {
      PaymentMethod.visa: 'visa',
      PaymentMethod.cash: 'cash',
    };

    await Firestore.instance.collection('orders').add({
      'address': _order.address,
      'company_id': _order.company['company_id'],
      'customer_id': _customer.customerId,
      'date': Timestamp.fromDate(_order.date),
      'location': _order.location,
      'locality': _order.locality,
      'package_id': _order.package['package_id'],
      'payment_method': paymentMethodStrs[_selectedPaymentMethod],
      'status': 0,
    });
  }
}
