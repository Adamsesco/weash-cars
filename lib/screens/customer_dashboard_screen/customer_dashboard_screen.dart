import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:app_review/app_review.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/screens/customer_dashboard_screen/widgets/done_screen.dart';
import 'package:weash_cars/screens/customer_dashboard_screen/widgets/on_the_way_screen.dart';
import 'package:weash_cars/screens/customer_dashboard_screen/widgets/order_summary.dart';
import 'package:weash_cars/screens/customer_dashboard_screen/widgets/working_on_screen.dart';
import 'package:weash_cars/services/CompaniesService.dart';
import 'package:weash_cars/services/OrdersService.dart';

import 'package:weash_cars/widgets/custom_app_bar.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/customer_store.dart';
import 'package:weash_cars/widgets/customer_drawer.dart';

class CustomerDashboardScreen extends StatefulWidget {
  @override
  _CustomerDashboardScreenState createState() =>
      _CustomerDashboardScreenState();
}

class _CustomerDashboardScreenState extends State<CustomerDashboardScreen> {
  final Location _locationService = Location();
  CustomerStore _customer;
  Future<List<Map<String, dynamic>>> _orders;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _isDialogVisible = false;
  _Filter _selectedFilter = _Filter.date;
  bool _hasUnconfirmedOrder = false;
  Future<List<Map<String, dynamic>>> _sponsoredCompanies;
  Future<Map<String, dynamic>> _adFuture;
  GeoPoint _location;
  StreamSubscription<String> _notificationsStream;
  Geoflutterfire _geo = Geoflutterfire();

  @override
  void initState() {
    super.initState();

    final application = Provider.of<ApplicationStore>(context, listen: false);
    _customer = application.customerStore;
    _orders = OrdersService.getOrders(customerId: _customer.customerId);
    _customer.orders.setOrdersFuture(_orders);
    final newOrder = _customer.newOrder;
    _hasUnconfirmedOrder = newOrder.company != null;

    if (_hasUnconfirmedOrder && newOrder.address != _customer.address) {
      final location = newOrder.location == null
          ? null
          : _geo.point(
              latitude: newOrder.location.latitude,
              longitude: newOrder.location.longitude);
      final address = newOrder.address;

      Firestore.instance
          .collection('users')
          .document(_customer.customerId)
          .updateData({
        'address': address,
        'location': location?.data,
      });
    }

    _firebaseMessaging.getToken().then((String token) {
      if (_customer.fcmToken != token) {
        _customer.setData(fcmToken: token);
        _customer.saveData();
      }
    });

    if (_notificationsStream == null) {
      _notificationsStream = application.notificationsStream.stream
          .listen((String notificationId) async {
        final database = application.database;
        final notifications = await database.query('notifications',
            where: 'id = ?', whereArgs: [notificationId]);
        print("Read from database");
        if (notifications != null && notifications.length > 0) {
          await database.delete('notifications',
              where: 'id = ?', whereArgs: [notificationId]);
          application.removeNotification();
          final data = notifications[0];
          final orderId = data['targetId'];
          print("Order id: $orderId");
          if (application.isPendingNotification) {
            application.isPendingNotification = false;
            switch (data['type']) {
              case 'EmployeeOnHisWay':
                print('On the way');
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => OnTheWayScreen()));
                break;
              case 'EmployeeStartedWorking':
                print('Started');
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => WorkingOnScreen()));
                break;
              case 'EmployeeDone':
                print('done');
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => DoneScreen(
                              orderId: orderId,
                            )));
                break;
            }
          } else {
            print('not pending');
            var orders = await _customer.orders.ordersFuture;
            final newOrder = await OrdersService.getOrder(orderId);
            if (newOrder != null &&
                !orders.any((Map<String, dynamic> order) =>
                    order['order_id'] == newOrder['order_id'])) {
              orders.add(newOrder);
              _customer.orders.setOrdersFuture(_customer.orders.ordersFuture);
            }
          }
        }
      });
    }

    _getAd();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return _hasUnconfirmedOrder
        ? OrderSummary()
        : Scaffold(
            drawer: Observer(
              builder: (BuildContext context) => CustomerDrawer(
                onCloseDrawer: () {
                  setState(() => _isDialogVisible = false);
                },
                avatarUrl: _customer.avatarUrl,
              ),
            ),
            body: Stack(
              children: <Widget>[
                Positioned.fill(
                  child: Container(
                    color: Colors.white,
                  ),
                ),
                Column(
                  children: <Widget>[
                    Observer(
                      builder: (BuildContext context) => CustomAppBar2(
                        onOpenDrawer: () {
                          setState(() {
                            _isDialogVisible = true;
                            print(Scaffold.of(context).hasDrawer);
                            Scaffold.of(context).openDrawer();
                          });
                        },
                        title: S.of(context).dashboard,
                        nbNotifications: 0,
                        avatar: _customer.avatarUrl == null
                            ? null
                            : NetworkImage(_customer.avatarUrl),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(27)),
                    Expanded(
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(30)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Text(
                                    S.of(context).orders,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Display',
                                      fontSize: 21,
                                      fontWeight: FontWeight.w900,
                                      color: Color(0xFF91A6D9),
                                    ),
                                  ),
                                  Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () async {
                                        final filter = await _showFiltersDialog(
                                            _selectedFilter);
                                        if (filter != null) {
                                          setState(() {
                                            _selectedFilter = filter;
                                          });
                                        }
                                      },
                                      child: Image(
                                        image: AssetImage(
                                            'assets/images/filters-icon.png'),
                                        color: Color(0xff011b7b),
                                        width: ScreenUtil().setWidth(27),
                                        height: ScreenUtil().setHeight(23),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(20)),
                            FutureBuilder(
                              future: _adFuture,
                              builder: (BuildContext context,
                                  AsyncSnapshot<Map<String, dynamic>>
                                      snapshot) {
                                switch (snapshot.connectionState) {
                                  case ConnectionState.active:
                                  case ConnectionState.waiting:
                                  case ConnectionState.none:
                                    return Container();
                                  case ConnectionState.done:
                                    if (!snapshot.hasError &&
                                        snapshot.hasData &&
                                        snapshot.data != null) {
                                      final sponsoredCompany = snapshot.data;

                                      return Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Transform.translate(
                                            offset: Offset(
                                                ScreenUtil().setWidth(
                                                    application.locale == 'ar'
                                                        ? 17
                                                        : -17),
                                                0),
                                            child: Text(
                                              S.of(context).sponsored,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                fontSize: 13,
                                                fontWeight: FontWeight.w600,
                                                color: Color(0xFFE4E4E4),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                              height:
                                                  ScreenUtil().setHeight(2)),
                                          Builder(
                                            builder: (BuildContext context) {
                                              final company = sponsoredCompany;

                                              return InkWell(
                                                onTap: company['link'] == null
                                                    ? null
                                                    : () async {
                                                        final link =
                                                            company['link'];
                                                        final canlaunch =
                                                            await canLaunch(
                                                                link);
                                                        if (canlaunch) {
                                                          launch(link);
                                                        }
                                                      },
                                                child: Container(
                                                  width: ScreenUtil()
                                                      .setWidth(344),
                                                  height: ScreenUtil()
                                                      .setHeight(82),
                                                  padding: EdgeInsets.all(3),
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: AssetImage(
                                                          'assets/images/sponsored-banner-dashed-border.png'),
                                                      fit: BoxFit.fill,
                                                    ),
                                                  ),
                                                  child: Image(
                                                    image: NetworkImage(
                                                        company['banner_url']),
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                        ],
                                      );
                                    }

                                    return Container();
                                }

                                return Container();
                              },
                            ),
                            Expanded(
                              child: Stack(
                                alignment: Alignment.bottomCenter,
                                children: <Widget>[
                                  Observer(
                                    builder: (BuildContext context) =>
                                        FutureBuilder<
                                            List<Map<String, dynamic>>>(
                                      future: _customer.orders.ordersFuture,
                                      builder: (ctx, snapshot) {
                                        switch (snapshot.connectionState) {
                                          case ConnectionState.waiting:
                                          case ConnectionState.active:
                                            return Center(
                                              child: CupertinoActivityIndicator(
                                                animating: true,
                                              ),
                                            );
                                          case ConnectionState.none:
                                            return Container();
                                          case ConnectionState.done:
                                            if (snapshot.hasError) {
                                              return Container();
                                            }

                                            final orders = snapshot.data;

                                            _showRatingDialog(context, orders);

                                            switch (_selectedFilter) {
                                              case _Filter.companyName:
                                                orders.sort((order1, order2) {
                                                  final name1 =
                                                      order1['company']
                                                              ['company_name']
                                                          .toString();
                                                  final name2 =
                                                      order2['company']
                                                              ['company_name']
                                                          .toString();
                                                  return name1.compareTo(name2);
                                                });
                                                break;

                                              case _Filter.date:
                                                orders.sort((order1, order2) {
                                                  final date1 = order1['date']
                                                      as DateTime;
                                                  final date2 = order2['date']
                                                      as DateTime;
                                                  return date2.compareTo(date1);
                                                });
                                                break;

                                              case _Filter.priceAsc:
                                                orders.sort((order1, order2) {
                                                  final price1 =
                                                      order1['package']['price']
                                                          as double;
                                                  final price2 =
                                                      order2['package']['price']
                                                          as double;
                                                  return price1
                                                      .compareTo(price2);
                                                });
                                                break;

                                              case _Filter.priceDesc:
                                                orders.sort((order1, order2) {
                                                  final price1 =
                                                      order1['package']['price']
                                                          as double;
                                                  final price2 =
                                                      order2['package']['price']
                                                          as double;
                                                  return price2
                                                      .compareTo(price1);
                                                });
                                                break;
                                            }

                                            return ListView.builder(
                                              padding: EdgeInsets.zero,
                                              itemCount: orders == null
                                                  ? 0
                                                  : orders.length + 1,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                if (index == orders.length) {
                                                  return SizedBox(
                                                      height: ScreenUtil()
                                                          .setHeight(87));
                                                }
                                                final order = orders[index];
                                                final orderDate =
                                                    order['date'] as DateTime;
                                                final orderStatus =
                                                    order['status'] as int;
                                                final orderId =
                                                    order['order_id']
                                                        .toString();
                                                final orderNumber =
                                                    order['order_number']
                                                        as int;
                                                final orderRating =
                                                    order['rating'] == null
                                                        ? null
                                                        : (order['rating']
                                                                as num)
                                                            .toDouble();

                                                final company =
                                                    order['company'];
                                                final companyName =
                                                    company['company_name']
                                                        .toString();
                                                final companyAvatarUrl =
                                                    company['avatar_url']
                                                        .toString();
                                                final companyPhoneNumber =
                                                    company['phone_number']
                                                        as String;

                                                final package =
                                                    order['package'];
                                                final packageTitle =
                                                    package['title'].toString();
                                                final packagePrice =
                                                    package['price'].toString();
                                                final packageCurrency =
                                                    package['currency']
                                                        .toString();

                                                List<String> pictureUrls;
                                                if (orderStatus >= 20) {
                                                  if (order['pictures_urls'] ==
                                                      null) {
                                                    pictureUrls = [];
                                                  } else {
                                                    pictureUrls = List<
                                                                String>.from(
                                                            order[
                                                                'pictures_urls'])
                                                        .where((url) =>
                                                            url != null)
                                                        .toList();
                                                  }
                                                }

                                                return _buildOrderCard(
                                                    UniqueKey(),
                                                    companyName,
                                                    companyAvatarUrl,
                                                    companyPhoneNumber,
                                                    packageTitle,
                                                    packagePrice,
                                                    packageCurrency,
                                                    orderDate,
                                                    orderStatus,
                                                    orderId,
                                                    orderNumber,
                                                    orderRating,
                                                    pictureUrls);
                                              },
                                            );
                                        }

                                        return Container();
                                      },
                                    ),
                                  ),
                                  Container(
                                    height: ScreenUtil().setHeight(87),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: ScreenUtil().setWidth(65)),
                                    color: Colors.white,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        // IconButton(
                                        //   onPressed: null,
                                        //   icon: Image(
                                        //     image: AssetImage(
                                        //         'assets/images/messages-icon.png'),
                                        //     width: ScreenUtil().setWidth(22),
                                        //     height: ScreenUtil().setHeight(22),
                                        //   ),
                                        // ),
                                        IconButton(
                                          onPressed: () => Navigator.pushNamed(
                                              context, '/orders/new'),
                                          iconSize: ScreenUtil().setWidth(62),
                                          icon: Image(
                                            image: AssetImage(
                                                'assets/images/new-order-icon.png'),
                                            width: ScreenUtil().setWidth(62),
                                            height: ScreenUtil().setWidth(62),
                                          ),
                                        ),
                                        // IconButton(
                                        //   onPressed: null,
                                        //   icon: Image(
                                        //     image: AssetImage(
                                        //         'assets/images/list-orders-icon.png'),
                                        //     width: ScreenUtil().setWidth(18),
                                        //     height: ScreenUtil().setHeight(23),
                                        //     color: Color(0xff021a89),
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                Visibility(
                  visible: _isDialogVisible,
                  child: Positioned.fill(
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                      child: Container(
                        color: Colors.grey.withOpacity(0.2),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
  }

  Future<void> _showRatingDialog(
      BuildContext context, List<Map<String, dynamic>> orders) async {
    final prefs = await SharedPreferences.getInstance();
    final alreadyRated = prefs.getBool('rated') ?? false;
    if (alreadyRated) {
      return;
    }

    final lastAsked = prefs.getInt('last_rate') ?? 0;
    final lastAskedDate = DateTime.fromMillisecondsSinceEpoch(lastAsked);
    final now = DateTime.now();
    if (!lastAskedDate.isBefore(now.subtract(Duration(days: 7)))) {
      return;
    }

    prefs.setInt('last_rate', now.millisecondsSinceEpoch);

    final hasConfirmedOrder =
        orders.any((order) => order['status'] as int == 21);

    if (hasConfirmedOrder) {
      print('rate');
      if (Platform.isIOS) {
        final value = await AppReview.requestReview;
        print("Rating: $value");
        prefs.setBool('rated', true);
      } else if (Platform.isAndroid) {
        final rate = await showCupertinoDialog<bool>(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            title: Text(S.of(context).rateThisApp),
            content: Text(S.of(context).ifYouLikeThisApp),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text(S.of(context).rate),
                onPressed: () => Navigator.of(context).pop(true),
              ),
              CupertinoDialogAction(
                child: Text(S.of(context).notNow),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          ),
        );

        if (rate) {
          final value = await AppReview.requestReview;
          print("Rating: $value");
          prefs.setBool('rated', true);
        }
      }
    }
  }

  Widget _buildOrderCard(
      Key key,
      String companyName,
      String companyAvatarUrl,
      String companyPhoneNumber,
      String packageTitle,
      String packagePrice,
      String packageCurrency,
      DateTime orderDate,
      int orderStatus,
      String orderId,
      int orderNumber,
      double orderRating,
      List<String> pictureUrls) {
    final application = Provider.of<ApplicationStore>(context);

    if (orderStatus == 30) {
      return Container();
    }

    final orderNumberStr = orderNumber == null
        ? '000000'
        : List.generate(6 - orderNumber.toString().length, (_) => '0').join() +
            orderNumber.toString();

    final textColor =
        orderStatus > 10 && orderStatus < 20 ? Colors.white : Color(0xFFA6A6A7);

    final doneOrderDecoration = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      color: Colors.white,
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Color(0x1a000000),
          blurRadius: 10,
        ),
      ],
    );

    final activeOrderDecoration = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      gradient: LinearGradient(
        colors: <Color>[Color(0xFF004AFF), Color(0xFF01104D)],
        begin:
            application.locale == 'ar' ? Alignment.topRight : Alignment.topLeft,
        end: application.locale == 'ar'
            ? Alignment.bottomLeft
            : Alignment.bottomRight,
      ),
      boxShadow: <BoxShadow>[
        BoxShadow(
          color: Color(0xFFA0AEE6).withOpacity(0.23),
          blurRadius: 11,
          offset: Offset(0, ScreenUtil().setHeight(10)),
        ),
      ],
    );

    final doneFooter = Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text(
              "${S.of(context).orderNumber} $orderNumberStr",
              style: TextStyle(
                fontFamily: 'SF Pro Text',
                fontSize: 9,
                fontWeight: FontWeight.w700,
                color: Color(0xFFA6A6A7),
              ),
            ),
            Column(
              children: <Widget>[
                if (orderRating == null)
                  InkWell(
                    onTap: () => _showReviewDialog(orderId),
                    child: Text(
                      S.of(context).giveFeedback,
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Text',
                        color: Color(0xff011b8e),
                        fontSize: 10,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                Container(
                  height: 11,
                  child: StarRating(
                    size: ScreenUtil().setHeight(10),
                    starCount: 5,
                    rating: orderRating == null ? 0.0 : orderRating,
                    color: Color(0xffffc300),
                    borderColor: Color(0xffffc300),
                  ),
                ),
              ],
            ),
            Visibility(
              visible: false,
              maintainSize: true,
              maintainAnimation: true,
              maintainState: true,
              child: Row(
                children: <Widget>[
                  Image(
                    image: AssetImage('assets/images/flag-icon.png'),
                  ),
                  SizedBox(width: ScreenUtil().setWidth(5.5)),
                  Text(
                    S.of(context).report,
                    style: TextStyle(
                      fontFamily: 'SF Pro Text',
                      fontSize: 10,
                      fontWeight: FontWeight.w700,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );

    final activeFooter = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            Image(
              image: AssetImage('assets/images/track-order-icon.png'),
              width: ScreenUtil().setWidth(20),
              height: ScreenUtil().setHeight(19),
              color: Colors.white,
            ),
            SizedBox(width: ScreenUtil().setWidth(10)),
            Text(
              S.of(context).trackOrder,
              style: TextStyle(
                fontFamily: application.locale == 'ar' ? 'Cairo' : 'Lato',
                fontFamilyFallback: <String>['SF Pro Text'],
                fontSize: ScreenUtil().setSp(12),
                fontWeight: FontWeight.w700,
                color: textColor,
              ),
            ),
          ],
        ),
        Text(
          "${S.of(context).orderNumber} $orderNumberStr",
          style: TextStyle(
            fontFamily: application.locale == 'ar' ? 'Cairo' : 'Lato',
            fontFamilyFallback: <String>['SF Pro Text'],
            fontSize: ScreenUtil().setSp(9),
            fontWeight: FontWeight.w700,
            color: textColor,
          ),
        ),
      ],
    );

    final pendingFooter = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Image(
              image: AssetImage('assets/images/pending-icon.png'),
              width: ScreenUtil().setWidth(19.1),
              height: ScreenUtil().setHeight(19.36),
              color: Color(0xFFFF8A11),
            ),
            SizedBox(width: ScreenUtil().setWidth(10)),
            Text(
              S.of(context).pending,
              style: TextStyle(
                fontFamily: application.locale == 'ar' ? 'Cairo' : 'Lato',
                fontFamilyFallback: <String>['SF Pro Text'],
                fontSize: ScreenUtil().setSp(12),
                fontWeight: FontWeight.w700,
                color: Color(0xFFFF8A11),
              ),
            ),
          ],
        ),
        InkWell(
          onTap: () async {
            final reasonNumber = await _showCancelDialog(orderId);
            if (reasonNumber != null) {
              var orders = await _customer.orders.ordersFuture;
              final order = orders.firstWhere(
                  (order) => order['order_id'] == orderId,
                  orElse: () => null);
              if (order != null) {
                orders.remove(order);
                _customer.orders.setOrdersFuture(_customer.orders.ordersFuture);
              }
            }
          },
          enableFeedback: false,
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(8),
              vertical: ScreenUtil().setHeight(6),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              // border: Border.all(color: Colors.red,),
              color: Color(0xFFF4F4F4),
            ),
            child: Text(
              "${S.of(context).cancel} ?",
              style: TextStyle(
                fontFamily: application.locale == 'ar' ? 'Cairo' : 'Lato',
                fontSize: 11,
                fontWeight: FontWeight.w700,
                // color: Colors.white,
                color: Colors.grey,
              ),
            ),
          ),
        ),
        if (orderNumber != null)
          Text(
            "${S.of(context).orderNumber} $orderNumberStr",
            style: TextStyle(
              fontFamily: application.locale == 'ar' ? 'Cairo' : 'Lato',
              fontFamilyFallback: <String>['SF Pro Text'],
              fontSize: ScreenUtil().setSp(9),
              fontWeight: FontWeight.w700,
              color: textColor,
            ),
          ),
      ],
    );

    final acceptedFooter = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.check,
              size: ScreenUtil().setWidth(16),
              color: Color(0xFF03C428),
            ),
            SizedBox(width: ScreenUtil().setWidth(10)),
            Text(
              'ACCEPTED',
              style: TextStyle(
                fontFamily: application.locale == 'ar' ? 'Cairo' : 'Lato',
                fontFamilyFallback: <String>['SF Pro Text'],
                fontSize: ScreenUtil().setSp(12),
                fontWeight: FontWeight.w700,
                color: Color(0xFF03C428),
              ),
            ),
          ],
        ),
        InkWell(
          onTap: () async {
            final reasonNumber = await _showCancelDialog(orderId);
            if (reasonNumber != null) {
              var orders = await _customer.orders.ordersFuture;
              final order = orders.firstWhere(
                  (order) => order['order_id'] == orderId,
                  orElse: () => null);
              if (order != null) {
                orders.remove(order);
                _customer.orders.setOrdersFuture(_customer.orders.ordersFuture);
              }
            }
          },
          enableFeedback: false,
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(8),
              vertical: ScreenUtil().setHeight(6),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              // border: Border.all(color: Colors.red,),
              color: Color(0xFFF4F4F4),
            ),
            child: Text(
              "${S.of(context).cancel} ?",
              style: TextStyle(
                fontFamily: application.locale == 'ar' ? 'Cairo' : 'Lato',
                fontSize: 11,
                fontWeight: FontWeight.w700,
                // color: Colors.white,
                color: Colors.grey,
              ),
            ),
          ),
        ),
        if (orderNumber != null)
          Text(
            "${S.of(context).orderNumber} $orderNumberStr",
            style: TextStyle(
              fontFamily: application.locale == 'ar' ? 'Cairo' : 'Lato',
              fontFamilyFallback: <String>['SF Pro Text'],
              fontSize: ScreenUtil().setSp(9),
              fontWeight: FontWeight.w700,
              color: textColor,
            ),
          ),
      ],
    );

    final declinedFooter = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Icon(
              Icons.clear,
              size: ScreenUtil().setWidth(16),
              color: Color(0xFFF20606),
            ),
            SizedBox(width: ScreenUtil().setWidth(10)),
            Text(
              'DECLINED',
              style: TextStyle(
                fontFamily: application.locale == 'ar' ? 'Cairo' : 'Lato',
                fontFamilyFallback: <String>['SF Pro Text'],
                fontSize: ScreenUtil().setSp(12),
                fontWeight: FontWeight.w700,
                color: Color(0xFFF20606),
              ),
            ),
          ],
        ),
        if (orderNumber != null)
          Text(
            "${S.of(context).orderNumber} $orderNumberStr",
            style: TextStyle(
              fontFamily: application.locale == 'ar' ? 'Cairo' : 'Lato',
              fontFamilyFallback: <String>['SF Pro Text'],
              fontSize: ScreenUtil().setSp(9),
              fontWeight: FontWeight.w700,
              color: textColor,
            ),
          ),
      ],
    );

    return Material(
      key: key,
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          switch (orderStatus) {
            case 11:
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => OnTheWayScreen()));
              break;
            case 12:
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => WorkingOnScreen()));
              break;
            case 20:
            case 21:
            case 30:
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => DoneScreen(
                            orderId: orderId,
                          )));
              break;
          }
        },
        child: Container(
          padding: EdgeInsets.only(
            top: ScreenUtil().setHeight(17.7),
            right: ScreenUtil().setWidth(10.7),
            bottom: ScreenUtil().setHeight(11.9),
            left: ScreenUtil().setWidth(15.1),
          ),
          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          decoration: orderStatus <= 10 || orderStatus >= 20
              ? doneOrderDecoration
              : activeOrderDecoration,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                fit: FlexFit.loose,
                child: Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: ScreenUtil().setWidth(94),
                        height: ScreenUtil().setHeight(84),
                        padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(8),
                          vertical: ScreenUtil().setHeight(6),
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.black.withOpacity(0.16),
                              blurRadius: 5,
                            ),
                          ],
                        ),
                        child: Image(
                          image: NetworkImage(companyAvatarUrl),
                          fit: BoxFit.contain,
                        ),
                      ),
                      SizedBox(width: ScreenUtil().setWidth(22)),
                      Container(
                        width: ScreenUtil().setWidth(180),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Stack(
//                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(),
                                Text(
                                  companyName,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'Lato',
                                    fontSize: ScreenUtil().setSp(20),
                                    fontWeight: FontWeight.w900,
                                    color: textColor,
                                  ),
                                ),
                                if (orderStatus < 21)
                                  Positioned.directional(
                                    textDirection: Directionality.of(context),
                                    end: 0,
                                    child: InkWell(
                                      onTap: () {
                                        launch("tel://$companyPhoneNumber");
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        padding: EdgeInsets.all(
                                            ScreenUtil().setWidth(4)),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.green,
                                        ),
                                        child: Icon(
                                          Icons.phone,
                                          size: ScreenUtil().setWidth(14),
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                            SizedBox(height: ScreenUtil().setHeight(10)),
                            Column(
                              children: <Widget>[
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        S.of(context).dateOfOrder,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'Lato',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: ScreenUtil().setSp(10),
                                          fontWeight: FontWeight.w700,
                                          color: textColor,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Text(
                                        DateFormat('d MMM yyy')
                                                .format(orderDate) +
                                            ' ' +
                                            S.of(context).at +
                                            ' ' +
                                            DateFormat('HH:mm')
                                                .format(orderDate),
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'Lato',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: ScreenUtil().setSp(9),
                                          fontWeight: FontWeight.w500,
                                          color: textColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: ScreenUtil().setHeight(4)),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        S.of(context).packageOrdered,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'Lato',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: ScreenUtil().setSp(10),
                                          fontWeight: FontWeight.w700,
                                          color: textColor,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Text(
                                        packageTitle,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'Lato',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: ScreenUtil().setSp(9),
                                          fontWeight: FontWeight.w500,
                                          color: textColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: ScreenUtil().setHeight(4)),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        S.of(context).packagePrice,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'Lato',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: ScreenUtil().setSp(10),
                                          fontWeight: FontWeight.w700,
                                          color: textColor,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Text(
                                        "$packagePrice $packageCurrency",
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'Lato',
                                          fontFamilyFallback: <String>[
                                            'SF Pro Text'
                                          ],
                                          fontSize: ScreenUtil().setSp(9),
                                          fontWeight: FontWeight.w500,
                                          color: textColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(15.8)),
              if (orderStatus == 0) pendingFooter,
              if (orderStatus == 1 || orderStatus == 10) acceptedFooter,
              if (orderStatus > 10 && orderStatus < 20) activeFooter,
              if (orderStatus >= 20 && orderStatus < 30) doneFooter,
              if (orderStatus == 31) declinedFooter,
            ],
          ),
        ),
      ),
    );
  }

  Future<int> _showCancelDialog(String orderId) async {
    return showGeneralDialog<int>(
        context: context,
        barrierDismissible: true,
        barrierLabel: '',
        transitionDuration: Duration(milliseconds: 200),
        pageBuilder: (BuildContext context, Animation<double> animation1,
            Animation<double> animation2) {
          animation1.addListener(() {
            if (animation1.value > 0) {
              setState(() {
                _isDialogVisible = true;
              });
            }
            if (animation1.value == 0) {
              setState(() {
                _isDialogVisible = false;
              });
            }
          });

          return ValueListenableBuilder(
            valueListenable: animation1,
            builder: (BuildContext context, double animationValue, _) =>
                _CancelDialog(
              orderId: orderId,
              animationValue: animationValue,
            ),
          );
        });
  }

  Future<_Filter> _showFiltersDialog(_Filter selectedFilter) {
    return showGeneralDialog<_Filter>(
        context: context,
        barrierDismissible: true,
        barrierLabel: '',
        transitionDuration: Duration(milliseconds: 200),
        pageBuilder: (BuildContext context, Animation<double> animation1,
            Animation<double> animation2) {
          return ValueListenableBuilder(
            valueListenable: animation1,
            builder: (BuildContext context, double animationValue, _) =>
                _FiltersDialog(selectedFilter, animationValue),
          );
        });
  }

  Future<_Review> _showReviewDialog(String orderId) {
    return showGeneralDialog<_Review>(
        context: context,
        barrierDismissible: true,
        barrierLabel: '',
        transitionDuration: Duration(milliseconds: 200),
        pageBuilder: (BuildContext context, Animation<double> animation1,
            Animation<double> animation2) {
          animation1.addListener(() {
            if (animation1.value > 0) {
              setState(() {
                _isDialogVisible = true;
              });
            }
            if (animation1.value == 0) {
              setState(() {
                _isDialogVisible = false;
              });
            }
          });
          return ValueListenableBuilder(
            valueListenable: animation1,
            builder: (BuildContext context, double animationValue, _) =>
                _ReviewDialog(orderId: orderId, animationValue: animationValue),
          );
        });
  }

  Future<void> _getAd() async {
    final location = await _getCurrentLocation();
    if (location == null) {
      return null;
    }

    final adFuture =
        CompaniesService.getAd(GeoPoint(location.latitude, location.longitude));
    if (mounted) {
      setState(() {
        _adFuture = adFuture;
      });
    } else {
      _adFuture = adFuture;
    }
//
//    final sponsoredCompanies = CompaniesService.getCompanies(
//        GeoPoint(location.latitude, location.longitude),
//        100,
//        VehicleType.sedan,
//        DateTime.now(),
//        true,
//        Directionality.of(context),
//        true);
//    if (mounted) {
//      setState(() {
//        _sponsoredCompanies = sponsoredCompanies;
//      });
//    } else {
//      _sponsoredCompanies = sponsoredCompanies;
//    }
  }

  Future<LocationData> _getCurrentLocation() async {
    await _locationService.changeSettings(
        accuracy: LocationAccuracy.NAVIGATION);

    LocationData location;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      bool serviceStatus = await _locationService.serviceEnabled();
      if (serviceStatus) {
        final permission = await _locationService.requestPermission();
        if (permission) {
          location = await _locationService.getLocation();
          return location;
        }
      } else {
        bool serviceStatusResult = await _locationService.requestService();
        if (serviceStatusResult) {
          return _getCurrentLocation();
        }
      }
    } on PlatformException catch (e) {
      print(e);
      return null;
    }

    return null;
  }
}

class _CancelDialog extends StatefulWidget {
  final String orderId;
  final double animationValue;

  _CancelDialog({Key key, this.orderId, this.animationValue}) : super(key: key);

  @override
  __CancelDialogState createState() => __CancelDialogState();
}

class __CancelDialogState extends State<_CancelDialog> {
  int _reasonNumber;
  List<String> _reasons;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();

    _reasons = [
      'Found better prices',
      'It took too long',
      'Other',
    ];
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Transform.scale(
          scale: widget.animationValue,
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(35),
                  vertical: ScreenUtil().setHeight(35),
                ),
                margin: EdgeInsets.only(
                  top: ScreenUtil().setHeight(202 - 188.7),
                  right: ScreenUtil().setWidth(23),
                  bottom: ScreenUtil().setHeight(43 / 2),
                  left: ScreenUtil().setWidth(23),
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Color(0xFFA0AEE6).withOpacity(0.5),
                      offset: Offset(0, ScreenUtil().setHeight(10)),
                      blurRadius: 11,
                    ),
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'This order will be canceled',
                      style: TextStyle(
                        fontFamily: 'SF Pro Display',
                        fontSize: 24,
                        fontWeight: FontWeight.w900,
                        color: Color(0xFFA0AEE6),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(30)),
                    Align(
                      alignment: application.locale == 'ar'
                          ? Alignment.topRight
                          : Alignment.topLeft,
                      child: Text(
                        'Reason for cancellation',
                        style: TextStyle(
                          fontFamily: 'SF Pro Display',
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Color(0xFFA0AEE6),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(10)),
                    for (var i = 0; i < _reasons.length; i++)
                      InkWell(
                        onTap: () => setState(() => _reasonNumber = i),
                        enableFeedback: false,
                        child: Container(
                          width: ScreenUtil().setWidth(260),
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(16),
                            vertical: ScreenUtil().setHeight(16),
                          ),
                          margin: EdgeInsets.symmetric(
                            vertical: ScreenUtil().setHeight(8),
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(6),
                            boxShadow: i == _reasonNumber
                                ? <BoxShadow>[
                                    BoxShadow(
                                      color:
                                          Color(0xFF939393).withOpacity(0.25),
                                      blurRadius: 30,
                                    ),
                                  ]
                                : null,
                          ),
                          child: Row(
                            children: <Widget>[
                              Text(
                                _reasons[i],
                                style: TextStyle(
                                  fontFamily: 'SF Pro Text',
                                  fontSize: 17,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xFF021A89),
                                ),
                              ),
                              Spacer(),
                              if (i == _reasonNumber)
                                Image(
                                  image: AssetImage(
                                      'assets/images/checkmark-icon.png'),
                                  width: ScreenUtil().setWidth(12),
                                  height: ScreenUtil().setHeight(8),
                                )
                            ],
                          ),
                        ),
                      ),
                  ],
                ),
              ),
              Positioned(
                top: 0,
                right: application.locale == 'ar'
                    ? null
                    : ScreenUtil().setWidth(10.7),
                left: application.locale == 'ar'
                    ? ScreenUtil().setWidth(10.7)
                    : null,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  enableFeedback: false,
                  child: Container(
                    width: ScreenUtil().setWidth(38.56),
                    height: ScreenUtil().setWidth(38.56),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color(0xff011f99),
                    ),
                    child: Image(
                      image: AssetImage('assets/images/close-icon.png'),
                      width: ScreenUtil().setWidth(10.35),
                      height: ScreenUtil().setWidth(10.35),
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                child: InkWell(
                  onTap: () async {
                    if (_reasonNumber != null) {
                      await _send();
                    }
                    Navigator.of(context).pop(_reasonNumber);
                  },
                  enableFeedback: false,
                  child: Container(
                    width: ScreenUtil().setWidth(116),
                    height: ScreenUtil().setHeight(43),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      gradient: LinearGradient(
                        colors: <Color>[Color(0xff031C8D), Color(0xff0A2FAF)],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                      ),
                    ),
                    child: _isLoading
                        ? CupertinoActivityIndicator(
                            animating: true,
                          )
                        : Text(
                            S.of(context).send,
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: 11,
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                          ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _send() async {
    setState(() {
      _isLoading = true;
    });

    await Firestore.instance
        .collection('orders')
        .document(widget.orderId)
        .updateData({
      'status': 30,
      'cancellation_reason': _reasonNumber,
    });

    setState(() {
      _isLoading = false;
    });
  }
}

class _ReviewDialog extends StatefulWidget {
  final String orderId;
  final double animationValue;

  _ReviewDialog({Key key, this.animationValue, this.orderId}) : super(key: key);

  @override
  __ReviewDialogState createState() => __ReviewDialogState();
}

class __ReviewDialogState extends State<_ReviewDialog> {
  double _rating = 0;
  String _review = '';

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);
    final keyboardHeight = MediaQuery.of(context).viewInsets.bottom;

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Transform.scale(
        scale: widget.animationValue,
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Positioned(
                // top: keyboardHeight == 0
                //     ? ScreenUtil().setHeight(202)
                //     : MediaQuery.of(context).size.height - keyboardHeight,
                bottom: keyboardHeight == 0
                    ? ScreenUtil().setHeight(235)
                    : ScreenUtil().setHeight(60),
                child: Container(
                  width: ScreenUtil().setWidth(329),
                  height: ScreenUtil().setHeight(375),
                  padding: EdgeInsets.only(
                    top: ScreenUtil().setHeight(35),
                    bottom: ScreenUtil().setHeight(27),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xFFA0AEE6).withOpacity(0.5),
                        offset: Offset(0, ScreenUtil().setHeight(10)),
                        blurRadius: 11,
                      ),
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Transform.translate(
                            offset: Offset(
                                ScreenUtil().setWidth(
                                    application.locale == 'ar' ? -35 : 35),
                                0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                S.of(context).howSatisfied,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Display',
                                  fontFamilyFallback: <String>['SF Pro Text'],
                                  fontSize: 24,
                                  fontWeight: FontWeight.w900,
                                  color: Color(0xFFA0AEE6),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      StarRating(
                        onRatingChanged: (double value) =>
                            setState(() => _rating = value),
                        rating: _rating,
                        size: ScreenUtil().setWidth(20.27),
                        color: Color(0xFFFFC300),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            width: ScreenUtil().setWidth(299),
                            height: ScreenUtil().setHeight(152),
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(19),
                              vertical: ScreenUtil().setHeight(25),
                            ),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    'assets/images/review-textarea-dashed-border.png'),
                                fit: BoxFit.fill,
                              ),
                            ),
                            child: TextField(
                              onChanged: (String value) =>
                                  setState(() => _review = value),
                              buildCounter: (BuildContext context,
                                      {currentLength, maxLength, isFocused}) =>
                                  Text(''),
                              textCapitalization: TextCapitalization.sentences,
                              maxLength: 144,
                              maxLines: null,
                              decoration: InputDecoration.collapsed(
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide.none,
                                ),
                                hintText: S.of(context).writeReview,
                                hintStyle: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'Helvetica Rounded LT',
                                  fontFamilyFallback: <String>['SF Pro Text'],
                                  fontSize: 12,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xFF6E7EA5).withOpacity(0.27),
                                ),
                              ),
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'Helvetica Rounded LT',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 12,
                                fontWeight: FontWeight.w700,
                                color: Color(0xFF6E7EA5),
                              ),
                            ),
                          ),
                          SizedBox(height: ScreenUtil().setHeight(6)),
                          Transform.translate(
                            offset: Offset(
                                ScreenUtil().setWidth(
                                    application.locale == 'ar' ? 14 : -14),
                                0),
                            child: Text(
                              (144 - _review.length).toString(),
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'Helvetica Rounded LT',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 12,
                                fontWeight: FontWeight.w700,
                                color: Color(0xFF6E7EA5).withOpacity(0.27),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                // top: ScreenUtil().setHeight(188.7) -
                //     MediaQuery.of(context).padding.bottom,
                bottom: keyboardHeight == 0
                    ? ScreenUtil().setHeight(584.7)
                    : ScreenUtil()
                        .setHeight(60.0 + 375.0 + (584.7 - 235 - 375)),
                right: application.locale == 'ar'
                    ? null
                    : ScreenUtil().setWidth(10.7),
                left: application.locale == 'ar'
                    ? ScreenUtil().setWidth(10.7)
                    : null,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  enableFeedback: false,
                  child: Container(
                    width: ScreenUtil().setWidth(38.56),
                    height: ScreenUtil().setWidth(38.56),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color(0xff011f99),
                    ),
                    child: Image(
                      image: AssetImage('assets/images/close-icon.png'),
                      width: ScreenUtil().setWidth(10.35),
                      height: ScreenUtil().setWidth(10.35),
                    ),
                  ),
                ),
              ),
              Positioned(
                // top: ScreenUtil().setHeight(557) -
                //     MediaQuery.of(context).padding.bottom,
                bottom: keyboardHeight == 0
                    ? ScreenUtil().setHeight(212)
                    : ScreenUtil().setHeight(60.0 - (235 - 212)),
                child: InkWell(
                  onTap: () async {
                    final review = new _Review(_rating, _review);
                    await Firestore.instance
                        .collection('orders')
                        .document(widget.orderId)
                        .updateData({
                      'rating': _rating,
                      'review': _review,
                    });
                    Navigator.of(context).pop(review);
                  },
                  enableFeedback: false,
                  child: Container(
                    width: ScreenUtil().setWidth(116),
                    height: ScreenUtil().setHeight(43),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      gradient: LinearGradient(
                        colors: <Color>[Color(0xff031C8D), Color(0xff0A2FAF)],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                      ),
                    ),
                    child: Text(
                      S.of(context).send,
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Text',
                        fontFamilyFallback: <String>['SF Pro Text'],
                        fontSize: 11,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _FiltersDialog extends StatefulWidget {
  final _Filter selectedFilter;
  final double animationValue;

  _FiltersDialog(this.selectedFilter, this.animationValue, {Key key})
      : super(key: key);

  @override
  __FiltersDialogState createState() => __FiltersDialogState();
}

class __FiltersDialogState extends State<_FiltersDialog> {
  _Filter _selectedFilter;

  @override
  void initState() {
    super.initState();
    _selectedFilter = widget.selectedFilter;
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);
    final animationEnded = widget.animationValue == 1.0;

    return Transform.translate(
      offset: Offset(0, -ScreenUtil().setHeight(26)),
      child: Stack(
        children: <Widget>[
          Positioned(
            top: ScreenUtil().setHeight(158),
            right: application.locale == 'ar' ? null : ScreenUtil().setWidth(8),
            left: application.locale == 'ar' ? ScreenUtil().setWidth(8) : null,
            child: Container(
              width: max(ScreenUtil().setWidth(27.0 + 48.0),
                  ScreenUtil().setWidth(223) * widget.animationValue),
              height: max(ScreenUtil().setHeight(23.0 + 52.0),
                  ScreenUtil().setHeight(240) * widget.animationValue),
              padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(24),
                vertical: ScreenUtil().setHeight(26),
              ),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Color(0x30000000),
                    ),
                  ]),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      animationEnded
                          ? Text(
                              S.of(context).sortBy,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'Lato',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: ScreenUtil().setSp(14),
                                fontWeight: FontWeight.w900,
                                color: Color(0xff021a89),
                                decoration: TextDecoration.none,
                              ),
                            )
                          : SizedBox(width: 0, height: 0),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => Navigator.of(context).pop(),
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Image(
                            image: AssetImage('assets/images/filters-icon.png'),
                            width: ScreenUtil().setWidth(27),
                            height: ScreenUtil().setHeight(23),
                            color: Color(0xffd2d2d6),
                          ),
                        ),
                      ),
                    ],
                  ),
                  animationEnded
                      ? SizedBox(height: ScreenUtil().setHeight(28))
                      : SizedBox(width: 0, height: 0),
                  animationEnded
                      ? Expanded(
                          child: Material(
                            color: Colors.transparent,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                FractionallySizedBox(
                                  widthFactor: 1,
                                  child: InkWell(
                                    highlightColor: Colors.transparent,
                                    onTap: () {
                                      setState(() {
                                        _selectedFilter = _Filter.date;
                                        Navigator.of(context).pop(_Filter.date);
                                      });
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        RichText(
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: S.of(context).orderDate,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'Lato',
                                                  fontFamilyFallback: <String>[
                                                    'SF Pro Text'
                                                  ],
                                                  fontSize:
                                                      ScreenUtil().setSp(9),
                                                  fontWeight: FontWeight.w900,
                                                  color: Color(0xff011b7b),
                                                  decoration:
                                                      TextDecoration.none,
                                                ),
                                              ),
                                              TextSpan(text: ' '),
                                              TextSpan(
                                                text: S.of(context).latestFirst,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'Lato',
                                                  fontFamilyFallback: <String>[
                                                    'SF Pro Text'
                                                  ],
                                                  fontSize:
                                                      ScreenUtil().setSp(9),
                                                  fontWeight: FontWeight.w500,
                                                  color: Color(0xff011b7b),
                                                  decoration:
                                                      TextDecoration.none,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        _selectedFilter == _Filter.date
                                            ? Image(
                                                image: AssetImage(
                                                    'assets/images/checkmark-icon.png'),
                                                width:
                                                    ScreenUtil().setWidth(12),
                                                height:
                                                    ScreenUtil().setHeight(8),
                                              )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                ),
                                FractionallySizedBox(
                                  widthFactor: 1,
                                  child: InkWell(
                                    highlightColor: Colors.transparent,
                                    onTap: () {
                                      setState(() {
                                        _selectedFilter = _Filter.companyName;
                                        Navigator.of(context)
                                            .pop(_Filter.companyName);
                                      });
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        RichText(
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: S.of(context).company,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'Lato',
                                                  fontFamilyFallback: <String>[
                                                    'SF Pro Text'
                                                  ],
                                                  fontSize:
                                                      ScreenUtil().setSp(9),
                                                  fontWeight: FontWeight.w900,
                                                  color: Color(0xff011b7b),
                                                  decoration:
                                                      TextDecoration.none,
                                                ),
                                              ),
                                              TextSpan(text: ' '),
                                              TextSpan(
                                                text: S.of(context).highestAz,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'Lato',
                                                  fontFamilyFallback: <String>[
                                                    'SF Pro Text'
                                                  ],
                                                  fontSize:
                                                      ScreenUtil().setSp(9),
                                                  fontWeight: FontWeight.w500,
                                                  color: Color(0xff011b7b),
                                                  decoration:
                                                      TextDecoration.none,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        _selectedFilter == _Filter.companyName
                                            ? Image(
                                                image: AssetImage(
                                                    'assets/images/checkmark-icon.png'),
                                                width:
                                                    ScreenUtil().setWidth(12),
                                                height:
                                                    ScreenUtil().setHeight(8),
                                              )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                ),
                                FractionallySizedBox(
                                  widthFactor: 1,
                                  child: InkWell(
                                    highlightColor: Colors.transparent,
                                    onTap: () {
                                      setState(() {
                                        _selectedFilter = _Filter.priceAsc;
                                        Navigator.of(context)
                                            .pop(_Filter.priceAsc);
                                      });
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        RichText(
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: S.of(context).price,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'Lato',
                                                  fontFamilyFallback: <String>[
                                                    'SF Pro Text'
                                                  ],
                                                  fontSize:
                                                      ScreenUtil().setSp(9),
                                                  fontWeight: FontWeight.w900,
                                                  color: Color(0xff011b7b),
                                                  decoration:
                                                      TextDecoration.none,
                                                ),
                                              ),
                                              TextSpan(text: ' '),
                                              TextSpan(
                                                text: S.of(context).lowestFirst,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'Lato',
                                                  fontFamilyFallback: <String>[
                                                    'SF Pro Text'
                                                  ],
                                                  fontSize:
                                                      ScreenUtil().setSp(9),
                                                  fontWeight: FontWeight.w500,
                                                  color: Color(0xff011b7b),
                                                  decoration:
                                                      TextDecoration.none,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        _selectedFilter == _Filter.priceAsc
                                            ? Image(
                                                image: AssetImage(
                                                    'assets/images/checkmark-icon.png'),
                                                width:
                                                    ScreenUtil().setWidth(12),
                                                height:
                                                    ScreenUtil().setHeight(8),
                                              )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                ),
                                FractionallySizedBox(
                                  widthFactor: 1,
                                  child: InkWell(
                                    highlightColor: Colors.transparent,
                                    onTap: () {
                                      setState(() {
                                        _selectedFilter = _Filter.priceDesc;
                                        Navigator.of(context)
                                            .pop(_Filter.priceDesc);
                                      });
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        RichText(
                                          text: TextSpan(
                                            children: <TextSpan>[
                                              TextSpan(
                                                text: S.of(context).price,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'Lato',
                                                  fontFamilyFallback: <String>[
                                                    'SF Pro Text'
                                                  ],
                                                  fontSize:
                                                      ScreenUtil().setSp(9),
                                                  fontWeight: FontWeight.w900,
                                                  color: Color(0xff011b7b),
                                                  decoration:
                                                      TextDecoration.none,
                                                ),
                                              ),
                                              TextSpan(text: ' '),
                                              TextSpan(
                                                text:
                                                    S.of(context).highestFirst,
                                                style: TextStyle(
                                                  fontFamily:
                                                      application.locale == 'ar'
                                                          ? 'Cairo'
                                                          : 'Lato',
                                                  fontFamilyFallback: <String>[
                                                    'SF Pro Text'
                                                  ],
                                                  fontSize:
                                                      ScreenUtil().setSp(9),
                                                  fontWeight: FontWeight.w500,
                                                  color: Color(0xff011b7b),
                                                  decoration:
                                                      TextDecoration.none,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        _selectedFilter == _Filter.priceDesc
                                            ? Image(
                                                image: AssetImage(
                                                    'assets/images/checkmark-icon.png'),
                                                width:
                                                    ScreenUtil().setWidth(12),
                                                height:
                                                    ScreenUtil().setHeight(8),
                                              )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      : SizedBox(width: 0, height: 0),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

enum _Filter {
  date,
  companyName,
  priceAsc,
  priceDesc,
}

class _Review {
  double rating;
  String review;

  _Review(this.rating, this.review);
}
