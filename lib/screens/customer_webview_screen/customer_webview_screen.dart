import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/customer_store.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';
import 'package:weash_cars/widgets/customer_drawer.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CustomerWebviewScreen extends StatefulWidget {
  final String url;

  CustomerWebviewScreen({this.url});

  @override
  _CustomerWebviewScreenState createState() => _CustomerWebviewScreenState();
}

class _CustomerWebviewScreenState extends State<CustomerWebviewScreen> {
  CustomerStore _customer;
  bool _isDialogVisible = false;

  @override
  void didChangeDependencies() {
    if (_customer == null) {
      final applicationStore = Provider.of<ApplicationStore>(context);
      _customer = applicationStore.customerStore;
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CustomerDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _customer.avatarUrl,
        ),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Observer(
              builder: (BuildContext context) => CustomAppBar2(
                onOpenDrawer: () {
                  setState(() {
                    _isDialogVisible = true;
                    Scaffold.of(context).openDrawer();
                  });
                },
                title: S.of(context).dashboard,
                transparentBackground: true,
                titleColor: Color(0xFF91A6D9),
                buttonColor: Color(0xFF0930C3),
                nbNotifications: 0,
                avatar: _customer.avatarUrl == null
                    ? null
                    : NetworkImage(_customer.avatarUrl),
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(29.5)),
            Expanded(
              child: WebView(
                initialUrl: widget.url,
                navigationDelegate: (NavigationRequest request) {
                  if (request.url == widget.url) {
                    return NavigationDecision.navigate;
                  }

                  canLaunch(request.url).then((value) {
                    if (value) {
                      launch(request.url);
                    }
                  });

                  return NavigationDecision.prevent;
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
