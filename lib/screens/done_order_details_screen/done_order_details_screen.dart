import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:sqflite/utils/utils.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/payment_method.dart';
import 'package:weash_cars/models/vehicle_type.dart';
import 'package:weash_cars/screens/company_dashboard_screen/widgets/incomes_chart.dart';
import 'package:weash_cars/services/IncomesService.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/balance_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/stores/orders_store.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class DoneOrderDetailsScreen extends StatefulWidget {
  final Map<String, dynamic> order;

  DoneOrderDetailsScreen({Key key, this.order}) : super(key: key);

  @override
  _DoneOrderDetailsScreenState createState() => _DoneOrderDetailsScreenState();
}

class _DoneOrderDetailsScreenState extends State<DoneOrderDetailsScreen> {
  CompanyStore _company;

  bool _isDialogVisible = false;
  bool _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_company == null) {
      final applicationStore = Provider.of<ApplicationStore>(context);
      _company = applicationStore.companyStore;
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CompanyDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _company.avatarUrl,
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image(
                image:
                    AssetImage('assets/images/company-screens-background.png'),
                matchTextDirection: true,
                fit: BoxFit.fill),
          ),
          Positioned(
            top: 0,
            child: Observer(
              builder: (BuildContext context) => CustomAppBar2(
                onOpenDrawer: () {
                  setState(() {
                    _isDialogVisible = true;
                    Scaffold.of(context).openDrawer();
                  });
                },
                title: S.of(context).dashboard,
                transparentBackground: true,
                titleColor: Color(0xFF91A6D9),
                buttonColor: Color(0xFF0930C3),
                nbNotifications: 0,
                avatar: _company.avatarUrl == null
                    ? null
                    : NetworkImage(_company.avatarUrl),
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().setHeight(140),
            bottom: 0,
            left: ScreenUtil().setWidth(30),
            right: ScreenUtil().setWidth(30),
            child: Builder(
              builder: (BuildContext context) {
                final order = widget.order;
                if (order == null) {
                  return CupertinoActivityIndicator(
                    animating: true,
                  );
                }

                final orderLocality = order['locality'] ?? ' ';
                final orderAddress = order['address'];
                final orderDate = order['date'] as DateTime;
                final orderNumber = order['order_number'] as int;
                final orderNumberStr =
                    List.generate(6 - orderNumber.toString().length, (_) => '0')
                            .join() +
                        orderNumber.toString();

                final customer = order['customer'];
                final customerAvatarUrl = customer['avatar_url'];
                final customerFirstName = customer['first_name'];
                final customerLastName = customer['last_name'];
                final customerPhoneNumber = customer['phone_number'];

                final package = order['package'];
                final packageTitle = package['title'];
                final packagePrice = package['price'] as double;
                final packageCurrency = _company.currency;
                final vehicleTypes = {
                  "sedan": S.of(context).sedan,
                  "motorcycle": S.of(context).motorcycle,
                  "suv": S.of(context).suv,
                  "boat": S.of(context).boat,
                };
                final vehicleType = vehicleTypes[package['vehicle_type']];

                final paymentMethodStrs = {
                  PaymentMethod.cash: S.of(context).Cashondelivery,
                  PaymentMethod.visa: S.of(context).online,
                };
                final paymentMethod =
                    paymentMethodStrs[order['payment_method'] as PaymentMethod];

                String employeeName;
                final assignedEmployeeId = order['assigned_employee_id'];
                if (assignedEmployeeId == _company.companyId) {
                  employeeName = _company.companyName;
                } else {
                  final assignedEmployee = _company.employees.firstWhere(
                      (Map<String, dynamic> employee) =>
                          employee['employee_id'] == assignedEmployeeId,
                      orElse: () {
                    print("No result");
                    return null;
                  });
                  if (assignedEmployee == null) {
                    return CupertinoActivityIndicator(
                      animating: true,
                    );
                  }
                  employeeName =
                      "${assignedEmployee['first_name']} ${assignedEmployee['last_name']}";
                }

                final rating = order['rating'] as double;
                final review = order['review'];
                final pictureUrls = order['pictures_urls'] == null
                    ? []
                    : order['pictures_urls']
                        .where((url) => url != null)
                        .toList();

                return ListView(
                  shrinkWrap: true,
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: ScreenUtil().setWidth(64.41),
                          height: ScreenUtil().setWidth(64.41),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: customerAvatarUrl == null
                                  ? AssetImage('assets/images/avatar-user.png')
                                  : NetworkImage(customerAvatarUrl),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(12.8)),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: ScreenUtil().setHeight(7)),
                            Text(
                              "$customerFirstName $customerLastName",
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontSize: 13,
                                fontWeight: FontWeight.w700,
                                color: Color(0xFF021A89),
                              ),
                            ),
                            Text(
                              customerPhoneNumber,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Compact Display',
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                                color: Color(0xFF021A89).withOpacity(0.98),
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(4)),
//                            Text(
//                              orderLocality,
//                              style: TextStyle(
//                                fontFamily: application.locale == 'ar'
//                                    ? 'Cairo'
//                                    : 'SF Pro Text',
//                                fontSize: 13,
//                                fontWeight: FontWeight.w300,
//                                color: Color(0xFF021A89),
//                              ),
//                            ),
                            Text(
                              "${S.of(context).orderN}     $orderNumberStr",
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Display',
                                fontSize: 10,
                                fontWeight: FontWeight.w900,
                                color: Color(0xFF021A89),
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(17)),
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Image(
                                  image: AssetImage(
                                      'assets/images/location-icon.png'),
                                  width: ScreenUtil().setWidth(13.42),
                                  fit: BoxFit.fill,
                                  color: Color(0xFFA0AEE6),
                                ),
                                SizedBox(width: ScreenUtil().setWidth(9.2)),
                                Flexible(
                                  fit: FlexFit.loose,
                                  child: SizedBox(
                                    width: ScreenUtil().setWidth(191),
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Text(
                                            orderAddress,
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Compact Display',
                                              fontSize: 12,
                                              fontWeight: FontWeight.w600,
                                              color: Color(0xFF021A89)
                                                  .withOpacity(0.98),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(49.6)),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Row(
                                children: <Widget>[
                                  Image(
                                    image: AssetImage(
                                        'assets/images/vehicle-type-icon.png'),
                                    width: ScreenUtil().setWidth(18),
                                    color: Color(0xFFA0AEE6),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(16)),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        S
                                            .of(context)
                                            .vehicleType
                                            .replaceFirst(':', ''),
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Compact Display',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w700,
                                          color: Color(0xFF021A89)
                                              .withOpacity(0.98),
                                        ),
                                      ),
                                      Text(
                                        vehicleType,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Compact Display',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFF021A89)
                                              .withOpacity(0.98),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Row(
                                children: <Widget>[
                                  Image(
                                    image: AssetImage(
                                        'assets/images/calendar-icon.png'),
                                    width: ScreenUtil().setWidth(18),
                                    color: Color(0xFFA0AEE6),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(16)),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        S
                                            .of(context)
                                            .dateAndTime
                                            .replaceFirst(':', ''),
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Compact Display',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w700,
                                          color: Color(0xFF021A89)
                                              .withOpacity(0.98),
                                        ),
                                      ),
                                      SizedBox(
                                        width: ScreenUtil().setWidth(120),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Text(
                                                DateFormat('dd/MM/y')
                                                        .format(orderDate) +
                                                    " ${S.of(context).at} " +
                                                    DateFormat('Hm')
                                                        .format(orderDate),
                                                style: TextStyle(
                                                  fontFamily: application
                                                              .locale ==
                                                          'ar'
                                                      ? 'Cairo'
                                                      : 'SF Compact Display',
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w600,
                                                  color: Color(0xFF021A89)
                                                      .withOpacity(0.98),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(19)),
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Row(
                                children: <Widget>[
                                  Image(
                                    image: AssetImage(
                                        'assets/images/package-icon.png'),
                                    width: ScreenUtil().setWidth(16.84),
                                    color: Color(0xFFA0AEE6),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(16)),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        S
                                            .of(context)
                                            .package
                                            .replaceFirst(':', ''),
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Compact Display',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w700,
                                          color: Color(0xFF021A89)
                                              .withOpacity(0.98),
                                        ),
                                      ),
                                      Text(
                                        packageTitle,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Compact Display',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFF021A89)
                                              .withOpacity(0.98),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Row(
                                children: <Widget>[
                                  Image(
                                    image: AssetImage(
                                        'assets/images/price-icon.png'),
                                    width: ScreenUtil().setWidth(17.04),
                                    color: Color(0xFFA0AEE6),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(16)),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        S.of(context).Price,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Compact Display',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w700,
                                          color: Color(0xFF021A89)
                                              .withOpacity(0.98),
                                        ),
                                      ),
                                      Text(
                                        "${packagePrice.toStringAsFixed(3)} $packageCurrency",
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Compact Display',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFF021A89)
                                              .withOpacity(0.98),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(19)),
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Row(
                                children: <Widget>[
                                  Image(
                                    image: AssetImage(
                                        'assets/images/payment-method-icon.png'),
                                    width: ScreenUtil().setWidth(22),
                                    color: Color(0xFFA0AEE6),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(16)),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        S.of(context).Paymentmethod,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Compact Display',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w700,
                                          color: Color(0xFF021A89)
                                              .withOpacity(0.98),
                                        ),
                                      ),
                                      Text(
                                        paymentMethod,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Compact Display',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFF021A89)
                                              .withOpacity(0.98),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Row(
                                children: <Widget>[
                                  Image(
                                    image: AssetImage(
                                        'assets/images/non-user-icon.png'),
                                    width: ScreenUtil().setWidth(18.18),
                                    color: Color(0xFFA0AEE6),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(16)),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        S.of(context).AssignedEmployee,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Compact Display',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w700,
                                          color: Color(0xFF021A89)
                                              .withOpacity(0.98),
                                        ),
                                      ),
                                      Text(
                                        employeeName,
                                        style: TextStyle(
                                          fontFamily: application.locale == 'ar'
                                              ? 'Cairo'
                                              : 'SF Compact Display',
                                          fontSize: 12,
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFF021A89)
                                              .withOpacity(0.98),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(46)),
                    Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            SizedBox(width: ScreenUtil().setWidth(36)),
                            Text(
                              S.of(context).Feedback,
                              style: TextStyle(
                                fontFamily: 'SF Compact Display',
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                                color: Color(0xFF021A89).withOpacity(0.98),
                              ),
                            ),
                            Spacer(),
                            StarRating(
                              rating: rating ?? 0,
                              size: ScreenUtil().setWidth(13.29),
                              color: Color(0xFFFFC300),
                            ),
                            SizedBox(width: ScreenUtil().setWidth(11)),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(9.3)),
                        Container(
                          width: ScreenUtil().setWidth(302),
                          height: ScreenUtil().setHeight(89),
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(10),
                            vertical: ScreenUtil().setHeight(8),
                          ),
                          decoration: BoxDecoration(
                            color: Colors.transparent,
                            image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/review-textarea-dashed-border.png'),
                              fit: BoxFit.fill,
                            ),
                          ),
                          child: Text(
                            review ?? S.of(context).noFeedBack,
                            style: TextStyle(
                              fontFamily: 'SF Compact Display',
                              fontSize: 13,
                              fontWeight: FontWeight.w600,
                              color: Color(0xFF021A89),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(28)),
                    SizedBox(
                      height: ScreenUtil().setHeight(91),
                      child: ListView.separated(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: pictureUrls == null ? 0 : pictureUrls.length,
                        separatorBuilder: (BuildContext context, int index) =>
                            SizedBox(width: ScreenUtil().setWidth(14)),
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            width: ScreenUtil().setWidth(91),
                            height: ScreenUtil().setHeight(91),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              image: DecorationImage(
                                image: NetworkImage(pictureUrls[index]),
                                fit: BoxFit.cover,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(50)),
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
