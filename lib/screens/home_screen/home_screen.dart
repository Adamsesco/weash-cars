import 'dart:async';
import 'dart:io';
import 'dart:math' as math;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/stores/application_store.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final Location _locationService = Location();
  ApplicationStore _application;
  String _errorMessage;

  @override
  void didChangeDependencies() {
    if (_application == null) {
      _checkPermissions();
      _application = Provider.of<ApplicationStore>(context);
      FocusScope.of(context).requestFocus(FocusNode());
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/splash_sceen_background.png'),
            fit: BoxFit.fill,
          ),
        ),
        child: _errorMessage == null
            ? Stack(
                fit: StackFit.expand,
                alignment: Alignment.center,
                children: <Widget>[
                  Image(
                    image: AssetImage('assets/images/logo.png'),
                  ),
                  Positioned(
                    bottom: ScreenUtil().setHeight(60),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        // FlatButton(
                        //   onPressed: () async {
                        //     final companiesDocuments = await Firestore.instance
                        //         .collection('users')
                        //         .where('role', isEqualTo: 'company')
                        //         .getDocuments();
                        //     if (companiesDocuments == null ||
                        //         companiesDocuments.documents.isEmpty) {
                        //       return;
                        //     }

                        //     await Future.forEach(companiesDocuments.documents,
                        //         (DocumentSnapshot document) async {
                        //       final companyId = document.documentID;
                        //       final minPrices = <String, double>{
                        //         'sedan': 0.0,
                        //         'motorcycle': 0.0,
                        //         'suv': 0.0,
                        //         'boat': 0.0,
                        //       };

                        //       final packagesDocuments = await Firestore.instance
                        //           .collection('packages')
                        //           .where('company_id', isEqualTo: companyId)
                        //           .getDocuments();
                        //       if (packagesDocuments != null &&
                        //           packagesDocuments.documents.isNotEmpty) {
                        //         final packages = packagesDocuments.documents;

                        //         packages.forEach((package) {
                        //           final data = package.data;

                        //           final vehicleType = data['vehicle_type'];
                        //           final price =
                        //               (data['price'] as num).toDouble();
                        //           final duration = data['duration'] as int;
                        //           final title = data['title'];

                        //           if (vehicleType == null ||
                        //               price == null ||
                        //               price == 0.0 ||
                        //               duration == null ||
                        //               // duration == 0 ||
                        //               title == null) {
                        //             package.reference.delete();

                        //             return;
                        //           }

                        //           final minPrice = minPrices[vehicleType] == 0.0
                        //               ? price
                        //               : math.min(
                        //                   price,
                        //                   minPrices[vehicleType] ??
                        //                       double.infinity);
                        //           minPrices[vehicleType] = minPrice;
                        //         });
                        //       }

                        //       print("$companyId $minPrices");
                        //       await Firestore.instance
                        //           .collection('users')
                        //           .document(companyId)
                        //           .updateData({
                        //         'min_prices': minPrices,
                        //       });
                        //     });

                        //     print('Finished');
                        //   },
                        //   child: Text('Min prices'),
                        // ),
                        //
                        //
                        // FlatButton(
                        //   onPressed: () async {
                        //     final companiesDocuments = await Firestore.instance
                        //         .collection('users')
                        //         .where('role', isEqualTo: 'company')
                        //         .getDocuments();
                        //     if (companiesDocuments == null ||
                        //         companiesDocuments.documents.isEmpty) {
                        //       print('No result');
                        //       return;
                        //     }

                        //     await Future.forEach(companiesDocuments.documents,
                        //         (DocumentSnapshot company) async {
                        //       final companyId = company.documentID;
                        //       final vehicleTypes = <String>{};

                        //       final packagesDocumnets = await Firestore.instance
                        //           .collection('packages')
                        //           .where('company_id', isEqualTo: companyId)
                        //           .getDocuments();
                        //       if (packagesDocumnets != null &&
                        //           packagesDocumnets.documents.isNotEmpty) {
                        //         final packages = packagesDocumnets.documents;

                        //         packages.forEach((DocumentSnapshot package) {
                        //           final vehicleType =
                        //               package.data['vehicle_type'] as String;
                        //           if (vehicleType != null &&
                        //               vehicleType.isNotEmpty) {
                        //             vehicleTypes.add(vehicleType);
                        //           }
                        //         });
                        //       }

                        //       print(
                        //           "Company: ${company.data['company_name']} Vehcile Types: $vehicleTypes");

                        //       await company.reference.updateData({
                        //         'vehicle_types': vehicleTypes.toList(),
                        //       });
                        //     });
                        //   },
                        //   child: Text('Vehicle Types'),
                        // ),
                        //
                        //
                        // FlatButton(
                        //   onPressed: () async {
                        //     final companiesDocuments = await Firestore.instance
                        //         .collection('users')
                        //         .where('role', isEqualTo: 'company')
                        //         .getDocuments();
                        //     if (companiesDocuments == null ||
                        //         companiesDocuments.documents.isEmpty) {
                        //       return;
                        //     }

                        //     await Future.forEach(companiesDocuments.documents,
                        //         (DocumentSnapshot document) async {
                        //       final companyId = document.documentID;
                        //       double minPrice = 0.0;

                        //       final packagesDocuments = await Firestore.instance
                        //           .collection('packages')
                        //           .where('company_id', isEqualTo: companyId)
                        //           .getDocuments();
                        //       if (packagesDocuments != null &&
                        //           packagesDocuments.documents.isNotEmpty) {
                        //         final packages = packagesDocuments.documents;

                        //         minPrice = packages.fold(double.maxFinite,
                        //             (double min, DocumentSnapshot package) {
                        //           final data = package.data;
                        //           final price =
                        //               (data['price'] as num).toDouble();

                        //           return price < min ? price : min;
                        //         });

                        //         await Firestore.instance
                        //             .collection('users')
                        //             .document(companyId)
                        //             .updateData({
                        //           'min_price': minPrice,
                        //         });
                        //       }
                        //     });

                        //     print('Finished');
                        //   },
                        //   child: Text('Min price'),
                        // ),
//                        FlatButton(
//                          onPressed: () async {
//                            final companiesDocuments = await Firestore.instance
//                                .collection('users')
//                                .where('role', isEqualTo: 'company')
//                                .getDocuments();
//                            if (companiesDocuments == null ||
//                                companiesDocuments.documents.isEmpty) {
//                              return;
//                            }
//
//                            await Future.forEach(companiesDocuments.documents,
//                                (DocumentSnapshot document) async {
//                              final companyId = document.documentID;
//                              final totalRatings = 5.0;
//                              final ratingsCount = 1;
//
//                              await Firestore.instance
//                                  .collection('users')
//                                  .document(companyId)
//                                  .updateData({
//                                'total_ratings': totalRatings,
//                                'ratings_count': ratingsCount,
//                              });
//                            });
//
//                            print('Finished');
//                          },
//                          child: Text('Ratings'),
//                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, '/orders/new');
                          },
                          enableFeedback: false,
                          child: Container(
                            width: ScreenUtil().setWidth(170.16),
                            height: ScreenUtil().setHeight(40.2),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              gradient: LinearGradient(
                                colors: [Color(0xFF0026A1), Color(0xFF000F66)],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black.withOpacity(0.4),
                                  offset: Offset(0, ScreenUtil().setHeight(3)),
                                  blurRadius: 6,
                                ),
                              ],
                            ),
                            child: Text(
                              S.of(context).quickOrder,
                              style: TextStyle(
                                fontFamily: _application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 11,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(15)),
                        InkWell(
                          onTap: () async {
                            Navigator.pushNamed(context, '/login');
                          },
                          child: Container(
                            padding: EdgeInsets.all(ScreenUtil().setWidth(8)),
                            child: RichText(
                              text: TextSpan(
                                style: TextStyle(
                                  fontFamily: _application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontFamilyFallback: <String>['SF Pro Text'],
                                  fontSize: 13,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xFF042698),
                                  decoration: TextDecoration.none,
                                ),
                                children: <TextSpan>[
                                  TextSpan(
                                    text: S.of(context).alreadyHaveAccount,
                                  ),
                                  TextSpan(text: ' '),
                                  TextSpan(
                                    text: S.of(context).login,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        // SizedBox(height: ScreenUtil().setHeight(10)),
                        InkWell(
                          onTap: () async {
                            Navigator.pushNamed(context, '/joinus');
                          },
                          child: Container(
                            padding: EdgeInsets.all(ScreenUtil().setWidth(8)),
                            child: RichText(
                              text: TextSpan(
                                style: TextStyle(
                                  fontFamily: _application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF Pro Text',
                                  fontSize: 13,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xFF042698),
                                  decoration: TextDecoration.none,
                                ),
                                children: <TextSpan>[
                                  TextSpan(text: S.of(context).areYouCompany),
                                  TextSpan(text: ' '),
                                  TextSpan(
                                    text: S.of(context).joinUs,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            : Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(20),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: Colors.red,
                      size: ScreenUtil().setHeight(60),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(20)),
                    Text(
                      _errorMessage,
                      style: TextStyle(
                        fontFamily: _application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Text',
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  Future<void> _checkPermissions() async {
    final serviceEnabled = await _checkLocationService();
    if (serviceEnabled) {
      final locationPermission = await _checkLocationPermission();
      if (locationPermission) {
        await _checkConnectivity();
      }
    }
  }

  Future<bool> _checkLocationService() async {
    await _locationService.changeSettings(
        accuracy: LocationAccuracy.NAVIGATION);

    try {
      bool serviceStatus = await _locationService.serviceEnabled();
      if (!serviceStatus) {
        bool serviceStatusResult = await _locationService.requestService();
        if (!serviceStatusResult) {
          _setErrorMessage(S.of(context).enableLocationService);
          return false;
        } else {
          return true;
        }
      } else {
        return true;
      }
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        _setErrorMessage(S.of(context).allowLocation);
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        _setErrorMessage(S.of(context).enableLocationService);
      } else {
        _setErrorMessage(S.of(context).allowLocation);
      }

      return false;
    }
  }

  Future<bool> _checkLocationPermission() async {
    await _locationService.changeSettings(accuracy: LocationAccuracy.LOW);

    try {
      await _locationService.getLocation();

      final permission = await _locationService.requestPermission();
      if (!permission) {
        _setErrorMessage(S.of(context).allowLocation);
      }
      return permission;
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        _setErrorMessage(S.of(context).allowLocation);
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        _setErrorMessage(S.of(context).enableLocationService);
      } else {
        _setErrorMessage(S.of(context).allowLocation);
      }

      return false;
    }
  }

  Future<bool> _checkConnectivity() async {
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        _setErrorMessage(S.of(context).connectToInternet);
        return false;
      }
    } on SocketException catch (_) {
      _setErrorMessage(S.of(context).connectToInternet);
      return false;
    }
  }

  void _setErrorMessage(String message) {
    if (mounted) {
      setState(() {
        _errorMessage = message;
      });
    } else {
      _errorMessage = message;
    }
  }
}
