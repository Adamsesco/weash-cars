import 'dart:io';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class AddNewEmployeeScreen extends StatefulWidget {
  @override
  _AddNewEmployeeScreenState createState() => _AddNewEmployeeScreenState();
}

class _AddNewEmployeeScreenState extends State<AddNewEmployeeScreen> {
  CompanyStore _company;

  GlobalKey<FormState> _formKey = new GlobalKey();
  FocusNode _lastNameFocusNode = new FocusNode();
  FocusNode _phoneNumberFocusNode = new FocusNode();

  String _firstName;
  String _lastName;
  String _phoneNumber;
  File _avatarFile;
  String _employeeType = 'classic';

  bool _isDialogVisible = false;
  bool _isLoading = false;

  @override
  void didChangeDependencies() {
    final applicationStore = Provider.of<ApplicationStore>(context);
    _company = applicationStore.companyStore;

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CompanyDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _company.avatarUrl,
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image(
                image:
                    AssetImage('assets/images/company-screens-background.png'),
                matchTextDirection: true,
                fit: BoxFit.fill),
          ),
          Positioned(
            top: 0,
            child: Observer(
              builder: (BuildContext context) => CustomAppBar2(
                onOpenDrawer: () {
                  setState(() {
                    _isDialogVisible = true;
                    Scaffold.of(context).openDrawer();
                  });
                },
                title: S.of(context).dashboard,
                transparentBackground: true,
                titleColor: Color(0xFF91A6D9),
                buttonColor: Color(0xFF0930C3),
                nbNotifications: 0,
                avatar: _company.avatarUrl == null
                    ? null
                    : NetworkImage(_company.avatarUrl),
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil().setHeight(184),
            bottom: 0,
            child: SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height -
                //     ScreenUtil().setHeight(184),
                child: Column(
                  // shrinkWrap: true,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Align(
                      alignment: application.locale == 'ar'
                          ? Alignment.topRight
                          : Alignment.topLeft,
                      child: Container(
                        margin: EdgeInsets.only(
                          left: application.locale == 'ar'
                              ? 0
                              : ScreenUtil().setWidth(25),
                          right: application.locale == 'ar'
                              ? ScreenUtil().setWidth(25)
                              : 0,
                        ),
                        child: Text(
                          S.of(context).addEmployee,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Display',
                            fontFamilyFallback: <String>['SF Pro Text'],
                            fontSize: 24,
                            fontWeight: FontWeight.w900,
                            color: Color(0xFFA0AEE6),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(53)),
                    Align(
                      alignment: application.locale == 'ar'
                          ? Alignment.topRight
                          : Alignment.topLeft,
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: _pickPhoto,
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Container(
                            width: ScreenUtil().setWidth(90),
                            height: ScreenUtil().setWidth(90),
                            margin: EdgeInsets.only(
                              left: application.locale == 'ar'
                                  ? 0
                                  : ScreenUtil().setWidth(45),
                              right: application.locale == 'ar'
                                  ? ScreenUtil().setWidth(45)
                                  : 0,
                            ),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color(0xFFF0F0F0),
                              image: _avatarFile == null
                                  ? null
                                  : DecorationImage(
                                      image: FileImage(_avatarFile),
                                      fit: BoxFit.cover,
                                    ),
                            ),
                            child: Center(
                              child: Image(
                                image:
                                    AssetImage('assets/images/camera-icon.png'),
                                width: ScreenUtil().setWidth(34.28),
                                height: ScreenUtil().setHeight(28.87),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(41)),
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(44)),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              S.of(context).firstName,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                color: Color(0xff021A89),
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(6)),
                            TextFormField(
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              textCapitalization: TextCapitalization.words,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 15,
                                fontWeight: FontWeight.w700,
                                color: Color(0xff021A89),
                              ),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(0),
                                  right: ScreenUtil().setWidth(0),
                                  bottom: ScreenUtil().setHeight(1.5),
                                ),
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xffC5D4F8),
                                    width: 0.3,
                                  ),
                                ),
                              ),
                              validator: (String val) {
                                if (val.length < 2) {
                                  return S.of(context).firstNameError;
                                }
                              },
                              onFieldSubmitted: (_) => FocusScope.of(context)
                                  .requestFocus(_lastNameFocusNode),
                              onSaved: (String val) => _firstName = val,
                            ),
                            SizedBox(height: ScreenUtil().setHeight(23.5)),
                            Text(
                              S.of(context).lastName,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                color: Color(0xff021A89),
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(6)),
                            TextFormField(
                              focusNode: _lastNameFocusNode,
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              textCapitalization: TextCapitalization.words,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 15,
                                fontWeight: FontWeight.w700,
                                color: Color(0xff021A89),
                              ),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(0),
                                  right: ScreenUtil().setWidth(0),
                                  bottom: ScreenUtil().setHeight(1.5),
                                ),
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xffC5D4F8),
                                    width: 0.3,
                                  ),
                                ),
                              ),
                              validator: (String val) {
                                if (val.length < 2) {
                                  return S.of(context).lastNameError;
                                }
                              },
                              onFieldSubmitted: (_) => FocusScope.of(context)
                                  .requestFocus(_phoneNumberFocusNode),
                              onSaved: (String val) => _lastName = val,
                            ),
                            SizedBox(height: ScreenUtil().setHeight(23.5)),
                            Text(
                              S.of(context).phoneNumber,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                                color: Color(0xff021A89),
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(6)),
                            TextFormField(
                              focusNode: _phoneNumberFocusNode,
                              keyboardType: TextInputType.phone,
                              textInputAction: TextInputAction.done,
                              textDirection: TextDirection.ltr,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF Pro Text',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: 15,
                                fontWeight: FontWeight.w700,
                                color: Color(0xff021A89),
                              ),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.only(
                                  left: ScreenUtil().setWidth(0),
                                  right: ScreenUtil().setWidth(0),
                                  bottom: ScreenUtil().setHeight(1.5),
                                ),
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0xffC5D4F8),
                                    width: ScreenUtil().setHeight(0.3),
                                  ),
                                ),
                              ),
                              validator: (String val) {
                                var pattern = r"^\+[0-9]{10,}$";
                                var regexp = new RegExp(pattern);
                                if (!regexp.hasMatch(val)) {
                                  return S.of(context).phoneNumberError;
                                }
                              },
                              onFieldSubmitted: (_) {
                                _addEmployee();
                                // FocusScope.of(context)
                                //     .requestFocus(FocusNode());
                              },
                              onSaved: (String val) => _phoneNumber = val,
                            ),
                            // SizedBox(height: ScreenUtil().setHeight(22)),
                            // Container(
                            //   margin: EdgeInsets.symmetric(
                            //       horizontal: ScreenUtil().setWidth(42.3)),
                            //   child: Row(
                            //     mainAxisAlignment:
                            //         MainAxisAlignment.spaceBetween,
                            //     children: <Widget>[
                            //       InkWell(
                            //         onTap: () => setState(
                            //             () => _employeeType = 'classic'),
                            //         enableFeedback: false,
                            //         child: Row(
                            //           children: <Widget>[
                            //             Container(
                            //               width: ScreenUtil().setWidth(12),
                            //               height: ScreenUtil().setWidth(12),
                            //               alignment: Alignment.center,
                            //               decoration: BoxDecoration(
                            //                 shape: BoxShape.circle,
                            //                 border: Border.all(
                            //                   color: Color(0xFF021A89),
                            //                   width: 1,
                            //                 ),
                            //               ),
                            //               child: Visibility(
                            //                 visible: _employeeType == 'classic',
                            //                 child: Container(
                            //                   width: ScreenUtil().setWidth(7),
                            //                   height: ScreenUtil().setWidth(7),
                            //                   decoration: BoxDecoration(
                            //                     shape: BoxShape.circle,
                            //                     color: Color(0xFF07D80F),
                            //                   ),
                            //                 ),
                            //               ),
                            //             ),
                            //             SizedBox(
                            //                 width: ScreenUtil().setWidth(5)),
                            //             Text(
                            //               'Classic',
                            //               style: TextStyle(
                            //                 fontFamily: 'SF Pro Text',
                            //                 fontSize: 15,
                            //                 fontWeight: FontWeight.w600,
                            //                 color: Color(0xFF021A89),
                            //               ),
                            //             ),
                            //           ],
                            //         ),
                            //       ),
                            //       InkWell(
                            //         onTap: () =>
                            //             setState(() => _employeeType = 'smart'),
                            //         enableFeedback: false,
                            //         child: Row(
                            //           children: <Widget>[
                            //             Container(
                            //               width: ScreenUtil().setWidth(12),
                            //               height: ScreenUtil().setWidth(12),
                            //               alignment: Alignment.center,
                            //               decoration: BoxDecoration(
                            //                 shape: BoxShape.circle,
                            //                 border: Border.all(
                            //                   color: Color(0xFF021A89),
                            //                   width: 1,
                            //                 ),
                            //               ),
                            //               child: Visibility(
                            //                 visible: _employeeType == 'smart',
                            //                 child: Container(
                            //                   width: ScreenUtil().setWidth(7),
                            //                   height: ScreenUtil().setWidth(7),
                            //                   decoration: BoxDecoration(
                            //                     shape: BoxShape.circle,
                            //                     color: Color(0xFF07D80F),
                            //                   ),
                            //                 ),
                            //               ),
                            //             ),
                            //             SizedBox(
                            //                 width: ScreenUtil().setWidth(5)),
                            //             Text(
                            //               'Smart',
                            //               style: TextStyle(
                            //                 fontFamily:
                            //                     application.locale == 'ar'
                            //                         ? 'Cairo'
                            //                         : 'SF Pro Text',
                            //                 fontSize: 15,
                            //                 fontWeight: FontWeight.w600,
                            //                 color: Color(0xFF021A89),
                            //               ),
                            //             ),
                            //           ],
                            //         ),
                            //       ),
                            //     ],
                            //   ),
                            // ),
                            SizedBox(height: ScreenUtil().setHeight(22.8)),
                            Container(
                              height: 0.3,
                              margin: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(42.3)),
                              color: Color(0xFFC5D4F8),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(51.5)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        InkWell(
                          onTap: _isLoading
                              ? null
                              : () {
                                  _addEmployee();
                                  // FocusScope.of(context)
                                  //     .requestFocus(FocusNode());
                                },
                          borderRadius: BorderRadius.circular(20),
                          child: Container(
                            width: ScreenUtil().setWidth(170.16),
                            height: ScreenUtil().setHeight(40.2),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              gradient: LinearGradient(
                                colors: <Color>[
                                  Color(0xff031C8D),
                                  Color(0xff0A2FAF)
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                              ),
                            ),
                            child: _isLoading
                                ? CupertinoActivityIndicator(
                                    animating: true,
                                  )
                                : Text(
                                    S
                                        .of(context)
                                        .addEmployee
                                        .replaceAll("\n", ' '),
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF UI Display',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: ScreenUtil().setSp(11),
                                      fontWeight: FontWeight.w700,
                                      color: Color(0xfffcfcfc),
                                    ),
                                  ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(20)),
                  ],
                ),
              ),
            ),
          ),
          // Visibility(
          //   visible: _isDialogVisible,
          //   child: Positioned.fill(
          //     child: BackdropFilter(
          //       filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          //       child: Container(
          //         color: Colors.grey.withOpacity(0.2),
          //       ),
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }

  Future<void> _pickPhoto() async {
    final imageFile = await showCupertinoModalPopup<File>(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        title: Text(S.of(context).changeProfilePicture),
        actions: <Widget>[
          CupertinoActionSheetAction(
            onPressed: () async {
              final file =
                  await ImagePicker.pickImage(source: ImageSource.camera);
              Navigator.of(context).pop(file);
            },
            child: Text(S.of(context).takeNewPhoto),
          ),
          CupertinoActionSheetAction(
            onPressed: () async {
              final file =
                  await ImagePicker.pickImage(source: ImageSource.gallery);
              Navigator.of(context).pop(file);
            },
            child: Text(S.of(context).chooseExistingPhoto),
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(
            S.of(context).Cancel,
            style: TextStyle(
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
    );

    setState(() {
      _avatarFile = imageFile;
    });
  }

  bool _saveForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> _addEmployee() async {
    if (_saveForm()) {
      setState(() {
        _isLoading = true;
      });

      final existingAccount = await Firestore.instance
          .collection('users')
          .where('phone_number', isEqualTo: _phoneNumber)
          .getDocuments();
      if (existingAccount != null && existingAccount.documents.isNotEmpty) {
        setState(() {
          _isLoading = false;
        });

        showCupertinoDialog(
          context: context,
          builder: (context) => CupertinoAlertDialog(
            content: Text(S.of(context).phoneNumberExists),
            actions: <Widget>[
              CupertinoDialogAction(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(S.of(context).ok),
              ),
            ],
          ),
        );

        return;
      }

      final employee = {
        'role': 'employee',
        'company_id': _company.companyId,
        'first_name': _firstName,
        'last_name': _lastName,
        'phone_number': _phoneNumber,
        'jobs_done': 0,
        'type': _employeeType,
        'is_available': true,
        // TODO: add cloud function to generate creation date
        'created_at': Timestamp.fromDate(DateTime.now()),
      };

      final employeeDocument =
          await Firestore.instance.collection('users').add(employee);

      String imageUrl;

      if (_avatarFile != null) {
        final extension =
            _avatarFile.path.substring(_avatarFile.path.lastIndexOf('.'));
        final storageRef = FirebaseStorage.instance
            .ref()
            .child('profile_pictures')
            .child("${employeeDocument.documentID}.$extension");

        final StorageUploadTask uploadTask = storageRef.putFile(
          _avatarFile,
          StorageMetadata(
            contentType: "image/$extension",
            customMetadata: <String, String>{
              'type': 'Profile Picture',
              'user_id': employeeDocument.documentID,
            },
          ),
        );

        final downloadUrl = await uploadTask.onComplete;
        imageUrl = await downloadUrl.ref.getDownloadURL();

        await Firestore.instance
            .collection('users')
            .document(employeeDocument.documentID)
            .updateData({
          'avatar_url': imageUrl,
        });
        employee['avatar_url'] = imageUrl;
      }

      employee['employee_id'] = employeeDocument.documentID;
      employee['created_at'] = (employee['created_at'] as Timestamp).toDate();
      _company.employees.add(employee);
      _company.setData(employees: _company.employees);

      setState(() {
        _isLoading = false;
      });

      showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(S.of(context).success),
          content: Text(S.of(context).employeeAdded),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).ok),
            ),
          ],
        ),
      );
    }
  }
}
