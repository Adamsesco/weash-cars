import 'dart:io';

import 'package:flutter/services.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/screens/home_screen/home_screen.dart';
import 'package:weash_cars/stores/application_store.dart';

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  final Location _locationService = Location();
  String _errorMessage;
  bool _didCheckPermissions = false;
  int _currentSlideIndex = 0;

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);
    if (application != null && !application.isFirstRun) {
      return HomeScreen();
    }

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    // TODO: use PageView instead of CarouselSlider.
    CarouselSlider carousel;
    carousel = CarouselSlider(
      height: MediaQuery.of(context).size.height,
      enlargeCenterPage: false,
      viewportFraction: 1.0,
      enableInfiniteScroll: false,
      onPageChanged: (int index) => setState(() => _currentSlideIndex = index),
      items: <Widget>[
        // First slide
        Stack(
          children: <Widget>[
            Positioned.fill(
              child: Image(
                image:
                    AssetImage('assets/images/we-come-to-you-background.png'),
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(
                    image: AssetImage('assets/images/we-come-to-you.png'),
                    width: ScreenUtil().setWidth(258),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(74)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        S.of(context).weComeToYou,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'Montserrat',
                          fontWeight: FontWeight.w700,
                          fontSize: ScreenUtil().setSp(27),
                          color: Colors.white,
                          decoration: TextDecoration.none,
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(17)),
                      Text(
                        S.of(context).atYourHome,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'Montserrat',
                          fontWeight: FontWeight.w400,
                          fontSize: ScreenUtil().setSp(14),
                          color: Colors.white,
                          decoration: TextDecoration.none,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(105)),
                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () => carousel.nextPage(
                          duration: Duration(milliseconds: 300),
                          curve: Curves.linear),
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                        width: ScreenUtil().setWidth(111),
                        height: ScreenUtil().setHeight(41),
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0x120d2e00)),
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Color(0x66000000),
                              offset: Offset(0, ScreenUtil().setHeight(3)),
                            ),
                          ],
                        ),
                        child: Row(
                          children: <Widget>[
                            SizedBox(width: ScreenUtil().setWidth(27)),
                            Text(
                              S.of(context).next,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF UI Display',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: ScreenUtil().setSp(11),
                                fontWeight: FontWeight.w700,
                                color: Color(0xff011e96),
                                letterSpacing: 0.22,
                              ),
                            ),
                            SizedBox(width: ScreenUtil().setWidth(29)),
                            Image(
                              image: AssetImage('assets/images/next-icon.png'),
                              width: ScreenUtil().setWidth(8),
                              height: ScreenUtil().setHeight(14),
                              matchTextDirection: true,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(104)),
                ],
              ),
            ),
          ],
        ),

        // Seconde slide
        Stack(
          children: <Widget>[
            Positioned.fill(
              child: Image(
                image: AssetImage('assets/images/lowest-price-background.png'),
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image(
                    image: AssetImage('assets/images/lowest-price.png'),
                    width: ScreenUtil().setWidth(258),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(88)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        S.of(context).lowestPrice,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'Montserrat',
                          fontWeight: FontWeight.w700,
                          fontFamilyFallback: <String>['SF Pro Text'],
                          fontSize: ScreenUtil().setSp(27),
                          color: Colors.white,
                          decoration: TextDecoration.none,
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(17)),
                      Text(
                        S.of(context).cheaperPrice,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'Montserrat',
                          fontWeight: FontWeight.w400,
                          fontFamilyFallback: <String>['SF Pro Text'],
                          fontSize: ScreenUtil().setSp(14),
                          color: Colors.white,
                          decoration: TextDecoration.none,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(105)),
                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () => carousel.nextPage(
                          duration: Duration(milliseconds: 300),
                          curve: Curves.linear),
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                        width: ScreenUtil().setWidth(111),
                        height: ScreenUtil().setHeight(41),
                        decoration: BoxDecoration(
                          border: Border.all(color: Color(0x120d2e00)),
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Color(0x66000000),
                              offset: Offset(0, ScreenUtil().setHeight(3)),
                            ),
                          ],
                        ),
                        child: Row(
                          children: <Widget>[
                            SizedBox(width: ScreenUtil().setWidth(27)),
                            Text(
                              S.of(context).next,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF UI Display',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: ScreenUtil().setSp(11),
                                fontWeight: FontWeight.w700,
                                color: Color(0xff011e96),
                                letterSpacing: 0.22,
                              ),
                            ),
                            SizedBox(width: ScreenUtil().setWidth(29)),
                            Image(
                              image: AssetImage('assets/images/next-icon.png'),
                              width: ScreenUtil().setWidth(8),
                              height: ScreenUtil().setHeight(14),
                              matchTextDirection: true,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(104)),
                ],
              ),
            ),
          ],
        ),

        // Third slide
        Stack(
          children: <Widget>[
            Positioned.fill(
              child: Image(
                image: AssetImage('assets/images/live-chat-background.png'),
                fit: BoxFit.cover,
              ),
            ),
            if (_errorMessage != null)
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(20),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Container(
                          height: ScreenUtil().setHeight(40),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                          ),
                        ),
                        Icon(
                          Icons.error,
                          color: Colors.red,
                          size: ScreenUtil().setHeight(60),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(20)),
                    Text(
                      _errorMessage,
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Text',
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                        decoration: TextDecoration.none,
                      ),
                    ),
                  ],
                ),
              ),
            if (_errorMessage == null)
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image(
                      image: AssetImage('assets/images/live-chat.png'),
                      height: ScreenUtil().setHeight(236),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(49)),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          S.of(context).liveChat,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'Montserrat',
                            fontWeight: FontWeight.w700,
                            fontFamilyFallback: <String>['SF Pro Text'],
                            fontSize: ScreenUtil().setSp(27),
                            color: Colors.white,
                            decoration: TextDecoration.none,
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(17)),
                        Text(
                          S.of(context).askBefore,
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'Montserrat',
                            fontWeight: FontWeight.w400,
                            fontFamilyFallback: <String>['SF Pro Text'],
                            fontSize: ScreenUtil().setSp(14),
                            color: Colors.white,
                            decoration: TextDecoration.none,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(89)),
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () async {
                          await _setAlreadyRun();
                          if (!_didCheckPermissions) {
                            _didCheckPermissions = true;
                            await _checkPermissions();
                            if (_errorMessage == null) {
                              Navigator.pushNamed(context, '/orders/new');
                            }
                          }
                        },
                        borderRadius: BorderRadius.circular(20),
                        child: Container(
                          width: ScreenUtil().setWidth(170.16),
                          height: ScreenUtil().setHeight(40.2),
                          decoration: BoxDecoration(
                            border: Border.all(color: Color(0x120d2e00)),
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white,
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                color: Color(0x66000000),
                                offset: Offset(0, ScreenUtil().setHeight(3)),
                              ),
                            ],
                          ),
                          child: Center(
                            child: Text(
                              S.of(context).quickOrder,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'SF UI Display',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: ScreenUtil().setSp(11),
                                fontWeight: FontWeight.w700,
                                color: Color(0xff011e96),
                                letterSpacing: 0.22,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(15)),
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () async {
                          await _setAlreadyRun();
                          if (!_didCheckPermissions) {
                            _didCheckPermissions = true;
                            await _checkPermissions();
                            if (_errorMessage == null) {
                              Navigator.pushNamed(context, '/login');
                            }
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.all(ScreenUtil().setWidth(8)),
                          child: Text(
                            "${S.of(context).alreadyHaveAccount} ${S.of(context).login}",
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF UI Display',
                              fontSize: ScreenUtil().setSp(13),
                              fontWeight: FontWeight.w300,
                              color: Colors.white,
                              decoration: TextDecoration.none,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(0)),
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () async {
                          if (!_didCheckPermissions) {
                            _didCheckPermissions = true;
                            await _checkPermissions();
                            if (_errorMessage == null) {
                              Navigator.pushNamed(context, '/joinus');
                            }
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.all(ScreenUtil().setWidth(8)),
                          child: Text(
                            "${S.of(context).areYouCompany} ${S.of(context).joinUs}",
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF UI Display',
                              fontSize: ScreenUtil().setSp(13),
                              fontWeight: FontWeight.w300,
                              color: Colors.white,
                              decoration: TextDecoration.none,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(84)),
                  ],
                ),
              ),
          ],
        ),
      ],
    );

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Positioned.fill(
          child: carousel,
        ),
        Positioned(
          top: ScreenUtil().setHeight(64),
          right: application.locale == 'ar' ? null : ScreenUtil().setWidth(39),
          left: application.locale == 'ar' ? ScreenUtil().setWidth(39) : null,
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () {
                if (_currentSlideIndex < 2) {
                  carousel.animateToPage(2,
                      duration: Duration(milliseconds: 300),
                      curve: Curves.linear);
                } else {
                  Navigator.of(context).pushNamed('/home');
                }
              },
              borderRadius: BorderRadius.circular(12),
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(14),
                  vertical: ScreenUtil().setHeight(8),
                ),
                child: Text(
                  S.of(context).skip,
                  style: TextStyle(
                    fontFamily:
                        application.locale == 'ar' ? 'Cairo' : 'SF UI Display',
                    fontSize: ScreenUtil().setSp(11),
                    fontWeight: FontWeight.w700,
                    color: Color(0xfffcfcfc),
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          top: ScreenUtil().setHeight(747),
          child: SizedBox(
            width: ScreenUtil().setWidth(69),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: List.generate(3, (int index) {
                bool isCurrent = index == _currentSlideIndex;
                return Container(
                  padding: EdgeInsets.all(ScreenUtil().setHeight(5)),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: isCurrent
                        ? Border.all(color: Color(0x88ffffff), width: 2)
                        : null,
                  ),
                  child: Container(
                    width: isCurrent
                        ? ScreenUtil().setHeight(11)
                        : ScreenUtil().setHeight(9),
                    height: isCurrent
                        ? ScreenUtil().setHeight(11)
                        : ScreenUtil().setHeight(9),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: isCurrent ? Colors.white : Color(0x88ffffff),
                    ),
                  ),
                );
              }),
            ),
          ),
        ),
      ],
    );
  }

  Future<void> _setAlreadyRun() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('isFirstRun', false);
  }

  Future<void> _checkPermissions() async {
    final serviceEnabled = await _checkLocationService();
    if (serviceEnabled) {
      final locationPermission = await _checkLocationPermission();
      if (locationPermission) {
        await _checkConnectivity();
      }
    }
  }

  Future<bool> _checkLocationService() async {
    await _locationService.changeSettings(
        accuracy: LocationAccuracy.NAVIGATION);

    try {
      bool serviceStatus = await _locationService.serviceEnabled();
      if (!serviceStatus) {
        bool serviceStatusResult = await _locationService.requestService();
        if (!serviceStatusResult) {
          _setErrorMessage(S.of(context).enableLocationService);
          return false;
        } else {
          return true;
        }
      } else {
        return true;
      }
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        _setErrorMessage(S.of(context).allowLocation);
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        _setErrorMessage(S.of(context).enableLocationService);
      } else {
        _setErrorMessage(S.of(context).allowLocation);
      }

      return false;
    }
  }

  Future<bool> _checkLocationPermission() async {
    await _locationService.changeSettings(accuracy: LocationAccuracy.LOW);

    try {
      await _locationService.getLocation();

      final permission = await _locationService.requestPermission();
      if (!permission) {
        _setErrorMessage(S.of(context).allowLocation);
      }
      return permission;
    } on PlatformException catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        _setErrorMessage(S.of(context).allowLocation);
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        _setErrorMessage(S.of(context).enableLocationService);
      } else {
        _setErrorMessage(S.of(context).allowLocation);
      }

      return false;
    }
  }

  Future<bool> _checkConnectivity() async {
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        _setErrorMessage(S.of(context).connectToInternet);
        return false;
      }
    } on SocketException catch (_) {
      _setErrorMessage(S.of(context).connectToInternet);
      return false;
    }
  }

  void _setErrorMessage(String message) {
    if (mounted) {
      setState(() {
        _errorMessage = message;
      });
    } else {
      _errorMessage = message;
    }
  }
}
