import 'dart:io';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/screens/make_order_screen/make_order_screen.dart';

import 'package:weash_cars/widgets/custom_app_bar.dart';
import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/customer_store.dart';
import 'package:weash_cars/widgets/customer_drawer.dart';

class CustomerProfileEditScreen extends StatefulWidget {
  @override
  _CustomerProfileEditScreenState createState() =>
      _CustomerProfileEditScreenState();
}

class _CustomerProfileEditScreenState extends State<CustomerProfileEditScreen> {
  CustomerStore _customer;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  GlobalKey<FormState> _formKey = new GlobalKey();
  FocusNode _lastNameFocusNode = new FocusNode();
  FocusNode _emailFocusNode = new FocusNode();
  FocusNode _phoneNumberFocusNode = new FocusNode();
  TextEditingController _addressController;

  String _firstName;
  String _lastName;
  String _email;
  String _phoneNumber;
  String _address;
  DateTime _creationDate;
  File _avatarFile;
  String _locale;
  bool _isDialogVisible = false;
  bool _isLoading = false;

  @override
  void didChangeDependencies() {
    final application = Provider.of<ApplicationStore>(context);
    _customer = application.customerStore;
    _firstName = _customer.firstName;
    _lastName = _customer.lastName;
    _email = _customer.email;
    _phoneNumber = _customer.phoneNumber;
    _address = _customer.address;
    _locale = application.locale;

    _addressController = new TextEditingController(text: _address);

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      key: _scaffoldKey,
      drawer: Builder(
        builder: (BuildContext context) => CustomerDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned.fill(
            child: Container(
              color: Colors.white,
            ),
          ),
          Positioned(
            top: ScreenUtil().setHeight(160),
            bottom: 0,
            child: SingleChildScrollView(
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: ScreenUtil().setWidth(284),
                      alignment: application.locale == 'ar'
                          ? Alignment.topRight
                          : Alignment.topLeft,
                      child: Text(
                        S.of(context).editYourProfile,
                        style: TextStyle(
                          fontFamily:
                              application.locale == 'ar' ? 'Cairo' : 'Lato',
                          fontSize: 21,
                          fontWeight: FontWeight.w900,
                          color: Color(0xFF91A6D9),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(40)),
                    Container(
                      child: SizedBox(
                        width: ScreenUtil().setWidth(284),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            // padding: const EdgeInsets.all(0),
                            children: <Widget>[
                              InkWell(
                                onTap: _pickPhoto,
                                highlightColor: Colors.transparent,
                                splashColor: Colors.transparent,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    CircleAvatar(
                                      radius: ScreenUtil().setWidth(45),
                                      backgroundColor: Color(0xFFF0F0F0),
                                      backgroundImage: _avatarFile == null
                                          ? _customer.avatarUrl == null
                                              ? null
                                              : NetworkImage(
                                                  _customer.avatarUrl)
                                          : FileImage(_avatarFile),
                                    ),
                                    Transform.translate(
                                      offset: Offset(
                                          ScreenUtil().setWidth(
                                              application.locale == 'ar'
                                                  ? 30
                                                  : -30),
                                          ScreenUtil().setHeight(6)),
                                      child: Container(
                                        width: ScreenUtil().setWidth(35),
                                        height: ScreenUtil().setWidth(35),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: DecorationImage(
                                            image: AssetImage(
                                                'assets/images/camera-icon-background.png'),
                                          ),
                                        ),
                                        child: Image(
                                          image: AssetImage(
                                              'assets/images/camera-white-icon.png'),
                                          width: ScreenUtil().setWidth(14.25),
                                          height: ScreenUtil().setHeight(12.04),
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(36)),
                              Align(
                                alignment: application.locale == 'ar'
                                    ? Alignment.topRight
                                    : Alignment.topLeft,
                                child: Text(
                                  S.of(context).firstName,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'Lato',
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff021A89),
                                  ),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(10)),
                              TextFormField(
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                textCapitalization: TextCapitalization.words,
                                initialValue: _firstName ?? '',
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'Lato',
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff021A89),
                                ),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(4),
                                    right: ScreenUtil().setWidth(4),
                                    bottom: ScreenUtil().setHeight(3.5),
                                  ),
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xffC5D4F8),
                                      width: 0,
                                    ),
                                  ),
                                ),
                                validator: (String val) {
                                  if (val.length < 2) {
                                    return S.of(context).firstNameError;
                                  }
                                },
                                onFieldSubmitted: (_) => FocusScope.of(context)
                                    .requestFocus(_lastNameFocusNode),
                                onSaved: (String val) => _firstName = val,
                              ),
                              SizedBox(height: ScreenUtil().setHeight(28.5)),
                              Align(
                                alignment: application.locale == 'ar'
                                    ? Alignment.topRight
                                    : Alignment.topLeft,
                                child: Text(
                                  S.of(context).lastName,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'Lato',
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff021A89),
                                  ),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(10)),
                              TextFormField(
                                focusNode: _lastNameFocusNode,
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                                textCapitalization: TextCapitalization.words,
                                initialValue: _lastName ?? '',
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'Lato',
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff021A89),
                                ),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(4),
                                    right: ScreenUtil().setWidth(4),
                                    bottom: ScreenUtil().setHeight(3.5),
                                  ),
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xffC5D4F8),
                                      width: 0,
                                    ),
                                  ),
                                ),
                                validator: (String val) {
                                  if (val.length < 2) {
                                    return S.of(context).lastNameError;
                                  }
                                },
                                onFieldSubmitted: (_) => FocusScope.of(context)
                                    .requestFocus(_emailFocusNode),
                                onSaved: (String val) => _lastName = val,
                              ),
                              SizedBox(height: ScreenUtil().setHeight(28.5)),
                              Align(
                                alignment: application.locale == 'ar'
                                    ? Alignment.topRight
                                    : Alignment.topLeft,
                                child: Text(
                                  S.of(context).email,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'Lato',
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff021A89),
                                  ),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(10)),
                              TextFormField(
                                focusNode: _emailFocusNode,
                                keyboardType: TextInputType.emailAddress,
                                textInputAction: TextInputAction.next,
                                textDirection: TextDirection.ltr,
                                initialValue: _email ?? '',
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'Lato',
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff021A89),
                                ),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(4),
                                    right: ScreenUtil().setWidth(4),
                                    bottom: ScreenUtil().setHeight(3.5),
                                  ),
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xffC5D4F8),
                                      width: 0,
                                    ),
                                  ),
                                ),
                                validator: (String val) {
                                  var pattern =
                                      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
                                  var regexp = new RegExp(pattern);
                                  if (val != null &&
                                      val.isNotEmpty &&
                                      !regexp.hasMatch(val)) {
                                    return S.of(context).emailError;
                                  }
                                },
                                onFieldSubmitted: (_) => FocusScope.of(context)
                                    .requestFocus(_phoneNumberFocusNode),
                                onSaved: (String val) => _email = val,
                              ),
                              SizedBox(height: ScreenUtil().setHeight(28.5)),
                              Align(
                                alignment: application.locale == 'ar'
                                    ? Alignment.topRight
                                    : Alignment.topLeft,
                                child: Text(
                                  S.of(context).phoneNumber,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'Lato',
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff021A89),
                                  ),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(10)),
                              TextFormField(
                                focusNode: _phoneNumberFocusNode,
                                keyboardType: TextInputType.phone,
                                textInputAction: TextInputAction.done,
                                textDirection: TextDirection.ltr,
                                initialValue: _phoneNumber ?? '',
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'Lato',
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff021A89),
                                ),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(4),
                                    right: ScreenUtil().setWidth(4),
                                    bottom: ScreenUtil().setHeight(3.5),
                                  ),
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xffC5D4F8),
                                      width: ScreenUtil().setHeight(0.3),
                                    ),
                                  ),
                                ),
                                validator: (String val) {
                                  var pattern = r"^\+[0-9]{10,}$";
                                  var regexp = new RegExp(pattern);
                                  if (!regexp.hasMatch(val)) {
                                    return S.of(context).phoneNumberError;
                                  }
                                },
                                onFieldSubmitted: (_) {
                                  _updateProfile();
                                  FocusScope.of(context)
                                      .requestFocus(FocusNode());
                                },
                                onSaved: (String val) => _phoneNumber = val,
                              ),
                              SizedBox(height: ScreenUtil().setHeight(28.5)),
                              Align(
                                alignment: application.locale == 'ar'
                                    ? Alignment.topRight
                                    : Alignment.topLeft,
                                child: Text(
                                  S.of(context).address,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'Lato',
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff021A89),
                                  ),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(10)),
                              TextField(
                                controller: _addressController,
                                textDirection: TextDirection.ltr,
                                enabled: false,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'Lato',
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff021A89),
                                ),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(
                                    left: ScreenUtil().setWidth(4),
                                    right: ScreenUtil().setWidth(4),
                                    bottom: ScreenUtil().setHeight(3.5),
                                  ),
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xffC5D4F8),
                                      width: ScreenUtil().setHeight(0.3),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(22.5)),
                              InkWell(
                                onTap: () async {
                                  final result = await Navigator.of(context)
                                      .push<Map<String, dynamic>>(
                                          MaterialPageRoute(
                                    builder: (context) => MakeOrderScreen(
                                        addressPickerOnly: true),
                                  ));

                                  if (result != null) {
                                    final address = result['address'] as String;
                                    final location =
                                        result['location'] as GeoPoint;

                                    if (address != null &&
                                        address.isNotEmpty &&
                                        location != null) {
                                      setState(() {
                                        _addressController.text = address;
                                        _address = address;
                                      });
                                    }
                                  }
                                },
                                child: Container(
                                  width: ScreenUtil().setWidth(216.52),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Image(
                                        image: AssetImage(
                                            'assets/images/map-icon.png'),
                                        width: ScreenUtil().setWidth(30.11),
                                        height: ScreenUtil().setHeight(25.81),
                                      ),
                                      SizedBox(
                                          width: ScreenUtil().setWidth(10.4)),
                                      Expanded(
                                        child: Text(
                                          S.of(context).toChangeTheAddress,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Compact Text',
                                            fontSize: 13,
                                            fontWeight: FontWeight.w800,
                                            letterSpacing: 0.40,
                                            color: Color(0xFF91A6D9),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: ScreenUtil().setHeight(29.5)),
                              Align(
                                alignment: application.locale == 'ar'
                                    ? Alignment.topRight
                                    : Alignment.topLeft,
                                child: Text(
                                  S.of(context).language,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontFamilyFallback: <String>['SF Pro Text'],
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xFF021A89),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: ScreenUtil().setWidth(35)),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  textDirection: TextDirection.ltr,
                                  children: <Widget>[
                                    InkWell(
                                      onTap: () =>
                                          setState(() => _locale = 'en'),
                                      enableFeedback: false,
                                      child: Row(
                                        textDirection: TextDirection.ltr,
                                        children: <Widget>[
                                          Container(
                                            width: ScreenUtil().setWidth(12),
                                            height: ScreenUtil().setWidth(12),
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                color: Color(0xFF021A89),
                                                width: 1,
                                              ),
                                            ),
                                            child: Visibility(
                                              visible: _locale == 'en',
                                              child: Container(
                                                width: ScreenUtil().setWidth(7),
                                                height:
                                                    ScreenUtil().setWidth(7),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: Color(0xFF07D80F),
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                              width: ScreenUtil().setWidth(5)),
                                          Text(
                                            'English',
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontFamilyFallback: <String>[
                                                'SF Pro Text'
                                              ],
                                              fontSize: 15,
                                              fontWeight: FontWeight.w700,
                                              color: Color(0xFF021A89),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    InkWell(
                                      onTap: () =>
                                          setState(() => _locale = 'ar'),
                                      enableFeedback: false,
                                      child: Row(
                                        textDirection: TextDirection.rtl,
                                        children: <Widget>[
                                          Container(
                                            width: ScreenUtil().setWidth(12),
                                            height: ScreenUtil().setWidth(12),
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                color: Color(0xFF021A89),
                                                width: 1,
                                              ),
                                            ),
                                            child: Visibility(
                                              visible: _locale == 'ar',
                                              child: Container(
                                                width: ScreenUtil().setWidth(7),
                                                height:
                                                    ScreenUtil().setWidth(7),
                                                decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: Color(0xFF07D80F),
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                              width: ScreenUtil().setWidth(5)),
                                          Text(
                                            'العربية',
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontFamilyFallback: <String>[
                                                'SF Pro Text'
                                              ],
                                              fontSize: 15,
                                              fontWeight: FontWeight.w700,
                                              color: Color(0xFF021A89),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(10)),
                    InkWell(
                      onTap: () {
                        _updateProfile();
                        // FocusScope.of(context).requestFocus(FocusNode());
                      },
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                        width: ScreenUtil().setWidth(170.16),
                        height: ScreenUtil().setHeight(40.2),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          gradient: LinearGradient(
                            colors: <Color>[
                              Color(0xff031C8D),
                              Color(0xff0A2FAF)
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                          ),
                        ),
                        child: _isLoading
                            ? CupertinoActivityIndicator(
                                animating: true,
                              )
                            : Text(
                                S.of(context).update,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF UI Display',
                                  fontFamilyFallback: <String>['SF Pro Text'],
                                  fontSize: ScreenUtil().setSp(11),
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xfffcfcfc),
                                ),
                              ),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(30)),
                    // TODO: show profile creation date.
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            top: 0,
            child: Builder(
              builder: (BuildContext context) => CustomAppBar2(
                onOpenDrawer: () {
                  setState(() {
                    _isDialogVisible = true;
                    Scaffold.of(context).openDrawer();
                  });
                },
                title: S.of(context).dashboard,
                onAvatarTap: _pickPhoto,
                nbNotifications: 0,
                avatar: _avatarFile == null
                    ? _customer.avatarUrl == null
                        ? null
                        : NetworkImage(_customer.avatarUrl)
                    : FileImage(_avatarFile),
              ),
            ),
          ),
          Visibility(
            visible: _isDialogVisible,
            child: Positioned.fill(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                child: Container(
                  color: Colors.grey.withOpacity(0.2),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _pickPhoto() async {
    final imageFile = await showCupertinoModalPopup<File>(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        title: Text(S.of(context).changeProfilePicture),
        actions: <Widget>[
          CupertinoActionSheetAction(
            onPressed: () async {
              final file =
                  await ImagePicker.pickImage(source: ImageSource.camera);
              Navigator.of(context).pop(file);
            },
            child: Text(S.of(context).takeNewPhoto),
          ),
          CupertinoActionSheetAction(
            onPressed: () async {
              final file =
                  await ImagePicker.pickImage(source: ImageSource.gallery);
              Navigator.of(context).pop(file);
            },
            child: Text(S.of(context).chooseExistingPhoto),
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(
            S.of(context).Cancel,
            style: TextStyle(
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
    );

    setState(() {
      _avatarFile = imageFile;
    });
  }

  bool _saveForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> _updateProfile() async {
    if (_saveForm()) {
      setState(() {
        _isLoading = true;
      });

      String imageUrl;

      if (_avatarFile != null) {
        final extension =
            _avatarFile.path.substring(_avatarFile.path.lastIndexOf('.'));
        final storageRef = FirebaseStorage.instance
            .ref()
            .child('profile_pictures')
            .child("${_customer.customerId}.$extension");

        final StorageUploadTask uploadTask = storageRef.putFile(
          _avatarFile,
          StorageMetadata(
            contentType: "image/$extension",
            customMetadata: <String, String>{
              'type': 'Profile Picture',
              'user_id': _customer.customerId,
            },
          ),
        );

        final downloadUrl = await uploadTask.onComplete;
        imageUrl = await downloadUrl.ref.getDownloadURL();
      }

      _customer.setData(
        firstName: _firstName,
        lastName: _lastName,
        email: _email,
        phoneNumber: _phoneNumber,
        avatarUrl: imageUrl == null ? _customer.avatarUrl : imageUrl,
        locale: _locale,
      );
      final application = Provider.of<ApplicationStore>(context);
      application.localeStream.add(_locale);
      await _customer.saveData();

      setState(() {
        _isLoading = false;
      });

      showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(S.of(context).success),
          content: Text(S.of(context).profileWasUpdated),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).ok),
            ),
          ],
        ),
      );
    }
  }
}
