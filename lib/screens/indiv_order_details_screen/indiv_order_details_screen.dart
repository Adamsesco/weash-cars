import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/payment_method.dart';
import 'package:weash_cars/screens/take_order_screen/take_order_screen.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class IndivOrderDetailsScreen extends StatefulWidget {
  Map<String, dynamic> order;

  IndivOrderDetailsScreen({Key key, this.order}) : super(key: key);

  @override
  _IndivOrderDetailsScreenState createState() =>
      _IndivOrderDetailsScreenState();
}

class _IndivOrderDetailsScreenState extends State<IndivOrderDetailsScreen> {
  ApplicationStore _application;
  CompanyStore _company;
  Map<String, dynamic> _order;

  bool _isDialogVisible = false;
  bool _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_company == null) {
      _application = Provider.of<ApplicationStore>(context);
      _company = _application.companyStore;
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CompanyDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _company.avatarUrl,
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image(
                image:
                    AssetImage('assets/images/company-screens-background.png'),
                matchTextDirection: true,
                fit: BoxFit.fill),
          ),
          Positioned.fill(
            child: Column(
              children: <Widget>[
                Observer(
                  builder: (BuildContext context) => CustomAppBar2(
                    onOpenDrawer: () {
                      setState(() {
                        _isDialogVisible = true;
                        Scaffold.of(context).openDrawer();
                      });
                    },
                    title: S.of(context).dashboard,
                    transparentBackground: true,
                    titleColor: Color(0xFF91A6D9),
                    buttonColor: Color(0xFF0930C3),
                    nbNotifications: 0,
                    avatar: _company.avatarUrl == null
                        ? null
                        : NetworkImage(_company.avatarUrl),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(33.8),
                    ),
                    child: Builder(
                      builder: (BuildContext context) {
                        _order = widget.order;
                        if (_order == null) {
                          return CupertinoActivityIndicator();
                        }

                        final orderLocality = _order['locality'] ?? ' ';
                        final orderAddress = _order['address'];
                        final orderDate = _order['date'] as DateTime;
                        final orderNumber = _order['order_number'] as int;
                        final orderNumberStr = List.generate(
                                6 - orderNumber.toString().length,
                                (_) => '0').join() +
                            orderNumber.toString();

                        final customer = _order['customer'];
                        final customerAvatarUrl = customer['avatar_url'];
                        final customerFirstName = customer['first_name'];
                        final customerLastName = customer['last_name'];

                        final package = _order['package'];
                        final packageTitle = package['title'];
                        final packagePrice = package['price'] as double;
                        final packageCurrency = _company.currency;
                        final vehicleTypes = {
                          "sedan": S.of(context).sedan,
                          "motorcycle": S.of(context).motorcycle,
                          "suv": S.of(context).suv,
                          "boat": S.of(context).boat,
                        };
                        final vehicleType =
                            vehicleTypes[package['vehicle_type']];

                        final paymentMethodStrs = {
                          PaymentMethod.cash: S.of(context).Cashondelivery,
                          PaymentMethod.visa: S.of(context).online,
                        };
                        final paymentMethod = paymentMethodStrs[
                            _order['payment_method'] as PaymentMethod];

                        return Column(
                          children: <Widget>[
                            Expanded(
                              child: ListView(
                                shrinkWrap: true,
                                children: <Widget>[
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Container(
                                        width: ScreenUtil().setWidth(64.41),
                                        height: ScreenUtil().setWidth(64.41),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: DecorationImage(
                                            image: customerAvatarUrl == null
                                                ? AssetImage(
                                                    'assets/images/avatar-user.png')
                                                : NetworkImage(
                                                    customerAvatarUrl),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                          width: ScreenUtil().setWidth(22.8)),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            "$customerFirstName $customerLastName",
                                            style: TextStyle(
                                              fontFamily:
                                                  _application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontSize: 13,
                                              fontWeight: FontWeight.w700,
                                              color: Color(0xFF021A89),
                                            ),
                                          ),
                                          SizedBox(
                                              height:
                                                  ScreenUtil().setHeight(1)),
                                          Text(
                                            orderLocality,
                                            style: TextStyle(
                                              fontFamily:
                                                  _application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontSize: 13,
                                              fontWeight: FontWeight.w300,
                                              color: Color(0xFF021A89),
                                            ),
                                          ),
                                          SizedBox(
                                            width: ScreenUtil().setWidth(201),
                                            child: Row(
                                              children: <Widget>[
                                                Expanded(
                                                  child: Text(
                                                    orderAddress,
                                                    style: TextStyle(
                                                      fontFamily:
                                                          _application.locale ==
                                                                  'ar'
                                                              ? 'Cairo'
                                                              : 'SF Pro Text',
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w800,
                                                      color: Color(0xFF021A89),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                      height: ScreenUtil().setHeight(58.8)),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: ScreenUtil().setWidth(61),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "${S.of(context).orderN}     $orderNumberStr",
                                          style: TextStyle(
                                            fontFamily:
                                                _application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Display',
                                            fontSize: 17,
                                            fontWeight: FontWeight.w900,
                                            color: Color(0xFFA0AEE6),
                                          ),
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(27)),
                                        Row(
                                          children: <Widget>[
                                            Image(
                                              image: AssetImage(
                                                  'assets/images/vehicle-type-icon.png'),
                                              width:
                                                  ScreenUtil().setWidth(20.53),
                                              color: Color(0xFFA0AEE6),
                                            ),
                                            SizedBox(
                                                width: ScreenUtil()
                                                    .setWidth(36.1)),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  S
                                                      .of(context)
                                                      .vehicleType
                                                      .replaceFirst(':', ''),
                                                  style: TextStyle(
                                                    fontFamily: _application
                                                                .locale ==
                                                            'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                    fontSize: 13,
                                                    fontWeight: FontWeight.w700,
                                                    color: Color(0xFF021A89)
                                                        .withOpacity(0.98),
                                                  ),
                                                ),
                                                Text(
                                                  vehicleType,
                                                  style: TextStyle(
                                                    fontFamily: _application
                                                                .locale ==
                                                            'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                    fontSize: 13,
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(0xFF021A89)
                                                        .withOpacity(0.98),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(13)),
                                        Row(
                                          children: <Widget>[
                                            Image(
                                              image: AssetImage(
                                                  'assets/images/package-icon.png'),
                                              width:
                                                  ScreenUtil().setWidth(19.2),
                                              color: Color(0xFFA0AEE6),
                                            ),
                                            SizedBox(
                                                width: ScreenUtil()
                                                    .setWidth(37.4)),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  S
                                                      .of(context)
                                                      .package
                                                      .replaceFirst(':', ''),
                                                  style: TextStyle(
                                                    fontFamily: _application
                                                                .locale ==
                                                            'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                    fontSize: 13,
                                                    fontWeight: FontWeight.w700,
                                                    color: Color(0xFF021A89)
                                                        .withOpacity(0.98),
                                                  ),
                                                ),
                                                Text(
                                                  packageTitle,
                                                  style: TextStyle(
                                                    fontFamily: _application
                                                                .locale ==
                                                            'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                    fontSize: 13,
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(0xFF021A89)
                                                        .withOpacity(0.98),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(13)),
                                        Row(
                                          children: <Widget>[
                                            Image(
                                              image: AssetImage(
                                                  'assets/images/payment-method-icon.png'),
                                              width:
                                                  ScreenUtil().setWidth(25.09),
                                              color: Color(0xFFA0AEE6),
                                            ),
                                            SizedBox(
                                                width: ScreenUtil()
                                                    .setWidth(33.5)),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  S.of(context).Paymentmethod,
                                                  style: TextStyle(
                                                    fontFamily: _application
                                                                .locale ==
                                                            'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                    fontSize: 13,
                                                    fontWeight: FontWeight.w700,
                                                    color: Color(0xFF021A89)
                                                        .withOpacity(0.98),
                                                  ),
                                                ),
                                                Text(
                                                  paymentMethod,
                                                  style: TextStyle(
                                                    fontFamily: _application
                                                                .locale ==
                                                            'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                    fontSize: 13,
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(0xFF021A89)
                                                        .withOpacity(0.98),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(13)),
                                        Row(
                                          children: <Widget>[
                                            Image(
                                              image: AssetImage(
                                                  'assets/images/calendar-icon.png'),
                                              width:
                                                  ScreenUtil().setWidth(20.41),
                                              color: Color(0xFFA0AEE6),
                                            ),
                                            SizedBox(
                                                width: ScreenUtil()
                                                    .setWidth(38.3)),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  S
                                                      .of(context)
                                                      .dateAndTime
                                                      .replaceFirst(':', ''),
                                                  style: TextStyle(
                                                    fontFamily: _application
                                                                .locale ==
                                                            'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                    fontSize: 13,
                                                    fontWeight: FontWeight.w700,
                                                    color: Color(0xFF021A89)
                                                        .withOpacity(0.98),
                                                  ),
                                                ),
                                                Text(
                                                  DateFormat('dd/MM/y')
                                                          .format(orderDate) +
                                                      " ${S.of(context).at} " +
                                                      DateFormat('Hm')
                                                          .format(orderDate),
                                                  style: TextStyle(
                                                    fontFamily: _application
                                                                .locale ==
                                                            'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                    fontSize: 13,
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(0xFF021A89)
                                                        .withOpacity(0.98),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(13)),
                                        Row(
                                          children: <Widget>[
                                            Image(
                                              image: AssetImage(
                                                  'assets/images/price-icon.png'),
                                              width:
                                                  ScreenUtil().setWidth(19.43),
                                              color: Color(0xFFA0AEE6),
                                            ),
                                            SizedBox(
                                                width: ScreenUtil()
                                                    .setWidth(37.3)),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  S.of(context).Price,
                                                  style: TextStyle(
                                                    fontFamily: _application
                                                                .locale ==
                                                            'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                    fontSize: 13,
                                                    fontWeight: FontWeight.w700,
                                                    color: Color(0xFF021A89)
                                                        .withOpacity(0.98),
                                                  ),
                                                ),
                                                Text(
                                                  "${packagePrice.toStringAsFixed(3)} $packageCurrency",
                                                  style: TextStyle(
                                                    fontFamily: _application
                                                                .locale ==
                                                            'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                    fontSize: 13,
                                                    fontWeight: FontWeight.w600,
                                                    color: Color(0xFF021A89)
                                                        .withOpacity(0.98),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: _takeOrder,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Container(
                                width: ScreenUtil().setWidth(170.16),
                                height: ScreenUtil().setHeight(40.2),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  gradient: LinearGradient(
                                    colors: <Color>[
                                      Color(0xFF0026A1),
                                      Color(0xFF000F66),
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.4),
                                      offset:
                                          Offset(0, ScreenUtil().setHeight(3)),
                                      blurRadius: 6,
                                    ),
                                  ],
                                ),
                                child: _isLoading
                                    ? CupertinoActivityIndicator()
                                    : Text(
                                        S.of(context).TAKEORDER,
                                        style: TextStyle(
                                          fontFamily:
                                              _application.locale == 'ar'
                                                  ? 'Cairo'
                                                  : 'SF Pro Text',
                                          fontSize: 11,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.white,
                                        ),
                                      ),
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(58.8)),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _takeOrder() async {
    if (_order == null) {
      return;
    }

    setState(() {
      _isLoading = true;
    });

    await Firestore.instance
        .collection('orders')
        .document(widget.order['order_id'])
        .updateData({
      'status': 10,
      'assigned_employee_id': _company.companyId,
    });

    setState(() {
      _isLoading = false;
    });

    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => TakeOrderScreen(
          order: widget.order,
        ),
      ),
    );
  }
}
