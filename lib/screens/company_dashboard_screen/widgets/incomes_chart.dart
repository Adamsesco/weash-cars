import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/services/IncomesService.dart';
import 'package:weash_cars/stores/application_store.dart';

typedef OnDateChanged(DateTime date, IncomeType incomeType);

class IncomesChart extends StatefulWidget {
  final Map<String, Income> incomes;
  final double balance;
  final String currency;
  final OnDateChanged onDateChanged;

  IncomesChart(this.incomes, this.balance, this.currency, {this.onDateChanged});

  @override
  _IncomesChartState createState() => _IncomesChartState();
}

class _IncomesChartState extends State<IncomesChart> {
  IncomeType _selectedType;
  Map<String, double> _series;
  List<String> _dayShortNames;
  List<String> _monthShortNames;
  int _selectedBarIndex;
  DateTime _selectedDate;

  @override
  void initState() {
    super.initState();

    _selectedType = IncomeType.monthly;
    _selectedBarIndex = DateTime.now().month - 1;
    _selectedDate = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    _dayShortNames = [
      S.of(context).mondayShort,
      S.of(context).tuesdayShort,
      S.of(context).wednesdayShort,
      S.of(context).thursdayShort,
      S.of(context).fridayShort,
      S.of(context).saturdayShort,
      S.of(context).sundayShort,
    ];
    _monthShortNames = [
      S.of(context).januaryShort,
      S.of(context).februaryShort,
      S.of(context).marchShort,
      S.of(context).aprilShort,
      S.of(context).mayShort,
      S.of(context).juneShort,
      S.of(context).julyShort,
      S.of(context).augustShort,
      S.of(context).septemberShort,
      S.of(context).octoberShort,
      S.of(context).novemberShort,
      S.of(context).decemberShort,
    ];

    if (_series == null) {
      _series = widget.incomes['monthly'].values.map(
          (int month, double value) =>
              MapEntry(_monthShortNames[month - 1], value));
    }

    return Column(
      children: <Widget>[
        SizedBox(
          width: ScreenUtil().setWidth(280),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              S.of(context).day,
              S.of(context).month,
              S.of(context).year
            ]
                .asMap()
                .map((int index, String title) {
                  IncomeType type;
                  int initialIndex;
                  Map<String, double> series;
                  switch (index) {
                    case 0:
                      type = IncomeType.daily;
                      initialIndex = DateTime.now().day - 1;
                      series = widget.incomes['daily'].values.map(
                          (int day, double value) =>
                              MapEntry(day.toString(), value));
                      break;
                    case 1:
                      type = IncomeType.monthly;
                      initialIndex = DateTime.now().month - 1;
                      series = widget.incomes['monthly'].values.map(
                          (int month, double value) =>
                              MapEntry(_monthShortNames[month - 1], value));
                      break;
                    case 2:
                      type = IncomeType.yearly;
                      initialIndex = widget.incomes['yearly'].values.length - 1;
                      series = widget.incomes['yearly'].values.map(
                          (int year, double value) =>
                              MapEntry(year.toString(), value));
                      break;
                  }

                  return MapEntry(
                      index,
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              _selectedType = type;
                              _selectedBarIndex = initialIndex;
                              _series = series;
                            });
                            if (widget.onDateChanged != null) {
                              widget.onDateChanged(_selectedDate, type);
                            }
                          },
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Container(
                            width: ScreenUtil().setWidth(71),
                            height: ScreenUtil().setHeight(28),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: _selectedType == type
                                  ? Colors.white
                                  : Color(0xff062496),
                            ),
                            child: Center(
                              child: Text(
                                title,
                                style: TextStyle(
                                  fontFamily: application.locale == 'ar'
                                      ? 'Cairo'
                                      : 'SF UI Display',
                                  fontWeight: FontWeight.w700,
                                  fontFamilyFallback: <String>['SF Pro Text'],
                                  fontSize: ScreenUtil().setSp(9),
                                  color: _selectedType == type
                                      ? Color(0xff062496)
                                      : Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ));
                })
                .values
                .toList(),
          ),
        ),
        SizedBox(height: ScreenUtil().setHeight(31)),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Builder(
                  builder: (context) {
                    final nat = widget.balance.toInt();
                    final frac = int.tryParse((widget.balance - nat)
                            .toStringAsFixed(3)
                            .replaceFirst('0.', '')) ??
                        0;
                    final fracStr = frac >= 10 ? frac.toString() : "0$frac";

                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      textDirection: TextDirection.ltr,
                      children: <Widget>[
                        SizedBox(
                          height: ScreenUtil().setHeight(29),
                          child: FittedBox(
                            fit: BoxFit.fitHeight,
                            child: Text(
                              "${widget.currency} $nat.",
                              textDirection: TextDirection.ltr,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'Helvetica Rounded LT',
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: ScreenUtil().setHeight(20),
                          child: FittedBox(
                            fit: BoxFit.fitHeight,
                            child: Text(
                              fracStr,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'Helvetica Rounded LT',
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
                SizedBox(
                  height: ScreenUtil().setHeight(12),
                  child: FittedBox(
                    fit: BoxFit.fitHeight,
                    child: Text(
                      S.of(context).currentBalance,
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF UI Display',
                        fontWeight: FontWeight.w300,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(15),
              child: FittedBox(
                fit: BoxFit.fitHeight,
                child: Text(
                  intl.DateFormat('EEEE, MMMM d').format(DateTime.now()),
                  style: TextStyle(
                    fontFamily:
                        application.locale == 'ar' ? 'Cairo' : 'SF UI Display',
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: ScreenUtil().setHeight(32)),
        Expanded(
          child: ListView.separated(
            scrollDirection: Axis.horizontal,
            physics: BouncingScrollPhysics(),
            separatorBuilder: (BuildContext context, int index) => SizedBox(
                width: index == _selectedBarIndex - 1
                    ? ScreenUtil().setWidth(31.6 - 4.29)
                    : index == _selectedBarIndex
                        ? ScreenUtil().setWidth(31.6 - 4.29)
                        : ScreenUtil().setWidth(31.6)),
            itemCount: _series.length,
            itemBuilder: (BuildContext context, int index) {
              final value = _series.values.toList()[index];
              final legend = _series.keys.toList()[index];
              final maxValue = _series.values.reduce(max);
              final heightFactor = maxValue == 0.0 ? 0 : value / maxValue;
              final isSelected = index == _selectedBarIndex;

              return Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      _selectedBarIndex = index;

                      switch (_selectedType) {
                        case IncomeType.daily:
                          _selectedDate = new DateTime(_selectedDate.year,
                              _selectedDate.month, index + 1);
                          break;

                        case IncomeType.monthly:
                          _selectedDate =
                              new DateTime(_selectedDate.year, index + 1);
                          break;

                        case IncomeType.yearly:
                          _selectedDate = new DateTime(int.tryParse(legend));
                          break;

                        case IncomeType.weekly:
                          break;
                      }
                    });
                    if (widget.onDateChanged != null) {
                      widget.onDateChanged(_selectedDate, _selectedType);
                    }
                  },
                  highlightColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Visibility(
                        visible: isSelected && heightFactor < 0.5,
                        child: RotatedBox(
                          quarterTurns: 3,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              SizedBox(
                                height: ScreenUtil().setHeight(18),
                                child: FittedBox(
                                  fit: BoxFit.fitHeight,
                                  child: Text(
                                    value.toStringAsFixed(3) + ' ',
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'Roboto',
                                      fontWeight: FontWeight.w900,
                                      color: Color(0xff00EAE1),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: ScreenUtil().setHeight(12),
                                child: FittedBox(
                                  fit: BoxFit.fitHeight,
                                  child: Text(
                                    widget.currency,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'Roboto',
                                      fontWeight: FontWeight.w900,
                                      color: Color(0xff00EAE1),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                        visible: isSelected && heightFactor < 0.5,
                        child: SizedBox(height: ScreenUtil().setHeight(11.9)),
                      ),
                      ControlledAnimation(
                        key: Key(_selectedType.toString()),
                        duration: Duration(milliseconds: 400),
                        tween: Tween(begin: 0.0, end: 1.0),
                        builder: (BuildContext context, animationValue) =>
                            Container(
                              height: ScreenUtil().setHeight(155.89) *
                                  heightFactor *
                                  animationValue,
                              width: isSelected
                                  ? ScreenUtil().setWidth(19)
                                  : ScreenUtil().setWidth(10.42),
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenUtil().setHeight(12.4)),
                              alignment: Alignment.topCenter,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                gradient: LinearGradient(
                                  colors: [
                                    Color(0xff033EAC),
                                    Color(0xff00EAE1)
                                  ],
                                  begin: Alignment.bottomCenter,
                                  end: Alignment.topCenter,
                                ),
                              ),
                              child: Visibility(
                                visible: isSelected && heightFactor >= 0.5,
                                child: RotatedBox(
                                  quarterTurns: 3,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: <Widget>[
                                      SizedBox(
                                        height: ScreenUtil().setHeight(18),
                                        child: FittedBox(
                                          fit: BoxFit.fitHeight,
                                          child: Text(
                                            value.toStringAsFixed(2) + ' ',
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'Roboto',
                                              fontWeight: FontWeight.w900,
                                              color: Color(0xff021A89),
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: ScreenUtil().setHeight(12),
                                        child: FittedBox(
                                          fit: BoxFit.fitHeight,
                                          child: Text(
                                            widget.currency,
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'Roboto',
                                              fontWeight: FontWeight.w900,
                                              color: Color(0xff021A89),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(11.9)),
                      RotatedBox(
                        quarterTurns: 3,
                        child: SizedBox(
                          height: ScreenUtil().setHeight(11),
                          child: FittedBox(
                            fit: BoxFit.fitHeight,
                            child: Text(
                              legend,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'Montserrat',
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
