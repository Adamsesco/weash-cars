import 'dart:math' as math;
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:simple_animations/simple_animations/controlled_animation.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/stores/application_store.dart';

class DelayedDialog extends StatefulWidget {
  final Map<String, dynamic> order;

  DelayedDialog({Key key, @required this.order}) : super(key: key);

  @override
  _DelayedDialogState createState() => _DelayedDialogState();
}

class _DelayedDialogState extends State<DelayedDialog> {
  Future<Duration> _timeRemaining;
  bool _isAccepting = false;
  bool _isDeclining = false;

  @override
  void initState() {
    super.initState();

    _timeRemaining = _getTimeRemaining();
  }

  Future<Duration> _getTimeRemaining() async {
    final orderId = widget.order['order_id'];
    final request = await http.get(
        "https://us-central1-weash-cars.cloudfunctions.net/orderDelay?orderId=$orderId");

    if (request.statusCode == 200) {
      final milliseconds = int.tryParse(request.body) ?? 0;
      return Duration(milliseconds: milliseconds);
    }

    return Duration(milliseconds: 0);
  }

  Future<void> _acceptOrder() async {
    if (widget.order == null) {
      return;
    }

    setState(() {
      _isAccepting = true;
    });

    await Firestore.instance
        .collection('orders')
        .document(widget.order['order_id'])
        .updateData({
      'status': 1,
    });

    setState(() {
      _isAccepting = false;
    });

    Navigator.pushReplacementNamed(context, "/dashboard/company");
  }

  Future<void> _declineOrder() async {
    if (widget.order == null) {
      return;
    }

    setState(() {
      _isDeclining = true;
    });

    await Firestore.instance
        .collection('orders')
        .document(widget.order['order_id'])
        .updateData({
      'status': 31,
    });

    setState(() {
      _isDeclining = false;
    });

    Navigator.pushReplacementNamed(context, "/dashboard/company");
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context, listen: false);
    final company = application.companyStore;

    final orderDate = widget.order['date'] as DateTime;
    final orderNumber = widget.order['order_number'] as int;
    final orderNumberStr =
        List.generate(6 - orderNumber.toString().length, (_) => '0').join() +
            orderNumber.toString();
    final orderAddress = widget.order['address'] as String;
    final package = widget.order['package'];
    final packageTitle = package['title'] as String;
    final packagePrice = package['price'] as double;
    final packageCurrency = company.currency;
    final vehicleTypes = {
      "sedan": S.of(context).sedan,
      "motorcycle": S.of(context).motorcycle,
      "suv": S.of(context).suv,
      "boat": S.of(context).boat,
    };
    final vehicleType = vehicleTypes[package['vehicle_type']];

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned.fill(
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                child: Container(),
              ),
            ),
          ),
          Positioned(
            child: Align(
              child: Container(
                width: MediaQuery.of(context).size.width -
                    ScreenUtil().setWidth(24),
                height: ScreenUtil().setHeight(647.15),
                margin: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(12),
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(32),
                ),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(19),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.16),
                      blurRadius: 6,
                      offset: Offset(0, ScreenUtil().setHeight(3)),
                    ),
                  ],
                ),
                child: Column(
                  children: <Widget>[
                    Spacer(flex: 2),
                    FutureBuilder<Duration>(
                        future: _timeRemaining,
                        builder: (BuildContext context,
                            AsyncSnapshot<Duration> snapshot) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.waiting:
                            case ConnectionState.active:
                              return CupertinoActivityIndicator();
                            case ConnectionState.none:
                              return Container();
                            case ConnectionState.done:
                              if (snapshot.hasError || snapshot.data == null) {
                                return Container();
                              }

                              return _Timer(
                                timeRemaining: snapshot.data,
                                order: widget.order,
                              );
                          }
                          return Container();
                        }),
                    Spacer(),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${S.of(context).dateAndTime.toUpperCase()}",
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: ScreenUtil().setSp(12),
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.24,
                            color: Color(0xFF2A419A),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "${DateFormat('d MMMM').format(orderDate)} ${S.of(context).at} ${DateFormat('HH:mm').format(orderDate)}",
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: ScreenUtil().setSp(12),
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0.24,
                              color: Color(0xFF2A419A),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(3)),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${S.of(context).number.toUpperCase()}: ",
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: ScreenUtil().setSp(12),
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.24,
                            color: Color(0xFF2A419A),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            orderNumberStr,
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: ScreenUtil().setSp(12),
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0.24,
                              color: Color(0xFF2A419A),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(3)),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${S.of(context).location.toUpperCase()}: ",
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: ScreenUtil().setSp(12),
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.24,
                            color: Color(0xFF2A419A),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            orderAddress,
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: ScreenUtil().setSp(12),
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0.24,
                              color: Color(0xFF2A419A),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(3)),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${S.of(context).vehicleType.toUpperCase()}",
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: ScreenUtil().setSp(12),
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.24,
                            color: Color(0xFF2A419A),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            vehicleType.toUpperCase(),
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: ScreenUtil().setSp(12),
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0.24,
                              color: Color(0xFF2A419A),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(3)),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${S.of(context).package.toUpperCase()}",
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: ScreenUtil().setSp(12),
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.24,
                            color: Color(0xFF2A419A),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            packageTitle.toUpperCase(),
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: ScreenUtil().setSp(12),
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0.24,
                              color: Color(0xFF2A419A),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(3)),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${S.of(context).price.toUpperCase()} ",
                          style: TextStyle(
                            fontFamily: application.locale == 'ar'
                                ? 'Cairo'
                                : 'SF Pro Text',
                            fontSize: ScreenUtil().setSp(12),
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.24,
                            color: Color(0xFF2A419A),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "${packagePrice.toStringAsFixed(3)} ${packageCurrency.toUpperCase()}",
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: ScreenUtil().setSp(12),
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0.24,
                              color: Color(0xFF2A419A),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Spacer(),
                    SizedBox(
                      width: ScreenUtil().setWidth(255),
                      child: Text(
                        S.of(context).willBeCanceled,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Display',
                          fontSize: ScreenUtil().setSp(11),
                          fontWeight: FontWeight.w900,
                          letterSpacing: 0.80,
                          color: Color(0xFFFFBA00),
                        ),
                      ),
                    ),
                    Spacer(),
                    Padding(
                      padding: EdgeInsets.only(
                        left: ScreenUtil().setWidth(23),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          InkWell(
                            onTap: _declineOrder,
                            child: Container(
                              // width: ScreenUtil().setWidth(169),
                              height: ScreenUtil().setHeight(40),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: _isDeclining
                                  ? CupertinoActivityIndicator()
                                  : Text(
                                      S.of(context).decline,
                                      style: TextStyle(
                                        fontFamily: application.locale == 'ar'
                                            ? 'Cairo'
                                            : 'SF Pro Display',
                                        fontSize: ScreenUtil().setSp(12),
                                        fontWeight: FontWeight.w900,
                                        letterSpacing: 0.20,
                                        color: Color(0xFF002297),
                                      ),
                                    ),
                            ),
                          ),
                          InkWell(
                            onTap: _acceptOrder,
                            child: Container(
                              width: ScreenUtil().setWidth(169),
                              height: ScreenUtil().setHeight(40),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Color(0xFF002297),
                              ),
                              child: _isAccepting
                                  ? CupertinoActivityIndicator()
                                  : Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          S.of(context).TAKEORDER,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Display',
                                            fontSize: ScreenUtil().setSp(12),
                                            fontWeight: FontWeight.w900,
                                            letterSpacing: 0.20,
                                            color: Colors.white,
                                          ),
                                        ),
                                        SizedBox(
                                            width: ScreenUtil().setWidth(11.8)),
                                        Image(
                                          image: AssetImage(
                                              'assets/images/arrow-icon.png'),
                                          width: ScreenUtil().setWidth(17.39),
                                          height: ScreenUtil().setHeight(12.15),
                                          color: Colors.white,
                                          matchTextDirection: true,
                                        ),
                                      ],
                                    ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Spacer(flex: 2),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _Timer extends StatefulWidget {
  final Duration timeRemaining;
  final Map<String, dynamic> order;

  _Timer({Key key, @required this.timeRemaining, @required this.order})
      : super(key: key);

  @override
  __TimerState createState() => __TimerState();
}

class __TimerState extends State<_Timer> {
  Duration _timeRemaining;
  DateTime _startTime;
  bool _disposed = false;

  @override
  void initState() {
    super.initState();

    _timeRemaining = widget.timeRemaining;
    _startTime = DateTime.now();
    _startTimer();
  }

  @override
  void dispose() {
    _disposed = true;

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final timerWidth = ScreenUtil().setWidth(227.42);
    final dotSize = ScreenUtil().setWidth(24.71);
    final stepAngle = math.pi / 8;
    final dotsCount = (math.pi * 2) / stepAngle;

    return ControlledAnimation(
      playback: Playback.LOOP,
      duration: Duration(seconds: 3),
      tween: Tween(begin: 0.0, end: dotsCount),
      builder: (context, animation) {
        return SizedBox(
          width: timerWidth,
          height: timerWidth,
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              for (var i = 0; i < dotsCount; i++)
                Builder(
                  builder: (BuildContext context) {
                    final angle = stepAngle * i;

                    final sin = math.sin(angle);
                    final offsetY = ((1 - sin) / 2) * (timerWidth - dotSize);

                    final cos = math.cos(angle);
                    final offsetX = ((1 + cos) / 2) * (timerWidth - dotSize);

                    final anim = animation as double;
                    final currentAnimationIndex = anim.round();
                    final distance = (currentAnimationIndex + i) % dotsCount;
                    final t = distance / dotsCount;
                    final transform = _scale(t, 0, 1, 0.2, 1);

                    return Positioned(
                      top: offsetY,
                      left: offsetX,
                      child: Transform.scale(
                        scale: transform,
                        child: Opacity(
                          opacity: transform,
                          child: Container(
                            width: dotSize,
                            height: dotSize,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color(0xFF021A89),
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              Positioned(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Builder(builder: (context) {
                      final hoursRemaining = _timeRemaining.inHours.toString();
                      final minutesRemaining =
                          (_timeRemaining.inMinutes % 60).toString();
                      final secondsRemanings =
                          (_timeRemaining.inSeconds % 60).toString();

                      return Text(
                        "$hoursRemaining:$minutesRemaining:$secondsRemanings",
                        style: TextStyle(
                          fontFamily: 'SF Pro Display',
                          fontSize: ScreenUtil().setSp(33),
                          fontWeight: FontWeight.w900,
                          letterSpacing: 0.40,
                          color: Color(0xFF91A6D9),
                        ),
                      );
                    }),
                    Text(
                      S.of(context).remaining,
                      style: TextStyle(
                        fontFamily: 'SF Pro Display',
                        fontSize: ScreenUtil().setSp(14),
                        fontWeight: FontWeight.w300,
                        letterSpacing: 0.40,
                        color: Color(0xFF91A6D9),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  double _scale(
      double n, double inMin, double inMax, double outMin, double outMax) {
    return (n - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
  }

  Future<void> _startTimer() async {
    final application = Provider.of<ApplicationStore>(context, listen: false);

    Future.delayed(Duration(seconds: 1), () async {
      if (_disposed) return;
      if (_timeRemaining.inSeconds <= 0) {
        final orderDocument =
            await Firestore.instance.document(widget.order['order_id']).get();
        if (orderDocument != null && orderDocument.exists) {
          final orderStatus = orderDocument.data['status'] as int;
          if (orderStatus == 0) {
            Firestore.instance
                .collection('orders')
                .document(widget.order['order_id'])
                .updateData({
              'status': 31,
            });

            await showCupertinoDialog(
              context: context,
              builder: (BuildContext context) => CupertinoAlertDialog(
                content: Text(
                  S.of(context).orderDeclinedAutomatically,
                  style: TextStyle(
                    fontFamily:
                        application.locale == 'ar' ? 'Cairo' : 'SF Pro Display',
                    fontSize: ScreenUtil().setSp(11),
                    fontWeight: FontWeight.w900,
                    letterSpacing: 0.80,
                    color: Color(0xFFFFBA00),
                  ),
                ),
                actions: <Widget>[
                  CupertinoDialogAction(
                    child: Text(S.of(context).ok),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            );
          }
        }

        Navigator.of(context).pushReplacementNamed('/dashboard/company');
        return;
      }

      setState(() {
        final now = DateTime.now();
        final timeSinceStart = now.difference(_startTime);
        _timeRemaining = Duration(
            milliseconds: widget.timeRemaining.inMilliseconds -
                timeSinceStart.inMilliseconds);
        if (_timeRemaining.inSeconds <= 0) {
          _timeRemaining = Duration.zero;
        }
      });
      _startTimer();
    });
  }
}
