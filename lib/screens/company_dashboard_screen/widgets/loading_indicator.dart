import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:weash_cars/stores/company_transactions_store.dart';

class LoadingIndicator extends StatelessWidget {
  const LoadingIndicator(this.store);

  final CompanyTransactionsStore store;

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) =>
          store.isListening
              ? Container()
              : const LinearProgressIndicator(),
    );
  }
}
