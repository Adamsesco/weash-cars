import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:app_review/app_review.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import 'package:background_fetch/background_fetch.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/screens/assign_employee_screen/assign_employee_screen.dart';
import 'package:weash_cars/screens/company_dashboard_screen/widgets/delayed_dialog.dart';
import 'package:weash_cars/screens/company_dashboard_screen/widgets/incomes_chart.dart';
import 'package:weash_cars/screens/confirm_order_screen/confirm_order_screen.dart';
import 'package:weash_cars/screens/done_order_details_screen/done_order_details_screen.dart';
import 'package:weash_cars/screens/indiv_order_details_screen/indiv_order_details_screen.dart';
import 'package:weash_cars/screens/manage_order_screen/manage_order_screen.dart';
import 'package:weash_cars/screens/take_order_screen/take_order_screen.dart';
import 'package:weash_cars/services/IncomesService.dart';
import 'package:weash_cars/services/OrdersService.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/balance_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/widgets/company_drawer.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

int counter = 0;
int oldCounter = 0;
StreamController updateStream = new StreamController.broadcast();

void declineDelayedOrders() async {
  print('[BackgroundFetch] Event received');

  final prefs = await SharedPreferences.getInstance();
  final companyId = prefs.getString('user_id');

  if (companyId != null && companyId.isNotEmpty) {
    await http.get(
        "https://us-central1-weash-cars.cloudfunctions.net/declineDelayedOrders?companyId=$companyId");
  }

  counter += 1;
  updateStream.sink.add(true);

  BackgroundFetch.finish();
}

class CompanyDashboardScreen extends StatefulWidget {
  @override
  _CompanyDashboardScreenState createState() => _CompanyDashboardScreenState();
}

class _CompanyDashboardScreenState extends State<CompanyDashboardScreen> {
  CompanyStore _company;
  BalanceStore _balance;
  Future<Map<String, Income>> _incomesFuture;
  Future<List<Map<String, dynamic>>> _orders;

  IncomeType _selectedIncomeType;
  DateTime _selectedDate;

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  StreamSubscription<String> _notificationsStream;

  _Filter _selectedFilter = _Filter.newOrders;
  GlobalKey _filtersButtonKey = GlobalKey();
  bool _isDialogVisible = false;

  @override
  void initState() {
    super.initState();

    if (_company == null) {
      final applicationStore =
          Provider.of<ApplicationStore>(context, listen: false);
      _company = applicationStore.companyStore;
      _balance = _company.balance;

      _initPlatformState();
      _checkDelayedOrders();
      _checkUnconfirmedOrders();

      if (!_balance.isListening) {
        _balance.subscribe(_company.companyId);
      }

      final incomesService = new IncomesService();
      final now = DateTime.now();
      _incomesFuture =
          incomesService.getIncomes(_company.companyId, now.year, now.month);
      _orders = OrdersService.getOrders(companyId: _company.companyId);
      _company.orders.setOrdersFuture(_orders);

      _selectedIncomeType = IncomeType.monthly;
      _selectedDate = DateTime.now();

      _firebaseMessaging.getToken().then((String token) {
        if (_company.fcmToken != token) {
          _company.setData(fcmToken: token);
          _company.saveData();
        }
      });

      if (_notificationsStream == null) {
        _notificationsStream = applicationStore.notificationsStream.stream
            .listen((String notificationId) async {
          final database = applicationStore.database;
          final notifications = await database.query('notifications',
              where: 'id = ?', whereArgs: [notificationId]);
          if (notifications != null && notifications.length > 0) {
            await database.delete('notifications',
                where: 'id = ?', whereArgs: [notificationId]);
            applicationStore.removeNotification();
            final data = notifications[0];
            switch (data['type']) {
              case 'OrderCreated':
                final orderId = data['targetId'] as String;
                if (applicationStore.isPendingNotification) {
                  applicationStore.isPendingNotification = false;
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => CompanyDashboardScreen(),
                    ),
                  );
                } else if (orderId != null && orderId.isNotEmpty) {
                  var orders = await _company.orders.ordersFuture;
                  final newOrder = await OrdersService.getOrder(orderId);
                  if (newOrder != null &&
                      !orders.any((Map<String, dynamic> order) =>
                          order['order_id'] == newOrder['order_id'])) {
                    orders.add(newOrder);
                    setState(() {
                      _company.orders
                          .setOrdersFuture(_company.orders.ordersFuture);
                    });
                  }
                }
                break;
            }
          }
        });
      }
    }
  }

  @override
  void dispose() {
    _balance.unsubscribe();
    if (_notificationsStream != null) {
      _notificationsStream.cancel();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (counter != oldCounter) {
      print("Counter updated: $counter");
      oldCounter = counter;

      _orders = OrdersService.getOrders(companyId: _company.companyId);
      _company.orders.setOrdersFuture(_orders);
    }

    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => CompanyDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _company.avatarUrl,
        ),
      ),
      body: StreamBuilder<Object>(
          stream: updateStream.stream,
          builder: (context, snapshot) {
            return Stack(
              children: <Widget>[
                Positioned.fill(
                  child: Image(
                    image: AssetImage(
                        'assets/images/company-dashboard-background.png'),
                    fit: BoxFit.cover,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Observer(
                      builder: (BuildContext context) => CustomAppBar2(
                        onOpenDrawer: () {
                          setState(() {
                            _isDialogVisible = true;
                            Scaffold.of(context).openDrawer();
                          });
                        },
                        title: S.of(context).dashboard,
                        transparentBackground: true,
                        titleColor: Color(0xFF91A6D9),
                        nbNotifications: 0,
                        avatar: _company.avatarUrl == null
                            ? null
                            : NetworkImage(_company.avatarUrl),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(29.5)),
                    SizedBox(
                      width: ScreenUtil().setWidth(335),
                      height: ScreenUtil().setHeight(328),
                      child: FutureBuilder(
                        future: _incomesFuture,
                        builder: (BuildContext context,
                            AsyncSnapshot<Map<String, Income>> snapshot) {
                          switch (snapshot.connectionState) {
                            case ConnectionState.none:
                            case ConnectionState.waiting:
                            case ConnectionState.active:
                              return Container();

                            case ConnectionState.done:
                              return IncomesChart(
                                snapshot.data,
                                _balance.balance,
                                _company.currency,
                                onDateChanged:
                                    (DateTime date, IncomeType type) {
                                  setState(() {
                                    _selectedDate = date;
                                    _selectedIncomeType = type;
                                  });
                                },
                              );
                          }

                          return Container();
                        },
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(43.1)),
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(31.1)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            S.of(context).orders,
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF Pro Text',
                              fontSize: 13,
                              letterSpacing: 1.0,
                              fontWeight: FontWeight.w700,
                              color: Color(0xffE4E4E4),
                            ),
                          ),
                          InkWell(
                            onTap: () async {
                              final filter = await _showFiltersDialog(
                                  _selectedFilter,
                                  _getWidgetPosition(_filtersButtonKey));
                              if (filter != null) {
                                setState(() {
                                  _selectedFilter = filter;
                                });
                              }
                            },
                            child: Image(
                              key: _filtersButtonKey,
                              image:
                                  AssetImage('assets/images/filters-icon.png'),
                              width: ScreenUtil().setWidth(24.17),
                              height: ScreenUtil().setWidth(20.14),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(9)),
                    Expanded(
                      child: Observer(
                        builder: (BuildContext context) => FutureBuilder(
                          future: _company.orders.ordersFuture,
                          builder: (BuildContext context,
                              AsyncSnapshot<List<Map<String, dynamic>>>
                                  snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.active:
                              case ConnectionState.waiting:
                              case ConnectionState.none:
                                return Center(
                                  child: CupertinoActivityIndicator(
                                    animating: true,
                                  ),
                                );
                              case ConnectionState.done:
                                if (snapshot.hasError) {
                                  return Container();
                                }

                                _showRatingDialog(context, snapshot.data ?? []);

                                final orders = snapshot.data
                                    .where((Map<String, dynamic> order) {
                                  final orderTimeStamp =
                                      order['date_created'] as Timestamp;
                                  if (orderTimeStamp == null) {
                                    return false;
                                  }

                                  final dateCreated = orderTimeStamp.toDate();

                                  switch (_selectedIncomeType) {
                                    case IncomeType.daily:
                                      return dateCreated.year ==
                                              _selectedDate.year &&
                                          dateCreated.month ==
                                              _selectedDate.month &&
                                          dateCreated.day == _selectedDate.day;

                                    case IncomeType.monthly:
                                      return dateCreated.year ==
                                              _selectedDate.year &&
                                          dateCreated.month ==
                                              _selectedDate.month;

                                    case IncomeType.yearly:
                                      return dateCreated.year ==
                                          _selectedDate.year;

                                    case IncomeType.weekly:
                                      return false;
                                  }

                                  return false;
                                }).toList();

                                switch (_selectedFilter) {
                                  case _Filter.newOrders:
                                    orders.sort((order1, order2) {
                                      final status1 = order1['status'] as int;
                                      final status2 = order2['status'] as int;
                                      return status1.compareTo(status2);
                                    });
                                    break;
                                  case _Filter.pending:
                                    orders.sort((order1, order2) {
                                      final status1 = order1['status'] as int;
                                      final status2 = order2['status'] as int;

                                      return status1.compareTo(status2);
                                    });
                                    break;
                                  case _Filter.done:
                                    orders.sort((order1, order2) {
                                      final status1 = order1['status'] as int;
                                      final status2 = order2['status'] as int;

                                      if (status1 >= 20 &&
                                          status1 < 30 &&
                                          (status2 < 20 || status2 >= 30)) {
                                        return 1.compareTo(2);
                                      } else if (status2 >= 20 &&
                                          status2 < 30 &&
                                          (status1 < 20 || status1 >= 30)) {
                                        return 2.compareTo(1);
                                      } else {
                                        return status1.compareTo(status2);
                                      }
                                    });
                                    break;
                                  case _Filter.cancelled:
                                    orders.sort((order1, order2) {
                                      final status1 = order1['status'] as int;
                                      final status2 = order2['status'] as int;

                                      if (status1 >= 30 &&
                                          status1 < 40 &&
                                          (status2 < 30 || status2 >= 40)) {
                                        return 1.compareTo(2);
                                      } else if (status2 >= 30 &&
                                          status2 < 40 &&
                                          (status1 < 30 || status1 >= 40)) {
                                        return 2.compareTo(1);
                                      } else {
                                        return status1.compareTo(status2);
                                      }
                                    });
                                    break;
                                  case _Filter.latestFirst:
                                    orders.sort((order1, order2) {
                                      final date1 =
                                          order1['date_created'] as Timestamp;
                                      final date2 =
                                          order2['date_created'] as Timestamp;
                                      return date2.compareTo(date1);
                                    });
                                    break;
                                }

                                return ListView.separated(
                                  padding: EdgeInsets.zero,
                                  physics: BouncingScrollPhysics(),
                                  separatorBuilder: (BuildContext context,
                                          int index) =>
                                      SizedBox(
                                          height: ScreenUtil().setHeight(5)),
                                  itemCount: orders == null ? 0 : orders.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    final order = orders[index];
                                    return _buildOrderCard(order);
                                  },
                                );
                            }

                            return Container();
                          },
                        ),
                      ),
                    ),
                  ],
                ),
                // Visibility(
                //   visible: _isDialogVisible,
                //   child: Positioned.fill(
                //     child: BackdropFilter(
                //       filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                //       child: Container(
                //         color: Colors.grey.withOpacity(0.2),
                //       ),
                //     ),
                //   ),
                // ),
              ],
            );
          }),
    );
  }

  Offset _getWidgetPosition(GlobalKey key) {
    final RenderBox renderBox = key.currentContext.findRenderObject();
    final position = renderBox.localToGlobal(Offset.zero);
    return position;
  }

  Future<_Filter> _showFiltersDialog(_Filter selectedFilter, Offset offset) {
    return showGeneralDialog<_Filter>(
        context: context,
        barrierDismissible: true,
        barrierLabel: '',
        transitionDuration: Duration(milliseconds: 200),
        pageBuilder: (BuildContext context, Animation<double> animation1,
            Animation<double> animation2) {
          return ValueListenableBuilder(
            valueListenable: animation1,
            builder: (BuildContext context, double animationValue, _) =>
                _FiltersDialog(selectedFilter, offset, animationValue),
          );
        });
  }

  Widget _buildOrderCard(Map<String, dynamic> order) {
    final application = Provider.of<ApplicationStore>(context);

    final orderStatus = order['status'] as int;
    final orderLocality = order['locality'] ?? ' ';
    final orderStatusColors = {
      0: Colors.red,
      1: Colors.red,
      2: Colors.red,
      10: Color(0xFFFFBA00),
      11: Color(0xFFFFBA00),
      12: Color(0xFFFFBA00),
      20: Color(0xFF07D80F),
      21: Color(0xFF07D80F),
      30: Colors.grey[400],
      31: Colors.grey[400],
      32: Colors.grey[400],
    };
    final orderNumber = order['order_number'] as int;
    final orderNumberStr =
        List.generate(6 - orderNumber.toString().length, (_) => '0').join() +
            orderNumber.toString();

    final customer = order['customer'];
    final customerAvatarUrl = customer['avatar_url'];
    final customerFirstName = customer['first_name'];
    final customerLastName = customer['last_name'];

    final package = order['package'];
    final packagePrice = package['price'];
    final packageCurrency = _company.currency;

    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () async {
          switch (orderStatus) {
            case 0:
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => ConfirmOrderScreen(
                    order: order,
                  ),
                ),
              );
              break;

            case 1:
              if (_company.employees != null &&
                  _company.employees.length == 0) {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => IndivOrderDetailsScreen(
                      order: order,
                    ),
                  ),
                );
              } else {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => AssignEmployeeScreen(
                      order: order,
                    ),
                  ),
                );
              }
              break;

            case 10:
            case 11:
            case 12:
              final assignedEmployeeId = order['assigned_employee_id'];

              if (assignedEmployeeId == null) {
                if (_company.employees != null &&
                    _company.employees.length == 0) {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => IndivOrderDetailsScreen(
                        order: order,
                      ),
                    ),
                  );
                } else {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => AssignEmployeeScreen(
                        order: order,
                      ),
                    ),
                  );
                }
              } else if (assignedEmployeeId == _company.companyId) {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => TakeOrderScreen(
                      order: order,
                    ),
                  ),
                );
              } else {
                final employee = _company.employees.firstWhere(
                  (Map<String, dynamic> employee) =>
                      employee['employee_id'] == assignedEmployeeId,
                  orElse: () => null,
                );

                if (employee == null) {
                  if (_company.employees != null &&
                      _company.employees.length == 0) {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => IndivOrderDetailsScreen(
                          order: order,
                        ),
                      ),
                    );
                  } else {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => AssignEmployeeScreen(
                          order: order,
                        ),
                      ),
                    );
                  }
                } else {
                  switch (employee['type']) {
                    case 'classic':
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ManageOrderScreen(
                            order: order,
                          ),
                        ),
                      );
                      break;

                    case 'smart':
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => AssignEmployeeScreen(
                            order: order,
                          ),
                        ),
                      );
                      break;

                    default:
                      if (_company.employees != null &&
                          _company.employees.length == 0) {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => IndivOrderDetailsScreen(
                              order: order,
                            ),
                          ),
                        );
                      } else {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => AssignEmployeeScreen(
                              order: order,
                            ),
                          ),
                        );
                      }
                      break;
                  }
                }
              }
              break;

            case 20:
            case 21:
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => DoneOrderDetailsScreen(
                    order: order,
                  ),
                ),
              );
              break;
          }
        },
        highlightColor: Colors.transparent,
        splashColor: Colors.transparent,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(16)),
          padding: EdgeInsets.symmetric(
            horizontal: ScreenUtil().setWidth(15),
            vertical: ScreenUtil().setHeight(11),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            image: orderStatus == 0 || orderStatus == 1
                ? DecorationImage(
                    image: AssetImage('assets/images/new-order-background.png'),
                    fit: BoxFit.cover,
                  )
                : null,
            gradient: LinearGradient(
              colors: [Color(0xffD8D8D8), Color(0xffffffff)],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: ScreenUtil().setWidth(46.53),
                        height: ScreenUtil().setWidth(46.53),
                        padding: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(2.3),
                          vertical: ScreenUtil().setWidth(2.3),
                        ),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                        ),
                        child: ClipPath(
                          clipper: ShapeBorderClipper(
                            shape: CircleBorder(),
                          ),
                          child: Image(
                            image: customerAvatarUrl == null
                                ? AssetImage('assets/images/avatar-user.png')
                                : NetworkImage(customerAvatarUrl),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(
                            ScreenUtil().setWidth(
                                application.locale == 'ar' ? 16 : -16),
                            -ScreenUtil().setHeight(2)),
                        child: Container(
                          width: ScreenUtil().setWidth(18),
                          height: ScreenUtil().setWidth(18),
                          padding: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(3),
                            vertical: ScreenUtil().setWidth(3),
                          ),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: orderStatusColors[orderStatus],
                            border: Border.all(
                              color: Colors.white,
                              width: 3,
                            ),
                          ),
                        ),
                      ),
                      // SizedBox(width: ScreenUtil().setWidth(19)),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: ScreenUtil().setHeight(8)),
                          Text(
                            "$customerFirstName $customerLastName"
                                .toUpperCase(),
                            style: TextStyle(
                              fontFamily: application.locale == 'ar'
                                  ? 'Cairo'
                                  : 'SF UI Display',
                              fontWeight: FontWeight.w700,
                              fontSize: 13,
                              color: orderStatus < 10
                                  ? Colors.white
                                  : Color(0xFF717171),
                              letterSpacing: 0.5,
                            ),
                          ),
                          SizedBox(height: ScreenUtil().setHeight(4)),
                          Text(
                            orderLocality,
                            style: TextStyle(
                              fontFamily: 'SF UI Display',
                              fontWeight: FontWeight.w300,
                              color: orderStatus < 10
                                  ? Colors.white
                                  : Color(0xFF717171),
                              fontSize: 13,
                              letterSpacing: 1,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(14.5),
                  ),
                  SizedBox(
                    height: ScreenUtil().setHeight(14),
                    child: FittedBox(
                      fit: BoxFit.fitHeight,
                      child: Text(
                        "${S.of(context).orderNumber}N$orderNumberStr",
                        style: TextStyle(
                          fontFamily:
                              application.locale == 'ar' ? 'Cairo' : 'Lato',
                          fontWeight: FontWeight.w700,
                          color: orderStatus < 10
                              ? Colors.white
                              : Color(0xFF717171),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  if (orderStatus == 0) ...[
                    Text(
                      S.of(context).newOrder,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Text',
                        fontSize: 18,
                        fontWeight: FontWeight.w800,
                        color: Color(0xFFFFFF00),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(10)),
                  ],
                  if (orderStatus == 1) ...[
                    Text(
                      'ACCEPTED',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: application.locale == 'ar'
                            ? 'Cairo'
                            : 'SF Pro Text',
                        fontSize: 18,
                        fontWeight: FontWeight.w800,
                        // color: Color(0xFF03C428),
                        color: Color(0xFFFFFF00),
                      ),
                    ),
                    SizedBox(height: ScreenUtil().setHeight(10)),
                  ],
                  if (orderStatus == 1 ||
                      (orderStatus >= 10 && orderStatus < 20)) ...[
                    Row(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            _shareOrder(order);
                          },
                          child: Padding(
                            padding: EdgeInsetsDirectional.only(
                              top: ScreenUtil().setHeight(4),
                              bottom: ScreenUtil().setHeight(4),
                              start: ScreenUtil().setWidth(6),
                            ),
                            child: Icon(
                              Icons.share,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(6)),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => DoneOrderDetailsScreen(
                                  order: order,
                                ),
                              ),
                            );
                          },
                          child: Padding(
                            padding: EdgeInsetsDirectional.only(
                              top: ScreenUtil().setHeight(4),
                              bottom: ScreenUtil().setHeight(4),
                              start: ScreenUtil().setWidth(6),
                            ),
                            child: Icon(
                              Icons.info_outline,
                              color: Colors.blueGrey,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: ScreenUtil().setHeight(10)),
                  ],
                  SizedBox(
                    height: ScreenUtil().setHeight(19),
                    child: FittedBox(
                      fit: BoxFit.fitHeight,
                      child: Text(
                        "$packagePrice $packageCurrency",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Text',
                          fontWeight: FontWeight.w800,
                          color: orderStatus < 10
                              ? Colors.white
                              : orderStatusColors[orderStatus],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _shareOrder(Map<String, dynamic> order) async {
//    final assignedEmployeeId = order['assigned_employee_id'] as String;
//    if (assignedEmployeeId == null || assignedEmployeeId.isEmpty) {
//      showCupertinoDialog(
//        context: context,
//        builder: (_) => CupertinoAlertDialog(
//          title: Text(S.of(context).An_error_occurred),
//          content:
//              Text(S.of(context).An_unexpected_error_happened_Please_try_again),
//          actions: <Widget>[
//            CupertinoDialogAction(
//              onPressed: () => Navigator.of(context).pop(),
//              child: Text(S.of(context).ok),
//            ),
//          ],
//        ),
//      );
//
//      return;
//    }
//    final employeesDocuments = await Firestore.instance
//        .collection('users')
//        .where('phone_number', isEqualTo: assignedEmployeeId)
//        .getDocuments();
//    if (employeesDocuments == null ||
//        employeesDocuments.documents == null ||
//        employeesDocuments.documents.length != 1) {
//      showCupertinoDialog(
//        context: context,
//        builder: (_) => CupertinoAlertDialog(
//          title: Text(S.of(context).An_error_occurred),
//          content:
//              Text(S.of(context).An_unexpected_error_happened_Please_try_again),
//          actions: <Widget>[
//            CupertinoDialogAction(
//              onPressed: () => Navigator.of(context).pop(),
//              child: Text(S.of(context).ok),
//            ),
//          ],
//        ),
//      );
//
//      return;
//    }
//
//    final employeeDocument = employeesDocuments.documents.first;
//    final employeePhone = employeeDocument.data['phone_number'];

    final package = order['package'] as Map<String, dynamic>;
    final packageTitle = package['title'] as String;

    final customer = order['customer'] as Map<String, dynamic>;
    final customerPhone = customer['phone_number'] as String;

    final date = order['date'] as DateTime;
    final formattedDate = DateFormat('y-MM-dd hh:mm').format(date);

    final address = order['address'] as String;

    final location = order['location'] as GeoPoint;
    final gmapsUrl =
        "https://www.google.com/maps/search/?api=1&query=${location.latitude},${location.longitude}";
    final postBody =
        '{"domain": "bit.ly", "title": "Order location", "long_url": "$gmapsUrl"}';
    print(postBody);

    final response = await http.post(
      'https://api-ssl.bitly.com/v4/bitlinks',
      body: postBody,
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer 898d9a8696f65f6cdea77038ec63f87b01b514a5'
      },
    );
    if (response == null ||
        response.statusCode != 200 ||
        response.body == null ||
        response.body.isEmpty) {
      showCupertinoDialog(
        context: context,
        builder: (_) => CupertinoAlertDialog(
          title: Text(S.of(context).An_error_occurred),
          content:
              Text(S.of(context).An_unexpected_error_happened_Please_try_again),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).ok),
            ),
          ],
        ),
      );

      return;
    }

    final responseJson = jsonDecode(response.body);
    if (responseJson == null || responseJson['link'] == null) {
      showCupertinoDialog(
        context: context,
        builder: (_) => CupertinoAlertDialog(
          title: Text(S.of(context).An_error_occurred),
          content:
              Text(S.of(context).An_unexpected_error_happened_Please_try_again),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).ok),
            ),
          ],
        ),
      );

      return;
    }

    final link = responseJson['link'] as String;

    final text =
        "$formattedDate\n$address\n$packageTitle\n$customerPhone\n$link\n\nVia Washy Car app.";
    print(text);

    Share.text(S.of(context).newOrder, text, 'text/plain');
  }

  Future<void> _showRatingDialog(
      BuildContext context, List<Map<String, dynamic>> orders) async {
    final prefs = await SharedPreferences.getInstance();
    final alreadyRated = prefs.getBool('rated') ?? false;
    if (alreadyRated) {
      return;
    }

    final lastAsked = prefs.getInt('last_rate') ?? 0;
    final lastAskedDate = DateTime.fromMillisecondsSinceEpoch(lastAsked);
    final now = DateTime.now();
    if (!lastAskedDate.isBefore(now.subtract(Duration(days: 7)))) {
      return;
    }

    prefs.setInt('last_rate', now.millisecondsSinceEpoch);

    final hasConfirmedOrder =
        orders.any((order) => order['status'] as int == 21);

    if (hasConfirmedOrder) {
      print('rate');
      if (Platform.isIOS) {
        final value = await AppReview.requestReview;
        print("Rating: $value");
        prefs.setBool('rated', true);
      } else if (Platform.isAndroid) {
        final rate = await showCupertinoDialog<bool>(
          context: context,
          builder: (BuildContext context) => CupertinoAlertDialog(
            title: Text(S.of(context).rateThisApp),
            content: Text(S.of(context).ifYouLikeThisApp),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text(S.of(context).rate),
                onPressed: () => Navigator.of(context).pop(true),
              ),
              CupertinoDialogAction(
                child: Text(S.of(context).notNow),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          ),
        );

        if (rate) {
          final value = await AppReview.requestReview;
          print("Rating: $value");
          prefs.setBool('rated', true);
        }
      }
    }
  }

  Future<void> _initPlatformState() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('user_id', _company.companyId);

    BackgroundFetch.configure(
            BackgroundFetchConfig(
                minimumFetchInterval: 15,
                stopOnTerminate: false,
                enableHeadless: true),
            declineDelayedOrders)
        .then((int status) {
      print('[BackgroundFetch] SUCCESS: $status');
    }).catchError((e) {
      print('[BackgroundFetch] ERROR: $e');
    });

    BackgroundFetch.registerHeadlessTask(declineDelayedOrders);

    BackgroundFetch.start().then((int status) {
      print('[BackgroundFetch] start success: $status');
    }).catchError((e) {
      print('[BackgroundFetch] start FAILURE: $e');
    });
  }

  Future<void> _checkDelayedOrders() async {
    final request = await http.get(
        "https://us-central1-weash-cars.cloudfunctions.net/getDelayedOrder?companyId=${_company.companyId}");
    if (request.statusCode == 200 && request.body.isNotEmpty) {
      final response = jsonDecode(request.body);
      final orderId = response['order_id'] as String;
      final delay = int.tryParse(response['delay'] as String) ?? 0;

      if (delay > 0) {
        final order = await OrdersService.getOrder(orderId);

        if (!mounted) {
          await Future.delayed(Duration(seconds: 2), () {});
        }

        showDialog(
            context: context,
            builder: (BuildContext context) {
              return DelayedDialog(
                order: order,
              );
            });
      }
    }
  }

  Future<void> _checkUnconfirmedOrders() async {
    final unconfirmedOrdersDocuments = await Firestore.instance
        .collection('orders')
        .where('company_id', isEqualTo: _company.companyId)
        .where('status', isEqualTo: 20)
        .where('done_date',
            isLessThanOrEqualTo:
                DateTime.now().subtract(Duration(hours: 1)).toUtc())
        .getDocuments();

    if (unconfirmedOrdersDocuments != null &&
        unconfirmedOrdersDocuments.documents.length > 0) {
      await Future.forEach(unconfirmedOrdersDocuments.documents,
          (DocumentSnapshot document) async {
        await Firestore.instance
            .collection('orders')
            .document(document.documentID)
            .updateData({
          'status': 21,
        });
      });
    }
  }
}

class _AvailabilityDialog extends StatefulWidget {
  final bool isAvailable;
  final double animationValue;

  _AvailabilityDialog(this.isAvailable, this.animationValue, {Key key})
      : super(key: key);

  @override
  _AvailabilityDialogState createState() => _AvailabilityDialogState();
}

class _AvailabilityDialogState extends State<_AvailabilityDialog> {
  bool _isAvailable;

  @override
  void initState() {
    super.initState();
    _isAvailable = widget.isAvailable;
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    final animationEnded = widget.animationValue == 1.0;

    return Stack(
      children: <Widget>[
        Positioned(
          top: ScreenUtil().setHeight(100),
          right: ScreenUtil().setWidth(20),
          child: Container(
            width: max(ScreenUtil().setWidth(27.0 + 48.0),
                ScreenUtil().setWidth(160) * widget.animationValue),
            height: max(ScreenUtil().setHeight(23.0 + 52.0),
                ScreenUtil().setHeight(180) * widget.animationValue),
            padding: EdgeInsets.symmetric(
              horizontal: ScreenUtil().setWidth(24),
              vertical: ScreenUtil().setHeight(26),
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    animationEnded
                        ? Text(
                            'Availability',
                            style: TextStyle(
                              fontFamily:
                                  application.locale == 'ar' ? 'Cairo' : 'Lato',
                              fontFamilyFallback: <String>['SF Pro Text'],
                              fontSize: ScreenUtil().setSp(14),
                              fontWeight: FontWeight.w900,
                              color: Color(0xff021a89),
                              decoration: TextDecoration.none,
                            ),
                          )
                        : SizedBox(width: 0, height: 0),
                  ],
                ),
                animationEnded
                    ? SizedBox(height: ScreenUtil().setHeight(28))
                    : SizedBox(width: 0, height: 0),
                animationEnded
                    ? Expanded(
                        child: Material(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              FractionallySizedBox(
                                widthFactor: 1,
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      _isAvailable = true;
                                      Navigator.of(context).pop(true);
                                    });
                                  },
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      RichText(
                                        text: TextSpan(
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: 'Available',
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'Lato',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: ScreenUtil().setSp(9),
                                                fontWeight: FontWeight.w900,
                                                color: Color(0xff011b7b),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      _isAvailable
                                          ? Image(
                                              image: AssetImage(
                                                  'assets/images/checkmark-icon.png'),
                                              width: ScreenUtil().setWidth(12),
                                              height: ScreenUtil().setHeight(8),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                              ),
                              FractionallySizedBox(
                                widthFactor: 1,
                                child: InkWell(
                                  onTap: () {
                                    setState(() {
                                      _isAvailable = false;
                                      Navigator.of(context).pop(false);
                                    });
                                  },
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      RichText(
                                        text: TextSpan(
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: 'Unavailable',
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'Lato',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: ScreenUtil().setSp(9),
                                                fontWeight: FontWeight.w900,
                                                color: Color(0xff011b7b),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      !_isAvailable
                                          ? Image(
                                              image: AssetImage(
                                                  'assets/images/checkmark-icon.png'),
                                              width: ScreenUtil().setWidth(12),
                                              height: ScreenUtil().setHeight(8),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                              ),
                              FractionallySizedBox(
                                widthFactor: 1,
                                child: InkWell(
                                  onTap: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      RichText(
                                        text: TextSpan(
                                          children: <TextSpan>[
                                            TextSpan(
                                              text: 'Schedule',
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'Lato',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: ScreenUtil().setSp(9),
                                                fontWeight: FontWeight.w900,
                                                color: Color(0xff011b7b),
                                                decoration: TextDecoration.none,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    : SizedBox(width: 0, height: 0),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class _FiltersDialog extends StatefulWidget {
  final _Filter selectedFilter;
  final Offset offset;
  final double animationValue;

  _FiltersDialog(this.selectedFilter, this.offset, this.animationValue,
      {Key key})
      : super(key: key);

  @override
  __FiltersDialogState createState() => __FiltersDialogState();
}

class __FiltersDialogState extends State<_FiltersDialog> {
  _Filter _selectedFilter;

  @override
  void initState() {
    super.initState();
    _selectedFilter = widget.selectedFilter;
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);
    final animationEnded = widget.animationValue == 1.0;

    return Transform.translate(
      offset: Offset(0, -ScreenUtil().setHeight(25.9)),
      child: Stack(
        children: <Widget>[
          Positioned.directional(
            textDirection: Directionality.of(context),
            top: widget.offset.dy,
            end: ScreenUtil().setWidth(15.3),
            child: Container(
              width: max(ScreenUtil().setWidth(27.0 + 48.0),
                  ScreenUtil().setWidth(221.96) * widget.animationValue),
              height: max(ScreenUtil().setHeight(23.0 + 52.0),
                  ScreenUtil().setHeight(240) * widget.animationValue),
              padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(23.7),
                vertical: ScreenUtil().setHeight(25.9),
              ),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Color(0x30000000),
                    ),
                  ]),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      animationEnded
                          ? Text(
                              S.of(context).sortBy,
                              style: TextStyle(
                                fontFamily: application.locale == 'ar'
                                    ? 'Cairo'
                                    : 'Lato',
                                fontFamilyFallback: <String>['SF Pro Text'],
                                fontSize: ScreenUtil().setSp(14),
                                fontWeight: FontWeight.w900,
                                color: Color(0xff021a89),
                                decoration: TextDecoration.none,
                              ),
                            )
                          : SizedBox(width: 0, height: 0),
                      Material(
                        child: InkWell(
                          onTap: () => Navigator.of(context).pop(),
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          child: Image(
                            image: AssetImage('assets/images/filters-icon.png'),
                            width: ScreenUtil().setWidth(24.17),
                            height: ScreenUtil().setWidth(20.14),
                            color: Color(0xffd2d2d6),
                          ),
                        ),
                      ),
                    ],
                  ),
                  animationEnded
                      ? SizedBox(height: ScreenUtil().setHeight(28))
                      : SizedBox(width: 0, height: 0),
                  animationEnded
                      ? Expanded(
                          child: Material(
                            color: Colors.transparent,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                FractionallySizedBox(
                                  widthFactor: 1,
                                  child: InkWell(
                                    highlightColor: Colors.transparent,
                                    onTap: () {
                                      setState(() {
                                        _selectedFilter = _Filter.newOrders;
                                        Navigator.of(context)
                                            .pop(_Filter.newOrders);
                                      });
                                    },
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          width: ScreenUtil().setWidth(8),
                                          height: ScreenUtil().setHeight(8),
                                          decoration: BoxDecoration(
                                            color: Colors.red,
                                            shape: BoxShape.circle,
                                          ),
                                        ),
                                        SizedBox(
                                            width: ScreenUtil().setWidth(10)),
                                        Text(
                                          S.of(context).newOrders,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Text',
                                            fontSize: ScreenUtil().setSp(9),
                                            fontWeight: FontWeight.w700,
                                            letterSpacing: 1.0,
                                            color: Color(0xff011b7b),
                                            decoration: TextDecoration.none,
                                          ),
                                        ),
                                        Spacer(),
                                        _selectedFilter == _Filter.newOrders
                                            ? Image(
                                                image: AssetImage(
                                                    'assets/images/checkmark-icon.png'),
                                                width:
                                                    ScreenUtil().setWidth(12),
                                                height:
                                                    ScreenUtil().setHeight(8),
                                              )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                ),
                                FractionallySizedBox(
                                  widthFactor: 1,
                                  child: InkWell(
                                    highlightColor: Colors.transparent,
                                    onTap: () {
                                      setState(() {
                                        _selectedFilter = _Filter.pending;
                                        Navigator.of(context)
                                            .pop(_Filter.pending);
                                      });
                                    },
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          width: ScreenUtil().setWidth(8),
                                          height: ScreenUtil().setHeight(8),
                                          decoration: BoxDecoration(
                                            color: Color(0xFFFFBA00),
                                            shape: BoxShape.circle,
                                          ),
                                        ),
                                        SizedBox(
                                            width: ScreenUtil().setWidth(10)),
                                        Text(
                                          S.of(context).pending,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Text',
                                            fontSize: ScreenUtil().setSp(9),
                                            fontWeight: FontWeight.w700,
                                            letterSpacing: 1.0,
                                            color: Color(0xff011b7b),
                                            decoration: TextDecoration.none,
                                          ),
                                        ),
                                        Spacer(),
                                        _selectedFilter == _Filter.pending
                                            ? Image(
                                                image: AssetImage(
                                                    'assets/images/checkmark-icon.png'),
                                                width:
                                                    ScreenUtil().setWidth(12),
                                                height:
                                                    ScreenUtil().setHeight(8),
                                              )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                ),
                                FractionallySizedBox(
                                  widthFactor: 1,
                                  child: InkWell(
                                    highlightColor: Colors.transparent,
                                    onTap: () {
                                      setState(() {
                                        _selectedFilter = _Filter.done;
                                        Navigator.of(context).pop(_Filter.done);
                                      });
                                    },
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          width: ScreenUtil().setWidth(8),
                                          height: ScreenUtil().setHeight(8),
                                          decoration: BoxDecoration(
                                            color: Color(0xFF07D80F),
                                            shape: BoxShape.circle,
                                          ),
                                        ),
                                        SizedBox(
                                            width: ScreenUtil().setWidth(10)),
                                        Text(
                                          S.of(context).DONE,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Text',
                                            fontSize: ScreenUtil().setSp(9),
                                            fontWeight: FontWeight.w700,
                                            letterSpacing: 1.0,
                                            color: Color(0xff011b7b),
                                            decoration: TextDecoration.none,
                                          ),
                                        ),
                                        Spacer(),
                                        _selectedFilter == _Filter.done
                                            ? Image(
                                                image: AssetImage(
                                                    'assets/images/checkmark-icon.png'),
                                                width:
                                                    ScreenUtil().setWidth(12),
                                                height:
                                                    ScreenUtil().setHeight(8),
                                              )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                ),
                                FractionallySizedBox(
                                  widthFactor: 1,
                                  child: InkWell(
                                    highlightColor: Colors.transparent,
                                    onTap: () {
                                      setState(() {
                                        _selectedFilter = _Filter.cancelled;
                                        Navigator.of(context).pop(_Filter.cancelled);
                                      });
                                    },
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          width: ScreenUtil().setWidth(8),
                                          height: ScreenUtil().setHeight(8),
                                          decoration: BoxDecoration(
                                            color: Colors.grey[400],
                                            shape: BoxShape.circle,
                                          ),
                                        ),
                                        SizedBox(
                                            width: ScreenUtil().setWidth(10)),
                                        Text(
                                          S.of(context).CANCELLED,
                                          style: TextStyle(
                                            fontFamily:
                                            application.locale == 'ar'
                                                ? 'Cairo'
                                                : 'SF Pro Text',
                                            fontSize: ScreenUtil().setSp(9),
                                            fontWeight: FontWeight.w700,
                                            letterSpacing: 1.0,
                                            color: Color(0xff011b7b),
                                            decoration: TextDecoration.none,
                                          ),
                                        ),
                                        Spacer(),
                                        _selectedFilter == _Filter.cancelled
                                            ? Image(
                                          image: AssetImage(
                                              'assets/images/checkmark-icon.png'),
                                          width:
                                          ScreenUtil().setWidth(12),
                                          height:
                                          ScreenUtil().setHeight(8),
                                        )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                ),
                                FractionallySizedBox(
                                  widthFactor: 1,
                                  child: InkWell(
                                    highlightColor: Colors.transparent,
                                    onTap: () {
                                      setState(() {
                                        _selectedFilter = _Filter.latestFirst;
                                        Navigator.of(context)
                                            .pop(_Filter.latestFirst);
                                      });
                                    },
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          width: ScreenUtil().setWidth(8),
                                          height: ScreenUtil().setHeight(8),
                                          decoration: BoxDecoration(
                                            color: Colors.deepPurple,
                                            shape: BoxShape.circle,
                                          ),
                                        ),
                                        SizedBox(
                                            width: ScreenUtil().setWidth(10)),
                                        Text(
                                          S.of(context).latestFirst,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Text',
                                            fontSize: ScreenUtil().setSp(9),
                                            fontWeight: FontWeight.w700,
                                            letterSpacing: 1.0,
                                            color: Color(0xff011b7b),
                                            decoration: TextDecoration.none,
                                          ),
                                        ),
                                        Spacer(),
                                        _selectedFilter == _Filter.latestFirst
                                            ? Image(
                                                image: AssetImage(
                                                    'assets/images/checkmark-icon.png'),
                                                width:
                                                    ScreenUtil().setWidth(12),
                                                height:
                                                    ScreenUtil().setHeight(8),
                                              )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      : SizedBox(width: 0, height: 0),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

enum _Filter {
  newOrders,
  pending,
  done,
  cancelled,
  latestFirst,
}
