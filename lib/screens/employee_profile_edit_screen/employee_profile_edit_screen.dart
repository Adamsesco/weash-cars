import 'dart:io';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/payment_method.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/employee_store.dart';
import 'package:weash_cars/stores/orders_store.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';
import 'package:weash_cars/widgets/employee_drawer.dart';

class EmployeeProfileEditScreen extends StatefulWidget {
  @override
  _EmployeeProfileEditScreenState createState() =>
      _EmployeeProfileEditScreenState();
}

class _EmployeeProfileEditScreenState extends State<EmployeeProfileEditScreen> {
  EmployeeStore _employee;
  GlobalKey<FormState> _formKey = new GlobalKey();
  FocusNode _lastNameFocusNode = new FocusNode();
  FocusNode _phoneNumberFocusNode = new FocusNode();
  String _firstName;
  String _lastName;
  String _phoneNumber;
  String _locale;
  File _avatarFile;

  bool _isDialogVisible = false;
  bool _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_employee == null) {
      final applicationStore = Provider.of<ApplicationStore>(context);
      _employee = applicationStore.employeeStore;
      _firstName = _employee.firstName;
      _lastName = _employee.lastName;
      _phoneNumber = _employee.phoneNumber;
      _locale = applicationStore.locale;
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => EmployeeDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _employee.avatarUrl,
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned.fill(
            child: Image(
              image:
                  AssetImage('assets/images/employee-screens-background.png'),
              matchTextDirection: true,
              fit: BoxFit.cover,
            ),
          ),
          Positioned(
            top: 0,
            child: Observer(
              builder: (BuildContext context) => CustomAppBar2(
                onOpenDrawer: () {
                  setState(() {
                    _isDialogVisible = true;
                    Scaffold.of(context).openDrawer();
                  });
                },
                title: S.of(context).dashboard,
                transparentBackground: true,
                titleColor: Color(0xFFA0AEE6),
                buttonColor: Color(0xFF0C2461),
                nbNotifications: 0,
                avatar: _employee.avatarUrl == null
                    ? null
                    : NetworkImage(_employee.avatarUrl),
              ),
            ),
          ),
          Positioned.fill(
            top: ScreenUtil().setHeight(64 + 58.54),
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(58.6)),
                  Transform.translate(
                    offset: Offset(
                        ScreenUtil()
                            .setWidth(application.locale == 'ar' ? -32 : 32),
                        0),
                    child: Align(
                      alignment: application.locale == 'ar'
                          ? Alignment.topRight
                          : Alignment.topLeft,
                      child: Text(
                        S.of(context).editYourProfile,
                        style: TextStyle(
                          fontFamily: application.locale == 'ar'
                              ? 'Cairo'
                              : 'SF Pro Display',
                          fontSize: 24,
                          fontWeight: FontWeight.w900,
                          color: Color(0xFFA0AEE6),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(53)),
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(45)),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            InkWell(
                              onTap: _pickPhoto,
                              highlightColor: Colors.transparent,
                              splashColor: Colors.transparent,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  CircleAvatar(
                                    radius: ScreenUtil().setWidth(45),
                                    backgroundColor: Color(0xFFF0F0F0),
                                    backgroundImage: _avatarFile == null
                                        ? _employee.avatarUrl == null
                                            ? null
                                            : NetworkImage(_employee.avatarUrl)
                                        : FileImage(_avatarFile),
                                  ),
                                  Transform.translate(
                                    offset: Offset(
                                        ScreenUtil().setWidth(
                                            application.locale == 'ar'
                                                ? 30
                                                : -30),
                                        ScreenUtil().setHeight(6)),
                                    child: Container(
                                      width: ScreenUtil().setWidth(35),
                                      height: ScreenUtil().setWidth(35),
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                          image: AssetImage(
                                              'assets/images/camera-icon-background.png'),
                                        ),
                                      ),
                                      child: Image(
                                        image: AssetImage(
                                            'assets/images/camera-white-icon.png'),
                                        width: ScreenUtil().setWidth(14.25),
                                        height: ScreenUtil().setHeight(12.04),
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(height: ScreenUtil().setHeight(20)),
                                Text(
                                  "${_employee.firstName} ${_employee.lastName}",
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontFamilyFallback: <String>['SF Pro Text'],
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFF021A89),
                                  ),
                                ),
                                Text(
                                  "${_employee.jobsDoneCount} ${S.of(context).jobsDone}",
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontFamilyFallback: <String>['SF Pro Text'],
                                    fontSize: 10,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xFF021A89),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(36)),
                        Flexible(
                          fit: FlexFit.loose,
                          child: Form(
                            key: _formKey,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  S.of(context).firstName,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontFamilyFallback: <String>['SF Pro Text'],
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff021A89),
                                  ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(6)),
                                TextFormField(
                                  keyboardType: TextInputType.text,
                                  textInputAction: TextInputAction.next,
                                  textCapitalization: TextCapitalization.words,
                                  initialValue: _firstName ?? '',
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xff021A89),
                                  ),
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(0),
                                      right: ScreenUtil().setWidth(0),
                                      bottom: ScreenUtil().setHeight(1.5),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide.none,
                                    ),
                                  ),
                                  validator: (String val) {
                                    if (val.length < 2) {
                                      return S.of(context).firstNameError;
                                    }
                                  },
                                  onFieldSubmitted: (_) =>
                                      FocusScope.of(context)
                                          .requestFocus(_lastNameFocusNode),
                                  onSaved: (String val) => _firstName = val,
                                ),
                                FractionallySizedBox(
                                  widthFactor: 1,
                                  child: Container(
                                    height: ScreenUtil().setHeight(0.3),
                                    color: Color(0xFFC5D4F8),
                                  ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(23.5)),
                                Text(
                                  S.of(context).lastName,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontFamilyFallback: <String>['SF Pro Text'],
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff021A89),
                                  ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(6)),
                                TextFormField(
                                  focusNode: _lastNameFocusNode,
                                  keyboardType: TextInputType.text,
                                  textInputAction: TextInputAction.next,
                                  textCapitalization: TextCapitalization.words,
                                  initialValue: _lastName ?? '',
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xff021A89),
                                  ),
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(0),
                                      right: ScreenUtil().setWidth(0),
                                      bottom: ScreenUtil().setHeight(1.5),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide.none,
                                    ),
                                  ),
                                  validator: (String val) {
                                    if (val.length < 2) {
                                      return S.of(context).lastNameError;
                                    }
                                  },
                                  onSaved: (String val) => _lastName = val,
                                ),
                                FractionallySizedBox(
                                  widthFactor: 1,
                                  child: Container(
                                    height: ScreenUtil().setHeight(0.3),
                                    color: Color(0xFFC5D4F8),
                                  ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(23.5)),
                                Text(
                                  S.of(context).phoneNumber,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontFamilyFallback: <String>['SF Pro Text'],
                                    fontSize: 13,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xFFD8D8D8),
                                  ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(6)),
                                TextFormField(
                                  keyboardType: TextInputType.phone,
                                  textInputAction: TextInputAction.done,
                                  textDirection: TextDirection.ltr,
                                  initialValue: _phoneNumber ?? '',
                                  enabled: false,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Text',
                                    fontWeight: FontWeight.w700,
                                    color: Color(0xFFD8D8D8),
                                  ),
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                      left: ScreenUtil().setWidth(0),
                                      right: ScreenUtil().setWidth(0),
                                      bottom: ScreenUtil().setHeight(1.5),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide.none,
                                    ),
                                  ),
                                  validator: (String val) {
                                    var pattern = r"^\+[0-9]{10,}$";
                                    var regexp = new RegExp(pattern);
                                    if (!regexp.hasMatch(val)) {
                                      return S.of(context).phoneNumberError;
                                    }
                                  },
                                  onFieldSubmitted: (_) {
                                    _save();
                                    // FocusScope.of(context)
                                    //     .requestFocus(FocusNode());
                                  },
                                  onSaved: (String val) => _phoneNumber = val,
                                ),
                                FractionallySizedBox(
                                  widthFactor: 1,
                                  child: Container(
                                    height: ScreenUtil().setHeight(0.3),
                                    color: Color(0xFFC5D4F8),
                                  ),
                                ),
                                SizedBox(height: ScreenUtil().setHeight(29.5)),
                                Align(
                                  alignment: application.locale == 'ar'
                                      ? Alignment.topRight
                                      : Alignment.topLeft,
                                  child: Text(
                                    S.of(context).language,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF Pro Text',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xFF021A89),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      horizontal: ScreenUtil().setWidth(35)),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    textDirection: TextDirection.ltr,
                                    children: <Widget>[
                                      InkWell(
                                        onTap: () =>
                                            setState(() => _locale = 'en'),
                                        enableFeedback: false,
                                        child: Row(
                                          textDirection: TextDirection.ltr,
                                          children: <Widget>[
                                            Container(
                                              width: ScreenUtil().setWidth(12),
                                              height: ScreenUtil().setWidth(12),
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                border: Border.all(
                                                  color: Color(0xFF021A89),
                                                  width: 1,
                                                ),
                                              ),
                                              child: Visibility(
                                                visible: _locale == 'en',
                                                child: Container(
                                                  width:
                                                      ScreenUtil().setWidth(7),
                                                  height:
                                                      ScreenUtil().setWidth(7),
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Color(0xFF07D80F),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                                width:
                                                    ScreenUtil().setWidth(5)),
                                            Text(
                                              'English',
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Pro Text',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: 15,
                                                fontWeight: FontWeight.w700,
                                                color: Color(0xFF021A89),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () =>
                                            setState(() => _locale = 'ar'),
                                        enableFeedback: false,
                                        child: Row(
                                          textDirection: TextDirection.rtl,
                                          children: <Widget>[
                                            Container(
                                              width: ScreenUtil().setWidth(12),
                                              height: ScreenUtil().setWidth(12),
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                border: Border.all(
                                                  color: Color(0xFF021A89),
                                                  width: 1,
                                                ),
                                              ),
                                              child: Visibility(
                                                visible: _locale == 'ar',
                                                child: Container(
                                                  width:
                                                      ScreenUtil().setWidth(7),
                                                  height:
                                                      ScreenUtil().setWidth(7),
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Color(0xFF07D80F),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                                width:
                                                    ScreenUtil().setWidth(5)),
                                            Text(
                                              'العربية',
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Pro Text',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: 15,
                                                fontWeight: FontWeight.w700,
                                                color: Color(0xFF021A89),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(80)),
                        InkWell(
                          onTap: _isLoading
                              ? null
                              : () {
                                  _save();
                                  // FocusScope.of(context)
                                  //     .requestFocus(FocusNode());
                                },
                          borderRadius: BorderRadius.circular(20),
                          child: Container(
                            width: ScreenUtil().setWidth(170.16),
                            height: ScreenUtil().setHeight(40.2),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              gradient: LinearGradient(
                                colors: <Color>[
                                  Color(0xff031C8D),
                                  Color(0xff0A2FAF)
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                              ),
                            ),
                            child: _isLoading
                                ? CupertinoActivityIndicator(
                                    animating: true,
                                  )
                                : Text(
                                    S.of(context).saveChanges,
                                    style: TextStyle(
                                      fontFamily: application.locale == 'ar'
                                          ? 'Cairo'
                                          : 'SF UI Display',
                                      fontFamilyFallback: <String>[
                                        'SF Pro Text'
                                      ],
                                      fontSize: ScreenUtil().setSp(11),
                                      fontWeight: FontWeight.w700,
                                      color: Color(0xfffcfcfc),
                                    ),
                                  ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(60.8)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: _isDialogVisible,
            child: Positioned.fill(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                child: Container(
                  color: Colors.grey.withOpacity(0.2),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _pickPhoto() async {
    final imageFile = await showCupertinoModalPopup<File>(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        title: Text(S.of(context).changeProfilePicture),
        actions: <Widget>[
          CupertinoActionSheetAction(
            onPressed: () async {
              final file =
                  await ImagePicker.pickImage(source: ImageSource.camera);
              Navigator.of(context).pop(file);
            },
            child: Text(S.of(context).takeNewPhoto),
          ),
          CupertinoActionSheetAction(
            onPressed: () async {
              final file =
                  await ImagePicker.pickImage(source: ImageSource.gallery);
              Navigator.of(context).pop(file);
            },
            child: Text(S.of(context).chooseExistingPhoto),
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(
            S.of(context).Cancel,
            style: TextStyle(
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
    );

    setState(() {
      _avatarFile = imageFile;
    });
  }

  bool _saveForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<void> _save() async {
    if (_saveForm()) {
      setState(() {
        _isLoading = true;
      });

      if (_avatarFile != null) {
        final extension =
            _avatarFile.path.substring(_avatarFile.path.lastIndexOf('.'));
        final storageRef = FirebaseStorage.instance
            .ref()
            .child('profile_pictures')
            .child("${_employee.employeeId}.$extension");

        final StorageUploadTask uploadTask = storageRef.putFile(
          _avatarFile,
          StorageMetadata(
            contentType: "image/$extension",
            customMetadata: <String, String>{
              'type': 'Profile Picture',
              'user_id': _employee.employeeId,
            },
          ),
        );

        final downloadUrl = await uploadTask.onComplete;
        final imageUrl = await downloadUrl.ref.getDownloadURL();
        _employee.setData(avatarUrl: imageUrl);
      }

      _employee.setData(
        firstName: _firstName,
        lastName: _lastName,
      );
      final application = Provider.of<ApplicationStore>(context);
      application.localeStream.add(_locale);

      await Firestore.instance
          .collection('users')
          .document(_employee.employeeId)
          .updateData({
        'first_name': _firstName,
        'last_name': _lastName,
        'phone_number': _phoneNumber,
        'avatar_url': _employee.avatarUrl,
        'locale': _locale,
      });

      setState(() {
        _isLoading = false;
      });

      showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(S.of(context).success),
          content: Text(S.of(context).changesSaved),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text(S.of(context).ok),
            ),
          ],
        ),
      );
    }
  }
}
