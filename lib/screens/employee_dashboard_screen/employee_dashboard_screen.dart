import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';
import 'package:weash_cars/generated/i18n.dart';
import 'package:weash_cars/models/payment_method.dart';
import 'package:weash_cars/screens/take_order_screen/take_order_screen.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/employee_store.dart';
import 'package:weash_cars/stores/orders_store.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';
import 'package:weash_cars/widgets/employee_drawer.dart';

class EmployeeDashboardScreen extends StatefulWidget {
  @override
  _EmployeeDashboardScreenState createState() =>
      _EmployeeDashboardScreenState();
}

class _EmployeeDashboardScreenState extends State<EmployeeDashboardScreen> {
  EmployeeStore _employee;
  OrdersStore _orders;

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  int _page = 0;
  bool _isDialogVisible = false;

  PageController _pageController = new PageController();

  @override
  void didChangeDependencies() {
    final applicationStore = Provider.of<ApplicationStore>(context);
    _employee = applicationStore.employeeStore;
    _orders = _employee.orders;

    if (!_orders.isListening) {
      _orders.subscribe(employeeId: _employee.employeeId);
    }

    _firebaseMessaging.getToken().then((String token) {
      if (_employee.fcmToken != token) {
        _employee.setData(fcmToken: token);
        Firestore.instance
            .collection('users')
            .document(_employee.employeeId)
            .updateData({
          'fcm_token': token,
        });
      }
    });

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _orders.unsubscribe();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final application = Provider.of<ApplicationStore>(context);

    ScreenUtil.instance =
        ScreenUtil(width: 375, height: 812, allowFontScaling: true)
          ..init(context);

    return Scaffold(
      drawer: Observer(
        builder: (BuildContext context) => EmployeeDrawer(
          onCloseDrawer: () {
            setState(() => _isDialogVisible = false);
          },
          avatarUrl: _employee.avatarUrl,
        ),
      ),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Image(
              image:
                  AssetImage('assets/images/employee-screens-background.png'),
              fit: BoxFit.cover,
              matchTextDirection: true,
            ),
          ),
          ListView(
            shrinkWrap: true,
            // crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Observer(
                builder: (BuildContext context) => CustomAppBar2(
                  onOpenDrawer: () {
                    setState(() {
                      _isDialogVisible = true;
                      Scaffold.of(context).openDrawer();
                    });
                  },
                  title: S.of(context).dashboard,
                  transparentBackground: true,
                  titleColor: Color(0xFFA0AEE6),
                  buttonColor: Color(0xFF0C2461),
                  nbNotifications: 0,
                  avatar: _employee.avatarUrl == null
                      ? null
                      : NetworkImage(_employee.avatarUrl),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(4.6)),
              Transform.translate(
                offset: Offset(
                    ScreenUtil()
                        .setWidth(application.locale == 'ar' ? -28 : 28),
                    0),
                child: Align(
                  alignment: application.locale == 'ar'
                      ? Alignment.topRight
                      : Alignment.topLeft,
                  child: Text(
                    S.of(context).orders,
                    style: TextStyle(
                      fontFamily: application.locale == 'ar'
                          ? 'Cairo'
                          : 'SF Pro Display',
                      fontFamilyFallback: <String>['SF Pro Text'],
                      fontSize: 21,
                      fontWeight: FontWeight.w900,
                      color: Color(0xFFA0AEE6),
                    ),
                  ),
                ),
              ),
              SizedBox(height: ScreenUtil().setHeight(17)),
              Observer(
                builder: (BuildContext context) {
                  final completedOrders = _orders.orders
                      .where((order) => (order['status'] as int) >= 20)
                      .toList();
                  return SizedBox(
                    width: ScreenUtil().setWidth(321),
                    height: ScreenUtil().setHeight(198),
                    child: ListView.separated(
                      itemCount:
                          completedOrders == null ? 0 : completedOrders.length,
                      separatorBuilder: (BuildContext context, int index) =>
                          SizedBox(height: ScreenUtil().setHeight(6)),
                      itemBuilder: (BuildContext context, int index) {
                        final order = completedOrders[index];
                        final orderLocality = order['locality'] ?? ' ';
                        final orderDate = order['date'] as DateTime;

                        final customer = order['customer'];
                        final customerAvatarUrl = customer['avatar_url'];
                        final customerFirstName = customer['first_name'];
                        final customerLastName = customer['last_name'];

                        return Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: ScreenUtil().setWidth(321),
                              // height: ScreenUtil().setHeight(62),
                              padding: EdgeInsets.only(
                                top: ScreenUtil().setHeight(12),
                                right: ScreenUtil().setWidth(
                                    application.locale == 'ar' ? 20.7 : 14),
                                bottom: ScreenUtil().setHeight(8),
                                left: ScreenUtil().setWidth(
                                    application.locale == 'ar' ? 14 : 20.7),
                              ),
                              decoration: BoxDecoration(
                                color: Color(0xFF011B7B),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: ScreenUtil().setWidth(41.04),
                                    height: ScreenUtil().setWidth(41.04),
                                    padding: EdgeInsets.symmetric(
                                      horizontal: ScreenUtil().setWidth(2),
                                      vertical: ScreenUtil().setWidth(2),
                                    ),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                    ),
                                    child: ClipPath(
                                      clipper: ShapeBorderClipper(
                                        shape: CircleBorder(),
                                      ),
                                      child: Image(
                                        image: customerAvatarUrl == null
                                            ? AssetImage(
                                                'assets/images/avatar-user.png')
                                            : NetworkImage(customerAvatarUrl),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: ScreenUtil().setWidth(4.2)),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Flexible(
                                        fit: FlexFit.loose,
                                        child: Text(
                                          "$customerFirstName $customerLastName",
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Text',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: 11,
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                          height: ScreenUtil().setHeight(1.1)),
                                      Flexible(
                                        fit: FlexFit.loose,
                                        child: Text(
                                          orderLocality,
                                          style: TextStyle(
                                            fontFamily: 'SF UI Display',
                                            fontSize: 11,
                                            fontWeight: FontWeight.w300,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Expanded(
                                    child: Align(
                                      alignment: application.locale == 'ar'
                                          ? Alignment.centerLeft
                                          : Alignment.centerRight,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          RichText(
                                            text: TextSpan(
                                              children: <TextSpan>[
                                                TextSpan(
                                                  text: intl.DateFormat('E')
                                                      .format(orderDate),
                                                  style: TextStyle(
                                                    fontFamily:
                                                        application.locale ==
                                                                'ar'
                                                            ? 'Cairo'
                                                            : 'SF Pro Text',
                                                    fontFamilyFallback: <
                                                        String>['SF Pro Text'],
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w600,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                                TextSpan(text: ' '),
                                                TextSpan(
                                                  text: intl.DateFormat('d')
                                                      .format(orderDate),
                                                  style: TextStyle(
                                                    fontFamily:
                                                        application.locale ==
                                                                'ar'
                                                            ? 'Cairo'
                                                            : 'SF Pro Text',
                                                    fontFamilyFallback: <
                                                        String>['SF Pro Text'],
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Text(
                                            intl.DateFormat('MMMM')
                                                .format(orderDate),
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontFamilyFallback: <String>[
                                                'SF Pro Text'
                                              ],
                                              fontSize: 12,
                                              fontWeight: FontWeight.w300,
                                              color: Colors.white,
                                            ),
                                          ),
                                          Text(
                                            intl.DateFormat('Hm')
                                                .format(orderDate),
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontFamilyFallback: <String>[
                                                'SF Pro Text'
                                              ],
                                              fontSize: 12,
                                              fontWeight: FontWeight.w700,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  );
                },
              ),
              SizedBox(height: ScreenUtil().setHeight(29)),
              Observer(
                builder: (BuildContext context) {
                  final newOrders = _orders.orders
                      .where((orders) => (orders['status'] as int) < 20)
                      .toList();
                  final order = newOrders == null || newOrders.length == 0
                      ? null
                      : newOrders[_page];
                  final orderId = order == null ? null : order['order_id'];
                  final orderDate =
                      order == null ? null : order['date'] as DateTime;
                  final orderNumber =
                      order == null ? 0 : order['order_number'] as int;
                  final orderNumberStr = List.generate(
                              6 - orderNumber.toString().length, (_) => '0')
                          .join() +
                      orderNumber.toString();

                  return Column(
                    children: <Widget>[
                      Container(
                        width: ScreenUtil().setWidth(343),
                        height: ScreenUtil().setHeight(388),
                        padding: EdgeInsets.only(
                          top: ScreenUtil().setHeight(29),
                          right: ScreenUtil().setWidth(21),
                          bottom: ScreenUtil().setHeight(22),
                          left: ScreenUtil().setWidth(12),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          gradient: LinearGradient(
                            colors: [Color(0xFFDCDDE1), Color(0xFFFFFFFF)],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  S.of(context).newOrders,
                                  style: TextStyle(
                                    fontFamily: application.locale == 'ar'
                                        ? 'Cairo'
                                        : 'SF Pro Display',
                                    fontFamilyFallback: <String>['SF Pro Text'],
                                    fontSize: 21,
                                    fontWeight: FontWeight.w900,
                                    color: Color(0xFFA0AEE6),
                                  ),
                                ),
                                order == null
                                    ? Container()
                                    : Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Text(
                                            'N$orderNumberStr',
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontFamilyFallback: <String>[
                                                'SF Pro Text'
                                              ],
                                              fontSize: 13,
                                              fontWeight: FontWeight.w800,
                                              color: Color(0xFFA0AEE6),
                                            ),
                                          ),
                                          Text(
                                            intl.DateFormat('d MMMM')
                                                    .format(orderDate) +
                                                ' at ' +
                                                intl.DateFormat('Hm')
                                                    .format(orderDate),
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Text',
                                              fontFamilyFallback: <String>[
                                                'SF Pro Text'
                                              ],
                                              fontSize: 11,
                                              fontWeight: FontWeight.w600,
                                              color: Color(0xFFA0AEE6),
                                            ),
                                          ),
                                        ],
                                      ),
                              ],
                            ),
                            SizedBox(height: ScreenUtil().setHeight(25)),
                            Expanded(
                              child: Container(
                                // height: ScreenUtil().setHeight(224),
                                margin: EdgeInsets.symmetric(
                                  horizontal: ScreenUtil().setWidth(27),
                                ),
                                child: PageView.builder(
                                  onPageChanged: (int page) =>
                                      setState(() => _page = page),
                                  itemCount:
                                      newOrders == null ? 0 : newOrders.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    final order = newOrders[index];
                                    final orderLocality =
                                        order['locality'] ?? ' ';
                                    final orderAddress = order['address'];
                                    final orderDate = order['date'] as DateTime;
                                    final orderPaymentMethod =
                                        order['payment_method'];
                                    final paymentMethods = {
                                      PaymentMethod.cash:
                                          "${S.of(context).cashOn} ${S.of(context).delivery}"
                                              .toUpperCase(),
                                      PaymentMethod.visa:
                                          S.of(context).online.toUpperCase(),
                                    };

                                    final customer = order['customer'];
                                    final customerAvatarUrl =
                                        customer['avatar_url'];
                                    final customerFirstName =
                                        customer['first_name'];
                                    final customerLastName =
                                        customer['last_name'];

                                    final package = order['package'];
                                    final packageTitle = package['title'];
                                    final packagePrice =
                                        package['price'] as double;
                                    final packageCurrency = package['currency'];
                                    final vehicleType = package['vehicle_type'];
                                    final vehicleTypeStrs = {
                                      'motorcycle': S.of(context).motorcycle,
                                      'sedan': S.of(context).sedan,
                                      'suv': S.of(context).suv,
                                      'boat': S.of(context).boat,
                                    };

                                    return ListView(
                                      shrinkWrap: true,
                                      padding: EdgeInsets.all(0),
                                      // mainAxisAlignment:
                                      //     MainAxisAlignment.start,
                                      // crossAxisAlignment:
                                      //     CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          S.of(context).client,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Display',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: 13,
                                            fontWeight: FontWeight.w900,
                                            color: Color(0xFFA0AEE6),
                                          ),
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(8)),
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              width:
                                                  ScreenUtil().setWidth(41.04),
                                              height:
                                                  ScreenUtil().setWidth(41.04),
                                              padding: EdgeInsets.symmetric(
                                                horizontal:
                                                    ScreenUtil().setWidth(2),
                                                vertical:
                                                    ScreenUtil().setWidth(2),
                                              ),
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.white,
                                              ),
                                              child: ClipPath(
                                                clipper: ShapeBorderClipper(
                                                  shape: CircleBorder(),
                                                ),
                                                child: Image(
                                                  image: customerAvatarUrl ==
                                                          null
                                                      ? AssetImage(
                                                          'assets/images/avatar-user.png')
                                                      : NetworkImage(
                                                          customerAvatarUrl),
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                                width: ScreenUtil()
                                                    .setWidth(14.1)),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  "$customerFirstName $customerLastName",
                                                  style: TextStyle(
                                                    fontFamily:
                                                        application.locale ==
                                                                'ar'
                                                            ? 'Cairo'
                                                            : 'SF Pro Text',
                                                    fontFamilyFallback: <
                                                        String>['SF Pro Text'],
                                                    fontSize: 11,
                                                    fontWeight: FontWeight.w700,
                                                    color: Color(0xFF0C2461),
                                                  ),
                                                ),
                                                SizedBox(
                                                    height: ScreenUtil()
                                                        .setHeight(1.1)),
                                                Text(
                                                  orderLocality,
                                                  style: TextStyle(
                                                    fontFamily: 'SF UI Display',
                                                    fontSize: 11,
                                                    fontWeight: FontWeight.w300,
                                                    color: Color(0xFF0C2461),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(20)),
                                        Text(
                                          S.of(context).summary,
                                          style: TextStyle(
                                            fontFamily:
                                                application.locale == 'ar'
                                                    ? 'Cairo'
                                                    : 'SF Pro Display',
                                            fontFamilyFallback: <String>[
                                              'SF Pro Text'
                                            ],
                                            fontSize: 13,
                                            fontWeight: FontWeight.w900,
                                            color: Color(0xFFA0AEE6),
                                          ),
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(10)),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              "${S.of(context).location} : ",
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: 14,
                                                fontWeight: FontWeight.w700,
                                                color: Color(0xFF021A89),
                                              ),
                                            ),
                                            Expanded(
                                              child: Text(
                                                orderAddress,
                                                style: TextStyle(
                                                  fontFamily:
                                                      'SF Compact Display',
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500,
                                                  color: Color(0xFF021A89),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(3)),
                                        Row(
                                          children: <Widget>[
                                            Text(
                                              S.of(context).vehicleType,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: 14,
                                                fontWeight: FontWeight.w700,
                                                color: Color(0xFF021A89),
                                              ),
                                            ),
                                            Text(
                                              vehicleTypeStrs[vehicleType],
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                                color: Color(0xFF021A89),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(3)),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              S.of(context).dateAndTime,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: 14,
                                                fontWeight: FontWeight.w700,
                                                color: Color(0xFF021A89),
                                              ),
                                            ),
                                            Expanded(
                                              child: Text(
                                                intl.DateFormat('EEEE d MMMM')
                                                        .format(orderDate) +
                                                    " ${S.of(context).at} " +
                                                    intl.DateFormat('Hm')
                                                        .format(orderDate),
                                                style: TextStyle(
                                                  fontFamily: application
                                                              .locale ==
                                                          'ar'
                                                      ? 'Cairo'
                                                      : 'SF Compact Display',
                                                  fontFamilyFallback: <String>[
                                                    'SF Pro Text'
                                                  ],
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500,
                                                  color: Color(0xFF021A89),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(3)),
                                        Row(
                                          children: <Widget>[
                                            Text(
                                              S.of(context).package,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: 14,
                                                fontWeight: FontWeight.w700,
                                                color: Color(0xFF021A89),
                                              ),
                                            ),
                                            Text(
                                              packageTitle,
                                              style: TextStyle(
                                                fontFamily:
                                                    application.locale == 'ar'
                                                        ? 'Cairo'
                                                        : 'SF Compact Display',
                                                fontFamilyFallback: <String>[
                                                  'SF Pro Text'
                                                ],
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500,
                                                color: Color(0xFF021A89),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                            height: ScreenUtil().setHeight(17)),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            RichText(
                                              text: TextSpan(
                                                children: <TextSpan>[
                                                  TextSpan(
                                                    text: paymentMethods[
                                                        orderPaymentMethod],
                                                    style: TextStyle(
                                                      fontFamily:
                                                          application.locale ==
                                                                  'ar'
                                                              ? 'Cairo'
                                                              : 'SF Pro Text',
                                                      fontFamilyFallback: <
                                                          String>[
                                                        'SF Pro Text'
                                                      ],
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w800,
                                                      color: Color(0xFFFABF3A),
                                                    ),
                                                  ),
                                                  TextSpan(
                                                    text: '    ',
                                                  ),
                                                  TextSpan(
                                                    text: packagePrice
                                                        .toInt()
                                                        .toString(),
                                                    style: TextStyle(
                                                      fontFamily:
                                                          application.locale ==
                                                                  'ar'
                                                              ? 'Cairo'
                                                              : 'SF Pro Text',
                                                      fontFamilyFallback: <
                                                          String>[
                                                        'SF Pro Text'
                                                      ],
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w800,
                                                      color: Color(0xFFFABF3A),
                                                    ),
                                                  ),
                                                  TextSpan(
                                                    text:
                                                        ".${packagePrice.toStringAsFixed(3).replaceFirst(RegExp(r'.*\.'), '')} $packageCurrency",
                                                    style: TextStyle(
                                                      fontFamily:
                                                          application.locale ==
                                                                  'ar'
                                                              ? 'Cairo'
                                                              : 'SF Pro Text',
                                                      fontFamilyFallback: <
                                                          String>[
                                                        'SF Pro Text'
                                                      ],
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w800,
                                                      color: Color(0xFFFABF3A),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ),
                            ),
                            SizedBox(height: ScreenUtil().setHeight(10)),
                            Visibility(
                              visible:
                                  newOrders != null && newOrders.length > 0,
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    InkWell(
                                      onTap: () => Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) => TakeOrderScreen(
                                            order: order,
                                          ),
                                        ),
                                      ),
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            S.of(context).takeThisOrder,
                                            style: TextStyle(
                                              fontFamily:
                                                  application.locale == 'ar'
                                                      ? 'Cairo'
                                                      : 'SF Pro Display',
                                              fontFamilyFallback: <String>[
                                                'SF Pro Text'
                                              ],
                                              fontSize: 12,
                                              fontWeight: FontWeight.w900,
                                              color: Color(0xFF0C2461),
                                            ),
                                          ),
                                          SizedBox(
                                              width:
                                                  ScreenUtil().setWidth(4.8)),
                                          Image(
                                            image: AssetImage(
                                                'assets/images/take-order-arrow-icon.png'),
                                            width: ScreenUtil().setWidth(17.39),
                                            height:
                                                ScreenUtil().setHeight(12.15),
                                            color: Color(0xFF0C2461),
                                            matchTextDirection: true,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(14)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: newOrders
                            .asMap()
                            .map((int index, Map<String, dynamic> order) =>
                                MapEntry(
                                    index,
                                    Row(
                                      children: <Widget>[
                                        CircleAvatar(
                                          radius: ScreenUtil().setWidth(
                                                  _page == index ? 11 : 9) /
                                              2,
                                          backgroundColor: _page == index
                                              ? Color(0xFF011F99)
                                              : Color(0xFF011F99)
                                                  .withOpacity(0.5),
                                        ),
                                        SizedBox(
                                            width: ScreenUtil().setHeight(
                                                index == newOrders.length - 1
                                                    ? 0
                                                    : 10)),
                                      ],
                                    )))
                            .values
                            .toList(),
                      ),
                    ],
                  );
                },
              ),
            ],
          ),
          Visibility(
            visible: _isDialogVisible,
            child: Positioned.fill(
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                child: Container(
                  color: Colors.grey.withOpacity(0.2),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
