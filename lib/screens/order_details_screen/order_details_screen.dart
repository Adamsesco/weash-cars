import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

import 'package:weash_cars/stores/application_store.dart';
import 'package:weash_cars/stores/company_store.dart';
import 'package:weash_cars/widgets/custom_app_bar.dart';

class OrderDetailsScreen extends StatefulWidget {
  final String orderId;

  OrderDetailsScreen({Key key, this.orderId}) : super(key: key);

  @override
  _OrderDetailsScreenState createState() => _OrderDetailsScreenState();
}

class _OrderDetailsScreenState extends State<OrderDetailsScreen> {
  Completer<GoogleMapController> _mapController = Completer();
  CompanyStore _company;
  Map<String, dynamic> _order;
  Map<String, dynamic> _customer;
  List<Map<String, dynamic>> _employees;
  Map<String, dynamic> _package;
  DateTime _orderDate;
  LatLng _orderLocation;
  Set<Marker> _markers = {};
  int _orderStatus;
  Color _orderStatusColor;
  bool _isAcceptLoading = false;
  bool _isRejectLoading = false;

  @override
  void didChangeDependencies() {
    final application = Provider.of<ApplicationStore>(context);
    _company = application.companyStore;
    _order = _company.orders.orders
        .where(
            (Map<String, dynamic> order) => order['order_id'] == widget.orderId)
        .toList()[0];
    _customer = _order['customer'];
    _package = _order['package'];
    _orderDate = _order['date'] as DateTime;
    final orderLocation = _order['location'] as GeoPoint;
    _orderLocation =
        new LatLng(orderLocation.latitude, orderLocation.longitude);
    _markers.add(Marker(
      markerId: MarkerId(_order['order_id']),
      position: _orderLocation,
      icon: BitmapDescriptor.defaultMarker,
    ));
    _orderStatus = _order['status'];
    _orderStatusColor = [
      Colors.grey,
      Colors.blue,
      Colors.blue,
      Colors.green,
      Colors.red,
    ][_orderStatus ~/ 10];

    _getEmployees(_company.companyId)
        .then((List<Map<String, dynamic>> employees) {
      setState(() {
        _employees = employees;
      });
    });

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        context,
        nbNotifications: 0,
        avatarUrl: _company.avatarUrl,
        popupMenuItems: <PopupMenuEntry<String>>[
          PopupMenuItem<String>(
            value: '/profile/company/edit',
            child: Text('Edit info'),
          ),
          PopupMenuItem<String>(
            value: '/packages/add',
            child: Text('Add package'),
          ),
          PopupMenuItem<String>(
            value: '/logout',
            child: Text('Logout'),
          ),
        ],
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: _buildHeader(),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6, vertical: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundImage: NetworkImage(
                          _order['customer']['avatar_url'] ??
                              'http://www.personalbrandingblog.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png',
                        ),
                      ),
                      const SizedBox(width: 14),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "${_customer['username']}",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 4),
                          Text(
                            "${_orderDate.day}/${_orderDate.month}/${_orderDate.year}",
                            style: TextStyle(
fontFamilyFallback: <String>[ 'SF Pro Text' ], fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  _orderStatus == 0
                      ? Container(
                          decoration: BoxDecoration(
                            color: Colors.blue,
                          ),
                          child: FlatButton(
                            onPressed: null,
                            child: Text(
                              'Send message',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        )
                      : Row(
                          children: <Widget>[
                            Text(
                              "${_package['price']} ${_package['currency']}",
                              style: TextStyle(
                                color: Colors.blue,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            const SizedBox(width: 14),
                            Container(
                              width: 10,
                              height: 10,
                              decoration: BoxDecoration(
                                color: _orderStatusColor,
                                shape: BoxShape.circle,
                              ),
                            ),
                          ],
                        ),
                ],
              ),
            ),
            Divider(),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6, vertical: 10),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        'Package : ',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(_package['title']),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 300,
              child: GoogleMap(
                onMapCreated: (GoogleMapController controller) {
                  _mapController.complete(controller);
                },
                mapType: MapType.normal,
                initialCameraPosition: CameraPosition(
                  target: _orderLocation,
                  zoom: 14.4746,
                ),
                markers: _markers,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<List<Map<String, dynamic>>> _getEmployees(String companyId) async {
    final employees = new List<Map<String, dynamic>>();

    final employeesDocuments = await Firestore.instance
        .collection('employees')
        .where('company_id', isEqualTo: companyId)
        .getDocuments();

    await Future.forEach(employeesDocuments.documents,
        (DocumentSnapshot employeeDocument) {
      final employee = <String, dynamic>{
        'employee_id': employeeDocument.documentID,
        'company_id': employeeDocument.data['company_id'],
        'email': employeeDocument.data['email'],
        'first_name': employeeDocument.data['first_name'],
        'last_name': employeeDocument.data['last_name'],
        'avatar_url': employeeDocument.data['avatar_url'],
      };

      employees.add(employee);
    });

    return employees;
  }

  Future<void> _rejectOrder() async {
    setState(() {
      _isRejectLoading = true;
    });

    Firestore.instance
        .collection('orders')
        .document(_order['order_id'])
        .updateData({
      'status': 4,
    });

    setState(() {
      _isRejectLoading = false;
    });

    Navigator.of(context).pop();
  }

  Future<void> _assignEmployee() async {
    // TODO: Don't show employees who are on mission on the list.
    final selectedEmployee = await showDialog<Map<String, dynamic>>(
      context: context,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: Text('Assign an employee to this order'),
          children: _employees
              .map((Map<String, dynamic> employee) => SimpleDialogOption(
                    onPressed: () => Navigator.pop(context, employee),
                    child: Row(
                      children: <Widget>[
                        CircleAvatar(
                          backgroundImage: NetworkImage(employee['avatar_url']),
                        ),
                        const SizedBox(width: 10),
                        Text(
                            "${employee['first_name']} ${employee['last_name']}"),
                      ],
                    ),
                  ))
              .toList(),
        );
      },
    );

    if (selectedEmployee != null) {
      setState(() {
        _isAcceptLoading = true;
      });

      await Firestore.instance
          .collection('orders')
          .document(_order['order_id'])
          .updateData({
        'status': 10,
        'assigned_employee_id': selectedEmployee['employee_id'],
      });

      setState(() {
        _isAcceptLoading = false;
      });

      Navigator.of(context).pop();
    }
  }

  Widget _buildHeader() {
    switch (_orderStatus) {
      case 0:
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              color: Colors.green,
              child: FlatButton(
                onPressed: _assignEmployee,
                child: _isAcceptLoading
                    ? CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                        strokeWidth: 2,
                      )
                    : Text(
                        'Assign',
                        style: TextStyle(color: Colors.white),
                      ),
              ),
            ),
            // const SizedBox(width: 20),
            // Container(
            //   color: Colors.red,
            //   child: FlatButton(
            //     onPressed: () => showDialog(
            //           context: context,
            //           builder: (BuildContext context) => AlertDialog(
            //                 title: Text('Reject order'),
            //                 content: Text(
            //                     'Are you sure that you want to reject this order ?'),
            //                 actions: <Widget>[
            //                   FlatButton(
            //                     child: Text('No'),
            //                     onPressed: () => Navigator.of(context).pop(),
            //                   ),
            //                   FlatButton(
            //                     child: Text('Yes'),
            //                     onPressed: () {
            //                       Navigator.of(context).pop();
            //                       _rejectOrder();
            //                     },
            //                   ),
            //                 ],
            //               ),
            //         ),
            //     child: _isRejectLoading
            //         ? CircularProgressIndicator(
            //             valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            //             strokeWidth: 2,
            //           )
            //         : Text(
            //             'Reject',
            //             style: TextStyle(color: Colors.white),
            //           ),
            //   ),
            // ),
          ],
        );

      case 10:
      case 11:
      case 12:
        // TODO: show when the employee started washing the vehicle (status 2)
        final employeeId = _order['assigned_employee_id'];
        final employeeFuture = Firestore.instance
            .collection('employees')
            .document(employeeId)
            .get();

        return Column(
          children: <Widget>[
            Text('Assigned to :'),
            const SizedBox(height: 10),
            FutureBuilder(
              future: employeeFuture,
              builder: (BuildContext context,
                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                  case ConnectionState.active:
                  case ConnectionState.waiting:
                    return CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                      strokeWidth: 2,
                    );
                  case ConnectionState.done:
                    if (snapshot.hasError) {
                      // TODO: handle error
                      return Container();
                    }

                    final employee = snapshot.data;
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundImage: NetworkImage(employee['avatar_url']),
                        ),
                        const SizedBox(width: 10),
                        Text(
                            "${employee['first_name']} ${employee['last_name']}"),
                      ],
                    );
                }
              },
            ),
          ],
        );

      case 20:
      case 21:
      case 30:
      default:
        return Container();
    }
  }
}
