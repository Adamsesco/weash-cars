import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:weash_cars/models/dist_duration.dart';
import 'package:weash_cars/models/vehicle_type.dart';
import 'package:google_maps_webservice/distance.dart' as distanceApi;

class CompaniesService {
  static final distanceApi.GoogleDistanceMatrix _distanceApi =
      new distanceApi.GoogleDistanceMatrix(
          apiKey: 'AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I');

  static Future<Map<String, dynamic>> getAd(GeoPoint location) async {
    const RADIUS = 100.0;

    final geo = Geoflutterfire();
    final center =
        geo.point(latitude: location.latitude, longitude: location.longitude);

    final adsCollectionReference = Firestore.instance
        .collection('adsv2')
        .where('is_valid', isEqualTo: true);
    final adsDocuments = await geo
        .collection(collectionRef: adsCollectionReference)
        .within(center: center, radius: RADIUS, field: 'company_location')
        ?.first;

    if (adsDocuments == null || adsDocuments.isEmpty) {
      return null;
    }

    final random = new Random();
    final randomIndex = random.nextInt(adsDocuments?.length ?? 0);
    final adDocument = adsDocuments[randomIndex];
    final ad = {
      'ad_id': adDocument.documentID,
      'banner_url': adDocument.data['banner_url'],
      'link': adDocument.data['link'],
    };

    final adRef =
        Firestore.instance.collection('adsv2').document(adDocument.documentID);
    await Firestore.instance.runTransaction((Transaction tx) async {
      final adSnapshot = await tx.get(adRef);
      if (adSnapshot.exists) {
        await tx.update(adRef, {'views': (adSnapshot.data['views'] ?? 0) + 1});
      }
    });

    return ad;
  }

  static Future<List<Map<String, dynamic>>> getCompanies(
      [GeoPoint location,
      double radius,
      VehicleType vehicleType,
      DateTime dateTime,
      bool isFlexibleTiming,
      TextDirection textDirection,
      bool sponsoredOnly = false]) async {
    final startTime = DateTime.now();

    List<Map<String, dynamic>> companies = [];
    final vehicleTypeStr =
        vehicleType.toString().replaceFirst(new RegExp(r'^.*\.'), '');

    final geo = Geoflutterfire();
    final center =
        geo.point(latitude: location.latitude, longitude: location.longitude);
    if (!sponsoredOnly) {
      final companiesCollectionReference = Firestore.instance
          .collection('users')
          .where('role', isEqualTo: 'company')
          .where('is_activated', isEqualTo: true)
          .where('vehicle_types', arrayContains: vehicleTypeStr);

      final companiesDocuments = await geo
          .collection(collectionRef: companiesCollectionReference)
          .within(center: center, radius: radius, field: 'location')
          .first;

      List<QuerySnapshot> availabilities = [];
      List<Map<String, DistDuration>> distances = [];
      if (companiesDocuments != null && companiesDocuments.isNotEmpty) {
        final List<Future<QuerySnapshot>> availabilityFutures = [];
        final List<Future<Map<String, DistDuration>>> distancesFutures = [];

        companiesDocuments.forEach((DocumentSnapshot companyDocument) {
          final companyId = companyDocument.documentID;
          final data = companyDocument.data;

          if (!(data['is_available'] as bool ?? true) ||
              data['min_prices'] == null) return;

          final daysAvailabilityRequest = Firestore.instance
              .collection('company_availability')
              .where('company_id', isEqualTo: companyId)
              .where('type', isEqualTo: 'day')
              .where('date',
                  isEqualTo:
                      DateTime(dateTime.year, dateTime.month, dateTime.day)
                          .toUtc())
              .where('is_day_off', isEqualTo: true)
              .getDocuments();
          availabilityFutures.add(daysAvailabilityRequest);

          final hoursAvailabilityRequest = Firestore.instance
              .collection('company_availability')
              .where('company_id', isEqualTo: companyId)
              .where('type', isEqualTo: 'hour')
              .where('date',
                  isEqualTo: DateTime(dateTime.year, dateTime.month,
                          dateTime.day, dateTime.hour)
                      .toUtc())
              .where('is_available', isEqualTo: false)
              .getDocuments();
          availabilityFutures.add(hoursAvailabilityRequest);

          final companyLocation =
              companyDocument.data['location']['geopoint'] as GeoPoint;
          final distance =
              _getDistance(companyId, companyLocation, location, textDirection);
          distancesFutures.add(distance);
        });

        availabilities = await Future.wait(availabilityFutures);
        distances = await Future.wait(distancesFutures);
      }

      final t1 = DateTime.now();
      await Future.forEach(companiesDocuments,
          (DocumentSnapshot companyDocument) async {
        bool isAvailable = companyDocument.data['is_available'] as bool ?? true;

        final schedule =
            (companyDocument.data['schedule'] as Map<dynamic, dynamic>)
                .map((day, daySchedule) {
          final dayNumber = int.tryParse(day as String);
          final timeFrom = daySchedule['from'] as int;
          final timeTo = daySchedule['to'] as int;
          final isDayOff = daySchedule['is_day_off'] as bool;

          return MapEntry(dayNumber, {
            'from': timeFrom,
            'to': timeTo,
            'is_day_off': isDayOff,
          });
        });

        List<Map<String, dynamic>> employeesAvailability;
        if (companyDocument.data['employees_availability'] != null) {
          final avs =
              (companyDocument.data['employees_availability'] as List<dynamic>)
                  .cast<Map<dynamic, dynamic>>()
                  .map((av) =>
                      av.map((key, value) => MapEntry(key.toString(), value)))
                  .toList();
          employeesAvailability = List<Map<String, dynamic>>.from(avs);
        } else {
          employeesAvailability = [];
        }
        final av = employeesAvailability.firstWhere((Map<String, dynamic> av) {
          final date = (av['date'] as Timestamp).toDate();
          return date == dateTime ||
              (dateTime.isAfter(date) &&
                  dateTime.isBefore(date.add(Duration(hours: 1))));
        }, orElse: () => null);
        if (av != null && (av['available_employees'] as int) == 0) {
          isAvailable = false;
        }

        if (isAvailable) {
          final isUnavilable = availabilities.any((snapshot) {
            if (snapshot.documents.isEmpty) return false;

            final first = snapshot.documents.first;
            final companyId = first.data['company_id'];

            return companyId == companyDocument.documentID;
          });
          print(isUnavilable);
          if (isUnavilable) {
            isAvailable = false;
          }
        }

        if (isAvailable) {
          bool isUnavailable = false;

          final day = dateTime.weekday - 1;
          final daySchedule = schedule[day];
          final hourFrom = (daySchedule['from'] as int) ~/ 100;
          final minuteFrom = (daySchedule['from'] as int) % 100;
          final hourTo = (daySchedule['to'] as int) ~/ 100;
          final minuteTo = (daySchedule['to'] as int) % 100;

          final dateTimeFrom = new DateTime(dateTime.year, dateTime.month,
              dateTime.day, hourFrom, minuteFrom);
          final dateTimeTo = new DateTime(
              dateTime.year, dateTime.month, dateTime.day, hourTo, minuteTo);

          if (!isFlexibleTiming) {
            isUnavailable = (daySchedule['is_day_off'] as bool) ||
                !(dateTime.isAfter(dateTimeFrom) &&
                    dateTime.isBefore(dateTimeTo));
          } else {
            isUnavailable = (daySchedule['is_day_off'] as bool) ||
                !(dateTime
                        .add(Duration(hours: 1, seconds: 1))
                        .isAfter(dateTimeFrom) &&
                    dateTime
                        .subtract(Duration(hours: 1, seconds: 1))
                        .isBefore(dateTimeTo));
          }

          isAvailable = !isUnavailable;
        }

        final distance = distances.firstWhere(
          (distance) => distance.containsKey(companyDocument.documentID),
          orElse: () =>
              {companyDocument.documentID: DistDuration(0, 0, textDirection)},
        );

        if (distance.values.first.distance > 30000) {
          return;
        }

        final company = <String, dynamic>{
          'company_id': companyDocument.documentID.toString(),
          'company_name': companyDocument.data['company_name'].toString(),
          'avatar_url': companyDocument.data['avatar_url'].toString(),
          'phone_number': companyDocument.data['phone_number'] as String,
          'location': companyDocument.data['location']['geopoint'] as GeoPoint,
          'locality': companyDocument.data['locality'],
          'distance': distance.values.first,
          'min_prices': companyDocument.data['min_prices'],
          'currency': companyDocument.data['currency'],
          'rating': companyDocument.data['rating'].toDouble() as double,
          'total_reviews': companyDocument.data['total_reviews'] as int,
          'schedule': schedule,
          'jobs_done': companyDocument.data['total_reviews'] as int ?? 0,
          'is_available': isAvailable &&
              (companyDocument.data['is_available'] as bool ?? true) &&
              companyDocument.data['min_prices'] != null,
        };

        companies.add(company);
      });
      final d1 = DateTime.now().difference(t1);
      print("d1: ${d1.inSeconds}");
    }

    final runningTime = DateTime.now().difference(startTime);
    print("Search running time: ${runningTime.inSeconds}");
    return companies;
  }

  static double _haversine(double lat1, double lon1, double lat2, double lon2) {
    final dLat = _toRadians(lat2 - lat1);
    final dLon = _toRadians(lon2 - lon1);
    lat1 = _toRadians(lat1);
    lat2 = _toRadians(lat2);
    final a = (pow(sin(dLat / 2), 2) +
        pow(sin(dLon / 2), 2) * cos(lat1) * cos(lat2)) as double;
    final c = 2 * asin(sqrt(a));
    return 6372800 * c;
  }

  static double _toRadians(double degree) {
    return degree * pi / 180;
  }

  static Future<Map<String, DistDuration>> _getDistance(String companyId,
      GeoPoint p1, GeoPoint p2, TextDirection textDirection) async {
    final distance =
        _haversine(p1.latitude, p1.longitude, p2.latitude, p2.longitude);
    final duration = distance.toInt() ~/ 10;
    final distDuration = DistDuration(distance, duration, textDirection);
    return {companyId: distDuration};

//    final origin = <distanceApi.Location>[
//      distanceApi.Location(p1.latitude, p1.longitude)
//    ];
//    final destionation = <distanceApi.Location>[
//      distanceApi.Location(p2.latitude, p2.longitude)
//    ];
//
//    final distanceResponse = await _distanceApi.distanceWithLocation(
//        origin, destionation,
//        unit: distanceApi.Unit.metric);
//    if (distanceResponse.isOkay &&
//        distanceResponse.results.length > 0 &&
//        distanceResponse.results[0].elements.length > 0 &&
//        distanceResponse.results[0].elements[0].distance != null &&
//        distanceResponse.results[0].elements[0].duration != null) {
//      final distance =
//          distanceResponse.results[0].elements[0].distance.value.toInt();
//      final duration =
//          distanceResponse.results[0].elements[0].duration.value.toInt();
//      final distDuration =
//          new DistDuration(distance.toDouble(), duration, textDirection);
//
//      return {companyId: distDuration};
//    }
//
//    return {companyId: DistDuration(0, 0, textDirection)};
  }
}
