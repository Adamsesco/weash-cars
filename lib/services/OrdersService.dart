import 'package:http/http.dart' as http;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:weash_cars/models/payment_method.dart';

class OrdersService {
  static Future<Map<String, dynamic>> getOrder(String orderId) async {
    final orderDocument =
        await Firestore.instance.collection('orders').document(orderId).get();
    if (orderDocument == null || !orderDocument.exists) {
      return null;
    }

    final order = await _getOrderData(orderDocument);
    return order;
  }

  static Future<List<Map<String, dynamic>>> getOrders(
      {String companyId, String customerId, String employeeId}) async {
    String idField;
    String id;
    Future<http.Response> declineDelayedOrdersFuture;

    if (companyId != null) {
      idField = 'company_id';
      id = companyId;

      declineDelayedOrdersFuture = http.get(
          "https://us-central1-weash-cars.cloudfunctions.net/declineDelayedOrders?companyId=$companyId");
    } else if (employeeId != null) {
      idField = 'assigned_employee_id';
      id = employeeId;
    } else if (customerId != null) {
      idField = 'customer_id';
      id = customerId;
    }

    final ordersDocuments = await Firestore.instance
        .collection('orders')
        .where(idField, isEqualTo: id)
        .getDocuments();

    if (ordersDocuments == null || ordersDocuments.documents.length == 0) {
      return [];
    }

    final ordersFutures = ordersDocuments.documents
        .map((orderDocument) => _getOrderData(orderDocument));

    final ordersTmp = await Future.wait(ordersFutures);
    final orders = ordersTmp.where((order) => order != null)?.toList();

    if (declineDelayedOrdersFuture != null) {
      await declineDelayedOrdersFuture;
    }

    return orders;
  }

  static Future<Map<String, dynamic>> _getOrderData(
      DocumentSnapshot orderDocument) async {
    List<Future<DocumentSnapshot>> documentsFutures = [null, null, null];
    List<DocumentSnapshot> documents;
    bool hasError = false;

    documentsFutures[0] = Firestore.instance
        .collection('users')
        .document(orderDocument.data['customer_id'])
        .get();

    documentsFutures[1] = Firestore.instance
        .collection('users')
        .document(orderDocument.data['company_id'])
        .get();

    documentsFutures[2] = Firestore.instance
        .collection('packages')
        .document(orderDocument.data['package_id'])
        .get();

    documents = await Future.wait(documentsFutures,
        eagerError: true, cleanUp: (d) => hasError = true);

    if (hasError) {
      return null;
    }

    final customerDocument = documents[0];
    if (customerDocument == null || customerDocument.data == null) return null;
    final customer = {
      'customer_id': orderDocument.data['customer_id'] as String,
      'first_name': customerDocument.data['first_name'] as String,
      'last_name': customerDocument.data['last_name'] as String,
      'phone_number': customerDocument.data['phone_number'] as String,
      'avatar_url': customerDocument.data['avatar_url'] as String,
    };

    final companyDocument = documents[1];
    if (companyDocument == null || companyDocument.data == null) return null;
    final company = {
      'company_id': companyDocument.data['company_id'],
      'company_name': companyDocument.data['company_name'],
      'avatar_url': companyDocument.data['avatar_url'],
      'phone_number': companyDocument.data['phone_number'] as String,
    };

    final packageDocument = documents[2];
    if (packageDocument == null || packageDocument.data == null) return null;
    final package = {
      'package_id': orderDocument.data['package_id'] as String,
      'company_id': packageDocument.data['company_id'] as String,
      'title': packageDocument.data['title'] as String,
      'description': packageDocument.data['description'] as String,
      'price': (packageDocument.data['price'] as num).toDouble(),
      'currency': packageDocument.data['currency'] as String,
      'vehicle_type': packageDocument.data['vehicle_type'],
    };

    final paymentMethod = orderDocument.data['payment_method'];
    final paymentMethods = {
      'visa': PaymentMethod.visa,
      'cash': PaymentMethod.cash,
    };

    final order = {
      'order_id': orderDocument.documentID,
      'order_number': orderDocument.data['order_number'] as int,
      'date_created': orderDocument.data['date_created'] as Timestamp,
      'customer': customer,
      'company': company,
      'package': package,
      'date': (orderDocument.data['date'] as Timestamp)?.toDate(),
      'address': orderDocument.data['address'] == null
          ? null
          : orderDocument.data['address'].toString(),
      'location': orderDocument.data['location'] as GeoPoint,
      'locality': orderDocument.data['locality'] == null
          ? null
          : orderDocument.data['locality'].toString(),
      'status': orderDocument.data['status'] as int,
      'assigned_employee_id':
          orderDocument.data['assigned_employee_id'].toString(),
      'payment_method': paymentMethods[paymentMethod],
      'pictures_urls': orderDocument.data['pictures_urls'],
      'rating': orderDocument.data['rating'],
      'review': orderDocument.data['review'],
    };

    return order;
  }
}
