import 'package:cloud_firestore/cloud_firestore.dart';

class AreasService {
  static Future<List<Map<String, String>>> getAreas(String country) async {
    final areasDocuments = await Firestore.instance
        .collection('areas')
        .where('country', isEqualTo: country)
        .getDocuments();
    final areas = areasDocuments.documents.map((DocumentSnapshot document) {
      final area = <String, String>{
        'area_id': document.documentID,
        'name': document.data['name'],
        'country': document.data['country'],
      };

      return area;
    }).toList();

    return areas;
  }
}
