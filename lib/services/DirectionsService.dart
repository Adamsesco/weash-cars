import 'package:google_maps_webservice/directions.dart';

class DirectionsService {
  static GoogleMapsDirections directionsApi;

  static void init() {
    directionsApi =
        GoogleMapsDirections(apiKey: 'AIzaSyBIuIetbCMpVGJccAepzh-J4Fa6tNYKa5I');
  }

  static void dispose() {
    if (directionsApi != null) {
      directionsApi.dispose();
    }
  }

  static Future<DirectionsResponse> getDirections(
      Location origin, Location destination) async {
    final directions =
        directionsApi.directionsWithLocation(origin, destination);
    return directions;
  }
}
