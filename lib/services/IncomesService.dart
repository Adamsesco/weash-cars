import 'dart:collection';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:quiver/time.dart';

class IncomesService {
  Map<String, Income> _incomes;

  IncomesService() {
    _incomes = <String, Income>{
      'daily': new Income(IncomeType.daily),
      'weekly': new Income(IncomeType.weekly),
      'monthly': new Income(IncomeType.monthly),
      'yearly': new Income(IncomeType.yearly),
    };
  }

  Map<String, Income> get incomes => _incomes;

  Future<Map<String, Income>> getIncomes(
      String companyId, int year, int month) async {
    final dailyIncomesDocuments = await Firestore.instance
        .collection('incomes')
        .where('company_id', isEqualTo: companyId)
        .where('type', isEqualTo: 'daily')
        .where('year', isEqualTo: year)
        .where('month', isEqualTo: month)
        .getDocuments();

    final monthlyIncomesDocuments = await Firestore.instance
        .collection('incomes')
        .where('company_id', isEqualTo: companyId)
        .where('type', isEqualTo: 'monthly')
        .where('year', isEqualTo: year)
        .getDocuments();

    final yearlyIncomesDocuments = await Firestore.instance
        .collection('incomes')
        .where('company_id', isEqualTo: companyId)
        .where('type', isEqualTo: 'yearly')
        .getDocuments();

    if (dailyIncomesDocuments != null &&
        dailyIncomesDocuments.documents.length > 0) {
      final incomes = Map<int, double>.from(dailyIncomesDocuments
          .documents[0].data['incomes']
          .map((key, value) => MapEntry<int, double>(
              int.tryParse(key.toString()), (value as num).toDouble())));
      _incomes['daily'] = new Income(IncomeType.daily)..values = incomes;
    } else {
      _incomes['daily'] = new Income(IncomeType.daily);
    }

    if (monthlyIncomesDocuments != null &&
        monthlyIncomesDocuments.documents.length > 0) {
      final incomes = Map<int, double>.from(monthlyIncomesDocuments
          .documents[0].data['incomes']
          .map((key, value) => MapEntry<int, double>(
              int.tryParse(key.toString()), (value as num).toDouble())));
      _incomes['monthly'] = new Income(IncomeType.monthly)..values = incomes;
    } else {
      _incomes['monthly'] = new Income(IncomeType.monthly);
    }

    if (yearlyIncomesDocuments != null &&
        yearlyIncomesDocuments.documents.length > 0) {
      final incomes = Map<int, double>.from(yearlyIncomesDocuments
          .documents[0].data['incomes']
          .map((key, value) => MapEntry<int, double>(
              int.tryParse(key.toString()), (value as num).toDouble())));
      _incomes['yearly'] = new Income(IncomeType.yearly)..values = incomes;
    } else {
      _incomes['yearly'] = new Income(IncomeType.yearly);
    }

    return _incomes;
  }
}

class Income {
  IncomeType _type;
  Map<int, double> _values;
  final _random = new Random();

  Income(IncomeType type) {
    _type = type;
    _initValues();
  }

  Map<int, double> get values => _values;

  set values(Map<int, double> vs) {
    if (vs == null) {
      _initValues();
    } else {
      _values = vs;

      final now = DateTime.now();
      final month = now.month;
      final year = now.year;
      final daysCounts = [
        31,
        isLeapYear(year) ? 29 : 28,
        31,
        30,
        31,
        30,
        31,
        31,
        30,
        31,
        30,
        31,
      ];

      switch (_type) {
        case IncomeType.daily:
          final daysCount = daysCounts[month - 1];
          if (_values.keys.length < daysCount) {
            for (var i = 1; i <= daysCount; i++) {
              if (!_values.containsKey(i)) {
                _values[i] = 0.0;
              }
            }
          }
          break;
        case IncomeType.monthly:
          if (_values.keys.length < 12) {
            for (var i = 1; i <= 12; i++) {
              if (!_values.containsKey(i)) {
                _values[i] = 0.0;
              }
            }
          }
          break;
        case IncomeType.yearly:
          if (_values.keys.length == 0) {
            _values[year] = 0.0;
          }
          break;
        default:
      }

      final sorted =
          SplayTreeMap<int, double>.from(_values, (a, b) => a.compareTo(b));
      _values = sorted;
    }
  }

  void _initValues() {
    switch (_type) {
      case IncomeType.daily:
        final now = DateTime.now();
        final month = now.month;
        final year = now.year;
        final daysCount = [
          31,
          isLeapYear(year) ? 29 : 28,
          31,
          30,
          31,
          30,
          31,
          31,
          30,
          31,
          30,
          31,
        ];
        _values = List.generate(daysCount[month - 1], (int index) => index)
            .asMap()
            .map((int key, int value) => MapEntry(key + 1, 0));
        break;

      case IncomeType.weekly:
        _values = List.generate(4, (int index) => index)
            .asMap()
            .map((int key, int value) => MapEntry(key, 0));
        break;

      case IncomeType.monthly:
        _values = List.generate(12, (int index) => index)
            .asMap()
            .map((int key, int value) => MapEntry(key + 1, 0));
        break;

      case IncomeType.yearly:
        final year = DateTime.now().year;
        _values = {year: 0};
        break;
    }
  }
}

enum IncomeType {
  daily,
  weekly,
  monthly,
  yearly,
}
