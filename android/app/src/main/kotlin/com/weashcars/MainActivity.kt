package com.weashcars

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import android.os.Bundle
import android.util.Log

import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity : FlutterActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val id = "washy_channel_id"
            val name = "WashyNotificationChannel"
            val description = "Washy Notification Channel"
            val importance = NotificationManager.IMPORTANCE_DEFAULT

            val mChannel = NotificationChannel(id, name, importance)
            mChannel.description = description

            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)

            // result.success("Notification channel $name created")
        } else {
            // No-op
            // result.success("Android version is less than Oreo")
        }

        Log.d("TAG", "Hello from Kotlin!")
        MethodChannel(flutterView, "notifications").setMethodCallHandler { call, result ->
            Log.d("TAG", "Call to method channel")
            if (call.method == "registerChannel") {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val id = call.argument<String>("id")
                    val name = call.argument<String>("name")
                    val description = call.argument<String>("description")
                    val importance = NotificationManager.IMPORTANCE_DEFAULT

                    val mChannel = NotificationChannel(id, name, importance)
                    mChannel.description = description

                    val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
                    notificationManager.createNotificationChannel(mChannel)

                    result.success("Notification channel $name created")
                } else {
                    // No-op
                    result.success("Android version is less than Oreo")
                }
            }
        }
    }
}
